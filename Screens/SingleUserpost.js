import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  ActivityIndicator,
  FlatList,
  Pressable,
  TextInput,
  Button,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  ImageBackground,
} from 'react-native';
import {Dimensions} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import RBSheet from 'react-native-raw-bottom-sheet';
import Toast from 'react-native-simple-toast';
import {RFValue} from 'react-native-responsive-fontsize';
import {Icon, Header} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';
import Share from 'react-native-share';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';

const styles = require('../Components/Style');

const win = Dimensions.get('window');

const options = {
  title: 'Pick an Image',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class SingleUserpost extends Component {
  constructor(props) {
    super(props);
    console.warn(this.props.route.params.feed);
    this.state = {
      name: this.props.route.params.name,
      image: this.props.route.params.pic,
      data: [],
      object: {},
      save: {},
      like: {},
      follow: {},
      isLoading: true,
      id: this.props.route.params.id,
      post: '',

    };
  }

  componentDidMount =()=>{
   this.fetch_feeds();
  }

  fetch_feeds=()=>{
     fetch(global.api + 'get-single-user-profile', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({   
        user_id:this.state.id
      })
    })
      .then(response => response.json())
      .then(json => {
        console.warn(json);
        if (json.status) {
          this.setState({name: json.data[0].f_name});
          this.setState({image: json.data[0].profile_pic});
          global.pic = this.state.image;
          json.data.map(value => {
            this.setState({
              data: value.feed,
            });
            {value.feed.map(item=>{
              const like = this.state.like;
            const object=this.state.object;
            const save = this.state.save;
            console.warn("feed like",item)
            console.warn(item.feed_like.length)
          // const follow = this.state.like;
          like[item.id] = item.feed_like.length;
          if (item.feed_like.length== 0) {
            object[item.id] = false;
          } else {
            object[item.id] = true;
          }
          if (item.feed_save == null) {
            save[item.id] = false;
          } else {
            save[item.id] = true;
          }

          this.setState({like})
          this.setState({object})
          this.setState({save})
            
          });
        }
            })}
            
        return json;
      })
      .catch(error => console.error(error))
      .finally(() => {
        this.setState({isLoading: false});
      });
  }

  //function for share
  myShare = async (title, content, url) => {
    const shareOptions = {
      title: title,
      message: content,
      url: url,
    }
    try {
      const ShareResponse = await Share.open(shareOptions);

    } catch (error) {
      console.log("Error=>", error)
    }
  }
 

  // feed like
  like = id => {
    const object = this.state.object;
    const like = this.state.like;
    if (this.state.object[id] == true) {
      object[id] = false;
      like[id] = like[id] - 1;
      var type = 'no';
    } else {
      object[id] = true;
      like[id] = like[id] + 1;
      var type = 'yes';
    }
    this.setState({like});
    this.setState({object});

    fetch(global.api + 'feed-like', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        feed_id: id,
        type: type,
      }),
    })
      .then(response => response.json())
      .then(json => {
        if (!json.status) {
          Toast.show("Something went wrong.");
        }
        else{
          // Toast.show(json.msg)
        }
      });
  };

  // feed save
  bookmark = id => {
    const save = this.state.save;
    if (this.state.save[id] == true) {
      save[id] = false;
      var type = 'unsave';
    } else {
      save[id] = true;
      var type = 'save';
    }
    this.setState({save});

    fetch(global.api + 'user-feed-save', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        feed_id: id,
        type: type,
      }),
    })
      .then(response => response.json())
      .then(json => {
        // console.log(json)

        if (!json.status) {
          Toast.show(json.msg);
        }
      });
  };

  // report feed
  report = () => {
    this.RBSheet.close();
    fetch(global.api + 'feed-report-user', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        feed_id: this.state.id,
        report: 'Report this post',
      }),
    })
      .then(response => response.json())
      .then(json => {
        console.warn(json);
        if (!json.status) {
          Toast.show(json.msg);
        } else {
          Toast.show(json.msg);
          this.fetch_feeds();
        }
      });
  };


  // Bottom sheet
  sheet(id, feed_index) {
    this.setState({id: id, feed_index: feed_index});
    this.RBSheet.open();
  }

  productCard = ({item}) => (
    <View style={style.card}>
      {/* Card Header */}
      <View style={style.cardHeader}>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('UserProfile')}>
          <View style={{flexDirection: 'row'}}>
            {/* logo */}
            <Image
              source={{uri: global.image_url + this.state.image}}
              style={style.profileImage}
            />
            {/* name and time */}
            <View style={{flexDirection: 'column', paddingLeft: 10}}>
              <Text style={style.name}>{this.state.name}</Text>
              <Text style={style.postTime}>
                {moment
                  .utc(item.created_at)
                  .local()
                  .startOf('seconds')
                  .fromNow()}
              </Text>
            </View>
          </View>
        </TouchableOpacity>

        {/* # dots icon for bottm sheet */}
        <View style={{flexDirection: 'row'}}>
          <Text
            style={{alignContent: 'flex-end', top: 5, left: 5}}
            onPress={() => this.sheet(item.id)}>
            <Icon
              name="ellipsis-vertical"
              type="ionicon"
              color="#7c7d7e"
              size={25}
            />
          </Text>
        </View>
      </View>

      {/* Bottom Sheet for Post options */}

      <RBSheet
        ref={ref => {
          this.RBSheet = ref;
        }}
        // animationType="fade"
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={160}
        customStyles={{
          container: {
            borderTopRightRadius: 20,
            borderTopLeftRadius: 20,
          },
          draggableIcon: {
            backgroundColor: '',
          },
        }}>
        {/* bottom sheet elements */}
        <View>
          {/* new container search view */}
          <View>
            {/* to share */}
            <View style={{flexDirection: 'row', padding: 10}}>
              <TouchableOpacity
                style={{flexDirection: 'row'}}
                onPress={()=>this.myShare(item.feed_description,'Checkout Our Latest Update',global.shareLink+'/feedView/'+item.id)}
              >
                <View
                  style={{
                    backgroundColor: '#f5f5f5',
                    height: 40,
                    width: 40,
                    justifyContent: 'center',
                    borderRadius: 50,
                  }}>
                  <Icon type="ionicon" name="share-social" />
                </View>
                <Text
                  style={[styles.h4, {alignSelf: 'center', marginLeft: 20}]}>
                  Share
                </Text>
              </TouchableOpacity>
            </View>

            {/* to report */}
            <View style={{flexDirection: 'row', padding: 10}}>
              <TouchableOpacity
                style={{flexDirection: 'row'}}
                onPress={() => this.report()}>
                <View
                  style={{
                    backgroundColor: '#f5f5f5',
                    height: 40,
                    width: 40,
                    justifyContent: 'center',
                    borderRadius: 50,
                  }}>
                  <Icon type="ionicon" name="trash-bin" />
                </View>
                <Text
                  style={[styles.h4, {alignSelf: 'center', marginLeft: 20}]}>
                  Report
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </RBSheet>

      {/* Image */}
      <View>
        <ImageBackground
          source={{
            uri:
              'https://demo.webixun.com/KamalJwellersApi/public/' +
              item.content_src,
          }}
          style={style.postImage}
          PlaceholderContent={
            <ActivityIndicator size="small" color="#0000ff" />
          }>
         
        </ImageBackground>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={{margin: 3, marginLeft: 10}}>
              <Icon
                name={this.state.object[item.id] ? 'thumb-up' : 'thumb-up'}
                color={this.state.object[item.id] ? '#BC3B3B' : '#7c7d7e'}
                onPress={() => this.like(item.id)}
                size={25}
              />
            </Text>

            <Text style={{margin: 5}}>
            <Icon
                type="ionicon"
                onPress={() =>
                  this.props.navigation.navigate('Comments', {
                    description: item.feed_description,
                    name: this.state.name,
                    time: item.created_at,
                    id: item.id,
                    pic: this.state.image,
                  })
                }
                name="chatbubble-outline"
                color="#222222"
                size={22}
              />
            </Text>

            <Text style={{margin: 5}}>
              <Icon
                type="ionicon"
                name="share-social-outline"
                color="#222222"
                size={22}
                onPress={()=>this.myShare(item.feed_description,'Checkout Our Latest Update',global.shareLink+'/feedView/'+item.id)}
              />
            </Text>
          </View>

          <View>
            <Text style={{margin: 5, justifyContent: 'flex-end'}}>
              <Icon
                type="ionicon"
                name={
                  this.state.save[item.id] ? 'bookmark' : 'bookmark-outline'
                }
                color={this.state.save[item.id] ? '#BC3B3B' : '#222222'}
                onPress={() => this.bookmark(item.id)}
                size={22}
              />
            </Text>
          </View>
        </View>

        {/* Like counts */}
        
        <View>
          {this.state.like[item.id] > 0 ? (
            <>
              <Text style={{margin: 1, marginLeft: 13}}>
                {this.state.like[item.id]} likes
              </Text>
            </>
          ) : (
            <></>
          )}
        </View>
        <Text
          style={{
            color: '#222222',
            padding: 10,marginTop:-8,
            fontFamily: 'Raleway-Regular',
          }}>
          {item.feed_description}
        </Text>
      </View>

      {/* <View>
                <Icon type="ionicon" name="heart" />
              </View> */}
    </View>
  );
  renderLeftComponent() {
    return (
      <View style={{flexDirection: 'row', width: win.width / 2}}>
        <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
        </View>
        <Text style={[styles.headerHeadingText]}>Posts</Text>
      </View>
    );
  }

  renderRightComponent() {
    return (
      <View>
        <Icon
          name="bookmark-outline"
          size={22}
          color="#222222"
          type="ionicon"
          onPress={() => this.props.navigation.navigate('SavedPost')}
          // style={{alignSelf:"center"}}
        />
      </View>
    );
  }
  render() {
    return (
      <View style={styles.container}>
        {/* View for header component */}
        <View>
          <Header
            // containerStyle={{height: 82}}
            statusBarProps={{barStyle: 'light-content'}}
            leftComponent={this.renderLeftComponent()}
            rightComponent={this.renderRightComponent()}
            ViewComponent={LinearGradient} // Don't forget this!
            linearGradientProps={{
              colors: ['white', 'white'],
              start: {x: 0, y: 0.5},
              end: {x: 1, y: 0.5},
            }}
          />
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View>
          {this.state.isLoading ? (
          <View style={{marginTop: 200}}>
            <ActivityIndicator size="large" color="#bc3b3b" />
          </View>
        ) : (
          <FlatList
            navigation={this.props.navigation}
            showsVerticalScrollIndicator={false}
            data={this.state.data}
            renderItem={this.productCard}
            keyExtractor={item => item.id}
          />
        )}
            
          </View>
        </ScrollView>

        {/* <Add navigation={this.props.navigation} /> */}
      </View>
    );
  }
}

export default SingleUserpost;


const style = StyleSheet.create({
  card: {
    backgroundColor: '#fff',
    marginBottom: 3,
    borderRadius: 20,
  },
  cardHeader: {
    flexDirection: 'row',
    padding: 5,
    justifyContent: 'space-between',
    // backgroundColor:"red"
  },
  profileImage: {
    height: 45,
    width: 45,
    borderRadius: 30,
  },
  name: {
    // fontFamily:"Raleway-SemiBold",
    fontFamily: 'Raleway-Medium',
    fontSize: RFValue(12, 580),
    color: '#bc3b3b',
  },
  postTime: {
    fontFamily: 'Roboto-Regular',
    // top:-2,
    // fontFamily: "DancingScript-Bold",
    color: 'grey',
    fontSize: RFValue(10, 580),
  },
  postImage: {
    width: Dimensions.get('window').width,
    height: 310,
    // height:"100%"
  },
  followButton: {
    // borderRadius:25,
    // borderColor:"#000",
    // borderWidth:1,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    // padding:5,
    height: 28,
    width: 90,
    top: 7,
    left: 75,
  },
  followButtonText: {
    // fontFamily:"Roboto-SemiBold",
    fontFamily: 'Raleway-Regular',
    fontSize: RFValue(11, 580),
    // alignSelf:"center",
    textAlign: 'center',
    top: -3,
    left: 3,
    color: '#bc3b3b',
  },

  unFollowButtonText: {
    color: '#222222',
    // fontFamily:"Roboto-SemiBold",
    fontFamily: 'Raleway-Regular',
    fontSize: 11,
    // alignSelf:"center",
    textAlign: 'center',
    top: 0,
    // left:3
  },
  shop: {
    backgroundColor: '#bc3b3b',
    padding: 9,
    paddingTop: 6,
    paddingVertical: 5,
    position: 'absolute',
    bottom: 15,
    left: 25,
    borderRadius: 25,
    // borderWidth:0.5,
    // borderColor:"#d3d3d3",
    shadowColor: '#fff',
    shadowOffset: {width: 50, height: 50},
    shadowOpacity: 2,
    shadowRadius: 2,
    elevation: 10,
  },
  add: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 50,
    position: 'absolute',
    bottom: 10,
    right: 10,
    height: 50,
    backgroundColor: '#bc3b3b',
    borderRadius: 100,
  },
});
