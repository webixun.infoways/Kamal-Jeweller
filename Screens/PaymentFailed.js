import React, { Component } from 'react';
;
import {
    View, Text,StyleSheet,
    Image,TouchableOpacity,
} from 'react-native';
const styles = require("../Components/Style")

class PaymentFailed extends Component{
    constructor(props){
        super(props);
    }
    render(){
        return(
            <View style={style.container}>
            
            <Text style={[style.h2,{color:"#bc3b3b",alignSelf:"center",marginTop:150}]}>Transaction Failed</Text>
            
            
            <View style={style.circle}>
            <Image source={require('../image/paymentfail.png')} style={style.logo}></Image>
            </View>  
            
            
            <TouchableOpacity onPress={() => this.props.navigation.navigate("MyCart")}>
                                        <View style={[styles.buttonStyles, { flexDirection: "row", top: 50, bottom: 10 }]}>
                                            
                                            <Text style={styles.buttonText}> Back to Orders</Text>
                                        </View>
                                    </TouchableOpacity>
            {/* <TouchableOpacity  
            onPress={()=>this.props.navigation.navigate("Cart")}
            style={style.buttonStyles}>
            <LinearGradient 
                colors={['#326bf3', '#0b2654']}
                style={style.signIn}>

                <Text style={[style.textSignIn, {color:'#fff'}]}>
                Back To Orders</Text>
            
            </LinearGradient>
            </TouchableOpacity> */}

        </View>
        )
    }
}

export default PaymentFailed;

// Internal Styling
const style = StyleSheet.create({
    container:{
        backgroundColor:"#fff",
        flex:1
    },
    circle:{
        alignSelf:"center",
        marginTop:30,
        // width:200,
        // height:200,
        borderRadius:200,
        elevation:5,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 0
        }
    },
    
    logo:{

        alignSelf:"center",
        width:180,
        height:180,
        borderRadius:100,
        // marginTop:30,
      
    },
    buttonGreen1: {
        minWidth: '80%',
        marginBottom: 5,
        backgroundColor: "#60aa8d",
        borderRadius: 5,
        height: 50,
        marginTop:50,
        justifyContent: "center",
        alignSelf: "center",
        
    },
    h2:{

        fontSize:24,
    }
})