import React, { Component } from 'react';
import {
    Text,View,ScrollView,
    StyleSheet,Pressable,
    TouchableOpacity,Dimensions
} from 'react-native';
import {Icon,Header} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

//Global Style Import
const styles = require('../Components/Style.js');
const win = Dimensions.get('window');

class Report extends Component{
    constructor(props){
        super(props);
    }

//for header left component
renderLeftComponent(){
    return(
        <View style={{width:win.width/2,flexDirection:"row"}}>
            <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
            </View>
            <Text style={[styles.headerHeadingText,
                {alignSelf:"center",}]}>Report</Text>
        </View>
    )
}



    render(){
        return(
            <View style={styles.container}>

                {/* header */}
                <Header 
                containerStyle={{height:82}}
                statusBarProps={{ barStyle: 'light-content' }}
                leftComponent={this.renderLeftComponent()}
                ViewComponent={LinearGradient} // Don't forget this!
                linearGradientProps={{
                colors: ['white', 'white'],
                start: { x: 0, y: 0.5 },
                end: { x: 1, y: 0.5 },
                }}
                />

                <ScrollView>
                {/* question View */}
                <View>
                    
                    <Text style={[style.texxt,{fontFamily:"Roboto-Bold",fontSize:RFValue(13, 580)}]}>
                        Why are you reporting this post?
                    </Text>

                    <Text style={{fontSize:RFValue(9, 580),fontFamily:"Roboto-Regular",padding:10}}>
                        Your report is anonymous, expect if you're reporting an intellectual 
                        property infringement. If someone is in immediate danger, call the
                        local emergency services - don't wait.
                    </Text>
                </View>

                {/* conditions View */}
                <View>
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("ReportSuccessful")}>
                        <View style={style.questView} >
                            <Text style={style.texxt}>It's Spam</Text>
                            <Icon type="ionicon" name="chevron-forward-outline" />
                        </View>
                    </TouchableOpacity >
                        
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("ReportSuccessful")}>    
                        <View style={style.questView}>
                            <Text style={style.texxt}>Nudity or sexual activity</Text>
                            <Icon type="ionicon" name="chevron-forward-outline" />
                        </View>
                    </TouchableOpacity>
                    
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("ReportSuccessful")}>
                        <View style={style.questView}>
                            <Text style={style.texxt}>Hate speech or symbols</Text>
                            <Icon type="ionicon" name="chevron-forward-outline" />
                        </View>
                    </TouchableOpacity>
                   
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("ReportSuccessful")}>
                        <View style={style.questView}>
                            <Text style={style.texxt}>I just don't like it</Text>
                            <Icon type="ionicon" name="chevron-forward-outline" />
                        </View>
                    </TouchableOpacity>
                    
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("ReportSuccessful")}>
                        <View style={style.questView}>
                            <Text style={style.texxt}>False Information</Text>
                            <Icon type="ionicon" name="chevron-forward-outline" />
                        </View>
                    </TouchableOpacity>
                    
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("ReportSuccessful")}>
                        <View style={style.questView}>
                            <Text style={style.texxt}>Bullying or harassment</Text>
                            <Icon type="ionicon" name="chevron-forward-outline" />
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("ReportSuccessful")}>
                        <View style={style.questView}>
                            <Text style={style.texxt}>Scam or fraud</Text>
                            <Icon type="ionicon" name="chevron-forward-outline" />
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("ReportSuccessful")}>
                        <View style={style.questView}>
                            <Text style={style.texxt}>Violence or dangerous organisations</Text>
                            <Icon type="ionicon" name="chevron-forward-outline" />
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("ReportSuccessful")}>
                        <View style={style.questView}>
                            <Text style={style.texxt}>Intellectual property violation</Text>
                            <Icon type="ionicon" name="chevron-forward-outline" />
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("ReportSuccessful")}>
                        <View style={style.questView}>
                            <Text style={style.texxt}>Sale of illegal and regulated goods</Text>
                            <Icon type="ionicon" name="chevron-forward-outline" />
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("ReportSuccessful")}>
                        <View style={style.questView}>
                            <Text style={style.texxt}>Suicide or self injury</Text>
                            <Icon type="ionicon" name="chevron-forward-outline" />
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("ReportSuccessful")}>
                        <View style={style.questView}>
                            <Text style={style.texxt}>Eating disorders</Text>
                            <Icon type="ionicon" name="chevron-forward-outline" />
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=>this.props.navigation.navigate("ReportSuccessful")}>
                        <View style={style.questView}>
                            <Text style={style.texxt}>Something else</Text>
                            <Icon type="ionicon" name="chevron-forward-outline" />
                        </View>
                    </TouchableOpacity>

                   
                </View>
            </ScrollView>
            </View>
        )
    }
}



export default Report;

const style=StyleSheet.create({
    text:{
        fontFamily:"Raleway-SemiBold",
        // fontSize:20,
        fontSize:RFValue(14.5, 580),
        margin:5
    },
    questView:{
        padding:10,
        borderBottomWidth:1,
        borderColor:"#d3d3d3",
        flexDirection:"row",
        justifyContent:"space-between"
    },
    texxt:{
        // fontSize:15,
        fontSize:RFValue(11, 580),
        fontFamily:"Roboto-SemiBold",
        padding:5
    }
})