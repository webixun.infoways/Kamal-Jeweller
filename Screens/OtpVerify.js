import React, { Component } from 'react';
import {
    Text,View,ScrollView,ActivityIndicator,
    StyleSheet,Image,Dimensions,
    TouchableOpacity,ImageBackground, Platform
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import OTPTextView from 'react-native-otp-textinput';
import Toast from 'react-native-simple-toast';
import {AuthContext} from '../AuthContextProvider';
import CountDown from 'react-native-countdown-component';
import { RFValue } from 'react-native-responsive-fontsize';
import OneSignal from 'react-native-onesignal';

//Global StyleSheet Import
const styles = require('../Components/Style.js');

class OtpVerify extends Component {
  static contextType = AuthContext;
    constructor(props){
        super(props);
        this.state = {
            otpInput: '',
            isLoading: false,
            contact_no:'',
            resend:false
          }
    }

    //function to navigate and verify otp
    verifyOtp=()=>
    {
      if(this.state.otpInput == ""){
        Toast.show("OTP is required!");
      }
      else{
         this.setState({ isLoading: true});
            var cc= this.props.route.params.contact_no;

            fetch(global.api+"otp-verification", {
                method: 'POST',
                headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    contact:cc,
                    otp:this.state.otpInput,
                    // verification_type:"user"
                  })
                })
                .then((response) => response.json())
                .then((json) => {
                  console.warn(json)
                  if(json.msg=='ok')
                  {
                      global.user=json.usr;
                      global.token="Bearer "+json.token;

                      OneSignal.sendTag("id",''+json.usr);
                      // OneSignal.sendTag("account_type","user-bmguj1212927ns");

                      if(json.user_type=='login')
                      {
                        const data={'token':"Bearer "+json.token,'user_id':json.usr,"use_type":"done"}
                        AsyncStorage.setItem('@auth_login', JSON.stringify(data));
                        global.user_type='login';
                        this.context.login("done");
                      }
                      else{
                        const data={'token':"Bearer "+json.token,'user_id':json.usr,"use_type":"steps"}
                        AsyncStorage.setItem('@auth_login', JSON.stringify(data));
                        global.step="steps";
                        this.context.login("steps");
                      }
                    }
                  else{
                    Toast.show(json.error);
                  }
                })
                .catch((error) => console.error(error))
                .finally(() => {
                  this.setState({ isLoading: false });
                });
              }
      
      }
    
    //to resend otp
    resend=()=>{
      this.setState({resend:false,isLoading: true});
          fetch(global.api+"mobile-verification", {
              method: 'POST',
              headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json'
              },
              body: JSON.stringify({
                  contact:this.props.route.params.contact_no,
                  // request_type:'resend'
                       })
              }).then((response) => response.json())
              .then((json) => {
                  console.warn(json);
                 
               if(json.msg=='ok')
                {
                  console.warn(json.msg)
                  Toast.show('Resent successfully!'); 
                  
                }
                else
                {
                  Toast.show(json.error);
                }
              })
              .catch((error) => console.error(error))
              .finally(() => {
                this.setState({ isLoading: false });
              });
    }


    render() {
        return (
            <View style={styles.container}>

              {/* headings */}
                <Text style={[styles.h3,{
                    alignSelf:"center",top:Platform.OS == "android" ? 30 : 65
                }]} >We have sent an OTP to your Mobile</Text>

                
                
                <Text style={[styles.p,{alignSelf:"center",top:Platform.OS == "android" ? 40 : 65}]}>
                    Enter the OTP we have sent to :
                </Text>
              <Text style={[styles.p,{alignSelf:"center",top:Platform.OS == "android" ? 45 : 70}]}>
                  <Text style={{ fontFamily: "Montserrat-Regular"}}>
                  +91 { this.props.route.params.contact_no}</Text>
                  <Text style={{color:"#326bf3", fontFamily: "Montserrat-Medium"}}onPress={()=>this.props.navigation.navigate('MobileLogin')} > Edit</Text></Text>
            
            
             {/* OTP TextInput */}
             <OTPTextView
                ref={(e) => (this.input1 = e)}
                containerStyle={style.textInputContainer}
                handleTextChange={(text) => this.setState({otpInput: text})}
                inputCount={4}
                textInputStyle={style.roundedTextInput}
                keyboardType="numeric"
                returnKeyType='done'
              />


              {
                !this.state.resend ?
                <View style={{flexDirection:"row",marginTop:100,alignSelf:"center"}}>
                <Text style={{marginTop:10,color:'#696969',fontFamily:"Montserrat-Medium"}}>Request OTP in</Text>
                    <CountDown
                      // style={{marginTop:10}}
                    // size={30}
                    until={60}
                    onFinish={() => this.setState({resend:true})}
                    digitStyle={{backgroundColor: 'transparent',}}
                    digitTxtStyle={{color: '#bc3b3b',fontFamily:Platform.OS == "android" ? "Montserrat-Regular" : null}}
                    separatorStyle={{color: '#bc3b3b'}}
                    timeToShow={['M', 'S']}
                    timeLabels={{m: null, s: null}}
                    showSeparator
                  />
                </View>
                :
                <View style={{alignSelf:"center",marginTop:100}}>
                    <Text onPress={()=>this.resend()} 
                      style={[styles.buttonText,{fontSize:RFValue(10,580),color:"#bc3b3b"}]}>RESEND OTP!</Text>
                </View>
              }
              

            {/* Button to continue */}
            {this.state.isLoading?

            <View style={{height:100}}>
              <ActivityIndicator size="large" color="#bc3b3b"
              style={{top:100}}/>
            </View>

            :
              <TouchableOpacity style={[styles.buttonStyles,{marginTop:30}]}
              onPress={()=>this.verifyOtp()}>
                  <Text style={styles.buttonText}>
                      Submit
                  </Text>
              </TouchableOpacity>
            }
            </View>
        )
    }
}

export default OtpVerify;

//Internal Styling
const style=StyleSheet.create({
    textInputContainer: {
        marginBottom: 20,
        paddingLeft:50,
        paddingRight:50,
        top:100,
      },
        roundedTextInput: {
        borderRadius: 10,
        borderWidth:1,
        borderBottomWidth:1,
        backgroundColor:"#FAFAFA",
        fontFamily: "Montserrat-Regular"
      }
})