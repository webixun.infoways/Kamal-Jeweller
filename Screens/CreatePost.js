import React, { Component } from 'react'
import { View, Text , ScrollView, Image,
    TextInput, Button, StyleSheet,TouchableOpacity, BackHandler, ImageBackground, ActivityIndicator} from 'react-native';
import {Dimensions} from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize'
import {Icon,Header} from "react-native-elements"
import LinearGradient from 'react-native-linear-gradient';
import Toast from 'react-native-simple-toast'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

const styles=require("../Components/Style")

const win = Dimensions.get('window');

const options = {
    title: "Pick an Image",
    storageOptions:{
        skipBackup: true,
        path:'images'
    }
}
class CreatePost extends Component {
    constructor(props){
        super(props);
        this.state={
            image:""
        }
    }

   

    renderLeftComponent(){
        return(
            <View style={{flexDirection:"row",width:win.width,}}>
                <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
                </View>
                <Text style={[styles.headerHeadingText,
                    ]}>Create Post</Text>
            </View>
        )
    }

    render(){
        return(
            <View style={styles.container}>
                <ScrollView  keyboardShouldPersistTaps='always'  >
            {/* View for header component */}
            <View>
                    <Header
                    // containerStyle={{height:82}}
                    statusBarProps={{ barStyle: 'light-content' }}
                    leftComponent={this.renderLeftComponent()}
                    // rightComponent={this.renderRightComponent()}
                    ViewComponent={LinearGradient} // Don't forget this!
                    linearGradientProps={{
                    colors: ['white', 'white'],
                    start: { x: 0, y: 0.5 },
                    end: { x: 1, y: 0.5 }
                    }}
                />
                </View>
                    <View>
                    <AddPost navigation={this.props.navigation} data={this.props.route.params.image} />
                    </View>
                </ScrollView>
            </View>
        )
    }
}
export default CreatePost

class AddPost extends Component{
    constructor(props){
        super(props);
        console.warn(this.props.data)
        this.state={
            maxLength : 500,
            textLength:0,
            image:"",
            address:"",
            status:false,
            description:"",
            isloading:false
}
}

onChangeText(text){
this.setState({
  textLength:  text.length,
  description:text
});
}

    add =()=>{
      
        if(this.state.description==""){
            Toast.show("Please write something to post")
        }
        else{
        this.setState({isloading:true});
            if(this.props.data != '')
            {
                var photo = {
                    uri: this.props.data,
                    type: 'image/jpg',
                    name:'akash.jpg'
                };
                 
            }
            var form = new FormData();
            form.append("description",this.state.description);
            form.append("feed_file", photo);
            console.warn(form)
            fetch(global.api+'add-feed', { 
                method: 'POST',
                body: form,
                   headers: {    
                         'Authorization':global.token  
                        }, 
                         }).then((response) => response.json())
                                 .then((json) => {
                                     console.warn(json)
                                     if(!json.status)
                                     {
                                         var msg=json.msg;
                                         Toast.show(msg);
                                     }
                                     else{
                                         Toast.show(json.msg)
                                         this.props.navigation.navigate("Studio",{last_feed:json.last_added_data})
                                     }
                                    return json;    
                                }).catch((error) => {  
                                        console.error(error);   
                                     }).finally(() => {
                                        this.setState({isloading:false})
                                     });
            }
        
    }
    render(){
        return(
            <View style={{flex:1}}>
                
                <View style={{margin:10,flexDirection:"row", alignItems:"center",}}>
                    <Image source={{uri:this.props.data}} 
                    style={{height:120,width:100}}/>
                     <View style={{padding:10,width:"75%",paddingTop:-50}}>
                <TextInput 
                    placeholder="Write a caption.."
                    maxLength={500}
                    value={this.state.description}
                    multiline={true}
                    onChangeText={this.onChangeText.bind(this)}
                    style={{paddingTop:-20}}
                    />
                <Text style={{
                fontSize:12,
                color:'grey',
                marginTop:10,
                alignSelf:"flex-end",
                textAlign: 'right'
                }}> 
                {this.state.textLength}/500 
                </Text>
                </View>
                </View>
 
                <View style={{padding:15,marginTop:-10}}>
                    <Text style={[styles.p,{color:"#7c7d7e"}]}>Collaborator (Optional)</Text>
                    <TextInput 
                    style={{
                        height:50,borderWidth:1,
                        borderRadius:5,alignSelf:"center",
                        alignContent:"center",borderColor:"#d3d3d3",
                        width:Dimensions.get('window').width/1.1,top:5,
                        fontFamily: "Raleway-Regular",fontSize:RFValue(12,580)
                    }}/>
                </View>

                <View style={{padding:15,marginTop:-20}}>
                    {!this.state.status ? 
                <View style={{backgroundColor:"#fff"}}>
                   
                   <GooglePlacesAutocomplete
                    placeholder='Add Location'
                    listViewDisplayed={false}
                    fetchDetails={true}
                    
                    returnKeyType={'default'}
                    onPress={(data, details = null) => {
                        this.setAddress(data,details)
                      }}
                    query={{
                        key: global.google_key,
                        language: 'en',
                    }}
                    styles={{
                        container: {
                        borderWidth:1,
                        borderRadius:5,alignSelf:"center",
                        alignContent:"center",borderColor:"#d3d3d3",
                        width:Dimensions.get('window').width/1.1,top:5,
                        fontFamily: "Raleway-Regular",fontSize:RFValue(12,580)
                        }
                    }}
                    />
              </View>
                    :
                    <View>
                    <View style={{ borderRadius:2,
                        top:10,
                        padding:10,
                        width:"100%",
                        alignSelf:"center",
                        fontFamily: "Raleway-Regular",
                        shadowColor: 'grey',
                        elevation: 1,
                        color:"#000",}}>
                        <Text>{this.state.address}</Text>
                        </View>
                        <Text style={[styles.p,{top:10,textAlign:"right"}]} 
                        onPress={()=>this.setState({status:false})} >
                            Change location
                        </Text>
                        </View>
                }    
                </View>
                {this.state.isloading ? 
                <View style={{marginTop:200}}> 
                    <ActivityIndicator size="large" color="#bc3b3b" />
                    </View>:
                    
                <TouchableOpacity  
                    onPress={()=>this.add()}
                     style={[styles.buttonStyles,{marginTop:20}]}>
                <Text style={[styles.buttonText, {color:'#fff'}]}>
                  Post</Text>
                </TouchableOpacity>
                }
                    
            </View>
        )
    }
}



const style=StyleSheet.create({
    buttonStyles: {
        backgroundColor: "#BC3B3B",
        flexDirection:"row",
        width: Dimensions.get("window").width / 1.4,
        alignSelf: "center",
        justifyContent: "center",
        alignItems: "center",
        height: 45,
        borderRadius: 25,
        marginTop: 40,
    },
    buttonText: {
        color: "#fff",
        top:-3,
        fontSize: RFValue(20, 580),
        fontFamily:"DancingScript-Bold"
    },
    
}
)