import React, { Component } from 'react';
import {
    Text,View,ScrollView,
    StyleSheet,Image,Dimensions,
    TouchableOpacity,TextInput,Pressable
} from 'react-native';
import { Header,Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { Searchbar } from 'react-native-paper';

//Global StyleSheet Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class Collections extends Component {

    constructor(props){
        super(props);
    }

    renderLeftComponent(){
        return(
            <View style={{flexDirection:"row",width:win.width}}>
                <Text style={styles.headerHeadingText}>Collection</Text>
            </View>
        )
    }

    renderRightComponent(){
        return(
            <View style={{ marginTop:5,padding:5,}}>
                <Icon
                name='cart'
                size={25}
                color='#4A4B4D' 
                type="ionicon"
                onPress={()=>this.props.navigation.navigate("MyCart")}
                // style={{margin:5}}
                /> 
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <ScrollView>
                    {/* View for header component */}
                    <View>
                        <Header
                        statusBarProps={{ barStyle: 'light-content' }}
                        leftComponent={this.renderLeftComponent()}
                        rightComponent={this.renderRightComponent()}
                        // data={this.props.route.params}
                        ViewComponent={LinearGradient} // Don't forget this!
                        linearGradientProps={{
                        colors: ['white', 'white'],
                        start: { x: 0, y: 0.5 },
                        end: { x: 1, y: 0.5 }
                        
                        }}
                    />
                    </View>

                    <View>
                        {/* View for search input */}
                        <View style={{backgroundColor:"#fff"}}>
                            <Searchbar
                            style={styles.search}
                            placeholder="Search here..."
                            inputStyle={{
                                color:"#7D7E7F",
                                fontSize:16,
                                fontFamily: "Montserrat-Regular",
                                paddingTop:7,
                            }}
                            icon={()=><Icon name="search-outline" type="ionicon"
                            onPress={()=>this.onClick()} size={25}
                            color="#7D7E7F"/>}
                            />
                        </View>
                    </View>

                    {/* <View style={{flexDirection:"row"}}>
                    <SideComponent/>
                        <ScrollView>
                            <MainComponent/>
                            <MainComponent/>
                            <MainComponent/>
                            <MainComponent/>
                            <MainComponent/>
                            <MainComponent/>
                            <MainComponent/>
                            <MainComponent/>
                        </ScrollView>
                    </View> */}
                    <SideComponent navigation={this.props.navigation}/>
                </ScrollView>
            </View>
        )
    }
}

class SideComponent extends Component{
    render(){
        return(
            <View style={{marginBottom:20}}>
             <View style={{flexDirection:"row"}}>
                {/* <View style={styles.sideBorder}></View> */}

                <ScrollView>
                    <MainComponent navigation={this.props.navigation}/>
                    <MainComponent navigation={this.props.navigation}/>
                    <MainComponent navigation={this.props.navigation}/>
                    <MainComponent navigation={this.props.navigation}/>
                    <MainComponent navigation={this.props.navigation}/>
                    <MainComponent navigation={this.props.navigation}/>
                    <MainComponent navigation={this.props.navigation}/>
                    <MainComponent navigation={this.props.navigation}/>
                </ScrollView>
             </View>
       
            </View>
        )
    }
}

class MainComponent extends Component{
    render(){
        return(
            <View style={{marginTop:10,marginBottom:10,left:7}}>
                <Pressable style={styles.categoryComponent}
                onPress={()=>{this.props.navigation.navigate("ProductList")}}>
                    <View style={style.imgView}>
                        <Image source={require('../image/greenRing.jpg')}
                        style={style.img}/>
                    </View>

                    <View style={{width:"100%",flexDirection:"row",justifyContent:"space-between",left:20}}>
                        <View style={{flexDirection:"column",alignSelf:"center",
                        bottom:5,marginLeft:5}}>
                            <Text style={[styles.headerHeadingText,{
                                alignSelf:"center",
                            }]}>
                            Rings</Text>

                            <Text style={[styles.p,{top:-5,left:8}]}>
                                120 items
                            </Text>
                        </View>

                    <View style={style.iconView}>
                        <Icon name="chevron-forward-outline" type="ionicon" 
                        color="#fff" />
                    </View>
                    </View>
                </Pressable>
            </View>
        )
    }
}

export default Collections;

const style=StyleSheet.create({
    img:{
        height:60,
        width:60,
        borderRadius:50,
        alignSelf:"center"
    },
    imgView:{
        position:'absolute',
        left:-25,
        // left:15,
        top:12,
        justifyContent:"center",
        width:"20%"
    },
    iconView:{
        backgroundColor:"#bc3b3b",
        shadowRadius: 50,
        left:20,
        shadowOffset: { width: 50, height: 50 },
        elevation:3,
        borderRadius:50,
        height:40,
        width:40,
        justifyContent:"center",
        alignSelf:'center'
    }
})
