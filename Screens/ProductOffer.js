import React, { Component } from 'react';
import {
    View, ScrollView,
    StyleSheet, Pressable,TextInput,
    Image, TouchableOpacity, Text, SafeAreaView, Dimensions
} from 'react-native';
import Swiper from 'react-native-swiper';
import { RFValue } from "react-native-responsive-fontsize";
import { Icon, Rating } from 'react-native-elements';
import {Picker} from '@react-native-picker/picker';

//Global Style Import
const style = require('../Components/Style.js');

class ProductOffer extends Component {

    constructor(props) {
        super(props);
        this.state={
            status:false,
            fav:false,
        }
    }

   

    render() {
        return (

            <View style={[style.container,{backgroundColor: "#fff" }]}>
             
              <View style={{height:"40%"}}>
                
                    {/* Main Slider */}
                <Swiper style={styles.wrapper}
                    showsButtons={false}
                    paginationStyle={{
                        bottom: 50,
                    }}
                    dotColor="#DDDDDD"
                    activeDotColor="#BC3B3B"
                //  autoplay={true}
                >
                    
                    {/* Slide 1 */}
                    <View style={styles.slide1}>
                       <Image source={require('../image/11.jpg')}
                        style={styles.slide1Image}/>
                    </View>

                    {/* Slide 2 */}
                    <View style={styles.slide2} >
                        <Image source={require('../image/11.jpg')}
                        style={styles.slide2Image}/>
                    </View>
                  

                    {/* Slide 3 */}
                    <View style={styles.slide3}>
                        <Image source={require('../image/11.jpg')}
                        style={styles.slide1Image}/>
                    </View>
                </Swiper>
                  {/* View for header */}
                  <View style={{flexDirection:'row',position:"absolute",
                  justifyContent:"space-between",width:"100%",}}>
                    <Text style={{margin:40,marginLeft:5,top:0,padding:10,borderRadius:50,opacity:0.5, backgroundColor:"white"}}
                    onPress={()=>this.props.navigation.goBack()}>
                        <Icon name="chevron-back-outline" size={27}
                        type="ionicon" color="#4A4B4D"/>
                    </Text>

                    <Text style={{margin:40,marginRight:10,top:0,padding:10,borderRadius:50,opacity:0.5, backgroundColor:"white"}}
                    onPress={()=>this.props.navigation.navigate("MyCart")}>
                    <Icon name="cart-outline" type="ionicon" 
                    color="#4A4B4D" size={27}/>
                    </Text>
                </View>
                
            
                </View>
               
               {/* view for main component call */}
                <View style={styles.mainContainer}> 
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <MainContainer navigation={this.props.navigation} />
                    </ScrollView>
                
                </View>
                    {/* view for sticky bottom buttons */}
                    <View style={{flexDirection:"row",marginTop:-20, height:50}}>
                    <Pressable style={style.stickyButton1}
                     onPress={()=>this.props.navigation.navigate("MyCart")}>
                        <Text style={style.stickyButton1Text}>
                            Buy now
                        </Text>
                    </Pressable>
                    <Pressable style={style.stickyButton2}
                     onPress={()=>this.props.navigation.navigate("MyCart")}>
                        <Text style={style.stickyButton2Text}>
                            Add to cart
                        </Text>
                    </Pressable>
                </View>
                  
            </View>
        )
    }
}

export default ProductOffer;

class MainContainer extends Component{

    constructor(props){
        super(props);
        this.state={
            fav:false,
            isLoading:false,
            selectedValue:"",
            selectedValue2:"",
            pinCode:"",
            status:true
        }
    } 
    like=()=>{
        if(this.state.fav==false){
            this.setState({fav:true})
        }
        else{
            this.setState({fav:false})
        }
    }

    

    render(){
        return(
            <View>
               <View>
                
                <View style={{flexDirection:"row",justifyContent:"space-around"}}>
                    {/* View for name and ratings */}
                    <View>
                    <Text style={[style.h3,{paddingTop:20,fontSize:17}]}>
                    Dainty Necklace Set</Text>

                    <Text  style={{top:5}}>
                    <Rating 
                        showRating={false}
                        readonly={true}
                        imageSize={15}
                        style={{marginLeft:10}}
                        />
                    </Text>
                    </View>

                    
                    {/* view for heart */}
                    <View style={styles.heartView}>
                    <Icon name="heart" type="ionicon" color={this.state.fav ? "red" : "#CCCDCD"}
                    onPress={()=>this.like()} />
                    </View>
                </View>


                {/* View for price */}
               <View style={{justifyContent:"flex-end"}}>
                   <Text style={[style.h3,{fontFamily: "Montserrat-SemiBold",alignSelf:"flex-end",
               fontSize:14, right:30,marginTop:10}]}>
                       ₹ 75,650
                   </Text>
                   <Text style={[style.small,,{fontFamily: "Montserrate-SemiBold",fontSize:12,alignSelf:"flex-end",
                    right:30}]}>
                       per Piece
                   </Text>
               </View>


               {/* View for line */}
               <View style={style.verticalLine}></View>

                {/* View for dropdowns */}
               <View>
                    <Text style={[styles.heading,{paddingLeft:20,top:10,fontSize:14, paddingBottom:10}]}>
                        Customize your order
                    </Text>

                    {/* picker for size */}
                    <Picker
                        selectedValue={this.state.selectedValue}
                        onValueChange={(e) => this.setState({selectedValue:e})}
                        style={{
                            backgroundColor:"#F2F2F2",
                            marginTop:15,
                            fontSize:10,
                            width:Dimensions.get('window').width/1.2,
                            alignSelf:"center",
                            height:25,
                            marginBottom:5
                        }}
                        mode="dialog"
                    >
                        <Picker.Item label="-Select the size-"  />
                        <Picker.Item label="16" value="16" />
                        <Picker.Item label="17" value="17" />
                        <Picker.Item label="18" value="18" />
                        <Picker.Item label="19" value="19" />
                    </Picker>
                    
                    {/* picker for gross weight */}
                    <Picker
                        selectedValue={this.state.selectedValue2}
                        onValueChange={(e) => this.setState({selectedValue2:e})}
                        style={{
                            backgroundColor:"#F2F2F2",
                            
                            marginTop:15,
                            width:Dimensions.get('window').width/1.2,
                            alignSelf:"center",
                            height:25,
                            marginBottom:5
                        }}
                        itemStyle={{
                            fontFamily: "Raleway-Medium",
                        }}
                    >
                        <Picker.Item label="-Select gross weight-" />
                        <Picker.Item label="3gm" value="3gm" />
                        <Picker.Item label="4gm" value="4gm" />
                        <Picker.Item label="5gm" value="5gm" />
                        <Picker.Item label="6gm" value="6gm" />
                    </Picker>

                    <Text style={[style.headingSmall,
                        {alignSelf:'center',top:5,bottom:5}]}>
                        Diamond weight 0.148 carat
                    </Text>
               </View>

                {/* View for line */}
                <View style={[style.verticalLine,
                {marginTop:70,width:Dimensions.get('window').width/1.1,
                alignSelf:"center"}]}>
                </View>

                {/* View for number of pieces */}
                <View style={{flexDirection:'row',width:"100%",marginTop:20,
                marginnBottom:20,justifyContent:"space-between"}}>
                    <View style={{width:"50%"}}>
                        <Text style={[styles.heading,{paddingLeft:20,fontSize:14,marginTop:5}]}>
                            Number of pieces
                        </Text>
                    </View>

                    <View style={{flexDirection:'row',marginTop:10,width:"50%",justifyContent:"space-evenly",right:10}}>
                        <TouchableOpacity style={styles.button}>
                            <Icon name="remove" type="ionicon" size={20} color="#BC3B3B"
                            style={{top:-2}}/>
                        </TouchableOpacity>

                        <View style={[styles.button]}>
                            <Text style={[style.h4,{top:0,alignSelf:"center",fontFamily: Platform.OS == "android" ? "Roboto-Medium" : null,color:"#BC3B3B"}]}>
                                1
                            </Text>
                        </View>

                        <TouchableOpacity style={styles.button}>
                            <Icon name="add" type="ionicon" size={20} color="#BC3B3B"
                            style={{top:-2}}/>
                        </TouchableOpacity>
                    </View>
                </View>

                {/* View for terms and conditions */}
                <View style={styles.view}>
                    <Text style={[style.h6,{padding:5,lineHeight:25,color:"#8C8D8E",alignSelf:"center"}]}>
                        *Weight and price of the jewellery item may vary subject
                        to the stock available.
                    </Text>
                </View>

                {/* View for description */}
                <View>
                    <Text style={[styles.heading,{paddingLeft:20,top:15,fontSize:14, bottom:10}]}>
                        Description
                    </Text>
                    <View style={{alignSelf:"center",width:Dimensions.get('window').width/1.1,marginTop:10}}>
                    <Text style={[styles.text,{top:10,fontSize:12, bottom:5,lineHeight:22}]} numberOfLines={4}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Aliquam nisl metus, rhoncus semper tellus at, ultricies 
                        dictum dui.
                    </Text>
                    </View>
                </View>

                 {/* View for line */}
                 <View style={[style.verticalLine,
                    {marginTop:25,width:Dimensions.get('window').width/1.1,
                    alignSelf:"center"}]}>
                </View>

                {/* view for pincode options */}
                <View>
                    <View style={{flexDirection:'row',justifyContent:"space-between",top:20}}>
                        <View>
                            <Text style={[styles.text,{marginLeft:20}]}>
                                Delivery options for 
                                 248001
                            </Text>
                            {this.state.status ?
                            <View>
                            <Text style={[style.headingSmall,
                            {alignSelf:'center',top:10,paddingLeft:5,fontSize:RFValue(11,580)}]}>
                                Standard Delivery is avaiable for pincode
                                 248001
                            </Text>
                            </View>
                            :
                            <View style={{paddingHorizontal:20}}>
                            <TextInput placeholder="Enter Pincode"
                            maxLength={6}
                            onChangeText={(text)=>this.setState({notes:text})}
                            style={ {fontSize:RFValue(12,580),fontFamily:"Montserrat-Medium", borderBottomWidth:1,borderColor:"#7c7d7e",marginBottom:10}} />
                            </View>
    }
                        </View>
                        
                        <TouchableOpacity 
                        onPress={()=>this.setState({status:false})}
                        style={[style.changeButton,{right:55,top:-5}]}>
                            <Text style={[styles.text,{color:"#262626",top:3}]}>
                                {this.state.status ? "Change" : "Check"}
                            </Text>
                            {/* <Icon name="chevron-forward-outline" type="ionicon" color="#828282" size={22}/> */}
                        </TouchableOpacity>
                    </View>
                </View>

                {/* View for line */}
                <View style={[style.verticalLine,{marginTop:50}]}></View>
                
                {/* View for buttons */}
                <View style={{marginBottom:10}}>
                    <Text style={[style.headingSmall,
                    {top:5,paddingLeft:5,fontSize:RFValue(10,580)}]}>
                        Only <Text style={{fontFamily: "Montserrat-Medium",}}>2 </Text>
                        left in stock.
                    </Text>

                    {/* <TouchableOpacity style={[style.buttonStyles,{marginTop:15,
                    width: Dimensions.get("window").width / 1.2,}]}>
                        <Text style={style.buttonText}>
                            Add to cart
                        </Text>
                    </TouchableOpacity> */}

                    <Pressable onPress={()=>this.props.navigation.navigate("BookAppointment")} style={[style.buttonStyles,{marginTop:30,bottom:5,
                    width: Dimensions.get("window").width / 1.2,}]}>
                        <Text style={style.buttonText}>
                            Book an appointment
                        </Text>
                    </Pressable>
                </View>
                

            
                    
                </View>
            </View>
        )
    }
}


//Internal Stylesheet
const styles = StyleSheet.create({
    slide1: {
        height:"100%",
        width: "100%",

    },
    slide2: {
        height:"100%",
        width: "100%",
        backgroundColor: "#fff"
    },
    slide3: {
        height:"100%",
        width: "100%",
        backgroundColor: "#fff"
    },
    slide1Image: {
        height: "100%",
        width: "100%",

    },
    slide2Image: {
        height: "100%",
        width: "100%",
    },
    slide3Image: {
        height: "100%",
        width: "100%",
    },
    
    mainContainer:{
        height:"100%",
        // position:"absolute",
        borderTopLeftRadius:50,
        borderTopRightRadius:50, 
        width:"100%",
        top:-30, 
        bottom:0,
        backgroundColor:"#fff",
        flex:1,
    },
    heartView:{
        shadowRadius: 50,
        shadowOffset: { width: 1, height:1 },
        elevation:2,
        height:50,
        width:50,
        backgroundColor:"#fff",
        justifyContent:"center",
        borderRadius:50,
        marginTop:10
    },
    heading:{
        fontSize:RFValue(13, 580),
        // fontFamily:"Raleway-Bold",
        fontFamily: "Montserrat-Medium",
        color:'#4a4b4d',
    },
    button:{
        borderColor:"#bc3b3b",
        borderWidth:1,
        borderRadius:10,
        width:35,
        height:22,
    
    },
    view:{
        borderColor:"#EBEBEB",
        borderWidth:1,
        borderRadius:5,
        width:Dimensions.get('window').width/1.1,
        marginTop:25,
        alignSelf:"center",
        paddingBottom:5,
        
    },
    text:{
        // fontFamily:"Raleway-Regular",
        fontFamily: "Montserrat-Medium",
        color:"#222222",
        fontSize:RFValue(10,580)
    }
  
})