import React, { Component } from 'react';
import { Dimensions, TextInput } from 'react-native';
import {
    View,ImageBackground,
    StyleSheet,Pressable,ScrollView,Linking,
    Image,Text,TouchableOpacity
} from 'react-native';
import {Icon,Header} from "react-native-elements"
import LinearGradient from 'react-native-linear-gradient';

//Global Style Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class AboutUs extends Component {

    constructor(props){
        super(props);
    }

    renderLeftComponent(){
        return(
            <View style={{flexDirection:"row",width:win.width}}>
                <View style={{padding:5,top:1}} >
                    <Icon onPress={()=>this.props.navigation.goBack()}
                    name="chevron-back-outline" type="ionicon" size={22}
                    />
                </View>
                <Text style={[styles.headerHeadingText,
                    ]}>About Us</Text>
            </View>
        )
    }

    render() {
        return (
            <View style={[styles.container,{backgroundColor:"#fafafa"}]}>
                 {/* View for header component */}
                 <View>
                    <Header
                    // containerStyle={{height:82}}
                    statusBarProps={{ barStyle: 'light-content' }}
                    leftComponent={this.renderLeftComponent()}
                    ViewComponent={LinearGradient} // Don't forget this!
                    linearGradientProps={{
                    colors: ['white', 'white'],
                    start: { x: 0, y: 0.5 },
                    end: { x: 1, y: 0.5 }
                    }}
                />
                </View>
                
                <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{paddingHorizontal:20, paddingTop:10, backgroundColor:"#fff",}}>
                    <Text style={[styles.h3,{textAlign:"justify",lineHeight:20, fontSize:15, fontFamily:"Raleway-Regular"}]}>
                    For precious jewellery, Kamal Jewellers in Rajpur Road, Dehradun is a recognised name in the city since 1970. With rich experience in the gems and jewellery business, this firm has become a celebrated name for stunning designs and collections. They are revered for their handpicked and exclusive designs that feature in their gold and diamond jewellery collections.{'\n'}{'\n'} From classic styles to bespoke pieces, the fine craftsmanship highlights each precious piece of jewellery. Adding spark to every occasion, there is something for everyone and suitable for special events like weddings and ceremonies. Occupying an unmistakable location on the Rajpur road in the neighbourhood of Rajpur Road, one can plan a visit to this showroom via any of the available modes of transport. Undoubtedly it is one of the best jewellery showrooms in Dehradun.{'\n'}{'\n'}

<Text style={{fontWeight:"bold"}}>Products and Services offered at Kamal Jewellers:</Text>
{'\n'}{'\n'}
Enter this venue to feast your eyes on precious jewellery designs in diamond rings, diamond set, pearl Polki and more. Kids kadda, ring, earring and pendant are also available here. Find religious artifacts for Lakshmi Ganesha pair, Maa Durga and Radha Krishna. This establishment into special services like designing bridal trousseau, jewellery upgrade and jewellery redesigning. The designs are stunningly beautiful with embellishments that make the beholder stand out from the crowd. Time and again, the collections are refreshed to keep with the trending and popular styles. You can conveniently pay for your purchase using Cash, Credit Card, American Express, Diner Club Card, Master Card, Visa Card, Cheques, Debit Cards.

                    </Text>
                </View>
                </ScrollView>
            </View>
        )
    }
}

export default AboutUs

const style=StyleSheet.create({

    header:{
        fontSize:20,
        marginLeft:10,
        color:"black"
    },

}
)