import React, { Component } from 'react'
import {
    View, Text, ScrollView, Image, Pressable, FlatList,Modal,
    TextInput, Button, StyleSheet, TouchableOpacity, BackHandler, ImageBackground, ActivityIndicator
} from 'react-native';
import { Dimensions } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize'
import { Header, Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import RBSheet from 'react-native-raw-bottom-sheet'
import Toast from 'react-native-simple-toast';
import RazorpayCheckout from 'react-native-razorpay';
import PaymentFailed from './PaymentFailed';
import navigationChange from '../helpers/index';

const styles = require("../Components/Style")

const win = Dimensions.get('window');

class Checkout extends Component {

    constructor(props) {
        super(props);
        console.warn('props',props)
        this.state = {
            text: "",
            area: '',
            city: '',
            postal_code: '',
            state: '',
            status: true,
            firstName: '',
            lastName: '',
            cardNo: '',
            cvv: '',
            expYear: '',
            expMonth: '',
            data: [],
            address: [],
            addressdata: [],
            card: [],
            item: this.props.route.params.data,
            order_id: "",
            isLoading: false,
            iconName: "radio-button-off",
            iconName1: "radio-button-off",
            iconName2: "radio-button-off",
            cardIcon: "radio-button-off",
            grandTotal: this.props.route.params.total_price,
            address_id: this.props.route.params.address_id,
            mode: "",
            payment_id: "",
            product_price: "",
            product_quantity: "",
            product_id: [],
            select: {},
            last_selected: "",
            name:'',
            contact_no:'',
            mail:'',
            visible:false,
            discount:this.props.route.params.discount,
            sub_total:this.props.route.params.sub_total
        }
    }

    componentDidMount() {
        // alert(this.state.address_id)
        this.saved_cards()
        // console.warn("address id is",this.state.address_id)
        this.get_address();
        this.set_address();
        this.get_profile();
        this.focusListener = this.props.navigation.addListener('focus', () => {
            this.saved_cards()
            this.get_address();
            this.set_address();
            this.get_profile();
        });
        this.state.item.map(value => {
            // console.warn(value);
            this.state.product_id.push(value.product_id);
            this.setState({ product_price: value.price });
            this.setState({ product_quantity: value.product_quantity });
        });
    }

    setvisible = (visible) => {
        this.setState({ visible: visible });
    }

    //setting state for cash
    cash = () => {
        if (this.state.iconName == "radio-button-off") {
            this.setState({ iconName: "radio-button-on", mode: "cod" })
            this.setState({ iconName1: "radio-button-off", })
            this.setState({ iconName2: "radio-button-off", cardIcon: "radio-button-off" })
        } else {
            this.setState({ iconName: "radio-button-off", select: "" })
        }
    }
    // Card
    card = () => {
        if (this.state.cardIcon == "radio-button-off") {
            this.setState({ cardIcon: "radio-button-on", mode: "card" })
            this.setState({ iconName1: "radio-button-off", })
            this.setState({ iconName2: "radio-button-off", iconName: "radio-button-off" })
        } else {
            this.setState({ cardIcon: "radio-button-off", select: "" })
        }
    }
    //setting state for female gender
    net = (id) => {
        // alert(id)
        if (this.state.iconName1 == "radio-button-off") {
            this.setState({ iconName1: "radio-button-on", mode: "netbanking" })
            this.setState({ iconName: "radio-button-off" })
            this.setState({ iconName2: "radio-button-off", cardIcon: "radio-button-off" })
        } else {
            this.setState({ iconName1: "radio-button-off", select: "" })
        }
    }

    //setting state for others gender
    upi = () => {
        if (this.state.iconName2 == "radio-button-off") {
            this.setState({ iconName2: "radio-button-on", mode: "upi" })
            this.setState({ iconName: "radio-button-off" })
            this.setState({ iconName1: "radio-button-off", cardIcon: "radio-button-off" })
        } else {
            this.setState({ iconName2: "radio-button-off", select: "" })
        }
    }


    renderLeftComponent() {
        return (
            <View style={{ flexDirection: "row", width: win.width }}>
                <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
                </View>
                <Text style={[styles.headerHeadingText,
                ]}>Payment Checkout</Text>
            </View>
        )
    }

    // fetching all saved addresses
    get_address = () => {
        fetch(global.api + 'get-address',
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': global.token
                },
            })
            .then((response) => response.json())
            .then((json) => {
                if (json.status) {
                    this.setState({ addressdata: json.data })
                    // console.warn(this.state.addressdata)
                    this.state.addressdata.map(value => {
                        this.setState({ address_id: value.id })
                    })
                    this.set_address();
                    this.chooseAddress(this.state.address_id);
                }
                else {
                    // Toast.show(json.msg)
                }
                return json;
            })
            .catch((error) => console.error(error))
            .finally(() => {
                this.setState({ isLoading: false });
            });
    }

    // fetching default address
    set_address = () => {
        fetch(global.api + "set-address", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': global.token
            },
            body: JSON.stringify({
                address_id: this.state.address_id,
            })
        }).then((response) => response.json())
            .then((json) => {
                if (json.status) {
                    this.setState({ address: json.data })
                }
                return json;
            }).catch((error) => {
                console.error(error);
            }).finally(() => {
                this.RBSheet.close()
            });

    }

    // choose address
    chooseAddress = (id) => {
        fetch(global.api + "set-address", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': global.token
            },
            body: JSON.stringify({
                address_id: id,
            })
        }).then((response) => response.json())
            .then((json) => {
                if (json.status) {
                    this.setState({ address: json.data })
                }
                else {
                    this.setState({ address: '' })
                }
                return json;
            }).catch((error) => {
                console.error(error);
            }).finally(() => {
                this.RBSheet.close()
            });

    }

    //to get user profile
    get_profile = () => {
        fetch(global.api + 'get-user-profile',
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': global.token
                },
            })
            .then((response) => response.json())
            .then((json) => {
                // console.warn("jkk",json)
                if (json.status) {
                    this.setState({ name: json.data[0].f_name })
                    this.setState({ contact_no: json.data[0].contact, mail: json.data[0].email })

                }
                else {
                    // Toast.show(json.msg)
                }
                return json;
            })
            .catch((error) => console.error(error))
            .finally(() => {
                this.setState({ isLoading: false });
            });
    }


    // Card add
    cardDetails = () => {
        let validation = /^[a-zA-Z" "]+$/;
        let NoValidation = /^[0-9]+$/;
        let monthValidation = /^[0-9]+$/;
        let yearValidation = /^[0-9]+$/;
        let noValid = NoValidation.test(this.state.cardNo)
        let monthValid = monthValidation.test(this.state.expMonth)
        let yearValid = yearValidation.test(this.state.expYear)
        let isValid = validation.test(this.state.firstName)
        let isvalid = validation.test(this.state.lastName)
        {
            this.setState({ msg: "" });
            if (this.state.cardNo == "" ||
                this.state.firstName == "" || this.state.lastName == "" ||
                this.state.expYear == "" || this.state.expMonth == "") {
                Toast.show("All fields are required !");
            }
            else if (this.state.cardNo.length < 16) {
                Toast.show("Enter a valid Card Number !");
            }
            else if (!noValid) {
                Toast.show("Enter a valid Card Number !");
            }
            else if (!monthValid) {
                Toast.show("Enter a valid Expiry Month !");
            }
            else if (!yearValid) {
                Toast.show("Enter a valid Expiry Year !");
            }
            else if (!isValid) {
                Toast.show("Enter a valid First name !");
            }
            else if (!isvalid) {
                Toast.show("Enter a valid Last name !");
            }
            else {
                this.setState({ isLoading: true });

                fetch(global.api + "add-card", {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': global.token
                    },
                    body: JSON.stringify({
                        // user_id: global.user,
                        card_no: this.state.cardNo,
                        name: this.state.firstName + this.state.lastName,
                        expiry_year: this.state.expYear,
                        expiry_month: this.state.expMonth,
                        cvv: this.state.cvv

                    })
                }).then((response) => response.json())
                    .then((json) => {
                        // console.warn(json)
                        if (!json.status) {
                            var msg = json.msg;
                            Toast.show(msg);
                        }
                        else {
                            this.RBSheet.close();
                            Toast.show(json.success)
                        }
                        return json;
                    }).catch((error) => {
                        console.error(error);
                    }).finally(() => {
                        this.setState({ isLoading: false }),
                            this.saved_cards();
                    });
            }
        }
    }

    // fetching saved cards
    saved_cards = () => {
        fetch(global.api + 'fetch-saved-card', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: global.token,
            },
        })
            .then(response => response.json())
            .then(json => {
                // console.warn(json);
                if (json.status) {
                    this.setState({ card: json.data });
                }
                // console.warn("Card data", this.state.card)
                return json;
            })
            .catch(error => console.error(error))
            .finally(() => {
                this.setState({ isLoading: false });
            });
    };

    // Place order
    place_order = () => {
        // console.warn(this.state.mode)
        var options = {

            description: 'Orders from the app',
            image: "https://demo.webixun.com/kj/kj.png",
            currency: 'INR',
            key: 'rzp_test_SsWgf6Qj4xwonA',
            amount: this.state.grandTotal * 100,
            name: this.state.name,
            order_id: this.state.order_id,
            method: "debit",
            prefill: {
                email: this.state.mail,
                contact: this.state.contact_no,
                method:this.state.mode
            },
        }
        if (this.state.mode != "") {
            fetch(global.api + 'place-order', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    Authorization: global.token,
                },
                body: JSON.stringify({
                    payment_mode: this.state.mode,
                    address_id: this.state.address_id,
                }),
            })
                .then(response => response.json())
                .then(json => {
                    // console.warn("order", json);
                    if (json.status) {
                        this.setState({ order_id: json.order_id })
                        if (json.message == "no") {
                            this.setState({visible:true})
                            // this.props.navigation.navigate("ThankYou");
                        }
                        else if (json.message == "payment") {
                            // alert("Payment Successful")
                            RazorpayCheckout.open(options).then((data) => {
                                // this.setState({ payment_id: JSON.stringify(data.razorpay_payment_id) });
                                // alert("hy")
                                this.payment_verify(data.razorpay_payment_id);
                                // this.RBSheet.close();
                            }).catch(error =>
                                // alert("Payment is not processed, You are reirecting to home page")
                                // <PaymentFailed />
                                this.props.navigation.navigate("PaymentFailed")
                            )
                        }
                        else if (json.message == "Order Placed!"){
                            // this.props.navigation.navigate("ThankYou");
                            this.setState({visible:true})
                        }
                    }
                    return json;
                })
                .catch(error => {
                    console.error(error);
                })
                .finally(() => {
                    this.setState({ isLoading: false });
                });
        }
        else {
            Toast.show("Payment mode is required!")
        }
    };

    // Payment verify
    payment_verify = (e) => {
        // console.warn("order_id", this.state.order_id)
        fetch(global.api + 'verify-payment', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: global.token,
            },
            body: JSON.stringify({
                razorpay_payment_id: e,
                order_id: this.state.order_id,

            }),
        })
            .then(response => response.json())
            .then(json => {
                // console.warn("payment", json);
                if (!json.status) {
                    var msg = json.errors;
                    Toast.show(msg);
                } else {
                    Toast.show("Order Placed Successfully!");
                    // this.props.navigation.navigate("ThankYou");
                    this.setState({visible:true})
                    //    this.props.my_cart()
                }
                return json;
            })
            .catch(error => {
                console.error(error);
            })
            .finally(() => {
                this.setState({ isLoading: false });
            });
    }

    backToHome = () => {
        navigationChange.isChanged = true;
        this.setState({ visible: false }), this.props.navigation.navigate('Home')
    }

    render() {
        return (

            <View style={{ flex: 1, }}>
                {/* View for header component */}
                <View>
                    <Header
                        // containerStyle={{ height: 82 }}
                        statusBarProps={{ barStyle: 'light-content' }}
                        leftComponent={this.renderLeftComponent()}
                        ViewComponent={LinearGradient} // Don't forget this!
                        linearGradientProps={{
                            colors: ['white', 'white'],
                            start: { x: 0, y: 0.5 },
                            end: { x: 1, y: 0.5 }
                        }}
                    />
                </View>

                {/* Delivery address */}
                {this.state.address_id == undefined ?
                null
                    :
                    <Address
                    navigation={this.props.navigation}
                    chooseAddress={this.chooseAddress}
                    my_cart={this.my_cart}
                    data={this.state.addressdata}
                    address={this.state.address} />}
                <ScrollView>
                    {/* Payment method */}
                    {/* <View style={{ paddingHorizontal: 10, backgroundColor: "#fff", marginTop: 5, paddingBottom: 10 }}>
                        <View style={[style.addressView]}>
                            <Text style={[styles.h3, { color: "#222222", fontSize: RFValue(12, 580) }]}>Suggested for you</Text>
                            <Text onPress={() => this.RBSheet.open()} style={{ color: "#bc3b3b", fontSize: RFValue(11, 580), top: 5, fontFamily: "Raleway-Medium", }}>+ Add Card</Text>
                        </View>

                       
                        <RBSheet
                            ref={ref => {
                                this.RBSheet = ref;
                            }}
                            height={480}
                            openDuration={250}
                            customStyles={{
                                container: {
                                    borderTopLeftRadius: 20,
                                    borderTopRightRadius: 20
                                }
                            }}
                        >

                            
                            <View>
                                <View style={{ borderBottomWidth: 1, marginHorizontal: 20, marginTop: 20, borderColor: "#d3d3d3" }}>
                                    <Text style={[style.methodText, { fontSize: RFValue(13, 580), alignSelf: "flex-start" }]} >Add Credit/Debit Card</Text>

                                </View>
                                <TextInput
                                    placeholder="Card Number"
                                    style={style.textInput}
                                    maxLength={16}
                                    keyboardType="number-pad"
                                    value={this.state.cardNo}
                                    onChangeText={(text) => { this.setState({ cardNo: text }) }}
                                />
                                <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", alignContent: "center", width: "75%", alignSelf: "center" }}>
                                    <Text style={{ fontSize: RFValue(10, 580), top: 4, fontFamily: "Raleway-Medium", }}>Expiry Date</Text>
                                    <TextInput
                                        placeholder="MM"
                                        maxLength={2}
                                        keyboardType="number-pad"
                                        value={this.state.expMonth}
                                        onChangeText={(text) => { this.setState({ expMonth: text }) }}
                                        style={[style.textInput, { width: "25%", paddingLeft: 28 }]} />

                                    <TextInput
                                        placeholder="YY"
                                        maxLength={2}
                                        keyboardType="number-pad"
                                        value={this.state.expYear}
                                        onChangeText={(text) => { this.setState({ expYear: text }) }}
                                        style={[style.textInput, { width: "25%", paddingLeft: 30 }]} />
                                </View>
                                

                                <TextInput
                                    placeholder="First Name"
                                    style={style.textInput}
                                    value={this.state.firstName}
                                    onChangeText={(text) => { this.setState({ firstName: text }) }} />

                                <TextInput
                                    placeholder="Last Name"
                                    style={style.textInput}
                                    value={this.state.lastName}
                                    onChangeText={(text) => { this.setState({ lastName: text }) }} />
                                {this.state.isLoading ?
                                    <View style={{ marginTop: 20 }}>
                                        <ActivityIndicator size='large' color='#bc3b3b' />
                                    </View> :
                                    <TouchableOpacity onPress={() => this.cardDetails()}>
                                        <View style={[styles.buttonStyles, { flexDirection: "row", top: 20, bottom: 10 }]}>
                                            <Icon name="add" size={25} type="ionicon" color="white" />
                                            <Text style={styles.buttonText}> Add Card</Text>
                                        </View>
                                    </TouchableOpacity>
                                }
                            </View>

                        </RBSheet>

                    
                        {this.state.card.map((value, id) => {
                            return (
                                <View style={style.pay}>
                                <Pressable onPress={()=>this.card(value.id)}  style={[style.pay, { paddingTop: 12,flexDirection:"row", }]}>
                                <View style={{flexDirection:"row"}}>
                                    <Image source={require("../image/cardpic.jpg")} style={{ width: 30, height: 30, }} />
                                    <Text style={{ top:5,marginLeft:10, fontFamily: "Roboto-Medium", fontSize: RFValue(12, 580) }}>
                                        {value.card_no}
                                    </Text>
                                </View>
                                <Icon name={!this.state.select[value.id]? "radio-button-off":"radio-button-on"} type="ionicon" size={15} style={{ top: 10 }} />
                                
                            </Pressable>
                            {this.state.select[value.id]?
                            <View style={{marginLeft:20,marginTop:-5}}>
                                    <Text style={styles.h4}>
                                        CVV
                                    </Text>
                                    <TextInput 
                                    maxLength={3}
                                    keyboardType="number-pad"
                                    secureTextEntry={true}

                                    style={{borderBottomWidth:1,borderBottomColor:"#696969",width:150,bottom:5,top:-15,paddingBottom:-15,}}
                                    />
                                </View>
                                :
                                null}
                            </View>
                            )
                        })}
                    </View> */}

                    {/* Other Options */}
                    <View style={{ paddingHorizontal: 10, backgroundColor: "#fff", marginTop: 5, paddingBottom: 10 }}>
                        <View style={[style.addressView]}>
                            <Text style={[styles.h3, { color: "#222222", fontSize: RFValue(12, 580) }]}>Options</Text>

                        </View>
                        <Pressable onPress={() => this.card()} style={[style.pay, { paddingVertical: 15, flexDirection: "row" }]}>
                            <Text style={{ fontFamily: "Raleway-Medium", fontSize: RFValue(12, 580) }}>Debit/Credit Card</Text>
                            <Icon name={this.state.cardIcon} type="ionicon" size={15} />
                        </Pressable>
                        <Pressable onPress={() => this.net()} style={[style.pay, { paddingVertical: 15, flexDirection: "row" }]}>
                            <Text style={{ fontFamily: "Raleway-Medium", fontSize: RFValue(12, 580) }}>
                                Net Banking
                            </Text>
                            <Icon name={this.state.iconName1} type="ionicon" size={15} style={{ top: 5 }} />
                        </Pressable>
                        <Pressable onPress={() => this.upi()} style={[style.pay, { paddingVertical: 15, flexDirection: "row" }]}>
                            <Text style={{ fontFamily: "Raleway-Medium", fontSize: RFValue(12, 580) }}>
                                UPI (Gpay, PhonePe)
                            </Text>
                            <Icon name={this.state.iconName2} type="ionicon" size={15} />
                        </Pressable>
                        <Pressable onPress={() => this.cash()} style={[style.pay, { paddingVertical: 15, flexDirection: "row" }]}>
                            <Text style={{ fontFamily: "Raleway-Medium", fontSize: RFValue(12, 580) }}>Cash on Delivery</Text>
                            <Icon name={this.state.iconName} type="ionicon" size={15} />
                        </Pressable>
                    </View>


                    {/* Total amount */}
                    <View style={{ paddingHorizontal: 20, backgroundColor: "#fff", marginTop: 5, paddingBottom: 10 }}>
                        <Text style={[styles.h3, { color: "#222222", fontSize: RFValue(12, 580) }]}>Price Details</Text>
                        <View style={[style.detailsView, { borderBottomWidth: 0, paddingVertical: 10 }]}>
                            <Text style={[styles.h4]}>Sub Total</Text>
                            <Text style={{ fontFamily: "Montserrat-Medium", fontSize: RFValue(11, 580) }}>₹ {this.state.sub_total}</Text>
                        </View>
                        <View style={[style.detailsView, { borderBottomWidth: 0, paddingVertical: 0 }]}>
                            <Text style={[styles.h4]}>Delivery Cost</Text>
                            <Text style={{ fontFamily: "Montserrat-Medium", fontSize: RFValue(11, 580) }}>₹ 0</Text>
                        </View>
                        <View style={[style.detailsView, { paddingBottom: 0 }]}>
                            <Text style={[styles.h4]}>Discount</Text>
                            <Text style={{color:"green", fontFamily: "Montserrat-Medium", fontSize: RFValue(11, 580) }}>- ₹ {parseInt(this.state.discount)}</Text>
                        </View>
                        <View style={[style.detailsView, { borderBottomWidth: 0 }]}>
                            <Text style={[styles.h4]}>Total</Text>
                            <Text style={{ fontFamily: "Montserrat-Medium", fontSize: RFValue(11, 580) }}> ₹ {this.state.grandTotal}/-</Text>
                        </View>
                    </View>

                    {/* <TouchableOpacity onPress={() => this.props.navigation.navigate("PaymentOptions")}>
            <View style={[styles.buttonStyles,{marginTop:20}]}>
              <Text style={styles.buttonText}>Cotinue</Text>
            </View>
          </TouchableOpacity> */}
                    {/* Submit Button */}
                    <View style={{ marginTop: 30 }}>
                        <TouchableOpacity onPress={() =>
                            //  this.props.navigation.navigate("ThankYou")
                            this.place_order()
                        }>
                            <View style={styles.buttonStyles}>
                                <Text style={styles.buttonText}> PLACE ORDER</Text>
                            </View>
                        </TouchableOpacity>


                    </View>

                    {/* <ThankYou navigation={this.props.navigation}
                        price={this.state.subTotal}
                        item={this.state.item}
                        msg={this.state.msg}
                        order_id={this.state.order_id}
                        cash={this.state.iconName}
                    /> */}
                </ScrollView>


                {/* thankyou model */}
            <Modal
             
                visible={this.state.visible}
                // visible={true}
                >
                  <View style={styles.container}>
                    <View style={{alignItems: 'center',marginTop:150}}>
                    <Image
                        source={require('../image/ty.png')}
                        style={{
                        width: 140,
                        height: 180,
                        marginTop: 10,
                        alignSelf: 'center',
                        }}
                    />
                    <Text style={[styles.h3]}>Thank You!</Text>
                    <Text
                        style={[
                        styles.h3,
                        {
                            marginHorizontal: 25,
                            fontSize: RFValue(10, 580),
                            textAlign: 'center',
                        },
                        ]}>
                        Your order is now being processed. We will let you know once the
                        order is picked from the outlet.Check the Order Status for further details.
                    </Text>

                    <TouchableOpacity
                         onPress={() => this.backToHome()}
                        style={[styles.buttonStyles, {top: 40}]}>
                        <Text style={[styles.buttonText, {color: '#fff'}]}>
                        Back to Home
                        </Text>
                    </TouchableOpacity>
                    </View>
                </View>
            </Modal>
            </View>
        )
    }
}

export default Checkout

class Address extends Component {
    constructor(props) {
        super(props);
        console.log(this.props.data)
        this.state = {

        }
    }

    delete = (id) => {
        Alert.alert(
            'Delete Address',
            'Are you sure you want to delete this address?',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel',
                },
                { text: 'OK', onPress: () => this.deleteAddress(id) },
            ],
            { cancelable: false },
        );
    }

    deleteAddress = (id) => {
        fetch(global.api + 'delete-address', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: global.token,
            },
            body: JSON.stringify({
                address_id: id,
            }),
        })
            .then(response => response.json())
            .then(json => {
                // console.warn(json);
                if (!json.status) {
                    var msg = json.msg;
                    Toast.show("Something went wrong.");
                } else {
                    Toast.show(json.msg),
                        this.RBSheet.close();
                    this.props.my_cart()
                }
                return json;
            })
            .catch(error => {
                console.error(error);
            })
            .finally(() => {
            });
    }

    render() {
        let address = this.props.data.map((address, id) => {
            global.address = address.name + " " + address.house_no + " " + address.landmark + " " + address.city + " " + address.state + " " + address.zip
            return (
                <View style={{ width: "100%", }}>

                    <View style={{ justifyContent: "space-evenly", marginBottom: 10, flexDirection: "row" }}>

                        <Pressable style={{ borderBottomWidth: 1, borderColor: "#d3d3d3", width: "70%", marginHorizontal: -20, paddingBottom: 10 }} onPress={() => {
                            this.props.chooseAddress(address.id),
                                this.RBSheet.close()

                        }}>
                            <View>
                                <Text style={[styles.h3, { fontSize: RFValue(11, 580), top: 4 }]}>
                                    {address.name}
                                </Text>
                                <Text style={[styles.p, { fontSize: RFValue(10, 580) }]}>
                                    {address.house_no}, {address.address} {address.landmark} {'\n'}{address.city} {address.state}  {address.zip}
                                </Text>
                            </View>
                        </Pressable>
                        <View style={{ flexDirection: "row", justifyContent: "space-between", top: 10 }}>
                            <Pressable style={{ top: 10, left: -15 }} onPress={() => this.props.navigation.navigate("EditAddress", { id: address.id })}>
                                <Icon name="create-outline" type="ionicon" size={20} />
                                {/* <Text style={styles.h5}>
                  Edit
                </Text> */}
                            </Pressable>
                            <Pressable style={{ top: 10, left: 5 }} onPress={() => this.delete(address.id)}>
                                <Icon name="trash-outline" type="ionicon" color="#bc3b3b" size={20} />
                                {/* <Text style={styles.h5}>
                  Delete
                </Text> */}
                            </Pressable>
                        </View>
                    </View>
                </View>
            )
        })
        return (
            <View style={{ flexDirection: "row", backgroundColor: "#fff", paddingBottom: 10, justifyContent: "space-between", paddingHorizontal: 5, marginBottom: 5 }}>
                <View >

                    <View style={{ flexDirection: "row", paddingLeft: 10 }}>
                        {this.props.data != "" ? (
                            <Text style={[styles.p, { fontSize: 13 }]}>
                                Deliver to :
                            </Text>
                        )
                            :
                            null
                        }

                        <Text style={[styles.h3, { fontSize: RFValue(10, 580), left: 5, top: 4 }]}>
                            {this.props.address.name}
                        </Text>
                    </View>
                    {this.props.data != "" ? (

                        <View style={{ paddingLeft: 10 }}>
                            <Text style={[styles.p, { fontSize: RFValue(10, 580) }]}>
                                {this.props.address.house_no},{this.props.address.landmark} {'\n'}{this.props.address.city} {this.props.address.state}, {this.props.address.zip}
                            </Text>
                        </View>
                    )
                        :
                        (
                            <View>
                                <Text style={[styles.h5, { left: 10, top: -5 }]}>
                                    Add your delivery address
                                </Text>
                            </View>
                        )
                    }
                </View>
                {this.props.data != "" ? (
                    <Pressable onPress={() => this.RBSheet.open()} style={{ top: 15, right: 5 }}>
                        <Text style={[styles.h4, { fontSize: RFValue(9, 580), color: "#bc3b3b" }]}>
                            CHANGE
                        </Text>
                    </Pressable>
                )
                    :
                    (
                        <Pressable onPress={() => this.props.navigation.navigate("NewAddress")} style={{ top: 15, right: 5 }}>
                            <Text style={[styles.h4, { fontSize: RFValue(9, 580), color: "#bc3b3b" }]}>
                                Add New
                            </Text>
                        </Pressable>
                    )
                }


                {/* Bottom Sheet for Camera */}
                <RBSheet
                    ref={ref => {
                        this.RBSheet = ref;
                    }}
                    closeOnDragDown={true}
                    closeOnPressMask={true}
                    height={350}
                    customStyles={{
                        container: {
                            borderTopRightRadius: 20,
                            borderTopLeftRadius: 20,
                        },
                        draggableIcon: {
                            backgroundColor: ""
                        }
                    }}
                >
                    {/* bottom sheet elements */}
                    <View>
                        <View style={{ width: "95%", paddingBottom: -19, marginHorizontal: 10, borderBottomWidth: 1, borderColor: "#d3d3d3", flexDirection: "row", justifyContent: "space-between" }}>
                            <Text style={[styles.bottomButton1Text, { alignSelf: "flex-start", fontSize: 14, top: -5, }]}>
                                CHANGE ADDRESS
                            </Text>
                            <Pressable onPress={() => this.props.navigation.navigate("NewAddress")} style={[styles.buttonStyles, { width: 100, height: 30, top: -5, borderRadius: 5, backgroundColor: "#fff", borderWidth: 1 }]} >
                                <Text style={[styles.bottomButton1Text, { fontSize: RFValue(10, 580), color: "#222222" }]}>
                                    ADD NEW
                                </Text>
                            </Pressable>
                        </View>
                        <ScrollView>
                            <View style={{ marginBottom: 70 }}>
                                {address}

                            </View>
                        </ScrollView>
                    </View>
                </RBSheet>

            

            </View>
        )
    }
}


const style = StyleSheet.create({
    heading: {
        fontSize: 20,
        marginLeft: 10,
        color: "black"
    },
    addressView: {
        flexDirection: "row",
        justifyContent: "space-between",
        borderBottomWidth: 1,
        borderColor: "#f5f5f5",
        paddingVertical: 10
    },
    pay: {
        paddingHorizontal: 20,
        paddingVertical: 5,
        justifyContent: "space-between",
        borderRadius: 7, marginBottom: 10,
        backgroundColor: "#f5f5f5"
    },
    detailsView: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: 25,
        borderBottomWidth: 1,
        borderColor: "#f5f5f5",
        paddingVertical: 10
    },
    textInput: {
        backgroundColor: "#F2F2F2",
        width: Dimensions.get('window').width / 1.2,
        borderRadius: 10,
        height: 45,
        alignSelf: "center",
        // borderRadius:20,
        marginTop: 15,
        paddingLeft: 20,
        fontFamily: "Roboto-Regular",
    },
    methodText: {
        fontSize: RFValue(12, 580),
        fontFamily: "Raleway-Medium",
        marginBottom: 7
    },
    text: {
        color: "#bc3b3b",
        fontSize: RFValue(13, 580),
        // top:5,
        fontFamily: "Montserrat-Medium"
    }
}
)
