import React, { Component } from 'react';
import { Dimensions, TextInput } from 'react-native';
import {
    View,ImageBackground,FlatList,
    StyleSheet,Pressable,ScrollView,Linking,
    Image,Text,TouchableOpacity
} from 'react-native';
import {Icon,Header} from "react-native-elements";
import LinearGradient from 'react-native-linear-gradient';
import { RFValue } from 'react-native-responsive-fontsize';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

//Global Style Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class Support extends Component{
    constructor(props){
        super(props);
        this.state={
            isLoading:false
        }
    }

    renderLeftComponent(){
        return(
            <View style={{flexDirection:"row",width:win.width/2,}}>
                <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
                </View>
                <Text style={[styles.headerHeadingText,
                    ]}>Support</Text>
            </View>
        )
    }
    
    render(){
        return(
            <View style={[styles.container,{backgroundColor:"#fafafa"}]}>
                {/* View for header component */}
                <View>
                    <Header
                    // containerStyle={{height:82}}
                    statusBarProps={{ barStyle: 'light-content' }}
                    leftComponent={this.renderLeftComponent()}
                    ViewComponent={LinearGradient} // Don't forget this!
                    linearGradientProps={{
                    colors: ['white', 'white'],
                    start: { x: 0, y: 0.5 },
                    end: { x: 1, y: 0.5 }
                    }}
                />
                </View>

                <ScrollView>

                <SupportView />

                
                {/* Mail View */}

                <Mail />

                {/* FAQ View */}
                <FAQ navigation={this.props.navigation} isLoading={this.state.isLoading}/>



            </ScrollView>
            <Whatsapp />
        </View>

        )
    }
}

// Support Top View

class SupportView extends Component{
    render(){
        return(
            <View style={{backgroundColor:"#fff",flexDirection:"row",borderTopWidth:2,borderColor:"#f5f5f5", padding:10}}>
                    <View style={style.leftView}>
                <Text style={[style.heading]}>Welcome to</Text>
                <Text style={[style.heading]}>
                 Customer Support</Text>
                 <Text style={styles.h5}>
                Please get in touch and we will be happy to help you.
                 </Text>
                 </View>
                 <View style={{width:Dimensions.get("window").width/2}}>
                 <Image source={require("../image/Capture1.png")} 
                 style={{width:100,height:140,alignSelf:"center"}} />
                 </View>
                </View>
        )
    }
}

// FAQ VIew

class FAQ extends Component{
    constructor(props){
        super(props);
        this.state={
            data:[]
        }
    }

    componentDidMount(){
        this.fetch_faq();
    }

    // fetching FAQs
    fetch_faq=()=>{
        this.setState({isLoading:true})
        fetch(global.api+'fetch_faq', 
        {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization':global.token 
              },
            })
            .then((response) => response.json())
            .then((json) => {
                console.warn(json)
                
                if(json.status){
                    this.setState({data:json.data})
                }
                else{
                    // Toast.show(json.msg)
                }             
                return json;
            })
            .catch((error) => console.error(error))
            .finally(() => {
                this.setState({ isLoading: false });
            });
    }

    questionCard=({item})=>(
        <TouchableOpacity onPress={()=>this.props.navigation.navigate("Answers",{item:item})}>
            <View style={style.questView} >   
                <Text style={styles.h4}>
                    {item.faq_title}
                </Text>
                <Icon type="ionicon" name="chevron-forward-outline" />  
            </View>
            </TouchableOpacity >
    );
    render(){
        return(
            
            <View style={{marginTop:10,backgroundColor:"#fff"}}>
            <Text style={[styles.heading,{color:"#bc3b3b"}]}>FAQ</Text>

            {/* Question View */}
            
            {this.props.isLoading ? 
            <View>
                <SkeletonPlaceholder>
                    <View>
                        <View style={{height:35,width:Dimensions.get('window').width/1.05,alignSelf:"center"}}/>
                        <View style={{height:35,width:Dimensions.get('window').width/1.05,alignSelf:"center",marginTop:5}}/>
                        <View style={{height:35,width:Dimensions.get('window').width/1.05,alignSelf:"center",marginTop:5}}/>
                        <View style={{height:35,width:Dimensions.get('window').width/1.05,alignSelf:"center",marginTop:5}}/>
                        <View style={{height:35,width:Dimensions.get('window').width/1.05,alignSelf:"center",marginTop:5}}/>
                        <View style={{height:35,width:Dimensions.get('window').width/1.05,alignSelf:"center",marginTop:5}}/>
                        <View style={{height:35,width:Dimensions.get('window').width/1.05,alignSelf:"center",marginTop:5}}/>
                    </View>
                </SkeletonPlaceholder>
            </View>
            :
            <FlatList
            navigation={this.props.navigation}
            showsVerticalScrollIndicator={false}
            data={this.state.data}
            renderItem={this.questionCard}
            keyExtractor={item=>item.id}
            />}
            </View>
        )
    }
}

// Mail View
class Mail extends Component{
    render(){
        return(
            <View style={{marginTop:10,backgroundColor:"#fff",padding:10}}>
                    <Text style={[styles.heading,{marginTop:0,color:"#bc3b3b", marginLeft:0}]}>
                        Mail us
                    </Text>
                    <Text style={styles.h5} onPress={() => Linking.openURL('mailto:support@kamaljewellers.com') }>
                        support@kamaljewellers.com
                    </Text>
                </View>
        )
    }
}

// Whatsapp help
class Whatsapp extends Component{

    render(){
        return(
            <View style={style.add}>
                <Pressable onPress={()=>{Linking.openURL('whatsapp://send?text=Hello, Is it Kamal Jewellers support?&phone=+918755255052')}}>
                    <Image source={require("../image/whatsapp.png")} style={{alignSelf:"center",width:45,height:45}} />
                   <Text style={styles.h4}>WhatsApp Us</Text>
                </Pressable>
            </View>
        )
    }
}
export default Support;

const style=StyleSheet.create({
    heading:{
        color:"#bc3b3b",
        fontSize:RFValue(13,580),
        fontFamily:"Raleway-SemiBold",
        // fontFamily: "Tangerine-Bold",
        // marginLeft:10
        // marginTop:45,
        // alignSelf:"center"
    },
    header:{
        fontSize:20,
        marginLeft:10,
        color:"black"
    },
    leftView:{
        flexDirection:"column",
        marginLeft:10,
        marginTop:-15,
        alignSelf:"center",
        width:Dimensions.get("window").width/2},

    questView:{padding:10,
        borderBottomWidth:1,
        borderColor:"#d3d3d3",
        flexDirection:"row",
        justifyContent:"space-between"
    },
    add:{
        
        alignItems: 'center',
        justifyContent: 'center',
        width: 110,
        position: 'absolute',
        bottom: 50,
        right: 5,
        height: 50,
        borderRadius: 100,
     
}
    
})