import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  ActivityIndicator,
  Pressable,
  Image,
  Text,
  Dimensions,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {Header, Icon} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import Share from 'react-native-share';
import RBSheet from 'react-native-raw-bottom-sheet';
import Toast from 'react-native-simple-toast';

//Global Style Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class Saved extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoading:true
    };
  }

  renderLeftComponent() {
    return (
      <View style={{flexDirection: 'row', width: win.width / 2}}>
        <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
        </View>
        <Text style={[styles.headerHeadingText]}>Saved</Text>
      </View>
    );
  }

  renderRightComponent() {
    return (
      <View>
        <Icon
          name="cart-outline"
          size={24}
          color="#222222"
          type="ionicon"
          onPress={() => this.props.navigation.navigate('MyCart')}
          // style={{alignSelf:"center"}}
        />
      </View>
    );
  }

  componentDidMount() {
    this.fetch_feeds();
    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.fetch_feeds();
    });
  }

  fetch_feeds = () => {
    fetch(global.api + 'user-save-feed-fetch', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    })
      .then(response => response.json())
      .then(json => {
        console.warn(json);
        if (json.status) {
          json.data.map(value=>{
            this.setState({data: value.feed_content});
          })
          
          console.warn(json.data);
        }
        return json;
      })
      .catch(error => console.error(error))
      .finally(() => {
        this.setState({isLoading: false});
      });
  };

  render() {
    
    return (
      <View style={[styles.container, ]}>
        {/* View for header component */}
        <View>
          <Header
            containerStyle={{height: 82}}
            statusBarProps={{barStyle: 'light-content'}}
            leftComponent={this.renderLeftComponent()}
            // rightComponent={this.renderRightComponent()}
            ViewComponent={LinearGradient} // Don't forget this!
            linearGradientProps={{
              colors: ['white', 'white'],
              start: {x: 0, y: 0.5},
              end: {x: 1, y: 0.5},
            }}
          />
        </View>
        {!this.state.isLoading ? 
        (this.state.data!="" ?
        (
            <View>
                
        <View style={{flexDirection: 'row', marginTop: 10, width: '100%'}}>
          {/* product card */}
          <Pressable
            style={style.card}
            onPress={() => this.props.navigation.navigate('SavedPost')}>
            {this.state.data.map(value => {
              return (
                <View style={{height: 100,
                  borderColor:"#C0C0C0", width: '50%',padding:2,borderWidth:1,}}>
                  <Image
                    source={{
                      uri:
                        'https://demo.webixun.com/KamalJwellersApi/public/' +
                        value.content_src,
                    }}
                    style={style.img}
                  />
                  <Text>
                    {value.content_status}
                  </Text>
                </View>
              );
            
            })}
            
          </Pressable>
        </View>
        <View style={{margin: 10, flexDirection: 'row'}}>
          <Text
            style={[
              styles.p,
              {color: '#222222', marginTop: 20, marginLeft: 50},
            ]}>
            All Posts
          </Text>
        </View>
        
        </View>
        )
        :
        (
        <View style={{alignItems:"center",marginTop:100}}>
          <Image source={require("../image/bookmark.png")} style={{width:120,height:120}} />
          <Text style={[styles.h4,{marginTop:10}]}>
            Nothing saved yet
          </Text>
        </View>
        )
        ):(
          <View
            style={{
              alignItems: 'center',
              flex: 1,
              backgroundColor: 'white',
              paddingTop: 250,
            }}>
            <ActivityIndicator size="large" color="#bc3b3b" />
            <Text style={styles.p}>Please wait...</Text>
          </View>
        )}
          
        
      </View>
    );
  }
}

export default Saved;

const style = StyleSheet.create({
  img: {
    width: '100%',
    height: '100%',
    // margin: 1,
    // borderRadius: 5,
    backgroundColor: '#36454F'
    // paddingHorizontal:1
  },

  card: {
    width: '45%',
    // borderWidth:0.2,
    marginHorizontal: 3,
    height: 200,
    backgroundColor: '#E5E4E2',
    borderRadius:10,
    // flexDirection:"row",
    // padding:5
  },
});
