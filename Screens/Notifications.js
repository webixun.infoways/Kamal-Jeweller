import React, {Component} from 'react';
import {TouchableOpacity} from 'react-native';
import {
  View,
  StyleSheet,
  Pressable,FlatList,
  Image,
  Text,
  Dimensions,
  ScrollView,
} from 'react-native';
import {Header, Icon} from 'react-native-elements';
import RBSheet from 'react-native-raw-bottom-sheet';
import Toast from 'react-native-simple-toast';
import LinearGradient from 'react-native-linear-gradient';
import { RFValue } from 'react-native-responsive-fontsize';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

//Global Style Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class Notifications extends Component {
    constructor(props){
        super(props);
        this.state={
            data:[],
            isLoading:false
        }
    }

    componentDidMount(){
        this.fetch_notifications();
    }

    // fetching notifications
    fetch_notifications=()=>{
      this.setState({isLoading:true})
        fetch(global.api+"fetch_user_notification", { 
            method: 'POST',
              headers: {    
                  Accept: 'application/json',  
                    'Content-Type': 'application/json',
                    'Authorization':global.token  
                   }, 
                    body: JSON.stringify({   
                            })}).then((response) => response.json())
                            .then((json) => {
                                console.warn(json)
                                if(!json.status)
                                {
                                    var msg=json.msg;
                                    Toast.show(msg);
                                }
                                else{
                                  console.warn(json.data.data.length)
                                this.setState({data:json.data.data})
                                }
                               return json;    
                           }).catch((error) => {  
                                   console.error(error);   
                                }).finally(() => {
                                   this.setState({isLoading:false})
                                });
    }
    
    renderLeftComponent() {
      return (
        <View style={{flexDirection: 'row', width: win.width / 2}}>
          <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
          </View>
          <Text style={[styles.headerHeadingText]}>Notifications</Text>
        </View>
      );
    }

  notificationCard=({item})=>(
    <View style={{flexDirection: 'row', width: '100%', padding: 10}}>
            {/* For profile Image/ user image */}
            <View style={{width: '15%'}}>
              <Image
                source={require('../image/logo/kj.png')}
                style={style.Image}
              />
            </View>

            <View
              style={{
                justifyContent: 'space-between',
                flexDirection: 'row',
                width: '85%',
              }}>
              {/* View for Notification Text content and time */}
              <TouchableOpacity>
                <View
                  style={{
                    flexDirection: 'column',
                    marginLeft: 15,
                    marginTop: 10,
                  }}>
                  <Text
                    style={[
                      styles.h4,
                      {fontFamily: 'Montserrat-SemiBold', fontSize: 13},
                    ]}>
                    Kamal jewellers
                    <Text
                      style={(styles.h3, {fontFamily: 'Montserrat-Regular'})}>
                      {' '}
                      added a new photo.
                    </Text>
                  </Text>
                  <Text style={[styles.h6, {color: 'grey'}]}>50 mins ago</Text>
                </View>
              </TouchableOpacity>
              {/* View for ellipsis icon */}
              {/* <View >
    <Text style={{marginTop:15,marginRight:10}}
    onPress={() => this.RBSheet.open()}>
        <Icon type="ionicon" name="ellipsis-horizontal" size={20}/>
    </Text>
</View> */}
            </View>

            {/* Bottom Sheet for notification options */}

            <RBSheet
              ref={ref => {
                this.RBSheet = ref;
              }}
              closeOnDragDown={true}
              closeOnPressMask={true}
              height={300}
              customStyles={{
                container: {
                  borderTopRightRadius: 20,
                  borderTopLeftRadius: 20,
                },
                draggableIcon: {
                  backgroundColor: 'transparent',
                },
              }}
              animationType="slide">
              {/* bottom sheet elements */}
              <View>
                {/* new container search view */}
                <View>
                  <Image
                    source={require('../image/logo/kj.png')}
                    style={[style.Image, {alignSelf: 'center'}]}
                  />
                  <Text
                    style={[
                      styles.small,
                      {
                        alignSelf: 'center',
                        fontFamily: 'Montserrat-SemiBold',
                        fontSize: 14,
                      },
                    ]}>
                    Kamal jewllers added a new photo.
                  </Text>

                  <View style={{flexDirection: 'row', padding: 10}}>
                    <View
                      style={{
                        backgroundColor: '#f5f5f5',
                        height: 40,
                        width: 40,
                        justifyContent: 'center',
                        borderRadius: 50,
                      }}>
                      <Icon type="ionicon" name="trash-bin" />
                    </View>
                    <Text
                      style={[
                        styles.h4,
                        {alignSelf: 'center', marginLeft: 10},
                      ]}>
                      Remove this notification
                    </Text>
                  </View>

                  <View style={{flexDirection: 'row', padding: 10}}>
                    <TouchableOpacity
                      style={{flexDirection: 'row'}}
                      onPress={() => Toast.show('Turned off notifications!')}>
                      <View
                        style={{
                          backgroundColor: '#f5f5f5',
                          height: 40,
                          width: 40,
                          justifyContent: 'center',
                          borderRadius: 50,
                        }}>
                        <Icon type="ionicon" name="notifications-off" />
                      </View>
                      <Text
                        style={[
                          styles.h4,
                          {alignSelf: 'center', marginLeft: 10},
                        ]}>
                        Turn off notifications{' '}
                      </Text>
                    </TouchableOpacity>
                  </View>

                  <View style={{flexDirection: 'row', padding: 10}}>
                    <TouchableOpacity
                      style={{flexDirection: 'row'}}
                      onPress={() => Toast.show('Reported Successfully!')}>
                      <View
                        style={{
                          backgroundColor: '#f5f5f5',
                          height: 40,
                          width: 40,
                          justifyContent: 'center',
                          borderRadius: 50,
                        }}>
                        <Icon type="ionicon" name="warning" />
                      </View>
                      <Text
                        style={[
                          styles.h4,
                          {alignSelf: 'center', marginLeft: 10},
                        ]}>
                        Report issue to notifications team
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </RBSheet>
          </View>
  );

  render() {
    return (
      <View style={styles.container}>
        {/* View for header component */}
        <View>
          <Header
            // containerStyle={{height: 82}}
            statusBarProps={{barStyle: 'light-content'}}
            leftComponent={this.renderLeftComponent()}
            ViewComponent={LinearGradient} // Don't forget this!
            linearGradientProps={{
              colors: ['white', 'white'],
              start: {x: 0, y: 0.5},
              end: {x: 1, y: 0.5},
            }}
          />
        </View>

        {this.state.isLoading ?
        <View>
          <Loaders />
        </View>
        :
        <View>
          {this.state.data.length >0 ?
        
          <FlatList
              navigation={this.props.navigation}
              showsVerticalScrollIndicator={false}
              data={this.state.data}
              renderItem={this.notificationCard}
              keyExtractor={item => item.id} 
            />
            
        :  
            <View style={{alignItems:"center",paddingTop:150}}>
              <Image
                  source={require('../image/notification.png')}
                  style={{width: 100, height: 100}}
                />
            <Text style={[styles.h3,{marginTop:20,fontSize:RFValue(12,580)}]}>
                No Notifications Found!
            </Text>
        </View>
        }
        </View>
        }
      </View>
    );
  }
}

export default Notifications;

class Loaders extends Component{
  render(){
    return(
      <View>
        <SkeletonPlaceholder>
            <View style={{flexDirection:"row"}}>
              <View style={{height:45,width:45,borderRadius:50,marginLeft:15,marginTop:13}}/> 
              <View style={{padding:10,marginTop:5}}>
                  <View style={{height:20,width:300}}/>
                  <View style={{height:15,width:150,marginTop:5}}/>
              </View>
            </View>
            
            <View style={{flexDirection:"row"}}>
              <View style={{height:45,width:45,borderRadius:50,marginLeft:15,marginTop:13}}/> 
              <View style={{padding:10,marginTop:5}}>
                  <View style={{height:20,width:300}}/>
                  <View style={{height:15,width:150,marginTop:5}}/>
              </View>
            </View>

            <View style={{flexDirection:"row"}}>
              <View style={{height:45,width:45,borderRadius:50,marginLeft:15,marginTop:13}}/> 
              <View style={{padding:10,marginTop:5}}>
                  <View style={{height:20,width:300}}/>
                  <View style={{height:15,width:150,marginTop:5}}/>
              </View>
            </View>

            <View style={{flexDirection:"row"}}>
              <View style={{height:45,width:45,borderRadius:50,marginLeft:15,marginTop:13}}/> 
              <View style={{padding:10,marginTop:5}}>
                  <View style={{height:20,width:300}}/>
                  <View style={{height:15,width:150,marginTop:5}}/>
              </View>
            </View>

            <View style={{flexDirection:"row"}}>
              <View style={{height:45,width:45,borderRadius:50,marginLeft:15,marginTop:13}}/> 
              <View style={{padding:10,marginTop:5}}>
                  <View style={{height:20,width:300}}/>
                  <View style={{height:15,width:150,marginTop:5}}/>
              </View>
            </View>

            <View style={{flexDirection:"row"}}>
              <View style={{height:45,width:45,borderRadius:50,marginLeft:15,marginTop:13}}/> 
              <View style={{padding:10,marginTop:5}}>
                  <View style={{height:20,width:300}}/>
                  <View style={{height:15,width:150,marginTop:5}}/>
              </View>
            </View>

            <View style={{flexDirection:"row"}}>
              <View style={{height:45,width:45,borderRadius:50,marginLeft:15,marginTop:13}}/> 
              <View style={{padding:10,marginTop:5}}>
                  <View style={{height:20,width:300}}/>
                  <View style={{height:15,width:150,marginTop:5}}/>
              </View>
            </View>

            <View style={{flexDirection:"row"}}>
              <View style={{height:45,width:45,borderRadius:50,marginLeft:15,marginTop:13}}/> 
              <View style={{padding:10,marginTop:5}}>
                  <View style={{height:20,width:300}}/>
                  <View style={{height:15,width:150,marginTop:5}}/>
              </View>
            </View>

            <View style={{flexDirection:"row"}}>
              <View style={{height:45,width:45,borderRadius:50,marginLeft:15,marginTop:13}}/> 
              <View style={{padding:10,marginTop:5}}>
                  <View style={{height:20,width:300}}/>
                  <View style={{height:15,width:150,marginTop:5}}/>
              </View>
            </View>

            <View style={{flexDirection:"row"}}>
              <View style={{height:45,width:45,borderRadius:50,marginLeft:15,marginTop:13}}/> 
              <View style={{padding:10,marginTop:5}}>
                  <View style={{height:20,width:300}}/>
                  <View style={{height:15,width:150,marginTop:5}}/>
              </View>
            </View>

        </SkeletonPlaceholder>
      </View>
    )
  }
}
class Notify extends Component {
  render() {
    return (
      <View>
        <View style={{flexDirection: 'row', width: '100%', padding: 10}}>
          {/* For profile Image/ user image */}
          <View style={{width: '15%'}}>
            <Image
              source={require('../image/logo/kj.png')}
              style={style.Image}
            />
          </View>

          <View
            style={{
              justifyContent: 'space-between',
              flexDirection: 'row',
              width: '85%',
            }}>
            {/* View for Notification Text content and time */}
            <TouchableOpacity>
              <View
                style={{
                  flexDirection: 'column',
                  marginLeft: 15,
                  marginTop: 10,
                }}>
                <Text
                  style={[
                    styles.h4,
                    {fontFamily: 'Montserrat-SemiBold', fontSize: 13},
                  ]}>
                  Kamal jewellers
                  <Text style={(styles.h3, {fontFamily: 'Montserrat-Regular'})}>
                    {' '}
                    added a new photo.
                  </Text>
                </Text>
                <Text style={[styles.h6, {color: 'grey'}]}>50 mins ago</Text>
              </View>
            </TouchableOpacity>
            {/* View for ellipsis icon */}
            {/* <View >
                        <Text style={{marginTop:15,marginRight:10}}
                        onPress={() => this.RBSheet.open()}>
                            <Icon type="ionicon" name="ellipsis-horizontal" size={20}/>
                        </Text>
                    </View> */}
          </View>

          {/* Bottom Sheet for notification options */}

          <RBSheet
            ref={ref => {
              this.RBSheet = ref;
            }}
            closeOnDragDown={true}
            closeOnPressMask={true}
            height={300}
            customStyles={{
              container: {
                borderTopRightRadius: 20,
                borderTopLeftRadius: 20,
              },
              draggableIcon: {
                backgroundColor: 'transparent',
              },
            }}
            animationType="slide">
            {/* bottom sheet elements */}
            <View>
              {/* new container search view */}
              <View>
                <Image
                  source={require('../image/logo/kj.png')}
                  style={[style.Image, {alignSelf: 'center'}]}
                />
                <Text
                  style={[
                    styles.small,
                    {
                      alignSelf: 'center',
                      fontFamily: 'Montserrat-SemiBold',
                      fontSize: 14,
                    },
                  ]}>
                  Kamal jewllers added a new photo.
                </Text>

                <View style={{flexDirection: 'row', padding: 10}}>
                  <View
                    style={{
                      backgroundColor: '#f5f5f5',
                      height: 40,
                      width: 40,
                      justifyContent: 'center',
                      borderRadius: 50,
                    }}>
                    <Icon type="ionicon" name="trash-bin" />
                  </View>
                  <Text
                    style={[styles.h4, {alignSelf: 'center', marginLeft: 10}]}>
                    Remove this notification
                  </Text>
                </View>

                <View style={{flexDirection: 'row', padding: 10}}>
                  <TouchableOpacity
                    style={{flexDirection: 'row'}}
                    onPress={() => Toast.show('Turned off notifications!')}>
                    <View
                      style={{
                        backgroundColor: '#f5f5f5',
                        height: 40,
                        width: 40,
                        justifyContent: 'center',
                        borderRadius: 50,
                      }}>
                      <Icon type="ionicon" name="notifications-off" />
                    </View>
                    <Text
                      style={[
                        styles.h4,
                        {alignSelf: 'center', marginLeft: 10},
                      ]}>
                      Turn off notifications{' '}
                    </Text>
                  </TouchableOpacity>
                </View>

                <View style={{flexDirection: 'row', padding: 10}}>
                  <TouchableOpacity
                    style={{flexDirection: 'row'}}
                    onPress={() => Toast.show('Reported Successfully!')}>
                    <View
                      style={{
                        backgroundColor: '#f5f5f5',
                        height: 40,
                        width: 40,
                        justifyContent: 'center',
                        borderRadius: 50,
                      }}>
                      <Icon type="ionicon" name="warning" />
                    </View>
                    <Text
                      style={[
                        styles.h4,
                        {alignSelf: 'center', marginLeft: 10},
                      ]}>
                      Report issue to notifications team
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </RBSheet>
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
  header: {
    fontSize: 20,
    marginLeft: 10,
    color: 'black',
  },

  Image: {
    height: 50,
    width: 45,
    borderRadius: 100,
    borderColor: 'grey',
    borderWidth: 0.2,
  },
});
