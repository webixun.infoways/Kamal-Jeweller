import React, { Component } from 'react';
import {
    Text,View,ScrollView,
    StyleSheet,Image,Dimensions,
    TouchableOpacity,TextInput,Pressable, ImageBackground, FlatList, ActivityIndicator
} from 'react-native';
import { Header,Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import { Searchbar } from 'react-native-paper';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import Toast from 'react-native-simple-toast';
import navigationChange from '../helpers/index.js';
//Global StyleSheet Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class Collection  extends Component{
    constructor(props){
        super(props);
    }

    renderLeftComponent(){
        return(
            <View style={{flexDirection:"row",width:win.width}}>
                <Text style={styles.headerHeadingText}>Collection</Text>
            </View>
        )
    }

    componentDidMount(){
        this.focusListener = this.props.navigation.addListener('focus', () => {
            if (navigationChange.isChanged) {
              navigationChange.isChanged = false;
              this.setState({ loader: true })
              this.props.navigation.navigate('Home');
            }})
    }

    renderRightComponent() {
        return (
          <View>
            <Icon
              name="bookmark-outline"
              size={24}
              color="#fff"
              type="ionicon"
            //   onPress={() => this.props.navigation.navigate('SavedPost')}
              // style={{alignSelf:"center"}}
            />
          </View>
        );
      }
    render(){
        return(
            <View  style={styles.container}>
                
                    {/* View for header component */}
                    <View >
                        <Header 
                        // containerStyle={{height:82}}
                        statusBarProps={{ barStyle: 'light-content' }}
                        leftComponent={this.renderLeftComponent()}
                        rightComponent={this.renderRightComponent()}
                        // data={this.props.route.params}
                        ViewComponent={LinearGradient} // Don't forget this!
                        linearGradientProps={{
                        colors: ['white', 'white'],
                        start: { x: 0, y: 0.5 },
                        end: { x: 1, y: 0.5 }
                        }} />
                    </View>
                    <ScrollView showsVerticalScrollIndicator={false}>

                        <Card navigation={this.props.navigation} />
                        

                </ScrollView>
            </View>
        )
    }
}
export default Collection

class Card extends Component{
    constructor(props){
        super(props);
        this.state={
            data:[],
            isLoading:true,
        }
    }

    componentDidMount(){
        this.get_category()
        this.focusListener=this.props.navigation.addListener('focus', ()=>{
            this.get_category();  
        }
        )
    }

    // Fetching user details
    get_category=()=>{

        fetch(global.api+'get-jwellery-category', 
        {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization':global.token 
              },
            })
            .then((response) => response.json())
            .then((json) => {
                
                if(json.status){
                    this.setState({data:json.data})
                    console.warn(this.state.data)
                
                }
                else{
                    Toast.show("No data found")
                }             
                return json;
            })
            .catch((error) => console.error(error))
            .finally(() => {
                this.setState({ isLoading: false });
            });
    }

    productCard=({item})=>(
        
        <View>
        <View style={{marginBottom:3,backgroundColor:"yellow"}}>
                <Pressable onPress={()=>{this.props.navigation.navigate("ProductList",{id:item.id,name:item.name})}}>
                <ImageBackground imageStyle={{opacity:0.95}} source={require("../image/background/golden.jpg")}
                style={style.backgroundImg}>
                    <Text style={style.mainText}>
                        {item.name}
                    </Text>
                    <Text style={[styles.h3,{top:14,fontSize:15, left:42}]}>
                                {item.product.length} items
                    </Text>
                    <View style={{alignSelf:"flex-end"}}>
                    <Image source={require('../image/goldenEarring.png')} 
                            style={style.image}/>
                            </View>
                </ImageBackground>
                </Pressable>
            </View>
        </View>
    );

    render(){
        
        return(
            <View>
                {/* {data} */}
                {this.state.isLoading ? 
                <View>
                    <Loaders />
                </View>
                    :
                    
                <FlatList
                navigation={this.props.navigation}
                showsVerticalScrollIndicator={false}
                data={this.state.data}
                renderItem={this.productCard}
                keyExtractor={item=>item.id}  />
                }

            {/* <View style={{marginBottom:3,backgroundColor:"yellow"}}>
                <Pressable onPress={()=>{this.props.navigation.navigate("ProductList")}}>
                <ImageBackground imageStyle={{opacity:0.95}} source={require("../image/background/golden.jpg")}
                style={style.backgroundImg}>
                    <Text style={style.mainText}>
                        Ear Rings
                    </Text>
                    <Text style={[styles.h3,{top:14,fontSize:15, left:42}]}>
                                120 items
                    </Text>
                    <View style={{alignSelf:"flex-end"}}>
                    <Image source={require('../image/goldenEarring.png')} 
                            style={style.image}/>
                            </View>
                </ImageBackground>
                </Pressable>
            </View> */}

            {/* <View style={{marginBottom:3,backgroundColor:"red"}}>
            <Pressable onPress={()=>{this.props.navigation.navigate("ProductList")}}>
                <ImageBackground imageStyle={{opacity:0.95}} source={require("../image/background/pattern.jpg")}
                style={style.backgroundImg}>
                    <Text style={style.mainText}>
                        Bangles
                    </Text>
                    <Text style={[styles.h3,{top:14,fontSize:15,left:42}]}>
                                120 items
                    </Text>
                    <View style={{alignSelf:"flex-end"}}>
                    <Image source={require('../image/bangle.png')} 
                            style={[style.image,{height:80,width:90,right:30}]}/>
                            </View>
                </ImageBackground>
                </Pressable>
            </View> */}
            {/* <View style={{marginBottom:3,backgroundColor:"yellow"}}>
            <Pressable onPress={()=>{this.props.navigation.navigate("ProductList")}}>
                <ImageBackground imageStyle={{opacity:0.85}} source={require("../image/background/whitepattern.jpg")}
                style={style.backgroundImg}>
                    <Text style={style.mainText}>
                       Necklace Set
                    </Text>
                    <Text style={[styles.h3,{top:14,fontSize:15, left:42}]}>
                                120 items
                    </Text>
                    <View style={{alignSelf:"flex-end"}}>
                    <Image source={require('../image/jewellery.png')} 
                           style={[style.image,{height:90,width:90,right:30}]}/>
                            </View>
                </ImageBackground>
                </Pressable>
            </View>
            
            <View style={{marginBottom:3,backgroundColor:"orange"}}>
            <Pressable onPress={()=>{this.props.navigation.navigate("ProductList")}}>
                <ImageBackground imageStyle={{opacity:0.85}} source={require("../image/background/green.jpg")}
                style={style.backgroundImg}>
                    <Text style={style.mainText}>
                       Rings
                    </Text>
                    <Text style={[styles.h3,{top:14,fontSize:15, left:42}]}>
                                120 items
                    </Text>
                    <View style={{alignSelf:"flex-end"}}>
                    <Image source={require('../image/diamondRing.png')} 
                           style={[style.image,{height:95,width:100,right:30}]}/>
                            </View>
                </ImageBackground>
                </Pressable>
            </View>
            <View style={{marginBottom:3,backgroundColor:"blue"}}>
            <Pressable onPress={()=>{this.props.navigation.navigate("ProductList")}}>
                <ImageBackground imageStyle={{opacity:0.95}} source={require("../image/background/bluebg.jpg")}
                style={style.backgroundImg}>
                    <Text style={style.mainText}>
                        Bracelets
                    </Text>
                    <Text style={[styles.h3,{top:14,fontSize:15, left:42}]}>
                                120 items
                    </Text>
                    <View style={{alignSelf:"flex-end"}}>
                    <Image source={require('../image/bracelet.png')} 
                            style={[style.image,{height:90,width:95,right:30}]}/>
                            </View>
                </ImageBackground>
                </Pressable>
            </View> */}
            </View>
        )
    }
}

class Loaders extends Component{
    render(){
        return(
            <View>
                <SkeletonPlaceholder>
                    <View>
                        <View style={{height:120,width:Dimensions.get('window').width}}/>
                        <View style={{height:120,width:Dimensions.get('window').width,marginTop:5}}/>
                        <View style={{height:120,width:Dimensions.get('window').width,marginTop:5}}/>
                        <View style={{height:120,width:Dimensions.get('window').width,marginTop:5}}/>
                        <View style={{height:120,width:Dimensions.get('window').width,marginTop:5}}/>
                        <View style={{height:120,width:Dimensions.get('window').width,marginTop:5}}/>
                    </View>
                </SkeletonPlaceholder>
            </View>
        )
    }
}

const style = StyleSheet.create(
    {
        backgroundImg:{
            width:"100%",
            height:110,
            // tintColor:"#000",
            // opacity:0.5
        },
        mainText:{
            left:40,fontFamily:"DancingScript-Bold",
            color:"#bc3b3b", top:20,fontSize:31
        },
        image:{
            top:-55,
            right:20,
            width:120,
            height:80,
        },
        text:{

        }
    }
)