import React, { Component } from 'react'
import { View, Image, Text, StyleSheet, Dimensions, FlatList, ScrollView, TouchableOpacity, TextInput } from "react-native"
import { Icon, Header } from "react-native-elements"
import RBSheet from 'react-native-raw-bottom-sheet'
import { RFValue } from 'react-native-responsive-fontsize'
import LinearGradient from 'react-native-linear-gradient';
import Toast from 'react-native-simple-toast'
import { ActivityIndicator } from 'react-native-paper'
import SkeletonPlaceholder from 'react-native-skeleton-placeholder'

//Global Style Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class Wishlist extends Component {

    constructor(props) {
        super(props);
    }

    renderLeftComponent() {
        return (
            <View style={{ flexDirection: "row", width: win.width / 2, }}>
                <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
                </View>
                <Text style={[styles.headerHeadingText,
                ]}>Wishlist</Text>
            </View>
        )
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "white" }}>
                {/* View for header component */}
                <View>
                    <Header
                        // containerStyle={{height:82}}
                        statusBarProps={{ barStyle: 'light-content' }}
                        leftComponent={this.renderLeftComponent()}
                        ViewComponent={LinearGradient} // Don't forget this!
                        linearGradientProps={{
                            colors: ['white', 'white'],
                            start: { x: 0, y: 0.5 },
                            end: { x: 1, y: 0.5 }
                        }}
                    />
                </View>
                <ScrollView showsVerticalScrollIndicator={false}>

                    <Card navigation={this.props.navigation} />

                </ScrollView>

            </View>
        )
    }
}

export default Wishlist

class Card extends Component {
    constructor(props) {
        super(props);
        this.state = {
            like: {},
            data: [],
            add: "Add",
            object: {},
            isLoading: true

        }
    }

    // Remove a product from cart
    remove = (id) => {
        const like = this.state.like;
        if (like[id] == true) {
            like[id] = false;
            var type = "yes"

        }
        else {
            like[id] = true;
            var type = "no"
        }
        this.setState({ like })
        fetch(global.api + "add-to-wishlist", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': global.token
            },
            body: JSON.stringify({
                type: type,
                product_id: id,
            })
        }).then((response) => response.json())
            .then((json) => {
                // console.warn(json)
                if (!json.status) {
                    var msg = json.msg;
                    Toast.show(msg);
                }
                else {
                    Toast.show(json.msg),
                        this.get_wishlist()
                }
                return json;
            }).catch((error) => {
                console.error(error);
            }).finally(() => {
                this.setState({ isLoading: false })
            });

    }
    componentDidMount() {
       this.get_wishlist()
    }

    get_wishlist = () => {
        fetch(global.api + "get-wishlist", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': global.token
            },
            body: JSON.stringify({
            })
        }).then((response) => response.json())
            .then((json) => {
               
                if (json.status) {
                    if(json.data.length>0){
                        this.setState({ data: json.data})
                    }
                    // console.warn(this.state.data)
                }
                else {
                    this.setState({ data: "" })
                }
                return json;
            }).catch((error) => {
                console.error(error);
            }).finally(() => {
                this.setState({ isLoading: false })
            });
    }

    // add to cart function
    add_to_cart = (id) => {
        this.remove(id);
        const object = this.state.object;
        if (object[id] == true) {
            object[id] = false;
            var type = "no"
        }
        else {
            object[id] = true;
            var type = "yes"
        }
        this.setState({ object })
        fetch(global.api + "add-to-cart", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': global.token
            },
            body: JSON.stringify({
                type: type,
                product_id: id,
                product_quantity: 1,
                size: 5

            })
        }).then((response) => response.json())
            .then((json) => {
                // console.warn(json)
                if (!json.status) {
                    var msg = json.msg;
                    Toast.show(msg);
                }
                else {
                    //    this.RBSheet.close();
                    Toast.show(json.msg)
                }
                return json;
            }).catch((error) => {
                console.error(error);
            }).finally(() => {
                this.setState({ isLoading: false })
            });

    }

    // flatlist item card
    productCard = ({ item }) => (
        <View>
            {item.product.map((value) =>{
            return(
        <View style={style.card} >


            {/* View for Image */}
            <View style={{ width: "24%" }}>
                    <Image
                    source={{ uri: global.img_url + value.picture[0].src }}
                    // source={require('../image/1.jpg')}
                    style={style.productImg} />
            </View>
                
            {/* View for Content */}

            <View style={style.contentView}>
                {/* View for name and heart */}
                <View style={{ flexDirection: "row", justifyContent: "space-between", }}>
                    {/* Text View */}
                    <View style={{ width: 185, }}>

                        <Text style={[styles.h3, { top: 5, fontSize: RFValue(10, 580), }]}>
                            {value.name}</Text>
                        <View style={{ flexDirection: "row", top: 7 }}>
                            <Text style={{ marginTop: 3 }}>
                                <Icon name="star" color="#FFBC02" size={15} />
                            </Text>
                            {value.rating==null ? <Text style={[styles.h5, { fontSize: RFValue(9, 580), left: 7,top:1, color: "#222222" }]}>N/A</Text> : 
                            <Text style={[styles.h5, { fontSize: RFValue(9, 580), left: 7, color: "#222222" }]}>{value.rating.ratings}</Text>
                            }
                        </View>
                    </View>
                    {/* View for heart icon  */}
                    <View>
                        <TouchableOpacity onPress={() => this.remove(value.id)} style={style.iconView}>
                            <Text style={{ alignSelf: "center", textAlign: "center" }}>
                                <Icon name="heart" color={this.state.like[value.id] ? "#d3d3d3" : "red"}
                                    style={{ justifyContent: "center", }} size={20} type="ionicon" />
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                {/* View for Price and offer */}
                <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 10 }}>
                    <View style={{ flexDirection: "row" }}>
                        <Text style={[styles.p, { fontFamily: "Montserrat-SemiBold", }]}>
                            ₹ {value.price}/-
                        </Text>
                    </View>

                    <View>
                        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
                            <TouchableOpacity style={[style.viewDetailsButton, { left: 15, flexDirection: "row", backgroundColor: "#bc3b3b", }]}
                                onPress={() => this.props.navigation.navigate("Products", { id: value.id })}
                            >
                               <Text style={[style.textButton, { color: "#fff", left: 3 }]}>
                                    View
                                </Text>
                            </TouchableOpacity>

                            {/* <TouchableOpacity style={[style.viewDetailsButton, {
                                marginRight: -20, backgroundColor: "#bc3b3b",
                                width: 85
                            }]}
                                onPress={() => this.add_to_cart(item.id)}
                            >
                                <Text style={[style.textButton, { color: "#fff", left: 3 }]}>
                                    Move to cart
                                </Text>
                            </TouchableOpacity> */}
                        </View>
                    </View>
                </View>
            </View>
        </View>
        )
            })}
        </View>
    );

    render() {
        return (
            <View>
                {this.state.isLoading ?
                    <View>
                        <Loaders />
                    </View>
                    :
                    (this.state.data.length >0) ?
                        <FlatList
                        navigation={this.props.navigation}
                        showsVerticalScrollIndicator={false}
                        data={this.state.data}
                        renderItem={this.productCard}
                        keyExtractor={item => item.id}
                        />
                        :
                        <View style={{ alignItems: "center", paddingTop: 150 }}>
                            <Image
                                source={require('../image/nowishlist.png')}
                                style={{ width: 160, height: 130 }}
                            />
                            <Text style={[styles.h3, { fontSize: RFValue(12, 580) }]}>
                                No Products in wishlist
                            </Text>
                        </View>
                }
            </View>
        )
    }
}



class Loaders extends Component{
    render(){
        return(
            <View>
                <SkeletonPlaceholder>
                    <View style={{ flexDirection: "row", marginTop: 10}}>
                        <View style={{ marginLeft: 5 }}>
                            <View style={{ width: win.width / 4.5, height: 90, borderRadius: 10 }} />
                        </View>
                        <View>
                            <View style={{ flexDirection: "row", }}>
                                <View>
                                    <View style={{ width: 220, height: 15, marginLeft: 10,marginTop:5 }} />
                                    <View style={{ width: 120, height: 15, marginLeft: 10, top: 5 }} />

                                    <View style={{ width: 50, height: 15, marginLeft: 10, top:10 }} />

                                    <View style={{ width: 80, height: 20, marginLeft: 10, top:15 }} />

                                </View>

                                <View style={{ height: 25, width: 25, right: 10,left:30,marginTop:2,borderRadius:10}}></View>

                                
                            </View>
                            
                        </View>
                    </View>

                    <View style={{ flexDirection: "row", marginTop: 10}}>
                        <View style={{ marginLeft: 5 }}>
                            <View style={{ width: win.width / 4.5, height: 90, borderRadius: 10 }} />
                        </View>
                        <View>
                            <View style={{ flexDirection: "row", }}>
                                <View>
                                    <View style={{ width: 220, height: 15, marginLeft: 10,marginTop:5 }} />
                                    <View style={{ width: 120, height: 15, marginLeft: 10, top: 5 }} />

                                    <View style={{ width: 50, height: 15, marginLeft: 10, top:10 }} />

                                    <View style={{ width: 80, height: 20, marginLeft: 10, top:15 }} />

                                </View>

                                <View style={{ height: 25, width: 25, right: 10,left:30,marginTop:2,borderRadius:10}}></View>

                                
                            </View>
                            
                        </View>
                    </View>


                    <View style={{ flexDirection: "row", marginTop: 10}}>
                        <View style={{ marginLeft: 5 }}>
                            <View style={{ width: win.width / 4.5, height: 90, borderRadius: 10 }} />
                        </View>
                        <View>
                            <View style={{ flexDirection: "row", }}>
                                <View>
                                    <View style={{ width: 220, height: 15, marginLeft: 10,marginTop:5 }} />
                                    <View style={{ width: 120, height: 15, marginLeft: 10, top: 5 }} />

                                    <View style={{ width: 50, height: 15, marginLeft: 10, top:10 }} />

                                    <View style={{ width: 80, height: 20, marginLeft: 10, top:15 }} />

                                </View>

                                <View style={{ height: 25, width: 25, right: 10,left:30,marginTop:2,borderRadius:10}}></View>

                                
                            </View>
                            
                        </View>
                    </View>


                    <View style={{ flexDirection: "row", marginTop: 10}}>
                        <View style={{ marginLeft: 5 }}>
                            <View style={{ width: win.width / 4.5, height: 90, borderRadius: 10 }} />
                        </View>
                        <View>
                            <View style={{ flexDirection: "row", }}>
                                <View>
                                    <View style={{ width: 220, height: 15, marginLeft: 10,marginTop:5 }} />
                                    <View style={{ width: 120, height: 15, marginLeft: 10, top: 5 }} />

                                    <View style={{ width: 50, height: 15, marginLeft: 10, top:10 }} />

                                    <View style={{ width: 80, height: 20, marginLeft: 10, top:15 }} />

                                </View>

                                <View style={{ height: 25, width: 25, right: 10,left:30,marginTop:2,borderRadius:10}}></View>

                                
                            </View>
                            
                        </View>
                    </View>


                    <View style={{ flexDirection: "row", marginTop: 10}}>
                        <View style={{ marginLeft: 5 }}>
                            <View style={{ width: win.width / 4.5, height: 90, borderRadius: 10 }} />
                        </View>
                        <View>
                            <View style={{ flexDirection: "row", }}>
                                <View>
                                    <View style={{ width: 220, height: 15, marginLeft: 10,marginTop:5 }} />
                                    <View style={{ width: 120, height: 15, marginLeft: 10, top: 5 }} />

                                    <View style={{ width: 50, height: 15, marginLeft: 10, top:10 }} />

                                    <View style={{ width: 80, height: 20, marginLeft: 10, top:15 }} />

                                </View>

                                <View style={{ height: 25, width: 25, right: 10,left:30,marginTop:2,borderRadius:10}}></View>

                                
                            </View>
                            
                        </View>
                    </View>


                    <View style={{ flexDirection: "row", marginTop: 10}}>
                        <View style={{ marginLeft: 5 }}>
                            <View style={{ width: win.width / 4.5, height: 90, borderRadius: 10 }} />
                        </View>
                        <View>
                            <View style={{ flexDirection: "row", }}>
                                <View>
                                    <View style={{ width: 220, height: 15, marginLeft: 10,marginTop:5 }} />
                                    <View style={{ width: 120, height: 15, marginLeft: 10, top: 5 }} />

                                    <View style={{ width: 50, height: 15, marginLeft: 10, top:10 }} />

                                    <View style={{ width: 80, height: 20, marginLeft: 10, top:15 }} />

                                </View>

                                <View style={{ height: 25, width: 25, right: 10,left:30,marginTop:2,borderRadius:10}}></View>

                                
                            </View>
                            
                        </View>
                    </View>


                    <View style={{ flexDirection: "row", marginTop: 10}}>
                        <View style={{ marginLeft: 5 }}>
                            <View style={{ width: win.width / 4.5, height: 90, borderRadius: 10 }} />
                        </View>
                        <View>
                            <View style={{ flexDirection: "row", }}>
                                <View>
                                    <View style={{ width: 220, height: 15, marginLeft: 10,marginTop:5 }} />
                                    <View style={{ width: 120, height: 15, marginLeft: 10, top: 5 }} />

                                    <View style={{ width: 50, height: 15, marginLeft: 10, top:10 }} />

                                    <View style={{ width: 80, height: 20, marginLeft: 10, top:15 }} />

                                </View>

                                <View style={{ height: 25, width: 25, right: 10,left:30,marginTop:2,borderRadius:10}}></View>

                                
                            </View>
                            
                        </View>
                    </View>
                </SkeletonPlaceholder>
            </View>
        )
    }
}

const style = StyleSheet.create({
    heading: {
        fontSize: 20,
        marginLeft: 10,
        color: "black"
    },
    text: {
        // fontFamily:"Raleway-SemiBold",
        fontFamily: "Montserrat-SemiBold",
        fontSize: 12,
        margin: 10
    },
    card: {
        backgroundColor: "#fff",
        alignSelf: "center",
        width: Dimensions.get("window").width,
        top: 10,
        // paddingHorizontal:10,
        flexDirection: "row",
        marginBottom: 10,
        shadowRadius: 50,
        shadowOffset: { width: 50, height: 50 },

    },
    productImg: {
        height: 80,
        // width:"70%",
        // borderWidth:0.2,
        borderRadius: 3,
        // borderColor:"black",
        margin: 10,
        marginLeft: 10
    },
    signIn: {
        width: '100%',
        height: 35,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        flexDirection: "row",
        elevation: 1,
    },
    textSignIn: {
        //   fontFamily:"Raleway-Bold",
        fontFamily: "Montserrat-SemiBold",
        fontSize: 12,
    },
    viewDetailsButton: {
        borderColor: "#000",
        height: 35,
        flexDirection: "row",
        justifyContent: "center",
        width: 70,
        alignContent: "center",
        alignItems: "center",
        alignSelf: "flex-end",
        borderRadius: 5,
        backgroundColor: "#f5f5f5"
        // position:"absolute",
        // top:80,
        // left:165
        //alignSelf:"flex-end"
    },
    textButton: {

        fontFamily: "Raleway-SemiBold",
        fontSize: RFValue(9, 580),
        alignSelf: "center",
        color: "#222222",
        marginLeft: -10

    }, iconView: {
        width: 32,
        height: 32,
        marginRight: -20,
        shadowColor: '#fafafa',
        shadowOpacity: 1,
        elevation: 1,
        padding: 6,
        shadowRadius: 2,
        shadowOffset: { width: 1, height: 1 },
        alignContent: "center",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 100
    },

    contentView: {
        flexDirection: "column", width: "68%", marginRight: 10, paddingBottom: 10, borderBottomWidth: 0.5, borderColor: "#d3d3d3",
    }
})
