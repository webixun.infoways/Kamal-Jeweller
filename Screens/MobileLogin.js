import React, { Component } from 'react';
import {
    Text,View,ScrollView,ActivityIndicator,
    StyleSheet,Image,Dimensions,
    TouchableOpacity,ImageBackground
} from 'react-native';
import { 
    Input,Icon,Button 
} from 'react-native-elements';
import Toast from 'react-native-simple-toast';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

//Global StyleSheet Import
const style = require('../Components/Style.js');


class MobileLogin extends Component 
{
  constructor(props)
    {
        super(props);
        this.state={
            contact_no:null,
            isLoading: false
        }
    }

    
    //function to check validation
    sendOtp = () =>
    {
        var contact_no=this.state.contact_no;
        var phoneNumber = this.state.contact_no;
        let rjx= /^[0]?[6789]\d{9}$/;     
        let isValid = rjx.test(phoneNumber)
        if(!isValid)
        {
            Toast.show('Enter Valid mobile number!');      
        }
        else{
            this.setState({isLoading:true});
            fetch(global.api+"mobile-verification", {
                method: 'POST',
                headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    contact:contact_no,
                    // verification_type:"user"
                })
                }).then((response) => response.json())
                .then((json) => {
                    
                    if(json.msg=='ok')
                    {
                        Toast.show('OTP sent successfully!');
                        this.props.navigation.navigate('OtpVerify',{
                            contact_no:this.state.contact_no
                        });
                    }
                    else
                    {
                       Toast.show(json.error);
                    }
                })
                .catch((error) => console.error(error))
                .finally(() => {
                  this.setState({ isLoading: false });
                });
            }
    }

    
    render()
    {
        return (
        
        <View style ={style.container}>
           <ScrollView>
            <View style={{flex:1}}>

                {/* Background Image View */}
                <ImageBackground source={require("../image/redbg.png")} style={styles.imageBackground} >
                    
                <Image source={require('../image/logo/kj.png')} style={styles.image}/>
                </ImageBackground>
            </View>
            
            {/* logo, input number and button view */}
            <View style={{backgroundColor:"#fff",padding:10}}>
                <Image source={require('../image/logo/kj1.png')}
                style={styles.Img1}/>
           
                <View style={{top:45,height:300,}}>
                <Text  style={style.heading}>Let's Get Started ! </Text>
                    
                    {/* Phone Input */}
                    <Input  
                    onChangeText={(e) => {this.setState({contact_no:e})}}
                    placeholder='Enter your mobile number'
                    maxLength={10}
                    keyboardType="number-pad"
                    returnKeyType='done'
                    leftIcon={
                    <Icon
                        name='call'
                        size={25}
                        type='ionicons'
                        color='black'
                        style ={{marginTop:15,borderRightWidth:1,paddingRight:10,borderRightColor:"grey"}}
                    />
                    }
                    style ={{marginTop:20,marginLeft:10,color:"#222222", fontSize:RFValue(12,580)}}
                />
                    <Text style={[style.headingSmall,{top:-19}]}>We will never share your number with anyone ever!</Text>
                    
                    {/* Continue button */}
                    {!this.state.isLoading ?
                    <TouchableOpacity style={[style.buttonStyles,{top:30}]}
                    onPress={()=>this.sendOtp()}>
                        <Text style={style.buttonText}>Continue</Text>
                    </TouchableOpacity>
                    :
                    <ActivityIndicator size="large" color="#bc3b3b" style={{alignSelf:'center',marginTop:20,marginRight:30}}/>
                                
                    }
                </View>
            </View>
            </ScrollView>
        </View>
        );
   };
};

export default MobileLogin;


//Internal StyleSheet
const styles=StyleSheet.create({
    imageBackground:{
        width:"100%",
        height:290,
        paddingBottom:10
    },
    image:{
        // height:"70%",
        // width:"62%",
        marginTop:220,
        // top:10,
        paddingBottom:30,
        // justifyContent:"center",
        alignSelf:"center",
        width:70,
        height:70,
        
    },
    Img1:{
        width:220,
        height:70,
        // marginTop:5,
        // position:'absolute',
        alignSelf:"center",
        
    }
    
  
})