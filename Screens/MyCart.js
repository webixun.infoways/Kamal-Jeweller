import React, { Component, useState } from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  Pressable,
  FlatList,
  TextInput,
  Button,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  ImageBackground,
  Alert,
  Platform,
} from 'react-native';
import { Dimensions } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { Header, Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import RBSheet from 'react-native-raw-bottom-sheet';
import Toast from 'react-native-simple-toast';
import { ActivityIndicator } from 'react-native-paper';
import RadioForm, {
  RadioButton,
  RadioButtonInput,
  RadioButtonLabel,
} from 'react-native-simple-radio-button';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import navigationChange from '../helpers/index.js';

//Global Style Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class MyCart extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      item: '',
      isLoading: false,
      price: 0,
      text: "",
      sum:0,
      area: '',
      city: '',
      postal_code: '',
      state: '',
      status: true,
      firstName: '',
      lastName: '',
      cardNo: '',
      cvv: '',
      expYear: '',
      expMonth: '',
      addressdata: [],
      address: [],
      iconName: "radio-button-off",
      iconName1: "radio-button-off",
      iconName2: "radio-button-off",
      tax: [],
      address_id:"",
      discount:"",
      market_price:""
    }
  }

  componentDidMount() {
    this.my_cart();
    this.get_address();
    this.set_address();
    this.focusListener = this.props.navigation.addListener('focus', () => {
      if (navigationChange.isChanged) {
        navigationChange.isChanged = false;
        this.setState({ loader: true })
        this.props.navigation.navigate('Home');
      }
      this.my_cart();
      this.get_address();
      this.set_address();
    });
  }

  my_cart = () => {
    this.setState({isLoading:true})
    fetch(global.api + 'my-cart', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    })
      .then(response => response.json())
      .then(json => {
        if (json.status) {
          
          this.setState({ data: json.data });
          console.warn("data",json)
          this.setState({discount:json.discount,price:json.our_total,market_price:json.mp_total})
          this.setState({ item: json.data.length });

          var sum = 0;
          json.data.map((value) => {
            // sum = parseFloat(
            //   sum +
            //   parseFloat(value.price) * parseFloat(value.product_quantity),
              
            // );
            // this.setState({ price: sum });
            // var mp=0;
            // json.data.map((key)=>{
            //   mp=parseFloat(
            //     mp +
            //     parseFloat(key.market_price) * parseFloat(value.product_quantity),
            // )})
            // this.setState({market_price:mp})

            if(value.p_taxes != undefined){
              value.p_taxes.map(tax => {
                this.setState({ tax: tax.charges })
              })
            }
          });
        } else {
          this.setState({ data: '' });
          this.setState({ item: '' });
        }

        return json;
      })
      .catch(error => console.error(error))
      .finally(() => {
        this.setState({ isLoading: false });
      });
  };



  // fetching all saved addresses
  get_address = () => {
    this.setState({isLoading:true})
    fetch(global.api + 'get-address',
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': global.token
        },
      })
      .then((response) => response.json())
      .then((json) => {
        if (json.status) {
          this.setState({ addressdata: json.data })
          // console.warn(this.state.addressdata)
          this.state.addressdata.map(value=>{
            this.setState({address_id:value.id})
          })
          this.set_address();
          this.chooseAddress(this.state.address_id);
        }
        else {
          // Toast.show(json.msg)
        }
        return json;
      })
      .catch((error) => console.error(error))
      .finally(() => {
        this.setState({ isLoading: false });
      });
  }

  // fetching default address
  set_address = () => {
    fetch(global.api + "set-address", {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': global.token
      },
      body: JSON.stringify({
        address_id: this.state.address_id,
      })
    }).then((response) => response.json())
      .then((json) => {
        if (json.status) {
          this.setState({ address: json.data })
        }
        return json;
      }).catch((error) => {
        console.error(error);
      }).finally(() => {
        this.RBSheet.close()
      });

  }

  // choose address
  chooseAddress = (id) => {
    fetch(global.api + "set-address", {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'Authorization': global.token
      },
      body: JSON.stringify({
        address_id: id,
      })
    }).then((response) => response.json())
      .then((json) => {
        if (json.status) {
          this.setState({ address: json.data })
        }
        else{
          this.setState({ address: '' })
        }
        return json;
      }).catch((error) => {
        console.error(error);
      }).finally(() => {
        this.RBSheet.close()
      });

  }
  renderLeftComponent() {
    return (
      <View style={{ flexDirection: 'row', width: win.width / 2 }}>
        <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
        </View>
        <Text style={[styles.headerHeadingText]}>My Cart</Text>
      </View>
    );
  }

  renderRightComponent() {
    return (
      <View style={{padding:5,top:2}}>
        <Icon
          name="heart-outline"
          size={24}
          color="#222222"
          type="ionicon"
          onPress={() => this.props.navigation.navigate('Wishlist')}
        // style={{alignSelf:"center"}}
        />
      </View>
    );
  }

  render() {
    const screenWidth = Dimensions.get('window').width;
    return (
      <View style={style.container}>
        {/* View for header component */}
        <View>
          <Header
            // containerStyle={{ height: 82 }}
            statusBarProps={{ barStyle: 'light-content' }}
            leftComponent={this.renderLeftComponent()}
            rightComponent={this.renderRightComponent()}
            ViewComponent={LinearGradient} // Don't forget this!
            linearGradientProps={{
              colors: ['white', 'white'],
              start: { x: 0, y: 0.5 },
              end: { x: 1, y: 0.5 },
            }}
          />
        </View>

        <ScrollView showsVerticalScrollIndicator={false}>
          {/* Component for cards */}
          {this.state.isLoading ? (
            <View>
            <SkeletonPlaceholder>
                <View style={{ flexDirection: "row", marginTop: 10}}>
                    <View style={{ marginLeft: 5 }}>
                        <View style={{ width: win.width / 4.5, height: 90, borderRadius: 10 }} />
                    </View>
                    <View>
                        <View style={{ flexDirection: "row", }}>
                            <View>
                                <View style={{ width: 220, height: 15, marginLeft: 10,marginTop:5 }} />
                                <View style={{ width: 120, height: 15, marginLeft: 10, top: 5 }} />

                                <View style={{ width: 50, height: 15, marginLeft: 10, top:10 }} />

                               <View style={{flexDirection:"row"}}>
                                 <View style={{height:20,width:20,marginTop:15,marginLeft:10}}/>
                                 <View style={{height:20,width:20,marginTop:15,marginLeft:10}}/>
                               </View>

                            </View>

                            <View style={{ height: 25, width: 25, right: 10,left:30,marginTop:2,borderRadius:10}}></View>

                            
                        </View>
                        
                    </View>
                </View>

                <View style={{ flexDirection: "row", marginTop: 10}}>
                    <View style={{ marginLeft: 5 }}>
                        <View style={{ width: win.width / 4.5, height: 90, borderRadius: 10 }} />
                    </View>
                    <View>
                        <View style={{ flexDirection: "row", }}>
                            <View>
                                <View style={{ width: 220, height: 15, marginLeft: 10,marginTop:5 }} />
                                <View style={{ width: 120, height: 15, marginLeft: 10, top: 5 }} />

                                <View style={{ width: 50, height: 15, marginLeft: 10, top:10 }} />

                               <View style={{flexDirection:"row"}}>
                                 <View style={{height:20,width:20,marginTop:15,marginLeft:10}}/>
                                 <View style={{height:20,width:20,marginTop:15,marginLeft:10}}/>
                               </View>

                            </View>

                            <View style={{ height: 25, width: 25, right: 10,left:30,marginTop:2,borderRadius:10}}></View>

                            
                        </View>
                        
                    </View>
                </View>

            </SkeletonPlaceholder>
            </View>
          ) : (
            <Card
              data={this.state.data}
              navigation={this.props.navigation}
              item={this.state.item}
              my_cart={this.my_cart}
            />
          )}

          {/* Component for details */}

          <AlsoLike navigation={this.props.navigation} />

          {this.state.data == "" ?
            <></>
            :
            <Address
            navigation={this.props.navigation}
            chooseAddress={this.chooseAddress}
            my_cart={this.my_cart}
            data={this.state.addressdata}
            address={this.state.address} />
          }
          {this.state.item < 1 ? null : (
            <Details
              navigation={this.props.navigation}
              price={this.state.price}
              data={this.state.data}
              tax={this.state.tax}
              discount={this.state.discount}
              market_price={this.state.market_price}
            />
          )}

          <Last navigation={this.props.navigation} />
        </ScrollView>
        {/* component for order button */}
        {this.state.item < 1 ? null : (
          <OrderButton
            navigation={this.props.navigation}
            price={this.state.price}
            data={this.state.data}
            item={this.state.item}
            address={this.state.addressdata}
            address_id={this.state.address_id}
            discount={this.state.discount}
            market_price={this.state.market_price}
          />
        )}
      </View>
    );
  }
}
export default MyCart;

class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fav: false,
      object: {},
      like: {},
      data: [],
      item: '',
      quantity: "",
      id: ""
    };
  }

  alertFunction(id) {
    Alert.alert(
      'Are you sure?',
      'Move this item from cart?',
      [
        {
          text: 'Remove',
          onPress: () => this.remove(id),
        },

        { text: 'Move to wishlist', onPress: () => this.wishlist(id) },
      ],
      { cancelable: true },
    );
  }

  // Increase quantity 
  pluss = (qty, id) => {
    // console.warn(qty, id)
    qty = qty + 1
    // Api to Increase quantity
    fetch(global.api + 'update-cart', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        cart_id: id,
        quantity: qty
        // type: type,
      }),
    })
      .then(response => response.json())
      .then(json => {
        // console.warn(json);

      })
      .catch(error => {
        console.error(error);
      })
      .finally(() => {
        this.props.my_cart();
      });

  }

  // Decrease quantity
  minus = (qty, id) => {
    // console.warn(qty, id)
    if (qty > 1) {
      qty = qty - 1
      // Api to decrease quantity
      fetch(global.api + 'update-cart', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: global.token,
        },
        body: JSON.stringify({
          cart_id: id,
          quantity: qty
          // type: type,
        }),
      })
        .then(response => response.json())
        .then(json => {
          // console.warn(json);
        })
        .catch(error => {
          console.error(error);
        })
        .finally(() => {
          this.props.my_cart();
        });
    }
    else {
      Toast.show("Number of pieces can not be less than one")
    }
  }

  // Move product to wishlist
  wishlist = id => {
    this.remove(id);
    const like = this.state.like;
    if (like[id] == true) {
      like[id] = false;
      var type = 'no';
    } else {
      like[id] = true;
      var type = 'yes';
    }
    this.setState({ like });
    fetch(global.api + 'add-to-wishlist', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        product_id: id,
        type: type,
      }),
    })
      .then(response => response.json())
      .then(json => {
        // console.warn(json);
        if (!json.status) {
          var msg = json.msg;
          Toast.show("Something went wrong.");
        } else {
          // Toast.show(json.msg);
        }
      })
      .catch(error => {
        console.error(error);
      })
      .finally(() => {

      });
  };

  // Remove a product from cart
  remove = id => {
    const object = this.state.object;
    if (object[id] == true) {
      object[id] = false;
      var type = 'yes';
    } else {
      object[id] = true;
      var type = 'no';
    }
    this.setState({ object });
    fetch(global.api + 'add-to-cart', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        type: type,
        product_id: id,
        product_quantity: 1,
        size: 1,
        size_option_id: 1,
        weight_option_id: 1,
      }),
    })
      .then(response => response.json())
      .then(json => {
        // console.warn(json);
        if (!json.status) {
          var msg = json.msg;
          Toast.show(msg);
        } else {
          Toast.show(json.msg), this.props.my_cart();
        }
        return json;
      })
      .catch(error => {
        console.error(error);
      })
      .finally(() => {
      });
  };

  // Cart product details

  productCard = ({ item }) => (
    <View style={style.card}>
      {/* View for Image */}
      <Pressable
        onPress={() =>
          this.props.navigation.navigate('Products', { id: item.product_id })
        }
        style={{ width: '27%' }}>
        <Image
          source={{uri:global.img_url+item.document.src}}
          style={style.productImg}
        />
      </Pressable>
      {/* View for Content */}

      <View style={style.contentView}>
        {/* View for name and heart */}
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          {/* Text View */}
          <View style={{ width: 170 }}>
            <Text style={[styles.h3, { top: 5, fontSize: RFValue(12, 580) }]}>
              {item.name}
            </Text>
          </View>
          {/* View for heart icon  */}
          <View>
            <TouchableOpacity
              onPress={() => this.alertFunction(item.product_id)}
              style={style.iconView}>
              <Text style={{ alignSelf: 'center', textAlign: 'center' }}>
                <Icon
                  name="trash"
                  color={this.state.fav ? 'red' : '#d3d3d3'}
                  style={{ justifyContent: 'center' }}
                  size={20}
                  type="ionicon"
                />
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        {/* View for Price and offer */}
        <View
          style={{
            flexDirection: 'row',
            marginTop: 3,
          }}>

          <View style={{ flexDirection: 'row' }}>
            <Text style={[styles.p, { fontFamily: 'Montserrat-SemiBold',textDecorationStyle:"solid",textDecorationLine:"line-through" }]}>
              ₹ {item.market_price}/-
            </Text> 
          </View>
          
          <View style={{ flexDirection: 'row' ,paddingLeft:20}}>
            <Text style={[styles.p, { fontFamily: 'Montserrat-SemiBold' }]}>
              ₹ {item.price}/-
            </Text> 
          </View>

        </View>
        {/* Quantity increase/Decrease */}
        <View style={{ flexDirection: 'row', marginTop: 10, width: "30%", justifyContent: "space-evenly", right: 10 }}>
          <TouchableOpacity style={style.button}
            onPress={() => {
              // this.minus(item.product_quantity, item.id)
              Toast.show("The product quantity should be 1")
            }}>
            <Icon name="remove" type="ionicon" size={15} color="#BC3B3B"
              style={{ top: -2 }} />
          </TouchableOpacity>

          <View style={[styles.button]}>
            <Text style={[style.h2, { top: -2, alignSelf: "center", fontFamily: Platform.OS == "android" ? "Roboto-Medium" : null, color: "#BC3B3B" }]}>
              {item.product_quantity}
            </Text>
          </View>

          <TouchableOpacity style={style.button} 
            onPress={() => {
              // this.pluss(item.product_quantity, item.id)
              Toast.show("The product quantity should be 1")
            }}>
            <Icon name="add" type="ionicon" size={15} color="#BC3B3B"
              style={{ top: -2 }} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );

  render() {
    return (
      <View>
        {this.props.item < 1 ? (
          <View
            style={{
              alignItems: 'center',
              backgroundColor: '#fff',
              flex: 1,
              padding: 20,
            }}>
              <Image source={require('../image/empty-cart.png')} style={{height:200,width:250,alignSelf:"center"}}/>
              <Text style={styles.p}>No products found in your cart!</Text>
          </View>
        ) : (
          <FlatList
            navigation={this.props.navigation}
            showsVerticalScrollIndicator={false}
            data={this.props.data}
            renderItem={this.productCard}
            keyExtractor={item => item.id}
          />
        )}
      </View>
    );
  }
}

class Address extends Component {
  constructor(props) {
    super(props);
    console.log(this.props.data)
    this.state = {

    }
  }

delete = (id) => {
  Alert.alert(
    'Delete Address',
    'Are you sure you want to delete this address?',
    [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      { text: 'OK', onPress: () => this.deleteAddress(id) },
    ],
    { cancelable: false },
  );
}

deleteAddress = (id) => {
  fetch(global.api + 'delete-address', {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: global.token,
    },
    body: JSON.stringify({
      address_id: id,
    }),
  })
    .then(response => response.json())
    .then(json => {
      // console.warn(json);
      if (!json.status) {
        var msg = json.msg;
        Toast.show("Something went wrong.");
      } else {
        Toast.show(json.msg),
        this.RBSheet.close();
        this.props.my_cart()
      }
      return json;
    })
    .catch(error => {
      console.error(error);
    })
    .finally(() => {
    });
}

  render() {
    let address = this.props.data.map((address, id) => {
      global.address = address.name + " " + address.house_no + " " + address.landmark + " " + address.city + " " + address.state + " " + address.zip
      return (
        <View style={{ width: "100%",}}>

          <View style={{ justifyContent: "space-evenly", marginBottom: 10, flexDirection: "row" }}>

            <Pressable style={{ borderBottomWidth: 1, borderColor: "#d3d3d3", width: "70%", marginHorizontal: -20, paddingBottom: 10 }} onPress={() => {
              this.props.chooseAddress(address.id),
              this.RBSheet.close()
              
            }}>
              <View>
                <Text style={[styles.h3, { fontSize: RFValue(11, 580), top: 4 }]}>
                  {address.name}
                </Text>
                <Text style={[styles.p, { fontSize: RFValue(10, 580) }]}>
                  {address.house_no}, {address.address} {address.landmark} {'\n'}{address.city} {address.state}  {address.zip}
                </Text>
              </View>
            </Pressable>
            <View style={{flexDirection:"row",justifyContent:"space-between",top:10}}>
            <Pressable style={{ top: 10,left:-15 }} onPress={() => this.props.navigation.navigate("EditAddress", { id: address.id })}>
              <Icon name="create-outline" type="ionicon" size={20} />
              {/* <Text style={styles.h5}>
                Edit
              </Text> */}
            </Pressable>
            <Pressable style={{ top: 10,left:5 }} onPress={() => this.delete(address.id)}>
              <Icon name="trash-outline" type="ionicon" color="#bc3b3b" size={20} />
              {/* <Text style={styles.h5}>
                Delete
              </Text> */}
            </Pressable>
            </View>
          </View>
        </View>
      )
    })
    return (
      <View style={{ flexDirection: "row", backgroundColor: "#fff", paddingBottom: 10, justifyContent: "space-between", paddingHorizontal: 5, marginBottom: 5 }}>
        <View >
          
          <View style={{ flexDirection: "row", paddingLeft: 10 }}>
            {this.props.data!=""?(
              <Text style={[styles.p, { fontSize: 13 }]}>
              Deliver to :
            </Text>
            )
            :
            null
            }
            
            <Text style={[styles.h3, { fontSize: RFValue(10, 580), left: 5, top: 4 }]}>
              {this.props.address.name}
            </Text>
          </View>
          {this.props.data!="" ? (
            
            <View style={{ paddingLeft: 10 }}>
            <Text style={[styles.p, { fontSize: RFValue(10, 580) }]}>
              {this.props.address.house_no},{this.props.address.landmark} {'\n'}{this.props.address.city} {this.props.address.state}, {this.props.address.zip}
            </Text>
          </View>
          )
          :
          (
            <View>
              <Text style={[styles.h5,{left:10,top:-5}]}>
                Add your delivery address
              </Text>
              </View>
          )
          }
          </View>
          {this.props.data!="" ? (
            <Pressable onPress={() => this.RBSheet.open()} style={{ top: 15, right: 5 }}>
            <Text style={[styles.h4, { fontSize: RFValue(9, 580), color: "#bc3b3b" }]}>
              CHANGE
            </Text>
          </Pressable>
          )
          :
          (
            <Pressable onPress={() => this.props.navigation.navigate("NewAddress")} style={{ top: 15, right: 5 }}>
            <Text style={[styles.h4, { fontSize: RFValue(9, 580), color: "#bc3b3b" }]}>
              Add New
            </Text>
          </Pressable>
          )
          }
        

        {/* Bottom Sheet for Camera */}
        <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          closeOnDragDown={true}
          closeOnPressMask={true}
          height={350}
          customStyles={{
            container: {
              borderTopRightRadius: 20,
              borderTopLeftRadius: 20,
            },
            draggableIcon: {
              backgroundColor: ""
            }
          }}
        >
          {/* bottom sheet elements */}
          <View>
            <View style={{ width: "95%", paddingBottom: -19, marginHorizontal: 10, borderBottomWidth: 1, borderColor: "#d3d3d3", flexDirection: "row", justifyContent: "space-between" }}>
              <Text style={[styles.bottomButton1Text, { alignSelf: "flex-start", fontSize: 14, top: -5, }]}>
                CHANGE ADDRESS
              </Text>
              <Pressable onPress={() => {this.RBSheet.close(),this.props.navigation.navigate("NewAddress")}} style={[styles.buttonStyles, { width: 100, height: 30, top: -5, borderRadius: 5, backgroundColor: "#fff", borderWidth: 1 }]} >
                <Text style={[styles.bottomButton1Text, { fontSize: RFValue(10, 580), color: "#222222" }]}>
                  ADD NEW
                </Text>
              </Pressable>
            </View>
            <ScrollView>
              <View style={{marginBottom:70}}>
            {address}
            
            </View>
          </ScrollView>
          </View>
        </RBSheet>

      </View>
    )
  }
}


class Details extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: true,
      notes: '',
    };
  }

  render() {
    return (
      <View style={{ paddingVertical: 10, backgroundColor: 'white' }}>
        <Text
          style={[
            styles.h3,
            {
              marginLeft: 20,
              fontSize: 15,
              fontFamily: 'Raleway-Medium',
              color: '#222222',
              marginTop: -10,
            },
          ]}>
          Price Details :
        </Text>
        <View style={[style.detailsView, { borderBottomWidth: 0 }]}>
          <Text style={[styles.h4]}>Total MRP</Text>
          <Text
            style={{
              color: '#bc3b3b',
              fontSize: RFValue(11, 580),
              fontFamily: 'Montserrat-Medium',
            }}>
            ₹ {this.props.market_price}
          </Text>
        </View>
        <View
          style={[
            style.detailsView,
            { borderBottomWidth: 0, paddingVertical: 5 },
          ]}>
          <Text style={[styles.h4]}>Discount</Text>
          <Text
            style={{
              color: 'green',
              fontSize: RFValue(11, 580),
              fontFamily: 'Montserrat-Medium',
            }}>
           - ₹ {parseInt(this.props.discount)}
          </Text>
        </View>

        <View style={[style.detailsView, { paddingBottom: 10 }]}>
          <Text style={[styles.h4]}>Delivery Cost</Text>
          <Text
            style={{
              color: '#bc3b3b',
              fontSize: RFValue(11, 580),
              fontFamily: 'Montserrat-Medium',
            }}>
            {' '}
            ₹ 0
          </Text>
        </View>

        <View style={[style.detailsView, { borderBottomWidth: 0 }]}>
          <Text style={[styles.h4]}>Total</Text>
          <Text
            style={{
              color: '#bc3b3b',
              fontSize: RFValue(11, 580),
              fontFamily: 'Montserrat-Medium',
            }}>
            {' '}
            ₹ {this.props.price}/-
          </Text>
        </View>
      </View>
    );
  }
}

// component for Button

class OrderButton extends Component {


  render() {
    return (
      <View
        style={{
          position: 'absolute',
          backgroundColor: '#fff',
          paddingVertical: 10,
          height: 80,
          width: Dimensions.get('window').width,
          bottom: 0,
        }}>
        <Text style={[styles.p, { alignSelf: 'center', top: -7 }]}>
          {this.props.item} items selected for order
        </Text>
        <Pressable
          onPress={() =>
            // this.checkout(){}
            {
              if(this.props.address!=""){
                this.props.navigation.navigate('Checkout', {
                  total_price: this.props.price,
                  data: this.props.data,
                  address_id:this.props.address_id,
                  discount:this.props.discount,
                  sub_total:this.props.market_price
                })
              }
              
              else{
                Toast.show("Add Delivery address first.")
              }
            
          }
          }
          style={[
            styles.buttonStyles,
            { width: '95%', borderRadius: 5, top: -5 },
          ]}>
          <Text style={[styles.buttonText, { fontSize: 16 }]}>PROCEED TO CHECKOUT</Text>
        </Pressable>
      </View>
    );
  }
}

class AlsoLike extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isloading:false
    };
  }
  componentDidMount() {
    this.fetch_products();
  }

  fetch_products = () => {
    this.setState({isloading:true})
    fetch(global.api + 'get-product-list', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        category_id: 7,
        sort: ""
      }),
    })
      .then(response => response.json())
      .then(json => {
        // console.warn("Also like", json.data.data);
        if (json.data.data.length > 0) {
          if (json.status) {
            this.setState({ data: json.data.data });
          } else {
            Toast.show('No data found');
          }
        }
        return json;
      })
      .catch(error => console.error(error))
      .finally(() => {
        this.setState({isloading: false});
      });
  };

  productCard = ({ item }) => (
    <View
      style={{
        padding: 10,
        justifyContent: 'space-between',
        flexDirection: 'row',
      }}>
      <Pressable
        onPress={() =>
          this.props.navigation.navigate('Products', { id: item.id })
        }>
        <View style={[styles.card1, { width: 120 }]}>
          <View style={{ flexDirection: 'column' }}>
            <Image
              source={{uri:global.img_url+item.picture[0].src}}
              style={[styles.recommendedImage, { height: 120, width: 120 }]}
            />
            <View style={{ paddingLeft: 10, top: 5, paddingRight: 10 }}>
              <View style={{flexDirection:"row"}}>
                <Text style={[styles.p, { fontWeight: 'bold', fontSize: 13 ,textDecorationLine:"line-through"}]}>
                  ₹ {item.market_price}
                </Text>
                <Text style={[styles.p, { fontWeight: 'bold', fontSize: 13,marginLeft:5 }]}>
                  ₹ {item.price}
                </Text>
              </View>
              <Text numberOfLines={2} style={[styles.h5]}>{item.name}</Text>
              {/* <Text style={[styles.h5]}>Necklace</Text> */}
            </View>
          </View>
        </View>
      </Pressable>
    </View>
  );

  render() {
    return (
      <View style={{ backgroundColor: '#fff', marginTop: 7, marginBottom: 5 }}>
        {/* heading */}
        <Text
          style={[
            styles.h3,
            {
              marginLeft: 20,
              fontSize: 15,
              fontFamily: 'Raleway-Medium',
              color: '#222222',
              marginTop: 5,
            },
          ]}>
          You may also like :
        </Text>

        {/* card 1 */}
        {this.state.isloading ?
        <View>  
          <SkeletonPlaceholder>
            <View style={{flexDirection:"row"}}>
              <View>
                <View style={{height:120,width:120,marginTop:10,marginLeft:15}}/>
                <View style={{height:20,width:120,marginTop:5,marginLeft:15}}/>
                <View style={{height:20,width:120,marginTop:5,marginLeft:15}}/>
              </View>

              <View>
                <View style={{height:120,width:120,marginTop:10,marginLeft:10}}/>
                <View style={{height:20,width:120,marginTop:5,marginLeft:10}}/>
                <View style={{height:20,width:120,marginTop:5,marginLeft:10}}/>
              </View>

              <View>
                <View style={{height:120,width:120,marginTop:10,marginLeft:10}}/>
                <View style={{height:20,width:120,marginTop:5,marginLeft:10}}/>
                <View style={{height:20,width:120,marginTop:5,marginLeft:10}}/>
              </View>
              </View>
          </SkeletonPlaceholder>

        </View>
        :
        <FlatList
          horizontal
          navigation={this.props.navigation}
          showsHorizontalScrollIndicator={false}
          data={this.state.data}
          renderItem={this.productCard}
          keyExtractor={item => item.id}
        />}
      </View>
    );
  }
}

class Last extends Component {
  render() {
    return (
      <View
        style={{
          flexDirection: 'row',
          marginBottom: 90,
          justifyContent: 'space-evenly',
          top: 10,
        }}>
        <Image source={require('../image/icon1.png')} style={style.img} />
        <Image source={require('../image/icon2.png')} style={style.img2} />
        <Image source={require('../image/icon3.png')} style={style.img2} />
        {/* <Image source={require('../image/icon4.png')}
                        style={style.img}/>     */}
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fafafa' },

  header: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: 'SemiBold',
    marginLeft: 90,
    marginTop: 13,
  },
  heading: {
    fontSize: 20,
    marginLeft: 10,
    color: 'black',
  },
  digits: {
    padding: 5,
    marginLeft: 10,
    width: 30,
    borderRadius: 50,
    borderColor: '#000',
    borderWidth: 1,
  },
  card: {
    backgroundColor: '#fff',
    alignSelf: 'center',
    width: Dimensions.get('window').width,
    top: 5,
    // paddingHorizontal:10,
    flexDirection: 'row',
    marginBottom: 5,
    shadowRadius: 50,
    shadowOffset: { width: 50, height: 50 },
  },
  productImg: {
    height: 85,
    width: '75%',
    // borderWidth:0.2,
    borderRadius: 5,
    borderColor: 'black',
    margin: 10,
    marginLeft: 10,
  },

  priceText: {
    alignSelf: 'flex-end',
    top: 5,
    right: 10,
    fontSize: RFValue(12, 580),
    fontFamily: 'Montserrat-SemiBold',
  },
  img: {
    height: 120,
    width: 65,
  },
  img2: {
    height: 120,
    width: 72,
  },
  contentView: {
    flexDirection: 'column',
    width: '68%',
    marginRight: 10,
    paddingBottom: 10,
    borderBottomWidth: 0.5,
    borderColor: '#d3d3d3',
  },
  detailsView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 25,
    borderBottomWidth: 1,
    borderColor: '#f5f5f5',
    paddingVertical: 5,
  },
  buttonStyles: {
    backgroundColor: '#BC3B3B',
    flexDirection: 'row',
    width: Dimensions.get('window').width / 1.4,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    borderRadius: 25,
    top: 30,
    marginBottom: 55,
  },
  buttonText: {
    color: '#fff',
    fontSize: RFValue(12, 580),
  },
  text: {
    // fontFamily:"Roboto-Regular",
    fontFamily: 'Montserrat-Medium',
    color: '#222222',
    padding: 5,
    fontSize: RFValue(11, 580),
  },
  button: {
    borderColor: "#bc3b3b",
    borderWidth: 1,
    borderRadius: 10,
    width: 20,
    height: 15,

  },
});
