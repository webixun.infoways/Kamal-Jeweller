import React, { Component } from 'react';
import {
    Text,View,ScrollView,ImageBackground,
    StyleSheet,Image,Dimensions,ActivityIndicator,
    TouchableOpacity,TextInput,Pressable
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { 
    Input,Icon,Button 
} from 'react-native-elements';
import RBSheet from "react-native-raw-bottom-sheet";
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ImagePicker from "react-native-image-crop-picker";
import { RFValue } from "react-native-responsive-fontsize";
import Toast from 'react-native-simple-toast';
import { AuthContext } from '../AuthContextProvider.js';

//Global StyleSheet Import
const styles = require('../Components/Style.js');


const options = {
    title: "Pick an Image",
    storageOptions:{
        skipBackup: true,
        path:'images',  
    },
    quality:0.5
}


class CompleteProfile extends Component {

    static contextType=AuthContext;
    constructor(props){
        super(props);
        this.state={
            upload_profile_picture:'https://cdn-icons-png.flaticon.com/512/149/149071.png',
            photo:'',
            image:"https://cdn-icons-png.flaticon.com/512/149/149071.png",
            isLoading:false,
            image_load:'',
            name:'',
            mail:'',
            token:''
        }
    }

    

    userDetails = () =>{
        let validation=/^[a-zA-Z" "]+$/;
        let isValid = validation.test(this.state.name)
          {
              this.setState({msg:""});
              if(this.state.name == "")
              {
                  Toast.show("User Name Field is required !");
              }
              else if(!isValid){
                  Toast.show("Enter a valid name !");
              }
              else if(this.state.image==""){
                Toast.show("Image is required !");
              }
              else{
                  this.setState({isLoading:true});  
                  var name=this.state.name;
                  var mail=this.state.mail;
  
                  fetch(global.api+"update-profile", { 
                       method: 'POST',
                         headers: {    
                             Accept: 'application/json',  
                               'Content-Type': 'application/json',
                               'Authorization': global.token  
                              }, 
                               body: JSON.stringify({   
                                  email: mail, 
                                  f_name: name,
                                    })}).then((response) => response.json())
                                       .then((json) => {
                                           console.warn(json)
                                           if(!json.status)
                                           {
                                               Toast.show("Enter valid email")
                                           }
                                           else{
                                            Toast.show("Profile successfully created!")
                                            global.use_type='done';
                                            AsyncStorage.getItem('@auth_login', (err, result) => {
                                                if (JSON.parse(result) != null) {
                                                  const data={"token":JSON.parse(result).token,"user_id":JSON.parse(result).user_id,"use_type":'done'};
                                                  AsyncStorage.setItem('@auth_login',JSON.stringify(data));
                                                  this.context.login('done');
                                                } 
                                            });
                                
                                           }
                                          return json;    
                                      }).catch((error) => {  
                                              console.error(error);   
                                           }).finally(() => {
                                              this.setState({isLoading:false})
                                           });
              }
          }
      }
  

    //function to launch camera
    camera =()=>{
        launchCamera(options, (response)=>{
            
            if(response.didCancel){
                console.warn(response)
                console.warn("User cancelled image picker");
            } else if (response.error){
                console.warn('ImagePicker Error: ', response.error);
            }else{
                // const source = {uri: response.assets.uri};
              let path = response.assets.map((path)=>{
                  return (
                     console.warn(path.uri) ,
                      this.setState({image:path.uri}) 
                  )
              });
              //  this.setState({image:path.uri})
            //   this.RBSheet.close()
              this.upload_image();
            }
        })
      }

       //function to launch gallery
    gallery =()=>{
        ImagePicker.openPicker({
            width:300,
            height:400,
            cropping:true,
        }).then(image=>{
            console.log(image);
            // this.setState({image:"Image Uploaded"})
            this.setState({image:image.path});
            this.upload_image();      
        })
    }

    //function to upload image
    upload_image = () =>
    {
        this.RBSheet.close()
        this.setState({image_load:true});
        var photo = {
            uri: this.state.image,
            type: 'image/jpeg',
            name:"akash.jpg" 
          };
          var form = new FormData();
          form.append("file", photo);

          fetch(global.api+"upload-profile-pic", { 
            method: 'POST',
            body: form,
              headers: {  
                'Content-Type': 'multipart/form-data' ,
                'Authorization': global.token 
                   }, 
                    }).then((response) => response.json())
                            .then((json) => {
                                console.warn(json); 
                                if(json.status){
                                    Toast.show("Image uploaded successfully")
                                }
                                else{
                                    Toast.show("Not updated")
                                }  
                           }).catch((error) => {  
                                   console.error(error);   
                                }).finally(() => {
                                    this.setState({image_load:false});
                                });
    }


    render() {
        return (
            <View style={styles.container}>

                <ScrollView >
               <View>
                  
                  <ImageBackground source={require("../image/redprofile.png")} style={style.imageBackground} >
                    {/* heading */}
                    <Text style={[styles.heading,{left:5,top:25,color:"white",fontSize:24}]}>
                        Welcome to
                        {'\n'} Kamal Jewellers
                    </Text>

                    {/* user image */}
                    {this.state.image_load ? 
                    <ActivityIndicator color="#bc3b3b" size="large"
                    style={style.profileImg}/>
                    :
                    (this.state.image=="")?
                    <Image 
                    source={require('../image/dummyuser.jpg')} 
                    style={style.profileImg}/>
                    :
                    <View>
                    <Image 
                    source={{
                        uri: this.state.image
                        }} 
                    style={style.profileImg}/>
                    </View>
                    }
                    <Text onPress={()=>this.RBSheet.open()}  style={style.camIcon}>
                    <Icon type="ionicon" name="camera"  size={20} color="#000"/>
                    </Text>
                    </ImageBackground>

                     {/* Bottom Sheet for gallery open */}
                        <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        animationType="slide"
                        closeOnDragDown={true}
                        closeOnPressMask={true}
                        height={150}
                        customStyles={{
                            container:{
                                borderTopLeftRadius:20,
                                borderTopRightRadius:20
                            },
                        wrapper: {
                            // backgroundColor: "transparent",
                            borderWidth: 1
                        },
                        draggableIcon: {
                            backgroundColor: "grey"
                        }
                        }}
                    >
                        {/* bottom sheet elements */}
                        <View>
                            
                            {/* open camera and library */}
                                <View style={{width:"100%",padding:20}}>
                                        <TouchableOpacity onPress={this.camera}>
                                            <Text style={style.icon}>
                                                <Icon name='camera' type="ionicon" color={'#bc3b3b'} size={25}/>
                                            </Text>
                                            <Text style={style.Text}>Take a picture</Text>
                                            </TouchableOpacity>

                                            <TouchableOpacity onPress={this.gallery} > 
                                            <Text style={style.icon}>
                                                <Icon name='folder' type="ionicon" color={'#bc3b3b'} size={25}/>
                                            </Text>
                                            <Text style={style.Text}>Select from library</Text>
                                        </TouchableOpacity>

                                </View>
                            
                        </View>
                    </RBSheet>

                    {/* Edit Profile button */}
                    {/* <Pressable style={{flexDirection:'row',alignItems:"center",
                    justifyContent:"center",top:15}}
                    onPress={()=>this.props.navigation.navigate("MyProfile")}>
                        <Icon name="create-outline" type="ionicon" color="#BC3B3B"/>
                        <Text style={[styles.h4,{color:"#BC3B3B"}]}>Edit Profile</Text>
                    </Pressable> */}

                    {/* View for inputs */}
                    <View>

                        {/* TextInput for name */}
                        <TextInput 
                        style={[style.textInput,{fontFamily: "Raleway-Regular",}]}
                        placeholder="Name"
                        // placeholderTextColor="#D19292"
                        value={this.state.name}
                        maxLength={40}
                        onChangeText={(text) => {this.setState({name:text})}}/>

                        {/* TextInput for mail */}
                        <TextInput 
                        style={[style.textInput,{marginTop:20,fontFamily: "Raleway-Regular",}]}
                        placeholder="Email (Optional)"
                        // placeholderTextColor="#D19292"
                        value={this.state.mail}
                        maxLength={55}
                        onChangeText={(text) => {this.setState({mail:text})}}/>
                    </View>


                    {/* View for button */}
                    <View>
                    {this.state.isLoading?

                    <View >
                    <ActivityIndicator size="large" color="#bc3b3b"
                    style={{marginTop:120}}/>
                    </View>

                    :
                        <TouchableOpacity style={[styles.buttonStyles,{marginTop:130}]}
                        onPress={()=>this.userDetails()}>
                            <Text style={styles.buttonText}>Create Profile</Text>
                        </TouchableOpacity>
                    }
                    </View>

               </View>
               </ScrollView>
            </View>
        )
    }
}

export default CompleteProfile;


//Internal styling
const style=StyleSheet.create({
    iconBack:{
        padding:10
    },
    imageBackground:{
        width:"100%",
        height:290,
        paddingBottom:10
    },
    buttonStyles:{
        width:"20%",
        justifyContent:"center",
        alignSelf:"flex-end",
        height: 30,
        alignItems: 'center',
        borderRadius: 10,
        flexDirection:"row",
        alignItems:'center',
        right:5,
        backgroundColor:"#BC3B3B",
        marginBottom:10
    },
    buttonText: {
        color: "#fff",
        fontSize: RFValue(12, 580),
    },
    profileImg:{
        height:120,
        width:120,
        alignSelf:"center",
        top:145,
        borderRadius:60,
        // elevation:1
        // backgroundColor:"red"
    },
    camIcon:{
        backgroundColor:"#dcdcdc",
        height:30,
        width:30,
        padding:5,
        alignContent:"center",
        borderRadius:30, 
        justifyContent:"center",
        alignSelf:"center",
        // left:230,
        left:45,
        top:120
    },
    icon:{  
        marginLeft:20,
        // fontSize:20,
        fontSize:RFValue(14.5, 580),
        marginBottom:10
    },
    Text:{
        position:"absolute",
        // fontSize:20,
        fontSize:RFValue(14.5, 580),
        marginLeft:80,
        fontFamily:"Raleway-Medium"
    },
    textInput:{
        backgroundColor:"#F2F2F2",
        width:Dimensions.get('window').width/1.2,
        borderRadius:10,
        height:45,
        alignSelf:"center",
        top:90,
        paddingLeft:20,
        color:"#222222",
        fontSize:RFValue(11,580),
    },
}
)
