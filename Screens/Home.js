import React, { Component } from 'react';
import {
  Text,
  View,
  ScrollView,
  FlatList,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  Pressable,ImageBackground,
  ActivityIndicator,
} from 'react-native';
import { Icon, Header } from 'react-native-elements';
import { fonts } from 'react-native-elements/dist/config';
import LinearGradient from 'react-native-linear-gradient';
import { RFValue } from 'react-native-responsive-fontsize';
import { Searchbar } from 'react-native-paper';
import Toast from 'react-native-simple-toast';
import SwiperFlatList from 'react-native-swiper-flatlist';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';


//Global StyleSheet Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');
const images = [{
    url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSKaRFWyp6WXfx_FfGICMX-6wbXvU_iC2V0Vw&usqp=CAU'
}, {
    url: 'https://www.gemsociety.org/wp-content/uploads/2019/02/vintage-custom-made.jpg'
}, {
    url: 'https://shop.southindiajewels.com/wp-content/uploads/2020/10/One-Gram-Gold-Kada-Bangle-02.jpg'
}]
class Home extends Component {
  constructor(props) {
    super(props);
    console.log(props.route.params);
    this.state = {
      item: '',
      content: [],
    };
  }

  //cart item
  componentDidMount = () => {
    // alert("dsfvdf"+global.token);
    this.my_cart();
    this.get_profile();
    // this.fetch_home_data();
    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.my_cart();
      this.get_profile();
      // this.fetch_home_data();
    });
  };

  // Fetching user details
  get_profile = () => {
    fetch(global.api + 'get-user-profile',
      {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization': global.token
        },
      })
      .then((response) => response.json())
      .then((json) => {
        console.warn(json)
        if (json.status) {
          global.pic = json.data[0].profile_pic
        }
        else {
          // Toast.show(json.msg)
        }
        return json;
      })
      .catch((error) => console.error(error))
      .finally(() => {
        this.setState({ isLoading: false });
      });
  }


  //api call for home multiple components
  my_cart = () => {
    fetch(global.api + 'my-cart', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    })
      .then(response => response.json())
      .then(json => {
        if (json.status) {
          this.setState({ item: json.data.length });
          //   alert(this.state.item)
        } else {
          this.setState({ data: '' });
          this.setState({ item: '' });
        }

        return json;
      })
      .catch(error => console.error(error))
      .finally(() => {
        this.setState({ isLoading: false });
      });
  };

  //for header Left component
  renderLeftComponent() {
    return (
      <View style={{width:win.width,backgroundColor:"#fff"}}>
        <Image
          source={require('../image/logo/headerlogo.png')}
          style={{ height: 30, width: 125 }}
        />
      </View>
    );
  }

  renderRightComponent() {
    return (
      <View
          style={{width:"170%", top:5,flexDirection:"row",justifyContent:"space-around"}}>
        <Icon
          name="search-outline"
          size={22}
          color="black"
          type="ionicon"
          onPress={() => this.props.navigation.navigate('Search')}
        // onPress={()=>this.onClick()}
        />
        <Icon
          name="heart-outline"
          size={22}
          color="black"
          type="ionicon"
          onPress={() => this.props.navigation.navigate('Wishlist')}
        // onPress={()=>this.onClick()}
        />
        <Icon
          name="notifications-outline"
          size={22}
          color="black"
          type="ionicon"
          onPress={() => this.props.navigation.navigate('Notifications')}
        // style={{marginTop:5}}
        />
        <Pressable onPress={() => this.props.navigation.navigate("MyCart")} style={{ marginRight: 10 }}>
          <Icon
            name='cart-outline'
            size={22}
            color='black'
            type="ionicon"

          // style={{marginTop:5}} 
          />
          {this.state.item == "" ? null :
            <View style={{ backgroundColor: "#bc3b3b", marginTop: -30, borderRadius: 60, left: 14 }} >
              <Text style={{ color: "#fff", left: 7 }}>{this.state.item}</Text>
            </View>
          }
        </Pressable>
      </View>
    );
  }

  //   flatlist render item
  homeData = ({ item }) => (
    <View>
      <View style={{ backgroundColor: '#fff', marginTop: 7 }}>
        {/* heading */}
        <Text
          style={[
            styles.h3,
            {
              marginLeft: 20,
              fontSize: 18,
              fontFamily: 'DancingScript-Bold',
              color: '#bc3b3b',
              marginTop: 5,
            },
          ]}>
          {item.tab_name}
        </Text>
        {item.product.map(value => {
          return value.picture.map(item => {
            return (
              <View style={{ padding: 10, marginTop: 5, flexDirection: 'row' }}>
                <ScrollView
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}>
                  <View>
                    <Image
                      source={{
                        url:
                          'https://demo.webixun.com/KamalJwellersApi/public/' +
                          item.src,
                      }}
                      style={styles.sliderImage}
                    />
                  </View>

                  
                </ScrollView>
              </View>
            );
          });
        })}
      </View>
    </View>
  );

  render() {
    return (
      <View style={[styles.container]}>
        {/* View for header */}
        <View>
          <Header
            statusBarProps={{ barStyle: 'light-content' }}
            leftComponent={this.renderLeftComponent()}
            rightComponent={this.renderRightComponent()}
            ViewComponent={LinearGradient} // Don't forget this!
            linearGradientProps={{
              colors: ['white', 'white'],
              start: { x: 0, y: 0.5 },
              end: { x: 1, y: 0.5 },
            }}
          />
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{ backgroundColor: '#f5f5f5' }}>

            {/* <FlatList 
            navigation={this.props.navigation}
            showsVerticalScrollIndicator={false}
            data={this.state.content}
            renderItem={this.homeData}
            keyExtractor={item => item.id}
            /> */}
            {/* Component call for collection */}
            <Collection navigation={this.props.navigation} />

            {/* <ShopFor navigation={this.props.navigation} />

            <Party navigation={this.props.navigation} />

            <BestRings navigation={this.props.navigation} />

            <Gold navigation={this.props.navigation} />

            <NeckWear /> */}

            {/* Component for wedding jewellery */}

            {/* <Recommended navigation={this.props.navigation} /> */}
            {/* <Wedding navigation={this.props.navigation} /> */}
          </View>

          {/* <View style={{marginTop:-15}}>
                    <Image source={require('../image/logo/dashboardlogo.png')} 
                    style={style.logo}/>
                </View> */}

          <View>
            <Last navigation={this.props.navigation} />
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default Home;

//Component for Categories
class Category extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View
        style={{ padding: 10, flexDirection: 'row', backgroundColor: '#fff' }}>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          {/* offers */}
          
            <Pressable
              onPress={() => this.props.navigation.navigate('ProductList')}>
              <View>
              <View style={styles.cardImg}>
                  <Image
                    source={require('../image/offer.jpg')}
                    style={[styles.image,{borderRadius:100}]}
                  />
                </View>
                <Text style={styles.catHeading}>Offers</Text>
              </View>
            </Pressable>
          {/* tradional */}

          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.cardImg}>
                <Image
                  source={require('../image/traditional.jpg')}
                  style={[styles.image,{borderRadius:100}]}
                />
              </View>
              <Text style={styles.catHeading}>Traditional</Text>
            </View>
          </Pressable>

          {/* minimilistic */}

          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.cardImg}>
                <Image
                  source={require('../image/minimilistic.jpg')}
                  style={[styles.image,{borderRadius:100}]}
                />
              </View>
              <Text style={styles.catHeading}>Minimilistic</Text>
            </View>
          </Pressable>

          {/* Dainty */}

          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.cardImg}>
                <Image
                  source={require('../image/dainty.jpg')}
                  style={[styles.image,{borderRadius:100}]}
                />
              </View>
              <Text style={styles.catHeading}>Dainty</Text>
            </View>
          </Pressable>
          {/* minimilistic */}

          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.cardImg}>
                <Image
                  source={require('../image/coin.png')}
                  style={[styles.image,{borderRadius:100}]}
                />
              </View>
              <Text style={styles.catHeading}>Coins</Text>
            </View>
          </Pressable>

        </ScrollView>
      </View>
    );
  }
}

//Component for Latest Collection
class Collection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: '',
      isLoading: true,
    };
  }

  componentDidMount() {
    this.fetch_home_data();
  }

  fetch_home_data = () => {
    this.setState({ isLoading: true });
    fetch(global.api + 'fetch-home-data', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    })
      .then(response => response.json())
      .then(json => {
        if(json.length>0){
            this.setState({ content: json });
        }
      })
      .catch(error => console.error(error))
      .finally(() => {
        this.setState({ isLoading: false });
      });
  };

  renderItem = ({ item }) => (
    <View>
      {item.product.map(value => {
        return (
          <View>
            {value.picture.map(key => {
              return (
                <Image source={{ uri: value.src }} style={styles.sliderImage} />
              );
            })}
          </View>
        );
      })}
    </View>
  );

  catrgories=({item})=>(
    <View>

    </View>
  )
  
  carousel = ({ item }) => (
    <TouchableOpacity style={[styles.child,]}>
        <ImageBackground
            resizeMode="cover"
            source={{ uri: item.url}}
            style={{
                // width: Dimensions.get('window').width,
                height: 350,
                // alignSelf: 'center',
                width:Dimensions.get('window').width
            }}></ImageBackground>

    </TouchableOpacity>
  );

  render() {
    return (
      
      <View 
      // style={styles.container}
      >
        {this.state.isLoading ? 
          <View>
            <Loaders />
          </View>
        :
        <>
        {this.state.content != ''
          ? 
          <View>
            
            {/* <Category />

            <SwiperFlatList
              style={{ flex: 1}}
              autoplay
              autoplayDelay={2}
              autoplayLoop
              index={0}
              showPagination
              paginationActiveColor='#bc3b3b'
              keyExtractor={item=>item.id}
              renderItem={this.carousel}
              data={images}
              paginationStyleItem={{
                width: 7,
                height: 7,
                marginLeft: 0, 
                marginRight: 5 }}></SwiperFlatList> */}

              {this.state.content.map(value => {
            return (
              <View style={{ backgroundColor: '#fff', marginTop: 7 }}>
                {/* heading */}
                {/* {this.props.content.tab_name} */}
                <Text
                  style={[
                    styles.h3,
                    {
                      marginLeft: 20,
                      fontSize: 18,
                      fontFamily: 'DancingScript-Bold',
                      color: '#bc3b3b',
                      marginTop: 5,
                    },
                  ]}>
                  {value.tab_name}
                </Text>
                {value.tab_name == 'Top Selling' ? (
                  <View
                    style={{ padding: 10, marginTop: 5, flexDirection: 'row',backgroundColor:"#fbfbfb" }}>
                      <ScrollView
                          horizontal={true}
                          showsHorizontalScrollIndicator={false}>
                    {value.product.map(prod => {
                      return (
                        
                          <Pressable onPress={() => this.props.navigation.navigate("Products", { id: prod.id })}>
                            <Image

                              source={{ uri: global.img_url + prod.picture[0].src }}
                              style={styles.sliderImage}
                            />
                          </Pressable>

                        
                      );
                    })}
                    </ScrollView>
                  </View>
                ) : (
                  <View
                    style={{ padding: 5, marginTop: 5, flexDirection: 'row',  }}>
                    <ScrollView
                      horizontal={true}
                      showsHorizontalScrollIndicator={false}>
                      {value.product.map(prod => {
                        return (
                          <View style={{borderRightWidth:1,borderColor:"#f5f5f5"}}>
                            <Pressable
                              onPress={() => this.props.navigation.navigate("Products", { id: prod.id })}>
                              <View style={{ }}>
                                <View
                                  style={[styles.card, { marginRight: 10,}]}>
                                  <Image
                                    source={{ uri: global.img_url + prod.picture[0].src }}
                                    style={{ width: 100, height: 100, }}
                                  />
                                </View>
                                <Text numberOfLines={2} style={[styles.h4, { width:100, marginLeft: 10 }]}>
                                  {prod.name}
                                </Text>
                              </View>
                            </Pressable>
                          </View>
                        );
                      })}
                    </ScrollView>
                  </View>
                )
                }
                
              </View>
            );
          })}
          </View>
        
          : null}
        
          </>
  }
      </View>
    );
  }
}

class Loaders extends Component{
  render(){
    return(
      <View>
         <SkeletonPlaceholder>
              <View>
                <View>
                  <View style={{height:30,width:Dimensions.get('window').width/1.04,alignSelf:"center",marginTop:5}}/>
                  <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
                  <View style={{height:130,width:120,marginTop:5}}/>
                  <View style={{height:130,width:120,marginTop:5}}/>
                  <View style={{height:130,width:120,marginTop:5}}/>
                  </View>
                </View>

                <View>
                  <View style={{height:30,width:Dimensions.get('window').width/1.04,alignSelf:"center",marginTop:10}}/>
                  <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
                  <View style={{height:190,width:185,marginTop:5}}/>
                  <View style={{height:190,width:185,marginTop:5}}/>
                  </View>
                </View>

                <View>
                  <View style={{height:30,width:Dimensions.get('window').width/1.04,alignSelf:"center",marginTop:5}}/>
                  <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
                  <View style={{height:130,width:120,marginTop:5}}/>
                  <View style={{height:130,width:120,marginTop:5}}/>
                  <View style={{height:130,width:120,marginTop:5}}/>
                  </View>
                </View>

                <View>
                  <View style={{height:30,width:Dimensions.get('window').width/1.04,alignSelf:"center",marginTop:5}}/>
                  <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
                  <View style={{height:130,width:120,marginTop:5}}/>
                  <View style={{height:130,width:120,marginTop:5}}/>
                  <View style={{height:130,width:120,marginTop:5}}/>
                  </View>
                </View>


              </View>
            </SkeletonPlaceholder>
      </View>
    )
  }
}
class Gold extends Component {
  render() {
    return (
      <View style={{ backgroundColor: '#fff', marginTop: 7 }}>
        {/* heading */}
        <Text
          style={[
            styles.h3,
            {
              marginLeft: 20,
              fontSize: 18,
              fontFamily: 'DancingScript-Bold',
              color: '#bc3b3b',
              marginTop: 5,
            },
          ]}>
          Gold Jewellery
        </Text>
        <View style={{ padding: 10, flexDirection: 'row' }}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View>
              <Image
                source={require('../image/gold1.jpg')}
                style={styles.sliderImage}
              />
            </View>

            <View>
              <Image
                source={require('../image/gold2.jpg')}
                style={styles.sliderImage}
              />
            </View>

            <View>
              <Image
                source={require('../image/gold3.jpg')}
                style={styles.sliderImage}
              />
            </View>
            <View>
              <Image
                source={require('../image/gold4.jpg')}
                style={styles.sliderImage}
              />
            </View>
            <View>
              <Image
                source={require('../image/gold5.jpg')}
                style={styles.sliderImage}
              />
            </View>
            <View>
              <Image
                source={require('../image/gold6.jpg')}
                style={styles.sliderImage}
              />
            </View>
            <View>
              <Image
                source={require('../image/gold7.jpg')}
                style={styles.sliderImage}
              />
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}
//Component for Shop for
class ShopFor extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={{ backgroundColor: '#fff', marginTop: 7 }}>
        <Text
          style={[
            styles.h3,
            {
              marginLeft: 20,
              fontSize: 18,
              fontFamily: 'DancingScript-Bold',
              color: '#bc3b3b',
              marginTop: 5,
            },
          ]}>
          Shop For
        </Text>

        <View style={{ padding: 10, marginTop: 10, flexDirection: 'row' }}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <Pressable
              onPress={() => this.props.navigation.navigate('ProductList')}>
              <View>
                <View style={styles.card}>
                  <Image
                    source={require('../image/s2.jpg')}
                    style={styles.image}
                  />
                </View>
                <Text style={styles.catHeading}>Earrings</Text>
              </View>
            </Pressable>

            <Pressable
              onPress={() => this.props.navigation.navigate('ProductList')}>
              <View>
                <View style={styles.card}>
                  <Image
                    source={require('../image/minimilistic.jpg')}
                    style={styles.image}
                  />
                </View>
                <Text style={styles.catHeading}>Necklace Sets</Text>
              </View>
            </Pressable>

            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/s4.png')}
                  style={styles.image}
                />
              </View>
              <Text style={styles.catHeading}>Bracelets</Text>
            </View>

            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/s1.png')}
                  style={styles.image}
                />
              </View>
              <Text style={styles.catHeading}>Nose Rings</Text>
            </View>

            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/s2.jpg')}
                  style={styles.image}
                />
              </View>
              <Text style={styles.catHeading}>Bangles</Text>
            </View>

            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/s4.png')}
                  style={styles.image}
                />
              </View>
              <Text style={styles.catHeading}>Anklets</Text>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

//Component for Wedding for
class Party extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={{ backgroundColor: '#fff', marginTop: 7 }}>
        <Text
          style={[
            styles.h3,
            {
              marginLeft: 20,
              fontSize: 18,
              fontFamily: 'DancingScript-Bold',
              color: '#bc3b3b',
              marginTop: 5,
            },
          ]}>
          Party Wear
        </Text>

        <View style={{ padding: 10, flexDirection: 'row' }}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <Pressable
              onPress={() => this.props.navigation.navigate('ProductList')}>
              <View style={[styles.card, { marginHorizontal: -5 }]}>
                <Image
                  source={require('../image/jewel1.jpg')}
                  style={{ height: 200, width: 130, borderRadius: 10 }}
                />
              </View>
            </Pressable>

            <Pressable
              onPress={() => this.props.navigation.navigate('ProductList')}>
              <View style={[styles.card, { marginHorizontal: -5 }]}>
                <Image
                  source={require('../image/jewel2.jpg')}
                  style={{ height: 200, width: 130, borderRadius: 10 }}
                />
              </View>
            </Pressable>
            <Pressable
              onPress={() => this.props.navigation.navigate('ProductList')}>
              <View style={[styles.card, { marginHorizontal: -5 }]}>
                <Image
                  source={require('../image/jewel3.jpg')}
                  style={{ height: 200, width: 130, borderRadius: 10 }}
                />
              </View>
            </Pressable>
            <Pressable
              onPress={() => this.props.navigation.navigate('ProductList')}>
              <View style={[styles.card, { marginHorizontal: -5 }]}>
                <Image
                  source={require('../image/jewel4.jpg')}
                  style={{ height: 200, width: 130, borderRadius: 10 }}
                />
              </View>
            </Pressable>

            <Pressable
              onPress={() => this.props.navigation.navigate('ProductList')}>
              <View style={[styles.card, { marginHorizontal: -5 }]}>
                <Image
                  source={require('../image/jewel5.jpg')}
                  style={{ height: 200, width: 130, borderRadius: 10 }}
                />
              </View>
            </Pressable>
            <Pressable
              onPress={() => this.props.navigation.navigate('ProductList')}>
              <View style={[styles.card, { marginHorizontal: -5 }]}>
                <Image
                  source={require('../image/jewel6.jpg')}
                  style={{ height: 200, width: 130, borderRadius: 10 }}
                />
              </View>
            </Pressable>
            <Pressable
              onPress={() => this.props.navigation.navigate('ProductList')}>
              <View style={[styles.card, { marginHorizontal: -5 }]}>
                <Image
                  source={require('../image/jewel7.jpg')}
                  style={{ height: 200, width: 130, borderRadius: 10 }}
                />
              </View>
            </Pressable>
          </ScrollView>
        </View>
      </View>
    );
  }
}

//Component for Wedding for
class Wedding extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={{ backgroundColor: '#fff', marginTop: 7 }}>
        <Text
          style={[
            styles.h3,
            {
              marginLeft: 20,
              fontSize: 18,
              fontFamily: 'DancingScript-Bold',
              color: '#bc3b3b',
              marginTop: 5,
            },
          ]}>
          Wedding Jewellery
        </Text>

        <View style={{ padding: 10, flexDirection: 'row' }}>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <Pressable
              onPress={() => this.props.navigation.navigate('ProductList')}>
              <View style={[styles.card, { marginHorizontal: -5 }]}>
                <Image
                  source={require('../image/1.jpg')}
                  style={{ height: 200, width: 130, borderRadius: 10 }}
                />
              </View>
            </Pressable>

            <Pressable
              onPress={() => this.props.navigation.navigate('ProductList')}>
              <View style={[styles.card, { marginHorizontal: -5 }]}>
                <Image
                  source={require('../image/2.jpg')}
                  style={{ height: 200, width: 130, borderRadius: 10 }}
                />
              </View>
            </Pressable>
            <Pressable
              onPress={() => this.props.navigation.navigate('ProductList')}>
              <View style={[styles.card, { marginHorizontal: -5 }]}>
                <Image
                  source={require('../image/7.jpg')}
                  style={{ height: 200, width: 130, borderRadius: 10 }}
                />
              </View>
            </Pressable>
            <Pressable
              onPress={() => this.props.navigation.navigate('ProductList')}>
              <View style={[styles.card, { marginHorizontal: -5 }]}>
                <Image
                  source={require('../image/3.jpg')}
                  style={{ height: 200, width: 130, borderRadius: 10 }}
                />
              </View>
            </Pressable>

            <Pressable
              onPress={() => this.props.navigation.navigate('ProductList')}>
              <View style={[styles.card, { marginHorizontal: -5 }]}>
                <Image
                  source={require('../image/8.jpg')}
                  style={{ height: 200, width: 130, borderRadius: 10 }}
                />
              </View>
            </Pressable>
            <Pressable
              onPress={() => this.props.navigation.navigate('ProductList')}>
              <View style={[styles.card, { marginHorizontal: -5 }]}>
                <Image
                  source={require('../image/5.jpg')}
                  style={{ height: 200, width: 130, borderRadius: 10 }}
                />
              </View>
            </Pressable>
            <Pressable
              onPress={() => this.props.navigation.navigate('ProductList')}>
              <View style={[styles.card, { marginHorizontal: -5 }]}>
                <Image
                  source={require('../image/6.jpg')}
                  style={{ height: 200, width: 130, borderRadius: 10 }}
                />
              </View>
            </Pressable>
          </ScrollView>
        </View>
      </View>
    );
  }
}

//Component for Shop for
class BestRings extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={{ backgroundColor: '#fff', marginTop: 7 }}>
        <Text
          style={[
            styles.h3,
            {
              marginLeft: 20,
              fontSize: 18,
              fontFamily: 'DancingScript-Bold',
              color: '#bc3b3b',
              marginTop: 5,
            },
          ]}>
          Best Rings
        </Text>

        <View
          style={{
            justifyContent: 'space-between',
            marginHorizontal: 10,
            marginTop: 10,
            flexDirection: 'row',
          }}>
          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/greenRing.jpg')}
                  style={[
                    styles.image,
                    { height: 90, width: 90, borderRadius: 50 },
                  ]}
                />
              </View>
              <Text style={styles.catHeading}>Green Ring</Text>
            </View>
          </Pressable>

          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/blueRing.jpg')}
                  style={[
                    styles.image,
                    { height: 90, width: 90, borderRadius: 50 },
                  ]}
                />
              </View>
              <Text style={styles.catHeading}>Blue Ring</Text>
            </View>
          </Pressable>

          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/engage.png')}
                  style={[
                    styles.image,
                    { height: 90, width: 90, borderRadius: 50 },
                  ]}
                />
              </View>
              <Text style={styles.catHeading}>Engagement Ring</Text>
            </View>
          </Pressable>
        </View>

        <View
          style={{
            justifyContent: 'space-between',
            marginHorizontal: 10,
            marginTop: 10,
            alignContent: 'center',
            flexDirection: 'row',
            marginBottom: 15,
          }}>
          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/blueRing.jpg')}
                  style={[
                    styles.image,
                    { height: 90, width: 90, borderRadius: 50 },
                  ]}
                />
              </View>
              <Text style={styles.catHeading}>Blue Ring</Text>
            </View>
          </Pressable>

          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/engage.png')}
                  style={[
                    styles.image,
                    { height: 90, width: 90, borderRadius: 50 },
                  ]}
                />
              </View>
              <Text style={styles.catHeading}>Engagement Ring</Text>
            </View>
          </Pressable>

          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/greenRing.jpg')}
                  style={[
                    styles.image,
                    { height: 90, width: 90, borderRadius: 50 },
                  ]}
                />
              </View>
              <Text style={styles.catHeading}>Green Ring</Text>
            </View>
          </Pressable>
        </View>
      </View>
    );
  }
}

//Component for Shop for
class NeckWear extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={{ backgroundColor: '#fff', marginTop: 7 }}>
        <Text
          style={[
            styles.h3,
            {
              marginLeft: 20,
              fontSize: 18,
              fontFamily: 'DancingScript-Bold',
              color: '#bc3b3b',
              marginTop: 5,
            },
          ]}>
          Neckwear
        </Text>

        <View
          style={{
            justifyContent: 'space-between',
            marginHorizontal: 10,
            marginTop: 10,
            flexDirection: 'row',
          }}>
          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/blueneck.jpg')}
                  style={[
                    styles.image,
                    { height: 130, width: 100, borderRadius: 5 },
                  ]}
                />
              </View>
              <Text style={styles.catHeading}>Diamond Necklace</Text>
            </View>
          </Pressable>

          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/whiteneck.jpg')}
                  style={[
                    styles.image,
                    { height: 130, width: 100, borderRadius: 5 },
                  ]}
                />
              </View>
              <Text style={styles.catHeading}>Pendant</Text>
            </View>
          </Pressable>

          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/stoneneck.jpg')}
                  style={[
                    styles.image,
                    { height: 130, width: 100, borderRadius: 5 },
                  ]}
                />
              </View>
              <Text style={styles.catHeading}>Dainty</Text>
            </View>
          </Pressable>
        </View>

        <View
          style={{
            justifyContent: 'space-between',
            marginHorizontal: 10,
            marginTop: 10,
            flexDirection: 'row',
          }}>
          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/stoneneck.jpg')}
                  style={[
                    styles.image,
                    { height: 130, width: 100, borderRadius: 5 },
                  ]}
                />
              </View>
              <Text style={styles.catHeading}>Dainty</Text>
            </View>
          </Pressable>

          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/blueneck.jpg')}
                  style={[
                    styles.image,
                    { height: 130, width: 100, borderRadius: 5 },
                  ]}
                />
              </View>
              <Text style={styles.catHeading}>Diamond</Text>
            </View>
          </Pressable>

          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/whiteneck.jpg')}
                  style={[
                    styles.image,
                    { height: 130, width: 100, borderRadius: 5 },
                  ]}
                />
              </View>
              <Text style={styles.catHeading}>Pendant</Text>
            </View>
          </Pressable>
        </View>
      </View>
    );
  }
}

class Recommended extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoading: true,
    };
  }
  componentDidMount() {
    this.fetch_products();
  }

  fetch_products = () => {
    fetch(global.api + 'get-product-list', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        category_id: 666,
      }),
    })
      .then(response => response.json())
      .then(json => {
        console.warn(json);
        if (json.data.data.length > 0) {
          if (json.status) {
            this.setState({ data: json.data.data });
          } else {
            Toast.show('No data found');
          }
        }
        return json;
      })
      .catch(error => console.error(error))
      .finally(() => {
        this.setState({ isLoading: false });
      });
  };

  productCard = ({ item }) => (
    <View
      style={{
        padding: 7,
        justifyContent: 'space-between',
        flexDirection: 'row',

      }}>
      <Pressable
        onPress={() =>
          this.props.navigation.navigate('Products', { id: item.id })
        }>
        <View style={[styles.card1, { width: 120 }]}>
          <View style={{ flexDirection: 'column' }}>
            <Image
              source={require('../image/blueearring.png')}
              style={[styles.recommendedImage, { height: 150, width: 100 }]}
            />
            <View style={{ paddingLeft: 10, top: 5, paddingRight: 10 }}>
              <Text style={[styles.p, { fontWeight: 'bold', fontSize: 13 }]}>
                ₹ {item.price}
              </Text>
              <Text style={[styles.h5]}>{item.name}</Text>
              <Text style={[styles.h5]}>Necklace</Text>
            </View>
          </View>
        </View>
      </Pressable>
    </View>
  );
  render() {
    return (
      <View style={{ backgroundColor: '#fff', marginTop: 7, marginBottom: 7 }}>
        {/* heading */}
        <Text
          style={[
            styles.h3,
            {
              marginLeft: 20,
              fontSize: 18,
              fontFamily: 'DancingScript-Bold',
              color: '#bc3b3b',
              marginTop: 5,
            },
          ]}>
          Recommended For You
        </Text>

        {/* card 1 */}
        {this.state.isLoading ? (
          <View style={{ padding: 50 }}>
            <ActivityIndicator size={25} color="#bc3b3b" />
          </View>
        ) : (
          <FlatList
            horizontal
            navigation={this.props.navigation}
            showsHorizontalScrollIndicator={false}
            data={this.state.data}
            renderItem={this.productCard}
            keyExtractor={item => item.id}
          />
        )}
      </View>
    );
  }
}

class Last extends Component {
  render() {
    return (
      <View>
        <Image
          source={require('../image/logo/dashboardlogo.png')}
          style={style.logo}
        />
        {/* view for card */}
        <View style={style.card}>
          <Text style={[styles.h6, { color: '#222222', alignSelf: 'center' }]}>
            "Prices are lower than traditional luxury brands, meaning
          </Text>
          <Text style={[styles.h6, { color: '#222222', alignSelf: 'center' }]}>
            you can stock up on the prices you love"
          </Text>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              top: 10,
            }}>
            <Image source={require('../image/icon1.png')} style={style.img} />
            <Image source={require('../image/icon2.png')} style={style.img2} />
            <Image source={require('../image/icon3.png')} style={style.img2} />
            <Image source={require('../image/icon4.png')} style={style.img} />
          </View>
        </View>
        {/* <View style={{backgroundColor:"#fff",marginTop:-50}}>
                    <Text style={[styles.h3,{marginLeft:20,marginTop:60}]}>
                        Locate a Store
                    </Text> */}

        {/* View for search input */}
        {/* <View style={{backgroundColor:"#fff"}}>
                       <View style={{backgroundColor:"#fff",flexDirection:"row"}}>
                       <Searchbar
                        style={style.search}
                        placeholder="Enter City or Pincode"
                        inputStyle={{
                            color:"#BC3B3B",
                            fontFamily: "Montserrat-SemiBold",
                            paddingTop:5,
                            fontSize:RFValue(11,580)
                        }}
                        icon={()=><Icon name="search-outline" type="ionicon"
                        onPress={()=>this.onClick()} size={25}
                        color="#7D7E7F"/>}
                        />

                        <TouchableOpacity style={style.goButton}>
                            <Text style={style.goButtonText}>
                                Go
                            </Text>
                        </TouchableOpacity>
                       </View>

                       <Text style={[styles.catHeading,{fontSize:RFValue(14,580),
                        top:10,bottom:10}]}>
                           OR
                       </Text>

                       <View style={{top:10,marginBottom:70}}>
                        
                        <TouchableOpacity style={styles.locationButton}
                        onPress={()=>this.props.navigation.navigate("ChangeAddress")}>
                            <Image source={require('../image/location.png')}
                            style={{height:30,width:30,alignSelf:"center",left:7}}/>
                            <Text style={style.text}>Use my location</Text>
                        </TouchableOpacity>
                       </View>

                    </View>
                    
                </View> */}
      </View>
    );
  }
}

//Internal Styling
const style = StyleSheet.create({
  p: {
    // fontFamily:"Raleway-Regular",
    fontFamily: 'Montserrat-SemiBold',
    top: -6,
    fontSize: RFValue(12, 580),
    color: '#D19292',
  },
  logo: {
    height: 70,
    width: Dimensions.get('window').width / 2,
    alignSelf: 'center',
    marginTop: 10
  },
  card: {
    backgroundColor: '#fff',
    padding: 10,
    marginBottom: 10,
    shadowRadius: 50,
    width: Dimensions.get('window').width / 1.1,
    shadowOffset: { width: 50, height: 50 },
    elevation: 2,
    borderRadius: 15,
    alignSelf: 'center',
    marginTop: 10,
    marginBottom: 50,
  },
  img: {
    height: 120,
    width: 65,
  },
  img2: {
    height: 120,
    width: 72,
  },
  search: {
    marginTop: 15,
    marginBottom: 5,
    elevation: 0,
    borderWidth: 2,
    height: 45,
    fontSize: RFValue(9, 580),
    borderColor: '#C2C3C3',
    borderRadius: 5,
    // alignSelf:"center",
    width: Dimensions.get('window').width / 1.4,
    marginLeft: 20,
    fontFamily: 'Montserrat-Bold',
  },
  goButton: {
    backgroundColor: '#BC3B3B',
    height: 45,
    width: 45,
    justifyContent: 'center',
    borderRadius: 5,
    marginLeft: 20,
    marginTop: 15,
  },
  goButtonText: {
    alignSelf: 'center',
    top: -3,
    color: '#fff',
    fontSize: RFValue(12, 580),
    // fontFamily:'Raleway-SemiBold',
    fontFamily: 'Montserrat-Bold',
  },
  text: {
    fontSize: RFValue(11, 580),
    alignSelf: 'center',
    left: 20,
    color: '#7c7d7e',
    fontFamily: 'Montserrat-SemiBold',
  },
});
