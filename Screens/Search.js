import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  Pressable,
  FlatList,
  PlatformColor,
  Platform,
} from 'react-native'
import {Header, Icon} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import RBSheet from 'react-native-raw-bottom-sheet';
import {RFValue} from 'react-native-responsive-fontsize';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import DropButton from '../Components/DropButton.js';
import {Searchbar} from 'react-native-paper';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {TextInput} from 'react-native-gesture-handler';
import Toast from 'react-native-simple-toast';

//Global StyleSheet Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchdata: [],
      search: '',
      nosearch: false,
      picture:[]
    };
  }

  componentDidMount() {
    this.textInputRef.focus();
    this.fetch_data();
    this.textInputRef.focus();
    this.focusListener=this.props.navigation.addListener('focus', ()=>{
            this.setState({searchdata:[]}) 
        })
  }

  fetch_data = () => {
    this.setState({nosearch: false});
    fetch(global.api + 'search-product', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        search: '',
      }),
    })
      .then(response => response.json())
      .then(json => {
        // console.warn(json)
        if (json.status) {
          this.setState({searchdata: json.data});
         
        } else {
          this.setState({searchdata: []});
        }
        return json;
      })
      .catch(error => {
        console.error(error);
      })
      .finally(() => {
        this.setState({isLoading: false});
      });
  };

  search = e => {
    // this.setState({search: e});
    fetch(global.api + 'search-product', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        search: e,
      }),
    })
      .then(response => response.json())
      .then(json => {
        if (json.status) {
          this.setState({searchdata: json.data});
          json.data.map((value)=>{
           this.setState({picture:value.picture.src})
           console.warn("aa",this.state.picture)
          })
        } else {
          this.setState({searchdata: []});
        }
        return json;
      })
      .catch(error => {
        console.error(error);
      })
      .finally(() => {
        this.setState({isLoading: false, nosearch: true});
      });
  };
  renderLeftComponent() {
    return (
      <View style={{flexDirection: 'row', width: win.width}}>
        <View style={{padding:5,top:Platform.OS == "android" ? 2 : 7}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
        </View>
        <TextInput
          ref={ref => (this.textInputRef = ref)}
          autoFocus={true}
          onChangeText={e => {this.search(e),this.setState({search:e})}}
          value={this.state.search}
          style={{color: '#222222', height: 45}}
          placeholder="Search for brands and products"
          // returnKeyType='done'
        />
      </View>
    );
  }

  // Search products list
  searchCard = ({item}) => (
    <>
    {this.state.search == '' ?
    <></>
    :
    <TouchableOpacity
    onPress={() => this.props.navigation.navigate('Products', {id: item.id})}
    style={{
      backgroundColor: '#fff',
      paddingHorizontal: 20,
      flexDirection: 'row',
    }}>
        {item.picture.length > 0 ?
        <Image
        source={{uri:global.img_url+item.picture[0].src}}
        style={{marginTop: 10, borderRadius: 50, width: 40, height: 40}}
        />
      :
      <></> }
    <Text style={[styles.h4, {marginTop: 20, marginLeft: 10}]}>
      {item.name}
    </Text>
  </TouchableOpacity>
    }
    </>
   
  );

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#fff'}}>
        {/* View for header component */}
        <View>
          <Header
            containerStyle={{height: 82}}
            statusBarProps={{barStyle: 'light-content'}}
            leftComponent={this.renderLeftComponent()}
            ViewComponent={LinearGradient} // Don't forget this!
            linearGradientProps={{
              colors: ['white', 'white'],
              start: {x: 0, y: 0.5},
              end: {x: 1, y: 0.5},
            }}
          />
        </View>
        {this.state.nosearch ? (
          <FlatList
            navigation={this.props.navigation}
            showsVerticalScrollIndicator={false}
            data={this.state.searchdata}
            renderItem={this.searchCard}
            keyExtractor={item => item.id}
          />
        ) : null}
        <ScrollView>
          {/* <RecentSearches navigation={this.props.navigation} /> */}
          <Trendings navigation={this.props.navigation} />
          <Interested navigation={this.props.navigation} />
          {/* <NeckWear navigation={this.props.navigation} /> */}
        </ScrollView>
      </View>
    );
  }
}
export default Search;

class RecentSearches extends Component {
  render() {
    return (
      <View
        style={{
          borderBottomWidth: 1,
          borderColor: '#dedede',
          backgroundColor: '#fff',
        }}>
        <Text
          style={[
            styles.h3,
            {
              fontSize: 14,
              marginLeft: 10,
              marginTop: 10,
              fontFamily: 'Raleway-Medium',
              color: '#222222',
            },
          ]}>
          Recent Searches
        </Text>
        <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
          <View style={{flexDirection: 'row', marginBottom: 10}}>
            {/* Others Stories */}
            <View>
              <TouchableOpacity
                style={[style.story, {padding: 0}]}
                onPress={() => this.props.navigation.navigate('ProductList')}>
                <Image
                  source={require('../image/greenRing.jpg')}
                  style={style.storyimg}
                />
              </TouchableOpacity>
              <Text
                style={[
                  styles.h5,
                  {
                    color: '#222222',
                    fontSize: RFValue(10, 580),
                    alignSelf: 'center',
                    left: 5,
                    marginTop: 3,
                  },
                ]}>
                Rings
              </Text>
            </View>

            <View>
              <TouchableOpacity
                style={[style.story, {padding: 0}]}
                onPress={() => this.props.navigation.navigate('ProductList')}>
                <Image
                  source={require('../image/bracelets.jpg')}
                  style={style.storyimg}
                />
              </TouchableOpacity>
              <Text
                style={[
                  styles.h5,
                  {
                    color: '#222222',
                    fontSize: RFValue(10, 580),
                    alignSelf: 'center',
                    left: 5,
                    marginTop: 3,
                  },
                ]}>
                Bracelets
              </Text>
            </View>

            <View>
              <TouchableOpacity
                style={[style.story, {padding: 0}]}
                onPress={() => this.props.navigation.navigate('ProductList')}>
                <Image
                  source={require('../image/necklaces.png')}
                  style={style.storyimg}
                />
              </TouchableOpacity>
              <Text
                style={[
                  styles.h5,
                  {
                    color: '#222222',
                    fontSize: RFValue(10, 580),
                    alignSelf: 'center',
                    left: 5,
                    marginTop: 3,
                  },
                ]}>
                Necklaces
              </Text>
            </View>

            <View>
              <TouchableOpacity
                style={[style.story, {padding: 0}]}
                onPress={() => this.props.navigation.navigate('ProductList')}>
                <Image
                  source={require('../image/stonering.jpg')}
                  style={style.storyimg}
                />
              </TouchableOpacity>
              <Text
                style={[
                  styles.h5,
                  {
                    color: '#222222',
                    fontSize: RFValue(10, 580),
                    alignSelf: 'center',
                    left: 5,
                    marginTop: 3,
                  },
                ]}>
                rings
              </Text>
            </View>

            <View>
              <TouchableOpacity
                style={[style.story, {padding: 0}]}
                onPress={() => this.props.navigation.navigate('ProductList')}>
                <Image
                  source={require('../image/s2.jpg')}
                  style={style.storyimg}
                />
              </TouchableOpacity>
              <Text
                style={[
                  styles.h5,
                  {
                    color: '#222222',
                    fontSize: RFValue(10, 580),
                    alignSelf: 'center',
                    left: 5,
                    marginTop: 3,
                  },
                ]}>
                Earrings
              </Text>
            </View>
            <View>
              <TouchableOpacity
                style={[style.story, {padding: 0}]}
                onPress={() => this.props.navigation.navigate('ProductList')}>
                <Image
                  source={require('../image/bracelets.jpg')}
                  style={style.storyimg}
                />
              </TouchableOpacity>
              <Text
                style={[
                  styles.h5,
                  {
                    color: '#222222',
                    fontSize: RFValue(10, 580),
                    alignSelf: 'center',
                    left: 5,
                    marginTop: 3,
                  },
                ]}>
                Bracelets
              </Text>
            </View>

            <View>
              <TouchableOpacity
                style={[style.story, {padding: 0}]}
                onPress={() => this.props.navigation.navigate('ProductList')}>
                <Image
                  source={require('../image/necklaces.png')}
                  style={style.storyimg}
                />
              </TouchableOpacity>
              <Text
                style={[
                  styles.h5,
                  {
                    color: '#222222',
                    fontSize: RFValue(10, 580),
                    alignSelf: 'center',
                    left: 5,
                    marginTop: 3,
                  },
                ]}>
                Necklaces
              </Text>
            </View>

            <View>
              <TouchableOpacity
                style={[style.story, {padding: 0}]}
                onPress={() => this.props.navigation.navigate('ProductList')}>
                <Image
                  source={require('../image/stonering.jpg')}
                  style={style.storyimg}
                />
              </TouchableOpacity>
              <Text
                style={[
                  styles.h5,
                  {
                    color: '#222222',
                    fontSize: RFValue(10, 580),
                    alignSelf: 'center',
                    left: 5,
                    marginTop: 3,
                  },
                ]}>
                Rings
              </Text>
            </View>

            <View>
              <TouchableOpacity
                style={[style.story, {padding: 0}]}
                onPress={() => this.props.navigation.navigate('ProductList')}>
                <Image
                  source={require('../image/s2.jpg')}
                  style={style.storyimg}
                />
              </TouchableOpacity>
              <Text
                style={[
                  styles.h5,
                  {
                    color: '#222222',
                    fontSize: RFValue(10, 580),
                    alignSelf: 'center',
                    left: 5,
                    marginTop: 3,
                  },
                ]}>
                Earrings
              </Text>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

class Trendings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }
  componentDidMount() {
    this.fetch_products();
  }

  fetch_products = () => {
    fetch(global.api + 'get-product-list', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        category_id: 9,
      }),
    })
      .then(response => response.json())
      .then(json => {
        console.warn(json);
        if (json.data.data.length > 0) {
          if (json.status) {
            this.setState({data: json.data.data});
          } else {
            Toast.show('No data found');
          }
        }
        return json;
      })
      .catch(error => console.error(error))
      .finally(() => {
        this.setState({isLoading: false});
      });
  };

  productCard = ({item}) => (
    <View
      style={{
        padding: 10,
        justifyContent: 'space-between',
        flexDirection: 'row',
      }}>
      <Pressable
        onPress={() =>
          this.props.navigation.navigate('Products', {id: item.id})
        }>
        <View style={[styles.card1, {width: 120}]}>
          <View style={{flexDirection: 'column'}}>
            <Image
              source={{uri:global.img_url+item.picture[0].src}}
              style={[styles.recommendedImage, {height: 130, width: 130}]}
            />
            <View style={{paddingLeft: 10, top: 5, paddingRight: 10}}>
              <Text style={[styles.p, {fontWeight: 'bold', fontSize: 13}]}>
                ₹ {item.price}
              </Text>
              <Text numberOfLines={2} style={[styles.h5]}>{item.name}</Text>
              {/* <Text style={[styles.h5]}>Necklace</Text> */}
            </View>
          </View>
        </View>
      </Pressable>
    </View>
  );
  render() {
    return (
      <View style={{backgroundColor: '#fff', marginTop: 7}}>
        {/* heading */}
        <Text
          style={[
            styles.h3,
            {
              marginLeft: 20,
              fontSize: 15,
              marginLeft: 10,
              fontFamily: 'Raleway-Medium',
              color: '#222222',
              marginTop: 10,
            },
          ]}>
          Popular Products :
        </Text>

        <FlatList
          horizontal
          navigation={this.props.navigation}
          showsHorizontalScrollIndicator={false}
          data={this.state.data}
          renderItem={this.productCard}
          keyExtractor={item => item.id}
        />
      </View>
    );
  }
}

class Interested extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoading: true,
    };
  }

  componentDidMount() {
    this.get_category();
    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.get_category();
    });
  }

  // Fetching user details
  get_category = () => {
    fetch(global.api + 'get-jwellery-category', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    })
      .then(response => response.json())
      .then(json => {
        if (json.status) {
          this.setState({data: json.data});
          console.warn("abcf",this.state.data);
        } else {
          Toast.show('No data found');
        }
        return json;
      })
      .catch(error => console.error(error))
      .finally(() => {
        this.setState({isLoading: false});
      });
  };

  render() {
    let cat = this.state.data.slice(0,4).map((cat, id) => {
      return (
        <View>
            
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('ProductList',{id:cat.id,name:cat.name})}
            style={{flexDirection: 'row'}}>
            <View style={[style.story, {height: 50, width: 50, elevation: 0}]}>
              <Image
                source={{uri:global.img_url+cat.product[0].picture[0].src}}
                style={[style.storyimg, {height: 50, width: 50}]}
              />
            </View>
            <Text style={[styles.h4, {alignSelf: 'center', marginLeft: 20}]}>
              {cat.name}
            </Text>
          </TouchableOpacity>
        </View>
      );
    });
    return (
      <View style={{backgroundColor: '#fff', marginTop: 7}}>
        {/* heading */}
        <Text
          style={[
            styles.h3,
            {
              marginLeft: 20,
              fontSize: 15,
              marginLeft: 10,
              fontFamily: 'Raleway-Medium',
              color: '#222222',
              marginTop: 10,
            },
          ]}>
          You may be interested in :
        </Text>

        {cat}
        {/* <FlatList
          navigation={this.props.navigation}
          data={this.state.data}
          renderItem={this.categoryCard}
          keyExtractor={item => item.id}
        /> */}
      </View>
    );
  }
}
//Component for Shop for
class NeckWear extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={{backgroundColor: '#fff', marginTop: 7, paddingBottom: 10}}>
        <Text
          style={[
            styles.h3,
            {
              marginLeft: 20,
              fontSize: 15,
              marginLeft: 10,
              fontFamily: 'Raleway-Medium',
              color: '#222222',
              marginTop: 10,
            },
          ]}>
          Best Neckwears
        </Text>

        <View
          style={{
            justifyContent: 'space-between',
            marginHorizontal: 10,
            marginTop: 10,
            flexDirection: 'row',
          }}>
          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/blueneck.jpg')}
                  style={[
                    styles.image,
                    {height: 130, width: 100, borderRadius: 5},
                  ]}
                />
              </View>
              <Text style={styles.catHeading}>Diamond Necklace</Text>
            </View>
          </Pressable>

          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/whiteneck.jpg')}
                  style={[
                    styles.image,
                    {height: 130, width: 100, borderRadius: 5},
                  ]}
                />
              </View>
              <Text style={styles.catHeading}>Pendant</Text>
            </View>
          </Pressable>

          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/stoneneck.jpg')}
                  style={[
                    styles.image,
                    {height: 130, width: 100, borderRadius: 5},
                  ]}
                />
              </View>
              <Text style={styles.catHeading}>Dainty</Text>
            </View>
          </Pressable>
        </View>

        <View
          style={{
            justifyContent: 'space-between',
            marginHorizontal: 10,
            marginTop: 10,
            flexDirection: 'row',
          }}>
          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/stoneneck.jpg')}
                  style={[
                    styles.image,
                    {height: 130, width: 100, borderRadius: 5},
                  ]}
                />
              </View>
              <Text style={styles.catHeading}>Dainty</Text>
            </View>
          </Pressable>

          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/blueneck.jpg')}
                  style={[
                    styles.image,
                    {height: 130, width: 100, borderRadius: 5},
                  ]}
                />
              </View>
              <Text style={styles.catHeading}>Diamond</Text>
            </View>
          </Pressable>

          <Pressable
            onPress={() => this.props.navigation.navigate('ProductList')}>
            <View>
              <View style={styles.card}>
                <Image
                  source={require('../image/whiteneck.jpg')}
                  style={[
                    styles.image,
                    {height: 130, width: 100, borderRadius: 5},
                  ]}
                />
              </View>
              <Text style={styles.catHeading}>Pendant</Text>
            </View>
          </Pressable>
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
  story: {
    // backgroundColor:"#fedda3",
    padding: 7,
    marginTop: 10,
    borderRadius: 30,
    width: 60,
    height: 60,
    elevation: 1,
    justifyContent: 'center',
    marginLeft: 16,
  },
  img: {
    width: '100%',
    height: '100%',
  },
  storyimg: {
    width: '100%',
    height: '100%',
    borderRadius: 30,
  },
});
