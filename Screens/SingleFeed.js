import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  ActivityIndicator,
  FlatList,
  Pressable,
  TextInput,
  Button,
  StyleSheet,
  TouchableOpacity,
  BackHandler,
  ImageBackground,Dimensions
} from 'react-native';
import RBSheet from 'react-native-raw-bottom-sheet';
import Toast from 'react-native-simple-toast';
import {RFValue} from 'react-native-responsive-fontsize';
import {Icon, Header} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';
import Share from 'react-native-share';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const styles = require('../Components/Style');

const win = Dimensions.get('window');

const options = {
  title: 'Pick an Image',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class SingleFeed extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name:'',
      image: '',
      data: [],
      object: {},
      save: {},
      like: {},
      follow: {},
      isLoading: false,
      id: '',
      post: '',
      feed_content:[],

    };
  }
  
  componentDidMount =()=>{
   this.fetch_feeds();
  }

  fetch_feeds = () => {
    this.setState({isLoading:true})
   fetch(global.api + 'get_single_feed_user', {
     method: 'POST',
     headers: {
       Accept: 'application/json',
       'Content-Type': 'application/json',
       Authorization: global.token,
     },
     body:JSON.stringify({   
          feed_id:this.props.route.params.feed_id
      })}).then(response => response.json())
     .then(json => {
       console.warn(json)
       if (json.status) {
         this.setState({ data: json.data });
         console.warn(json.data);
         json.data.map((value, key) => {
           const object = this.state.object;
           const save = this.state.save;
           const like = this.state.like;
           // const follow = this.state.like;
           like[value.id] = value.feed_like_count;
           if (value.feed_like == null) {
             object[value.id] = false;
           } else {
             object[value.id] = true;
           }

           // if (value.user_follow == null) {
           //   follow[value.user_id] = false;
           // } else {
           //   follow[value.user_id] = true;
           // }

           if (value.feed_save == null) {
             save[value.id] = false;
           } else {
             save[value.id] = true;
           }

           this.setState({ object });
           // this.setState({follow});
           this.setState({ save });
           this.setState({ like });
         });
       } else {
         Toast.show('No data found');
       }
       return json;
     })
     .catch(error => console.error(error))
     .finally(() => {
       this.setState({ isLoading: false, load_more: false,});
     });
 };

  //function for share
  myShare = async (title, content, url) => {
    const shareOptions = {
      title: title,
      message: content,
      url: url,
    }
    try {
      const ShareResponse = await Share.open(shareOptions);

    } catch (error) {
      console.log("Error=>", error)
    }
  }
 

  // feed like
  like = id => {
    const object = this.state.object;
    const like = this.state.like;
    if (this.state.object[id] == true) {
      object[id] = false;
      like[id] = like[id] - 1;
      var type = 'no';
    } else {
      object[id] = true;
      like[id] = like[id] + 1;
      var type = 'yes';
    }
    this.setState({like});
    this.setState({object});

    fetch(global.api + 'feed-like', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        feed_id: id,
        type: type,
      }),
    })
      .then(response => response.json())
      .then(json => {
        if (!json.status) {
          Toast.show("Something went wrong.");
        }
        else{
          // Toast.show(json.msg)
        }
      });
  };

  // feed save
  bookmark = id => {
    const save = this.state.save;
    if (this.state.save[id] == true) {
      save[id] = false;
      var type = 'unsave';
    } else {
      save[id] = true;
      var type = 'save';
    }
    this.setState({save});

    fetch(global.api + 'user-feed-save', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        feed_id: id,
        type: type,
      }),
    })
      .then(response => response.json())
      .then(json => {
        // console.log(json)

        if (!json.status) {
          Toast.show(json.msg);
        }
      });
  };

  // report feed
  report = () => {
    this.RBSheet.close();
    fetch(global.api + 'feed-report-user', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        feed_id: this.state.id,
        report: 'Report this post',
      }),
    })
      .then(response => response.json())
      .then(json => {
        console.warn(json);
        if (!json.status) {
          Toast.show(json.msg);
        } else {
          Toast.show(json.msg);
          this.fetch_feeds();
        }
      });
  };


  // Bottom sheet
  sheet(id, feed_index) {
    this.setState({id: id, feed_index: feed_index});
    this.RBSheet.open();
  }

  productCard = ({item}) => (
    <View style={style.card}>
      {/* Card Header */}
      <View style={style.cardHeader}>
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('UserProfile',{id:item.user_id})}>
          <View style={{flexDirection: 'row'}}>
            {/* logo */}
            <>
            {(item.user_profile_pic == null) ?
              <Image
              source={require('../image/dummyuser.jpg')}
              style={style.profileImage}
            />
           :
            <Image
                source={{uri: global.image_url + item.user_profile_pic}}
                style={style.profileImage}
              />
            }
            </>
            {/* name and time */}
            <View style={{flexDirection: 'column', paddingLeft: 10}}>
              <Text style={style.name}>{item.user_name}</Text>
              <Text style={style.postTime}>
                {moment
                  .utc(item.created_at)
                  .local()
                  .startOf('seconds')
                  .fromNow()}
              </Text>
            </View>
          </View>
        </TouchableOpacity>

        {/* # dots icon for bottm sheet */}
        <View style={{flexDirection: 'row'}}>
          <Text
            style={{alignContent: 'flex-end', top: 5, left: 5}}
            onPress={() => this.sheet(item.id)}>
            <Icon
              name="ellipsis-vertical"
              type="ionicon"
              color="#7c7d7e"
              size={25}
            />
          </Text>
        </View>
      </View>

      {/* Bottom Sheet for Post options */}

      <RBSheet
        ref={ref => {
          this.RBSheet = ref;
        }}
        // animationType="fade"
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={160}
        customStyles={{
          container: {
            borderTopRightRadius: 20,
            borderTopLeftRadius: 20,
          },
          draggableIcon: {
            backgroundColor: '',
          },
        }}>
        {/* bottom sheet elements */}
        <View>
          {/* new container search view */}
          <View>
            {/* to share */}
            <View style={{flexDirection: 'row', padding: 10}}>
              <TouchableOpacity
                style={{flexDirection: 'row'}}
                onPress={()=>this.myShare(item.feed_description,'Checkout Our Latest Update',global.shareLink+'/feedView/'+item.id)}
              >
                <View
                  style={{
                    backgroundColor: '#f5f5f5',
                    height: 40,
                    width: 40,
                    justifyContent: 'center',
                    borderRadius: 50,
                  }}>
                  <Icon type="ionicon" name="share-social" />
                </View>
                <Text
                  style={[styles.h4, {alignSelf: 'center', marginLeft: 20}]}>
                  Share
                </Text>
              </TouchableOpacity>
            </View>

            {/* to report */}
            <View style={{flexDirection: 'row', padding: 10}}>
              <TouchableOpacity
                style={{flexDirection: 'row'}}
                onPress={() => this.report()}>
                <View
                  style={{
                    backgroundColor: '#f5f5f5',
                    height: 40,
                    width: 40,
                    justifyContent: 'center',
                    borderRadius: 50,
                  }}>
                  <Icon type="ionicon" name="trash-bin" />
                </View>
                <Text
                  style={[styles.h4, {alignSelf: 'center', marginLeft: 20}]}>
                  Report
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </RBSheet>

      {/* Image */}
      <View>
        {
          item.feed_content.map((feed)=>{
            return(
              <ImageBackground
              source={{
                uri:
                  'https://demo.webixun.com/KamalJwellersApi/public/' +
                  feed.content_src,
              }}
              style={style.postImage}
              PlaceholderContent={
                <ActivityIndicator size="small" color="#0000ff" />
              }>
             
            </ImageBackground>
            )
          })
        }
       
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View style={{flexDirection: 'row'}}>
            <Text style={{margin: 3, marginLeft: 10}}>
              <Icon
                name={this.state.object[item.id] ? 'thumb-up' : 'thumb-up'}
                color={this.state.object[item.id] ? '#BC3B3B' : '#7c7d7e'}
                onPress={() => this.like(item.id)}
                size={25}
              />
            </Text>

            <Text style={{margin: 5}}>
            <Icon
                type="ionicon"
                onPress={() =>
                  this.props.navigation.navigate('Comments', {
                    description: item.feed_description,
                    name: this.state.name,
                    time: item.created_at,
                    id: item.id,
                    pic: this.state.image,
                  })
                }
                name="chatbubble-outline"
                color="#222222"
                size={22}
              />
            </Text>

            <Text style={{margin: 5}}>
              <Icon
                type="ionicon"
                name="share-social-outline"
                color="#222222"
                size={22}
                onPress={()=>this.myShare(item.feed_description,'Checkout Our Latest Update',global.shareLink+'/feedView/'+item.id)}
              />
            </Text>
          </View>

          <View>
            <Text style={{margin: 5, justifyContent: 'flex-end'}}>
              <Icon
                type="ionicon"
                name={
                  this.state.save[item.id] ? 'bookmark' : 'bookmark-outline'
                }
                color={this.state.save[item.id] ? '#BC3B3B' : '#222222'}
                onPress={() => this.bookmark(item.id)}
                size={22}
              />
            </Text>
          </View>
        </View>

        {/* Like counts */}
        
        <View>
          {this.state.like[item.id] > 0 ? (
            <>
              <Text style={{margin: 1, marginLeft: 13}}>
                {this.state.like[item.id]} likes
              </Text>
            </>
          ) : (
            <></>
          )}
        </View>
        <Text
          style={{
            color: '#222222',
            padding: 10,marginTop:-8,
            fontFamily: 'Raleway-Regular',
          }}>
          {item.feed_description}
        </Text>
      </View>

      {/* <View>
                <Icon type="ionicon" name="heart" />
              </View> */}
    </View>
  );

  renderLeftComponent() {
    return (
      <View style={{flexDirection: 'row', width: win.width / 2}}>
        <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
        </View>
        <Text style={[styles.headerHeadingText]}>Post</Text>
      </View>
    );
  }


  render() {
    return (
      <View style={styles.container}>
        {/* View for header component */}
        <View>
          <Header
            // containerStyle={{height: 82}}
            statusBarProps={{barStyle: 'light-content'}}
            leftComponent={this.renderLeftComponent()}
            ViewComponent={LinearGradient} // Don't forget this!
            linearGradientProps={{
              colors: ['white', 'white'],
              start: {x: 0, y: 0.5},
              end: {x: 1, y: 0.5},
            }}
          />
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View>
          {this.state.isLoading ? (
          <View>
            <Loaders />
          </View>
        ) : (
          <FlatList
            navigation={this.props.navigation}
            showsVerticalScrollIndicator={false}
            data={this.state.data}
            renderItem={this.productCard}
            keyExtractor={item => item.id}
          />
        )}
            
          </View>
        </ScrollView>

        {/* <Add navigation={this.props.navigation} /> */}
      </View>
    );
  }
}

export default SingleFeed;

class Loaders extends Component{
  render(){
    return(
      <View>
        <SkeletonPlaceholder>
          <View>
            <View style={{flexDirection: 'row', alignItems: 'center',marginLeft:5,marginTop:5}}>
              <View style={{width: 60, height: 60, borderRadius: 50}} />
              <View style={{marginLeft: 10}}>
                <View style={{width: 150, height: 20, borderRadius: 4}} />
                <View style={{marginTop: 5, width: 90, height: 15, borderRadius: 4}}/>
              </View>
            </View>
            <View style={{marginTop: 5, marginBottom: 30,marginLeft:10}}>
              <View style={{ width: 370, height: 200, borderRadius: 4}}/>

              <View style={{flexDirection:"row",marginTop:5}}>
                  <View style={{height:40,width:40,borderRadius:100}}/>
                  <View style={{height:40,width:40,borderRadius:100,marginLeft:5}}/>
                  <View style={{height:40,width:40,borderRadius:100,marginLeft:5}}/>
              </View>

              <View style={{marginTop: 5, width: 350, height: 15, borderRadius: 4}}/>
              <View style={{marginTop:5, width: 250, height: 15, borderRadius: 4}}/>
            </View>
          </View>

        </SkeletonPlaceholder>
      </View>
    )
  }
}


const style = StyleSheet.create({
  card: {
    backgroundColor: '#fff',
    marginBottom: 3,
    borderRadius: 20,
  },
  cardHeader: {
    flexDirection: 'row',
    padding: 5,
    justifyContent: 'space-between',
    // backgroundColor:"red"
  },
  profileImage: {
    height: 45,
    width: 45,
    borderRadius: 30,
  },
  name: {
    // fontFamily:"Raleway-SemiBold",
    fontFamily: 'Raleway-Medium',
    fontSize: RFValue(12, 580),
    color: '#bc3b3b',
  },
  postTime: {
    fontFamily: 'Roboto-Regular',
    // top:-2,
    // fontFamily: "DancingScript-Bold",
    color: 'grey',
    fontSize: RFValue(10, 580),
  },
  postImage: {
    width: Dimensions.get('window').width,
    height: 310,
    // height:"100%"
  },
  followButton: {
    // borderRadius:25,
    // borderColor:"#000",
    // borderWidth:1,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    // padding:5,
    height: 28,
    width: 90,
    top: 7,
    left: 75,
  },
  followButtonText: {
    // fontFamily:"Roboto-SemiBold",
    fontFamily: 'Raleway-Regular',
    fontSize: RFValue(11, 580),
    // alignSelf:"center",
    textAlign: 'center',
    top: -3,
    left: 3,
    color: '#bc3b3b',
  },

  unFollowButtonText: {
    color: '#222222',
    // fontFamily:"Roboto-SemiBold",
    fontFamily: 'Raleway-Regular',
    fontSize: 11,
    // alignSelf:"center",
    textAlign: 'center',
    top: 0,
    // left:3
  },
  shop: {
    backgroundColor: '#bc3b3b',
    padding: 9,
    paddingTop: 6,
    paddingVertical: 5,
    position: 'absolute',
    bottom: 15,
    left: 25,
    borderRadius: 25,
    // borderWidth:0.5,
    // borderColor:"#d3d3d3",
    shadowColor: '#fff',
    shadowOffset: {width: 50, height: 50},
    shadowOpacity: 2,
    shadowRadius: 2,
    elevation: 10,
  },
  add: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 50,
    position: 'absolute',
    bottom: 10,
    right: 10,
    height: 50,
    backgroundColor: '#bc3b3b',
    borderRadius: 100,
  },
});
