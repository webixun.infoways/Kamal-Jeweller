import React, { Component } from 'react'
import { View, Text , ScrollView, Image,Pressable,
    TextInput, Button, StyleSheet,TouchableOpacity, BackHandler, ImageBackground, ActivityIndicator} from 'react-native';
import {Dimensions} from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize'
import { Header,Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import RBSheet from 'react-native-raw-bottom-sheet'
import ThankYou from './ThankYou';
import Toast from 'react-native-simple-toast';

const styles=require("../Components/Style")

const win = Dimensions.get('window');

class PaymentOptions extends Component{
    constructor(props){
        super(props);
        this.state={
            iconName:"radio-button-off",
            iconName1:"radio-button-off",
            iconName2:"radio-button-off",
        }
    }

 

    renderLeftComponent(){
        return(
            <View style={{flexDirection:"row",width:win.width/2,}}>
                <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
                </View>
                <Text style={[styles.headerHeadingText,
                    ]}>Payment</Text>
            </View>
        )
    }
    render(){
        return(
            <View>
                 <View>
                    <Header
                    containerStyle={{height:82}}
                    statusBarProps={{ barStyle: 'light-content' }}
                    leftComponent={this.renderLeftComponent()}
                    ViewComponent={LinearGradient} // Don't forget this!
                    linearGradientProps={{
                    colors: ['white', 'white'],
                    start: { x: 0, y: 0.5 },
                    end: { x: 1, y: 0.5 }
                    }}
                />
                </View>
               
            </View>
        )
    }
}

export default PaymentOptions;

const style=StyleSheet.create({
    heading:{
        fontSize:20,
        marginLeft:10,
        color:"black"
    },
    addressView:{
        flexDirection:"row",
        justifyContent:"space-between",
        borderBottomWidth:1,
        borderColor:"#f5f5f5",
        paddingVertical:10
     },
     pay:{
         paddingHorizontal:20,
         flexDirection:"row",paddingVertical:5,
         justifyContent:"space-between",
          borderRadius:7,marginBottom:10,
           backgroundColor:"#f5f5f5"
        },
        detailsView:{
            flexDirection:"row",
            justifyContent:"space-between",
            paddingHorizontal:25,
            borderBottomWidth:1,
            borderColor:"#f5f5f5",
            paddingVertical:10
         },
         textInput:{
            backgroundColor:"#F2F2F2",
            width:Dimensions.get('window').width/1.2,
            borderRadius:10,
            height:45,
            alignSelf:"center",
            // borderRadius:20,
            marginTop:15,
            paddingLeft:20,
            fontFamily: "Raleway-Regular",
        },
        methodText:{
            fontSize:RFValue(12,580),
            fontFamily: "Raleway-Medium",
            marginBottom:7
        },
        text:{
            color:"#bc3b3b",
            fontSize:RFValue(13,580),
            // top:5,
            fontFamily: "Montserrat-Medium"
        }
}
)
