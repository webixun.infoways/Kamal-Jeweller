import React, { Component } from 'react'
import {View, Image,Text,Pressable, 
    StyleSheet, ScrollView,
    TouchableOpacity,Dimensions, Platform
}  from "react-native";
import LinearGradient from 'react-native-linear-gradient';
import {Icon,Header} from "react-native-elements"
import { RFValue } from 'react-native-responsive-fontsize';
import { AuthContext } from '../AuthContextProvider.js';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-simple-toast';
import navigationChange from '../helpers/index.js';

//Global StyleSheet Import
const style = require('../Components/Style.js');
const win = Dimensions.get('window');

class More extends Component {
    static contextType=AuthContext;
    constructor(props){
        super(props);
        this.state={
            item:""
        }
    }

     //cart item
     componentDidMount=()=>{
         this.my_cart();
        this.focusListener = this.props.navigation.addListener('focus', () => {
            this.my_cart();
                if (navigationChange.isChanged) {
                  navigationChange.isChanged = false;
                  this.setState({ loader: true })
                  this.props.navigation.navigate('Home');
                }
          });
       
  }

  my_cart=()=>{
    fetch(global.api + 'my-cart', {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: global.token,
        },
      })
        .then(response => response.json())
        .then(json => {
          console.warn(json);
          if (json.status) {
            this.setState({item: json.data.length});
          //   alert(this.state.item)
            
          } else {
            this.setState({data: ''});
            this.setState({item: ''});
          }
  
          return json;
        })
        .catch(error => console.error(error))
        .finally(() => {
          this.setState({isLoading: false});
        });
  }

    renderLeftComponent(){
        return(
            <View style={{flexDirection:"row",width:win.width/2}}>
                <Text style={style.headerHeadingText}>
                    More</Text>
            </View>
        )
    }

    renderRightComponent() {
        return (
          <View>
            <Icon
              name="bookmark-outline"
              size={24}
              color="transparent"
              type="ionicon"
              onPress={() => this.props.navigation.navigate('SavedPost')}
              // style={{alignSelf:"center"}}
            />
          </View>
        );
      }

    
    render() {
        return (
            <View style={{flex:1,backgroundColor:"fafafa"}}>

                {/* View for header component */}
                    <View >
                        <Header
                        // containerStyle={{height:82}}
                        statusBarProps={{ barStyle: 'light-content' }}
                        leftComponent={this.renderLeftComponent()}
                        // rightComponent={this.renderRightComponent()}
                        // data={this.props.route.params}
                        ViewComponent={LinearGradient} // Don't forget this!
                        linearGradientProps={{
                        colors: ['white', 'white'],
                        start: { x: 0, y: 0.5 },
                        end: { x: 1, y: 0.5 }
                        }}/>
                    </View>

                <ScrollView showsVerticalScrollIndicator={false} >

                 {/* My Profile card */}
                 <TouchableOpacity 
                 onPress={()=>this.props.navigation.navigate("MyProfile")}
                //  onPress={()=>this.props.navigation.navigate("SingleFeed")}
                 >
                    <View style={styles.cardView}>
                    <Text style={styles.iconView}>
                    <Icon name="person" color="#bc3b3b" size={20} type="ionicon" />
                    </Text>
                        <View style={{width:"90%", flexDirection:"row", justifyContent:"space-between"}}>
                        <Text style={styles.text}>
                        My Profile
                        </Text>
                        <Text style={styles.arrowIconView}>
                            <Icon name="chevron-forward-outline" size={22} type="ionicon" color="#fff" style={{left:5,top:3}} />
                        </Text>
                        </View>
                    </View>
                </TouchableOpacity>
                
                 {/* Wishlist card */}
                 <TouchableOpacity onPress={()=>this.props.navigation.navigate("Wishlist")}>
                    <View style={styles.cardView}>
                    <Text style={styles.iconView}>
                    <Icon name="heart" color="#bc3b3b" size={20} type="ionicon" />
                    </Text>
                        <View style={{width:"90%", flexDirection:"row", justifyContent:"space-between"}}>
                        <Text style={styles.text}>
                        Wishlist
                        </Text>
                        <Text style={styles.arrowIconView}>
                        <Icon name="chevron-forward-outline" size={22} type="ionicon" color="#fff" style={{left:5,top:3}} />
                        </Text>
                        </View>
                    </View>
                </TouchableOpacity>

                {/* Notifications card */}
                
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("Notifications")}>
                <View style={styles.cardView}>
                <Text style={styles.iconView}>
                <Icon name="notifications" color="#bc3b3b" size={20} />
                </Text>
                    <View style={{width:"90%", flexDirection:"row", justifyContent:"space-between"}}>
                    <Text style={styles.text}>
                    Notifications
                    </Text>
                    <Text style={styles.arrowIconView}>
                    <Icon name="chevron-forward-outline" size={22} type="ionicon" color="#fff" style={{left:5,top:3}} />
                    </Text>
                    </View>
                </View>
                </TouchableOpacity>


                

                {/* My Orders card */}
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("MyOrders")}>
                <View style={styles.cardView}>
                <Text style={styles.iconView}>
                <Icon name="briefcase" color="#bc3b3b" size={20} type="ionicon" />
                </Text>
                    <View style={{width:"90%", flexDirection:"row", justifyContent:"space-between"}}>
                    <Text style={styles.text}>
                    My Orders
                    </Text>
                    <Text style={styles.arrowIconView}>
                    <Icon name="chevron-forward-outline" size={22} type="ionicon" color="#fff" style={{left:5,top:3}} />
                    </Text>
                    </View>
                </View>
                </TouchableOpacity>               

                {/* About Us card */}
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("AboutUs")}>
                <View style={styles.cardView}>
                <Text style={styles.iconView}>
                <Icon name="information-circle" color="#bc3b3b" size={20} type="ionicon" />
                </Text>
                    <View style={{width:"90%", flexDirection:"row", justifyContent:"space-between"}}>
                    <Text style={styles.text}>
                    About Us
                    </Text>
                    <Text style={styles.arrowIconView}>
                    <Icon name="chevron-forward-outline" size={22} type="ionicon" color="#fff" style={{left:5,top:3}} />
                    </Text>
                    </View>
                </View>
                </TouchableOpacity>


                 {/* Support card */}
                
                 <TouchableOpacity onPress={()=>this.props.navigation.navigate("Support")}>
                <View style={[styles.cardView,]}>
                <Text style={styles.iconView}>
                <Icon name="mail" size={20} color="#bc3b3b" />
                </Text>
                    <View style={{width:"90%", flexDirection:"row", justifyContent:"space-between"}}>
                    <Text style={styles.text}>
                    Support
                    </Text>
                    <Text style={styles.arrowIconView}>
                    <Icon name="chevron-forward-outline" size={22} type="ionicon" color="#fff" style={{left:5,top:3}} />
                    </Text>
                    </View>
                </View>
                </TouchableOpacity>

                 {/* Logout card */}
                 <TouchableOpacity onPress={()=>this.context.logout()}>
                <View style={[styles.cardView]}>
                <Text style={styles.iconView}>
                <Icon name="exit" color="#bc3b3b" size={20} type="ionicon" />
                </Text>
                    <View style={{width:"90%", flexDirection:"row", justifyContent:"space-between"}}>
                    <Text style={styles.text}>
                    Logout
                    </Text>
                    <Text style={styles.arrowIconView}>
                    <Icon name="chevron-forward-outline" size={22} type="ionicon" color="#fff" style={{left:5,top:3}} />
                    </Text>
                    </View>
                </View>
                </TouchableOpacity>

                

                </ScrollView>
            </View>
        )
    }
}

export default More


const styles=StyleSheet.create({
    heading:{
        fontSize:20,
        color:"black"
    },
    cardView:{
        backgroundColor:"#fff",
        alignItems:"center",
         height:60,
         width:"100%",
        //  marginRight:45,
         borderRadius:7,marginTop:5,
        flexDirection:"row"
        },
        iconView:{
            // backgroundColor:"#d3d3d3",
            alignItems:"center",
             padding:10,
             left:Platform.OS == "ios" ? 15 : 8,
              borderRadius:20,
              marginTop:Platform.OS == "ios" ? 20 : 0,
        },
        
        arrowIconView:{
            // backgroundColor:"#d3d3d3",
            alignItems:"center",
            padding:5,
            height:40,
            // left:10,
            width:40,
            justifyContent:"center",
            alignSelf:'center',
            borderRadius:20
            // backgroundColor:"#d3d3d3",
            // shadowRadius: 50,
            // left:15,
            // shadowOffset: { width: 50, height: 50 },
            // elevation:3,
            // borderRadius:50,
            // height:40,
            // width:40,
            // justifyContent:"center",
            // alignSelf:'center'
        },
        text:{
            fontSize:RFValue(11.8,580),
            color:"#222222",
            top:-4,
            alignSelf:"center",
            marginLeft:20,
            fontFamily: "Raleway-Medium",
        }
})




