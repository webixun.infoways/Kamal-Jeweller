import React, { Component } from 'react';
import {
    Text,
    View,
    ScrollView,
    Modal,
    StyleSheet,
    Image,
    Dimensions,
    TouchableOpacity,
    Pressable,
    FlatList,
    ActivityIndicator,
} from 'react-native';
import { Header, Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import RBSheet from 'react-native-raw-bottom-sheet';
import { RFValue } from 'react-native-responsive-fontsize';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import DropButton from '../Components/DropButton.js';
import Toast from 'react-native-simple-toast';
import { Video } from 'react-native-video';

//Global StyleSheet Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window')

class Following extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            data: [],
            follow: {}
        }

    }
    componentDidMount() {
        this.get_following();
    }
    // Api call for Following
    get_following = () => {
        this.setState({isLoading:true})
        fetch(global.api + 'get-following-detail',
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': global.token
                },
            })
            .then((response) => response.json())
            .then((json) => {
                console.warn(json.data.length)
                if(json.data.length>0){
                json.data.map(value => {
                    value.following.map(item => {
                        this.setState({ data: item.user })
                    })
                })
            }
            else{
                this.setState({data:[]})
            }

                return json;
            })
            .catch((error) => console.error(error))
            .finally(() => {
                this.setState({ isLoading: false });
            });
    }

    // remove
    remove = id => {
        const follow = this.state.follow;
        if (this.state.follow[id] == true) {
            follow[id] = false;
            var type = 'follow';
        } else {
            follow[id] = true;
            var type = 'unfollow';
        }
        this.setState({ follow });

        fetch(global.api + 'follow-user', {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: global.token,
            },
            body: JSON.stringify({
                following_id: id,
                type: type,
            }),
        })
            .then(response => response.json())
            .then(json => {
                // console.log(json)

                if (!json.status) {
                    Toast.show(json.msg);
                } else {
                    Toast.show(json.msg)
                    this.get_following()
                }
            });
    };

    renderLeftComponent() {
        return (
            <View style={{ flexDirection: "row", width: win.width / 2, }}>
                <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
                </View>
                <Text style={[styles.headerHeadingText,
                ]}>Followings</Text>
            </View>
        );
    }
    render() {
        return (
            <View style={styles.container}>
                {/* View for header component */}
                <View>
                    <Header
                        // containerStyle={{height: 82}}
                        statusBarProps={{ barStyle: 'light-content' }}
                        leftComponent={this.renderLeftComponent()}
                        // rightComponent={this.renderRightComponent()}
                        ViewComponent={LinearGradient} // Don't forget this!
                        linearGradientProps={{
                            colors: ['white', 'white'],
                            start: { x: 0, y: 0.5 },
                            end: { x: 1, y: 0.5 },
                        }}
                    />
                </View>
                {this.state.isLoading ?
                    <View style={{ marginTop: 200 }}>
                        <ActivityIndicator size="large" color="#bc3b3b" />
                    </View>
                    :
                    <View>
                        {this.state.data.length < 1 ?
                            <View>
                                <Image source={require('../image/no_followings.png')} style={{height:200,width:200,alignSelf:"center",marginTop:180}}/>
                                <Text style={{color:"#bc3b3b",fontSize:RFValue(13,580),alignSelf:"center"}}>You are not following anyone!</Text>
                            </View>
                            
                            :
                            <>
                                {this.state.data.map(value => {
                                    return (
                                        <View style={{ flexDirection: "row", justifyContent: "space-between", marginHorizontal: 20, marginTop: 10 }}>
                                            <Pressable onPress={() => this.props.navigation.navigate("UserProfile", { id: value.id })} style={{ flexDirection: "row" }}>
                                                <Image
                                                    source={{
                                                        uri: global.image_url + value.profile_pic
                                                    }}
                                                    style={style.profileImg} />
                                                <Text style={[styles.h4, { marginTop: 20, marginLeft: 10 }]}>
                                                    {value.f_name}
                                                </Text>
                                            </Pressable>
                                            <Pressable onPress={() => this.remove(value.id)} style={{ borderWidth: 1, width: 75, alignItems: "center", borderRadius: 10, height: 25, marginTop: 20 }} >
                                                <Text style={styles.h5}>
                                                    Remove
                                                </Text>
                                            </Pressable>
                                        </View>
                                    )
                                })}
                            </>
                        }


                    </View>

                    // <FlatList
                    //     navigation={this.props.navigation}
                    //     showsVerticalScrollIndicator={false}
                    //     data={this.state.data}
                    //     renderItem={this.productCard}
                    //     keyExtractor={item => item.id} />
                }
            </View>
        )
    }
}
export default Following

const style = StyleSheet.create({
    profileImg: {
        height: 50,
        width: 50,
        top: 6,
        borderRadius: 50,
        //   alignSelf: 'center',
        // left:20
    },
})