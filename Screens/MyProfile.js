import React, { Component } from 'react'
import { View, Image, Text, Pressable, ImageBackground, TextInput, StyleSheet, ActivityIndicator, Dimensions, ScrollView, TouchableOpacity, Platform } from "react-native"
import { Icon, Input, Header } from "react-native-elements"
import RBSheet from 'react-native-raw-bottom-sheet';
import { launchCamera, launchImageLibrary } from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import { RFValue } from 'react-native-responsive-fontsize';
import LinearGradient from 'react-native-linear-gradient';
import Toast from 'react-native-simple-toast'
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

//Global StyleSheet Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

const options = {
    title: "Pick an Image",
    storageOptions: {
        skipBackup: true,
        path: 'images'
    },
    quality: 0.5
}

class MyProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: "",
            address: "",
            contact_no: "",
            mail: "",
            address: "",
            area: "",
            city: "",
            state: "",
            isLoading: false,
            buttonLoading:false,
            image: "",
            upload_profile_picture: 'https://cdn-icons-png.flaticon.com/512/149/149071.png',
        }
    }


    //function to launch camera
    camera = () => {
        launchCamera(options, (response) => {

            if (response.didCancel) {
                console.warn(response)
                console.warn("User cancelled image picker");
            } else if (response.error) {
                console.warn('ImagePicker Error: ', response.error);
            } else {
                // const source = {uri: response.assets.uri};
                let path = response.assets.map((path) => {
                    return (
                        console.warn(path.uri),
                        this.setState({ image: path.uri })
                    )
                });
                //  this.setState({image:path.uri})
                //   this.RBSheet.close()
                this.upload_image();
            }
        })
    }

    //function to launch gallery
    gallery = () => {
        ImagePicker.openPicker({
            width: 300,
            height: 400,
            cropping: true,
        }).then(image => {
            console.log(image);
            // this.setState({image:"Image Uploaded"})
            this.setState({ image: image.path });
            this.upload_image();
        })
    }

    //function to upload image
    upload_image = () => {
        this.RBSheet.close()
        this.setState({ image_load: true });
        var photo = {
            uri: this.state.image,
            type: 'image/jpeg',
            name: "akash.jpg"
        };
        var form = new FormData();
        form.append("file", photo);

        fetch(global.api + "upload-profile-pic", {
            method: 'POST',
            body: form,
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': global.token
            },
        }).then((response) => response.json())
            .then((json) => {
                console.warn(json);
                if (json.status) {
                    Toast.show("Image uploaded successfully");
                    this.get_profile();
                }
                else {
                    Toast.show("Not updated")
                }
            }).catch((error) => {
                console.error(error);
            }).finally(() => {
                this.setState({ image_load: false });
            });
    }

    componentDidMount() {
        this.get_profile()
    }

    // Fetching user details
    get_profile = () => {
        fetch(global.api + 'get-user-profile',
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': global.token
                },
            })
            .then((response) => response.json())
            .then((json) => {
                console.warn(json)
                if (json.status) {
                    this.setState({ name: json.data[0].f_name })
                    this.setState({ contact_no: json.data[0].contact, mail: json.data[0].email })

                    this.setState({ image: json.data[0].profile_pic })
                }
                else {
                    // Toast.show(json.msg)
                }
                return json;
            })
            .catch((error) => console.error(error))
            .finally(() => {
                this.setState({ isLoading: false });
            });
    }

    userDetails = () => {
        let validation = /^[a-zA-Z" "]+$/;
        let isValid = validation.test(this.state.name)
        {
            this.setState({ msg: "" });
            if (this.state.name == "") {
                Toast.show("User Name Field is required !");
            }
            else if (!isValid) {
                Toast.show("Enter a valid name !");
            }
            else {
                this.setState({ isLoading: true,buttonLoading:true });
                var name = this.state.name;
                var mail = this.state.mail;


                fetch(global.api + "update-profile", {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                        'Authorization': global.token
                    },
                    body: JSON.stringify({
                        email: mail,
                        f_name: name,
                        contact: this.state.contact_no,
                    })
                }).then((response) => response.json())
                    .then((json) => {
                        console.warn(json)
                        if (!json.status) {
                            Toast.show("Enter valid email!");
                        }
                        else {
                            Toast.show("Profile successfully updated!");
                            this.props.navigation.goBack()

                        }
                        return json;
                    }).catch((error) => {
                        console.error(error);
                    }).finally(() => {
                        this.setState({ isLoading: false,buttonLoading:false })
                    });
            }
        }
    }

    render() {
        return (

            <View style={{ flex: 1, backgroundColor: "white" }}>

                {/* View for profile image  */}
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View>

                        <ImageBackground source={require("../image/redprofile.png")} style={style.imageBackground} 
                          >
                            <TouchableOpacity 
                            style={{ margin: 40, marginLeft: 10,marginTop:Platform.OS  == "ios" ? 20 : 20, backgroundColor:"#fff", marginTop:Platform.OS == "ios" ? 50 : 40, alignItems: "center", padding:5,borderRadius: 100, opacity: 0.5,  height: 30, width: 30 }}
                            onPress={() => this.props.navigation.goBack()}>
                                <Icon name="arrow-back-outline" type="ionicon" size={20}
                                   color={Platform.OS == "ios" ? "#000" : "#fff"} />
                            </TouchableOpacity>
                            {/* user image */}
                            {this.state.image_load ?
                               <View>
                                   <SkeletonPlaceholder>
                                       <View>
                                           <View style={{height: 150,
                                            width: 150,borderRadius:100,alignSelf:"center"}}/>
                                       </View>
                                   </SkeletonPlaceholder>
                               </View>
                                :
                                <View>
                                    {this.state.image == "" ?
                                        <Image
                                            source={require('../image/dummyuser.jpg')}
                                            style={style.profileImg} />
                                        :
                                        <Image
                                            source={{
                                                uri: global.image_url + this.state.image
                                            }}
                                            style={style.profileImg} />
                                    }
                                </View>
                            }
                            <TouchableOpacity onPress={() => this.RBSheet.open()} style={style.camIcon}>
                                <Icon type="ionicon" name="camera" size={20} color="#000" />
                            </TouchableOpacity>
                        </ImageBackground>

                        {/* Bottom Sheet for gallery open */}
                        <RBSheet
                            ref={ref => {
                                this.RBSheet = ref;
                            }}
                            animationType="slide"
                            closeOnDragDown={true}
                            closeOnPressMask={true}
                            height={150}
                            customStyles={{
                                container: {
                                    borderTopLeftRadius: 20,
                                    borderTopRightRadius: 20
                                },
                                wrapper: {
                                    // backgroundColor: "transparent",
                                    borderWidth: 1
                                },
                                draggableIcon: {
                                    backgroundColor: "transparent"
                                }
                            }}
                        >
                            {/* bottom sheet elements */}
                            <View>

                                {/* open camera and library */}
                                <View style={{ width: "100%", padding: 20 }}>
                                    <TouchableOpacity onPress={this.camera}>
                                        <Text style={style.icon}>
                                            <Icon name='camera' type="ionicon" color={'#bc3b3b'} size={25} />
                                        </Text>
                                        <Text style={style.Text}>Take a picture</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity onPress={this.gallery} >
                                        <Text style={style.icon}>
                                            <Icon name='folder' type="ionicon" color={'#bc3b3b'} size={25} />
                                        </Text>
                                        <Text style={style.Text}>Select from library</Text>
                                    </TouchableOpacity>

                                </View>

                            </View>
                        </RBSheet>

                        {/* Edit Profile button */}
                        {/* <Pressable style={{flexDirection:'row',alignItems:"center",
                    justifyContent:"center",top:15}}
                    onPress={()=>this.props.navigation.navigate("MyProfile")}>
                        <Icon name="create-outline" type="ionicon" color="#BC3B3B"/>
                        <Text style={[styles.h4,{color:"#BC3B3B"}]}>Edit Profile</Text>
                    </Pressable> */}

                        {/* View for inputs */}
                        {this.state.isLoading ?
                        <View>
                            <SkeletonPlaceholder>
                                <View>
                                    <View style={{width: Dimensions.get('window').width / 1.2,
                                        borderRadius: 10,
                                        height: 45,marginTop:90,
                                        alignSelf: "center",}}/>

                                    <View style={{width: Dimensions.get('window').width / 1.2,
                                        borderRadius: 10,
                                        height: 45,marginTop:20,
                                        alignSelf: "center",}}/>
                                </View>
                            </SkeletonPlaceholder>
                        </View>
                        :
                        <View>

                            {/* TextInput for name */}
                            <TextInput
                                style={[style.textInput, {  }]}
                                placeholder="Name"
                                // placeholderTextColor="#D19292"
                                value={this.state.name}
                                maxLength={40}
                                onChangeText={(text) => { this.setState({ name: text }) }} />

                            {/* TextInput for mail */}
                            <TextInput
                                style={[style.textInput, { marginTop: 20,  }]}
                                placeholder="Email (Optional)"
                                // placeholderTextColor="#D19292"
                                value={this.state.mail}
                                maxLength={55}
                                onChangeText={(text) => { this.setState({ mail: text }) }} />

                            {/* TextInput for mail */}
                            {/* <TextInput 
                        style={[style.textInput,{marginTop:20,fontFamily: "Roboto-Regular",}]}
                        placeholder="Mobile No"
                        value={this.state.contact_no}
                        maxLength={10}
                        keyboardType='numeric'
                        onChangeText={(text) => {this.setState({contact_no:text})}}/> */}
                            {/* TextInput for mail */}
                            {/* <TextInput 
                        style={[style.textInput,{marginTop:20,fontFamily: "Raleway-Regular",}]}
                        placeholder="Address"
                        // placeholderTextColor="#D19292"
                        value={this.state.mail}
                        maxLength={55}
                        onChangeText={(text) => {this.setState({mail:text})}}/> */}
                        </View>
                        }


                        {/* View for button */}
                        <View>
                            {this.state.buttonLoading ?

                                <View >
                                    <ActivityIndicator size="large" color="#bc3b3b"
                                        style={{ marginTop: 120 }} />
                                </View>

                                :
                                <TouchableOpacity style={[styles.buttonStyles, { marginTop: 130 }]}
                                    onPress={() => this.userDetails()}>
                                    <Text style={styles.buttonText}>Save</Text>
                                </TouchableOpacity>
                            }
                        </View>

                    </View>
                </ScrollView>
            </View>
        )
    }
}

export default MyProfile



const style = StyleSheet.create({
    iconBack: {
        padding: 10,
        top: 15
    },
    imageBackground: {
        width: "100%",
        height: 200,
        paddingBottom: 10
    },
    buttonStyles: {
        width: "20%",
        justifyContent: "center",
        alignSelf: "flex-end",
        height: 30,
        alignItems: 'center',
        borderRadius: 10,
        flexDirection: "row",
        alignItems: 'center',
        right: 5,
        backgroundColor: "#BC3B3B",
        marginBottom: 10
    },
    buttonText: {
        color: "#fff",
        fontSize: RFValue(12, 580),
    },
    profileImg: {
        height: 150,
        width: 150,
        alignSelf: "center",
        top: -7,
        borderRadius: 100,
        // elevation:1
        // backgroundColor:"red"
    },
    camIcon: {
        backgroundColor: "#dcdcdc",
        height: 30,
        width: 30,
        padding: 5,
        alignContent: "center",
        borderRadius: 30,
        justifyContent: "center",
        alignSelf: "center",
        top: -30,
        left: 50,
    },
    icon: {
        marginLeft: 20,
        // fontSize:20,
        fontSize: RFValue(14.5, 580),
        marginBottom: 10
    },
    Text: {
        position: "absolute",
        // fontSize:20,
        fontSize: RFValue(14.5, 580),
        marginLeft: 80,
        fontFamily: "Raleway-Medium"
    },
    textInput: {
        backgroundColor: "#F2F2F2",
        width: Dimensions.get('window').width / 1.2,
        borderRadius: 10,
        height: 45,
        alignSelf: "center",
        top: 90,
        paddingLeft: 20,
        color: "#222222",
        fontSize: RFValue(11, 580),
    },
}
)