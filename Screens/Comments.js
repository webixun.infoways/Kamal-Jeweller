import React, {Component} from 'react';
import {
  View,
  ImageBackground,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Pressable,
  ActivityIndicator,
  Image,
  Text,
  Dimensions,
  ScrollView,
  Keyboard,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {Header, Icon} from 'react-native-elements';
import {TextInput} from 'react-native-gesture-handler';
import Toast from 'react-native-simple-toast';
import moment from 'moment';
import RBSheet from 'react-native-raw-bottom-sheet';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

//Global Style Import
const styles = require('../Components/Style.js');
const win = Dimensions.get('window');

class Comments extends Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.taskInput = React.createRef();
    this.textInput = React.createRef();
    this.state = {
      input: '',
      object: {},
      data: [],
      feed_id: this.props.route.params.id,
      isLoading: true,
      posting: false,
      edit: false,
      id: '',
      description:"",
      name:'',
      time:'',
      profile_pic:''

    };
  }

  componentDidMount() {
    this.fetch_comment();
    this.fetch_feeds();
    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.fetch_comment();
      this.fetch_feeds();
    });
  }

  // Fetching feed comments
  fetch_comment = () => {
    fetch(global.api + 'fetch-feed-comment', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        feed_id: this.state.feed_id,
      }),
    })
      .then(response => response.json())
      .then(json => {
        console.warn(json);
        if (!json.status) {
          var msg = json.msg;
          // Toast.show(msg);
          this.setState({data: ''});
        } else {
          //    this.RBSheet.close();
          this.setState({data: json.data});
          console.warn(this.state.data.length);
        }
        return json;
      })
      .catch(error => {
        console.error(error);
      })
      .finally(() => {
        this.setState({isLoading: false});
      });
  };

  //function for fetching feeds
  fetch_feeds()
  {
      fetch(global.api+"get_single_feed_user", {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization':global.token 
        },
        body: JSON.stringify({
            feed_id:this.state.feed_id
        })})
        .then((response) => response.json())
        .then((json) => {
            console.warn("single feed",json)
          if(json.data.length>0){
              json.data.map((value) =>{
                    this.setState({description:value.feed_description,name:value.user_name,profile_pic:value.user_profile_pic,
                        time:value.created_at})
                        console.warn("user",this.state.profile_pic)
                            
              })
          }

          
        })
        .catch((error) => {  
              console.error(error);   
            }).finally(() => {
              this.setState({isLoading:false})
            })
  }

  // Add Comment
  add_comment = id => {
    if (this.state.input == '') {
      Toast.show('Write a comment');
    } else {
      this.setState({posting: true});
      fetch(global.api + 'feed-comment-add', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          Authorization: global.token,
        },
        body: JSON.stringify({
          feed_id: id,
          comment: this.state.input,
        }),
      })
        .then(response => response.json())
        .then(json => {
          console.log(json);
          if (!json.status) {
            //    Toast.show(json.msg)
          } else {
            //  Toast.show(json.msg)
          }
          this.fetch_comment();
          Keyboard.dismiss();
          this.setState({input: '', posting: false});
        })
        .catch(error => console.error(error))
        .finally(() => {
          this.setState({isLoading: false});
        });
    }
  };

  edit_comment = () => {
    // alert(this.state.id)
    console.warn(this.state.id);
    this.props.navigation.navigate('EditComment', {
      data: this.state.id,
      // category:this.props.category
    });
    this.RBSheet.close();
  };

  delete_comment = () => {
    // console.warn(item_id)
    this.RBSheet.close();
    fetch(global.api + 'delete-comment', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        comment_id: this.state.id,
        // token:token,
      }),
    })
      .then(response => response.json())
      .then(json => {
        //   console.warn(json)
        if (json.status) {
          //   Toast.show(json.msg);
          Toast.show('Comment Deleted');
        } else {
          Toast.show(json.msg);
        }
        this.fetch_comment();

        return json;
      })
      .catch(error => {
        console.error(error);
      })
      .finally(() => {
        this.setState({isLoading: false});
      });
  };

  // function for bottom sheet
  sheet = id => {
    this.RBSheet.open();
    this.setState({id: id});
  };

  //for header left component
  renderLeftComponent() {
    return (
      <View style={{width: win.width / 2, flexDirection: 'row'}}>
       <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
                </View>
        <Text style={[styles.headerHeadingText, {alignSelf: 'center'}]}>
          Comments
        </Text>
      </View>
    );
  }

  renderItem = ({item}) => (
    <View>
      <View
        style={{
          flexDirection: 'row',
          paddingTop: 20,
          paddingLeft: 10,
          paddingBottom: 10,
        }}>
        <Image
          source={{uri: global.image_url + item.profile_pic}}
          style={style.profileImage}
        />
        <View style={{width: '80%'}}>
          <Text style={style.name}>{item.f_name}</Text>
          <Text
            style={{
              marginLeft: 10,
              fontSize: RFValue(11, 580),
              fontFamily: 'MontSerrat-SemiBold',
            }}>
            {item.comment}
          </Text>
          <Text style={style.postTime}>
            {moment.utc(item.updated_at).local().startOf('seconds').fromNow()}
          </Text>
        </View>
        {item.profile_pic!=global.pic?null:
        <Pressable onPress={() => this.sheet(item.id)}>
          <Icon name="ellipsis-vertical" type="ionicon" size={22} />
        </Pressable>
}
      </View>
      {/* Bottom Sheet for Post options */}

      <RBSheet
        ref={ref => {
          this.RBSheet = ref;
        }}
        // animationType="slide"
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={160}
        customStyles={{
          container: {
            borderTopRightRadius: 20,
            borderTopLeftRadius: 20,
          },
          draggableIcon: {
            backgroundColor: '',
          },
        }}>
        {/* bottom sheet elements */}
        <View>
          {/* new container search view */}
          <View>
            {/* to share */}
            <View style={{flexDirection: 'row', padding: 10}}>
              <TouchableOpacity
                style={{flexDirection: 'row'}}
                onPress={() =>
                  this.props.navigation.navigate('EditComment', {
                    name: item.name,
                    id: this.state.id,
                    pic: item.profile_pic,
                    comment: item.comment,
                  })
                }>
                <View
                  style={{
                    backgroundColor: '#f5f5f5',
                    height: 40,
                    width: 40,
                    justifyContent: 'center',
                    borderRadius: 50,
                  }}>
                  <Icon type="ionicon" name="create-outline" />
                </View>
                <Text
                  style={[styles.h4, {alignSelf: 'center', marginLeft: 20}]}>
                  Edit
                </Text>
              </TouchableOpacity>
            </View>

            {/* to report */}
            <View style={{flexDirection: 'row', padding: 10}}>
              <TouchableOpacity
                style={{flexDirection: 'row'}}
                onPress={() => this.delete_comment(item.id)}>
                <View
                  style={{
                    backgroundColor: '#f5f5f5',
                    height: 40,
                    width: 40,
                    justifyContent: 'center',
                    borderRadius: 50,
                  }}>
                  <Icon type="ionicon" name="trash-outline" />
                </View>
                <Text
                  style={[styles.h4, {alignSelf: 'center', marginLeft: 20}]}>
                  Delete
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </RBSheet>
    </View>
  );

  render() {
    return (
      <View
        style={[styles.container, {backgroundColor: '#fff', height: '100%'}]}>
        <Header
          // containerStyle={{height: 82}}
          statusBarProps={{barStyle: 'light-content'}}
          // centerComponent={this.renderCenterComponent()}
          leftComponent={this.renderLeftComponent()}
          ViewComponent={LinearGradient} // Don't forget this!
          linearGradientProps={{
            colors: ['white', 'white'],
            start: {x: 0, y: 0.5},
            end: {x: 1, y: 0.5},
          }}
        />

        <ScrollView>
          <View style={style.descriptionView}>
            <Image
              source={{uri: global.image_url + this.state.profile_pic}}
              style={[style.profileImage, {marginTop: -10,height:45,width:45}]}
            />
            <View style={{width: '80%'}}>
              <Text style={style.name}>{this.state.name}</Text>
              <Text
                style={{
                  marginLeft: 10,
                  fontSize: RFValue(11, 580),
                  fontFamily: 'MontSerrat-SemiBold',
                  color: 'grey',
                }}>
                {this.state.description}
              </Text>
              <Text style={style.postTime}>
                {moment
                  .utc(this.state.time)
                  .local()
                  .startOf('seconds')
                  .fromNow()}
              </Text>
            </View>
          </View>

          {/* Indivudual comment View */}

          {!this.state.isLoading ? (
            this.state.data.length >0 ? (
              <View style={{flex: 1, marginBottom: 70}}>
                {/* {details} */}
                <FlatList
                  navigation={this.props.navigation}
                  showsVerticalScrollIndicator={false}
                  data={this.state.data}
                  renderItem={this.renderItem}
                  keyExtractor={item => item.id}
                />
              </View>
            ) : (
              <View
                style={{
                  marginTop: 10,
                  alignItems: 'center',
                  paddingTop: 50,
                  flex: 1,
                }}>
                <Image
                  style={{width: 200, height: 150, marginTop: 80}}
                  source={require('../image/noFeed.png')}
                />
                <Text style={[styles.h4, {marginTop: 5, color: 'grey'}]}>
                  No Comments Found!
                </Text>
              </View>
            )
          ) : (
            <View
              style={{
                alignItems: 'center',
                flex: 1,
                backgroundColor: 'white',
                paddingTop: 250,
              }}>
              <ActivityIndicator size="large" color="#bc3b3b" />
              <Text style={styles.p}>Please wait...</Text>
            </View>
          )}
        </ScrollView>
        {/* Comment Post View */}

        <View
          style={{
            flexDirection: 'row',
            position: 'absolute',
            bottom: 0,
            width: '100%',
            borderTopWidth: 1,
            borderColor: '#fafafa',
            paddingLeft: 10,
            backgroundColor: '#f5f5f5',
            paddingBottom: 5,
          }}>
          <Image
            source={{uri: global.image_url + this.state.profile_pic}}
            style={[style.profileImage, {marginTop: 10}]}
          />
          <TextInput
            style={{
              width: '80%',
              paddingLeft: 10,
              top:3,
              fontSize: RFValue(11, 580),
              fontFamily: 'MontSerrat-SemiBold',
            }}
            placeholder="Comment here..."
            value={this.state.input}
            onChangeText={v => {
              this.setState({input: v});
            }}
          />
          {!this.state.posting ? (
            <Text style={{marginTop: 15}}>
              <Icon
                name="send"
                size={24}
                type="ionicon"
                onPress={() => {
                  this.add_comment(this.state.feed_id);
                }}
              />
            </Text>
          ) : (
            <View>
              <ActivityIndicator
                size="small"
                color="#bc3b3b"
                style={{top: 20}}
              />
            </View>
          )}
        </View>
      </View>
    );
  }
}
export default Comments;

//internal stylesheet
const style = StyleSheet.create({
  text: {
    // fontFamily:"Raleway-SemiBold",
    fontFamily: 'MontSerrat-SemiBold',
    fontSize: RFValue(16, 580),
    margin: 5,
  },
  descriptionView: {
    flexDirection: 'row',
    marginTop:5,
    paddingTop: 10,
    fontSize:RFValue(10,580),
    paddingLeft: 10,
    paddingRight: 20,
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderColor: '#f5f5f5',
  },

  profileImage: {
    height: 37,
    width: 37,
    borderRadius: 50,
    // marginTop:20,
    // marginLeft:10
  },
  name: {
    fontFamily: 'Raleway-SemiBold',
    // fontFamily: "DancingScript-Bold",
    fontSize: RFValue(12, 580),
    marginLeft: 10,
    marginTop: -15,
    color:"#bc3b3b"
  },
  postTime: {
    // fontFamily:"Roboto-Regular",
    fontFamily: 'MontSerrat-SemiBold',
    color: 'grey',
    marginLeft: 10,
    fontSize: RFValue(9, 580),
  },
});
