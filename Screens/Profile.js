import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
  Modal,
  StyleSheet,
  Image,
  Dimensions,
  TouchableOpacity,
  Pressable,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import {Header, Icon} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import RBSheet from 'react-native-raw-bottom-sheet';
import {RFValue} from 'react-native-responsive-fontsize';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import DropButton from '../Components/DropButton.js';
import Toast from 'react-native-simple-toast';
import {Video} from 'react-native-video';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

//Global StyleSheet Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

const options = {
  title: 'Pick an Image',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
  quality: 0.5,
};

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeIndex: 0,
      modalVisible: false,
      isLoading: true,
      data: [],
      image: '',
      name: '',
      followers: '',
      following: '',
      post: [],
      feed: [],
    };
  }
  componentDidMount() {
    this.setState({name: global.name});
  }

  setModalVisible = visible => {
    this.setState({modalVisible: visible});
  };
  
  segmentClick = index => {
    this.setState({
      activeIndex: index,
    });
  };

  //
  postCard = ({item}) => (
    <Pressable
      style={style.card}
      onPress={() =>
        this.props.navigation.navigate('ProfilePost', {
          data: this.state.data,
          name: this.state.name,
          pic: this.state.image,
        })
      }>
      {/* <Image source={require('../image/v2.jpg')}
                        style={style.img}/> */}
      <Image
        source={{
          uri:
            'https://demo.webixun.com/KamalJwellersApi/public/' +
            item.content_src,
        }}
        style={style.img}
      />
    </Pressable>
  );

    //function to launch camera
camera =()=>{
    launchCamera(options, (response)=>{
        
        if(response.didCancel){
            console.warn(response)
            console.warn("User cancelled image picker");
        } else if (response.error){
            console.warn('ImagePicker Error: ', response.error);
        }else{
            // const source = {uri: response.assets.uri};
          let path = response.assets.map((path)=>{
              return (
                //  console.warn(path.uri) 
                  this.setState({image:path.uri}) 
              )
          });
          //  this.setState({image:path.uri})
          this.RBSheet.close(),
          this.props.navigation.navigate("CreatePost",{image:this.state.image})
        }
    })
    }
    
    
    //function to launch gallery
    gallery =()=>{
    ImagePicker.openPicker({
        width:300,
        height:400,
        cropping:true,
    }).then(image=>{
        console.log(image);
        // this.setState({image:"Image Uploaded"})
        this.setState({image:image.path});
        // this.upload_image();   
        
        this.props.navigation.navigate("CreatePost",{image:this.state.image})   
    })
    }

  renderSection = () => {
    if (this.state.activeIndex == 0) {
      return (
        <View>
          {!this.state.isLoading ? (
            <View>
              {this.state.data != '' ? (
                <FlatList
                  columnWrapperStyle={{flex: 1 / 3}}
                  numColumns={3}
                  navigation={this.props.navigation}
                  showsVerticalScrollIndicator={false}
                  data={this.state.data}
                  renderItem={this.postCard}
                  keyExtractor={item => item.id}
                />
              ) : (
                <View style={{alignItems: 'center', paddingTop: 70}}>
                  <TouchableOpacity style={{justifyContent:"center"}} 
                   onPress={()=>this.RBSheet.open()}>
                      <Image
                      source={require('../image/add.png')}
                      style={{width: 50, height: 50,alignSelf:"center"}}
                      />
                      <Text
                        style={[
                          styles.h4,
                          {
                            marginTop: 15,
                            fontSize: RFValue(11, 580),
                            justifyContent: 'center',
                            textAlign: 'center',
                            alignSelf:"center"
                          },
                        ]}>
                        When you share your photos,{'\n'}they will appear here
                      </Text>
                      <Text
                        onPress={()=>this.RBSheet.open()}
                        style={[
                          styles.h3,
                          {
                            marginTop: 15,
                            fontSize: RFValue(12, 580),
                            justifyContent: 'center',
                            textAlign: 'justify',
                            color: '#bc3b3b',
                            alignSelf:"center"
                          },
                        ]}>
                        Share your first photo
                      </Text>
                  </TouchableOpacity>
                  
                  {/* Bottom Sheet for Camera */}
                  <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          closeOnDragDown={true}
          closeOnPressMask={true}
          height={130}
          customStyles={{
            container: {
              borderTopRightRadius: 20,
              borderTopLeftRadius: 20,
            },
            draggableIcon: {
              backgroundColor: "grey"
            },
          }}>
          {/* bottom sheet elements */}
          <View>
            {/* open camera and library */}
            <View style={{width: '100%', padding: 20}}>
              <TouchableOpacity onPress={this.camera}>
                <Text style={style.icon}>
                  <Icon
                    name="camera"
                    type="ionicon"
                    color={'#bc3b3b'}
                    size={25}
                  />
                </Text>
                <Text style={style.Text}>Take a picture</Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={this.gallery}>
                <Text style={style.icon}>
                  <Icon
                    name="folder"
                    type="ionicon"
                    color={'#bc3b3b'}
                    size={25}
                  />
                </Text>
                <Text style={style.Text}>Select from library</Text>
              </TouchableOpacity>
            </View>
          </View>
        </RBSheet>
                </View>
              )}
            </View>
          ) : (
            <View>
              <SkeletonPlaceholder>
                <View>
                  <View style={{flexDirection:"row",}}>
                    <View style={{height:130,width:130,marginLeft:4}}/>
                    <View style={{height:130,width:130,marginLeft:4}}/>
                    <View style={{height:130,width:130,marginLeft:4}}/>
                  </View>

                  <View style={{flexDirection:"row",marginTop:4}}>
                    <View style={{height:130,width:130}}/>
                    <View style={{height:130,width:130,marginLeft:4}}/>
                    <View style={{height:130,width:130,marginLeft:4}}/>
                  </View>

                  <View style={{flexDirection:"row",marginTop:4}}>
                    <View style={{height:130,width:130}}/>
                    <View style={{height:130,width:130,marginLeft:4}}/>
                    <View style={{height:130,width:130,marginLeft:4}}/>
                  </View>


                </View>
              </SkeletonPlaceholder>
            </View>
          )}
        </View>
      );
    } else {
      return (
        <View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '100%',
            }}>
            {/* product card */}
            <Pressable
              style={style.card}
              onPress={() => this.props.navigation.navigate('Video')}>
              <Image source={require('../image/v1.jpg')} style={style.img1} />
            </Pressable>

            {/* product card 2 */}
            <Pressable
              style={style.card}
              onPress={() => this.props.navigation.navigate('Videos')}>
              <Image source={require('../image/v2.jpg')} style={style.img1} />
            </Pressable>

            {/* product card 2 */}
            <Pressable
              style={style.card}
              onPress={() => this.props.navigation.navigate('Videos')}>
              <Image source={require('../image/v3.jpg')} style={style.img1} />
            </Pressable>

            <Modal
              visible={this.state.modalVisible}
              onRequestClose={() => this.setModalVisible(false)}
              // presentationStyle={"fullScreen"}
              //swipeDirection={["up", "down", "left", "right"]}
              // transparent={true}
            >
              <View>
                <Image
                  source={require('../image/v2.jpg')}
                  style={{height: 200, width: win.width}}
                  //                     controls={true}
                  // ref={(ref) => {
                  // this.player = ref}}
                />
              </View>
            </Modal>
          </View>
        </View>
      );
    }
  };

  renderLeftComponent() {
    return (
      <View style={{flexDirection: 'row', width: win.width / 2}}>
        <Text style={styles.headerHeadingText}>Profile</Text>
      </View>
    );
  }

  renderRightComponent() {
    return (
      <View style={{padding:5,top:2}}>
        <Icon
          name="bookmark-outline"
          size={24}
          color="#222222"
          type="ionicon"
          onPress={() => this.props.navigation.navigate('SavedPost')}
          // style={{alignSelf:"center"}}
        />
      </View>
    );
  }

  componentDidMount = () => {
    this.get_profile();
    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.get_profile();
    });
  };

  get_profile = () => {
    this.setState({isLoading:true})
    fetch(global.api + 'get-user-profile', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    })
      .then(response => response.json())
      .then(json => {
        console.warn(json);
        if (json.status) {
          this.setState({name: json.data[0].f_name});
          this.setState({image: json.data[0].profile_pic});
          global.pic = this.state.image;
          json.data.map(value => {
            this.setState({
              data: value.feed,
              post: value.feed.length,
              followers: value.follower_count,
              following: value.following_count,
            });
          });
        } else {
          // Toast.show(json.msg)
        }
        return json;
      })
      .catch(error => console.error(error))
      .finally(() => {
        this.setState({isLoading: false});
      });
  };
  render() {
    return (
      <View style={styles.container}>
        {/* View for header component */}
        <View>
          <Header
            // containerStyle={{height: 82}}
            statusBarProps={{barStyle: 'light-content'}}
            leftComponent={this.renderLeftComponent()}
            rightComponent={this.renderRightComponent()}
            ViewComponent={LinearGradient} // Don't forget this!
            linearGradientProps={{
              colors: ['white', 'white'],
              start: {x: 0, y: 0.5},
              end: {x: 1, y: 0.5},
            }}
          />
        </View>

        <ScrollView>
          <View>
            <ProfileDetails
              navigation={this.props.navigation}
              image={this.state.image}
              get_profile={this.get_profile}
              post={this.state.post}
              followers={this.state.followers}
              following={this.state.following}
              isLoading={this.state.isLoading}
            />
            {/* profile name view */}
            <View style={{marginTop: -7, flexDirection: 'row'}}>
              <Text style={[styles.h3, {marginLeft: 20}]}>Hi,</Text>
              {this.state.isLoading ?
              <SkeletonPlaceholder>
                <View>
                  <View style={{height:25,width:200,marginLeft:5,marginTop:2}}/>
                </View>
              </SkeletonPlaceholder>
              :
              <Text
                style={[
                  styles.h3,
                  {fontSize: RFValue(14, 580), top: 0, marginLeft: 5},
                ]}>
                {this.state.name}
              </Text>
              }
            </View>
            <DropButton navigation={this.props.navigation} />

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-around',
                marginTop: 15,
              }}>
              <TouchableOpacity
                style={{
                  borderColor: '#f5f5f5',
                  borderWidth: 1,
                  width: Dimensions.get('window').width ,
                  padding: 10,
                  alignContent: 'center',
                }}
                onPress={() => this.segmentClick(0)}
                activeOpacity={this.state.activeIndex == 0}>
                <Icon
                  name="apps"
                  type="ionicon"
                  color={this.state.activeIndex == 0 ? '#BC3B3B' : '#4a4b4d'}
                />
              </TouchableOpacity>

              {/* <TouchableOpacity
                style={{
                  borderColor: '#f5f5f5',
                  borderWidth: 1,
                  width: Dimensions.get('window').width / 2,
                  padding: 10,
                  alignContent: 'center',
                }}
                onPress={() => this.segmentClick(1)}
                activeOpacity={this.state.activeIndex == 1}>
                <Icon
                  name="videocam"
                  type="ionicon"
                  color={this.state.activeIndex == 1 ? '#BC3B3B' : '#4a4b4d'}
                />
              </TouchableOpacity> */}
            </View>

            {this.renderSection()}
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default Profile;

class ProfileDetails extends Component {
  constructor(props){
    super(props);
    this.state={
      image_load:false,
    }
  }
  camera = () => {
    launchCamera(options, response => {
      if (response.didCancel) {
        console.warn(response);
        console.warn('User cancelled image picker');
      } else if (response.error) {
        console.warn('ImagePicker Error: ', response.error);
      } else {
        // const source = {uri: response.assets.uri};
        let path = response.assets.map(path => {
          return (
            console.warn(path.uri),
            //  console.warn(path.uri)
            this.setState({image: path.uri})
          );
        });
        //  this.setState({image:path.uri})
        //   this.RBSheet.close()
        this.upload_image();
      }
    });
  };

  //function to launch gallery

  gallery = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      console.log(image);
      // this.setState({image:"Image Uploaded"})
      this.setState({image: image.path});
      this.upload_image();
    });
  };

  //function to upload image
  upload_image = () => {
    this.RBSheet.close();
    this.setState({image_load: true});
    var photo = {
      uri: this.state.image,
      type: 'image/jpeg',
      name: 'akash.jpg',
    };
    var form = new FormData();
    form.append('file', photo);

    fetch(global.api + 'upload-profile-pic', {
      method: 'POST',
      body: form,
      headers: {
        'Content-Type': 'multipart/form-data',
        Authorization: global.token,
      },
    })
      .then(response => response.json())
      .then(json => {
        console.warn(json);
        if (json.status) {
          Toast.show('Image uploaded successfully');
        } else {
          Toast.show('Not updated');
        }
      })
      .catch(error => {
        console.error(error);
      })
      .finally(() => {
        this.setState({image_load: false});
        this.props.get_profile();
      });
  };

  render() {
    return (
      <View style={{flexDirection: 'row', width: '100%'}}>
        {/* profile image and camera icon view */}
        <View style={{width: '25%'}}>
       {this.props.isLoading ?
        <View>
          <SkeletonPlaceholder>
            <View>
              <View style={{height:90,width:90,borderRadius:100,marginTop:5,marginLeft:5}}/>
            </View>
          </SkeletonPlaceholder>
        </View>
        :
        <View>
           {
            this.state.image_load ?
              <View>
                <ActivityIndicator color="#bc3b3b" size="small"
                  style={style.profileImg} />
              </View>
              :
              <View>
                  {this.props.image == "" ?
                      <Image
                          source={require('../image/dummyuser.jpg')}
                          style={style.profileImg} />
                      :
                      <Image
                          source={{
                              uri: global.image_url + this.props.image
                          }}
                          style={style.profileImg} />
                  }
              </View>
          }
        </View>
        }
          <TouchableOpacity onPress={() => this.RBSheet.open()} style={style.camIcon}>
            <Icon type="ionicon" name="camera" size={17} color="#000" />
          </TouchableOpacity>
        </View>

        {/* Bottom Sheet for gallery open */}
        <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          closeOnDragDown={true}
          closeOnPressMask={true}
          height={130}
          customStyles={{
            container: {
              borderTopRightRadius: 20,
              borderTopLeftRadius: 20,
            },
            draggableIcon: {
              backgroundColor: '',
            },
          }}>
          {/* bottom sheet elements */}
          <View>
            {/* open camera and library */}
            <View style={{width: '100%', padding: 20}}>
              <TouchableOpacity onPress={this.camera}>
                <Text style={style.icon}>
                  <Icon
                    name="camera"
                    type="ionicon"
                    color={'#bc3b3b'}
                    size={25}
                  />
                </Text>
                <Text style={style.Text}>Take a picture</Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={this.gallery}>
                <Text style={style.icon}>
                  <Icon
                    name="folder"
                    type="ionicon"
                    color={'#bc3b3b'}
                    size={25}
                  />
                </Text>
                <Text style={style.Text}>Select from library</Text>
              </TouchableOpacity>
            </View>
          </View>
        </RBSheet>
        

        {this.props.isLoading ?
          <View>
            <SkeletonPlaceholder>
              <View style={{flexDirection:"row",marginTop:30,marginLeft:10}}>
                <View style={{height:40,width:80}}/>
                <View style={{height:40,width:80,marginLeft:15}}/>
                <View style={{height:40,width:80,marginLeft:15}}/>
              </View>
            </SkeletonPlaceholder>
          </View>
        :
          <View style={{width: '75%', flexDirection: 'column', paddingTop: 18}}>
            {/* edit profile button */}
            <View
              style={{
                flexDirection: 'row',
                marginLeft: 10,
                justifyContent: 'space-evenly',
                marginTop: 12,
              }}>
              <View
                style={{
                  flexDirection: 'column',
                  //  borderRightWidth:2,borderColor:"#d3d3d3",
                  paddingRight: 30,
                }}>
                <Text
                  style={[
                    styles.h4,
                    {fontFamily: 'Montserrat-Medium', alignSelf: 'center'},
                  ]}>
                  {this.props.post}
                </Text>
                <Text
                  style={[
                    styles.smallHeading,
                    {fontSize: RFValue(10, 580), fontFamily: 'Raleway-Medium'},
                  ]}>
                  Posts
                </Text>
              </View>

              <Pressable onPress={()=>this.props.navigation.navigate("Followers")}
                style={{
                  flexDirection: 'column',
                  // borderRightWidth:2,borderColor:"#d3d3d3",
                  paddingRight: 30,
                }}>
                <Text
                  style={[
                    styles.h4,
                    {fontFamily: 'Montserrat-Medium', alignSelf: 'center'},
                  ]}>
                  {this.props.followers}
                </Text>
                <Text
                  style={[
                    styles.smallHeading,
                    {fontSize: RFValue(10, 580), fontFamily: 'Raleway-Medium'},
                  ]}>
                  Followers
                </Text>
              </Pressable>

              <Pressable onPress={()=>this.props.navigation.navigate("Following")} style={{flexDirection: 'column'}}>
                <Text
                  style={[
                    styles.h4,
                    {fontFamily: 'Montserrat-Medium', alignSelf: 'center'},
                  ]}>
                  {this.props.following}
                </Text>
                <Text
                  style={[
                    styles.smallHeading,
                    {fontSize: RFValue(10, 580), fontFamily: 'Raleway-Medium'},
                  ]}>
                  Following
                </Text>
              </Pressable>
            </View>
          </View>
        }
      </View>
    );
  }
}

const style = StyleSheet.create({
  profileImg: {
    height: 80,
    width: 80,
    top: 6,
    borderRadius: 50,
    alignSelf: 'center',
    // left:20
  },
  camIcon: {
    backgroundColor: '#dcdcdc',
    height: 28,
    width: 28,
    padding: 6,
    alignContent: 'center',
    borderRadius: 30,
    justifyContent: 'center',
    left: 30,
    top: -16,
    alignSelf: 'center',
  },
  icon: {
    marginLeft: 20,
    // fontSize:20,
    fontSize: RFValue(14.5, 580),
    marginBottom: 10,
  },
  Text: {
    position: 'absolute',
    // fontSize:20,
    fontSize: RFValue(14, 580),
    marginLeft: 80,
    // fontFamily:"Raleway-Medium",
    fontFamily: 'Raleway-SemiBold',
  },
  img: {
    width: '100%',
    height: 130,
    marginRight: 5,
    // borderColor:"#000",
    // borderWidth:0.5
  },
  card: {
    width: '33.3%',
    borderWidth: 0.2,
    // padding:5
  },
  img1: {
    width: '100%',
    height: 200,
    // borderColor:"#000",
    // borderWidth:0.5
  },
});
