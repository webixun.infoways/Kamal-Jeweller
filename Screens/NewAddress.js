import React, {Component, useState} from 'react';
import { View, Text , ScrollView, Image,Pressable,
    TextInput, Button, StyleSheet,TouchableOpacity, BackHandler, ImageBackground, ActivityIndicator} from 'react-native';
import {Dimensions} from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { Header,Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import RBSheet from 'react-native-raw-bottom-sheet';
import Toast from 'react-native-simple-toast'


//Global Style Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class NewAddress extends Component{
    constructor(props){
        super(props);
        this.state={
            name:"",
            contact:"",
            pinCode:"",
            city:"",
            landmark:"",
            state:"",
            address:"",
            area:"",
            house_no:"",
            isLoading:false


        }
    }
    renderLeftComponent(){
        return(
            <View style={{flexDirection:"row",width:win.width/2,}}>
               <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
                </View>
                <Text style={[styles.headerHeadingText,
                    ]}>Add New Address</Text>
            </View>
        )
    }

    // Set Address function
    add_address=()=>{
        if(this.state.name=="" || this.state.contact=="" || this.state.zip=="" || this.state.city=="" || this.state.landmark=="" || this.state.state=="" || this.state.area=="" || this.state.house_no==""   )
        {
            Toast.show("All fields are required!")
        }
        else if(this.state.pinCode.length != 6){
            Toast.show("Please enter valid pincode!")
        }
        else{
        this.setState({isLoading:true})
        fetch(global.api + "check-delivery", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': global.token
            },
            body: JSON.stringify({
                pincode: this.state.pinCode,
            })
        }).then((response) => response.json())
            .then((json) => {
                console.warn(json)
                if (!json.status) {
                    Toast.show(json.data)
                    this.setState({isLoading:false})
                }
                else {
                    fetch(global.api+"add-address", { 
                        method: 'POST',
                          headers: {    
                              Accept: 'application/json',  
                                'Content-Type': 'application/json',
                                'Authorization':global.token  
                               }, 
                                body: JSON.stringify({   
                                    user_id:global.user,
                                   name:this.state.name,
                                   contact:this.state.contact,
                                   zip:this.state.pinCode,
                                   city:this.state.city,
                                   landmark:this.state.landmark,
                                   state:this.state.state,
                                   address:this.state.area,
                                   house_no:this.state.house_no
                                        })}).then((response) => response.json())
                                        .then((json) => {
                                            console.warn(json)
                                            if(!json.status)
                                            {
                                               Toast.show("Enter valid details!")
                                            }
                                            else{
                                               this.props.navigation.goBack();
                                               Toast.show(json.success)
                                            }
                                           return json;    
                                       }).catch((error) => {  
                                               console.error(error);   
                                            }).finally(() => {
                                               this.setState({isLoading:false})
                                            });

                }
                return json;
            }).catch((error) => {
                console.error(error);
            }).finally(() => {

            });
        
                }
    }

    fetch_address_by_pincode=(e)=>{
        // console.warn(e)
        this.setState({pinCode:e})
        fetch('https://demo.webixun.com/api-fetch-city.php?pincode='+e, 
        {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization':global.token 
              },
            })
            .then((response) => response.json())
            .then((json) => {
                console.warn(json)
               this.setState({city:json.district,state:json.state})      
                return json;
            })
            .catch((error) => console.error(error))
            .finally(() => {
                this.setState({ isLoading: false });
            });
    }
    render(){
        return(
            <View  style={styles.container}
            >
                
                    {/* View for header component */}
                    <View >
                        <Header 
                    //    containerStyle={{height:82}}
                        statusBarProps={{ barStyle: 'light-content' }}
                        leftComponent={this.renderLeftComponent()}
                        // rightComponent={this.renderRightComponent()}
                        // data={this.props.route.params}
                        ViewComponent={LinearGradient} // Don't forget this!
                        linearGradientProps={{
                        colors: ['white', 'white'],
                        start: { x: 0, y: 0.5 },
                        end: { x: 1, y: 0.5 }
                        }} />
                    </View>
                        <ScrollView >
                    <View style={{backgroundColor:"#f5f5f5",paddingHorizontal:10}}>
                        <Text style={[styles.h3,{margin:5,fontSize:RFValue(12,580)}]}>
                            Contact Details
                        </Text>
                    </View>

                    {/* Component for contact fieldds */}
                    <View style={style.card}>
                <View >
                <TextInput style={style.textInput}
                placeholder="Name*"
                onChangeText={(e)=>this.setState({name:e})}
                value={this.state.name} />
                <TextInput style={style.textInput}
                placeholder="Mobile No*"
                maxLength={10}
                keyboardType='numeric'
                onChangeText={(e)=>this.setState({contact:e})}
                value={this.state.contact} />
                </View>
            
            </View>

                    <View style={{backgroundColor:"#f5f5f5",paddingHorizontal:10}}>
                        <Text style={[styles.h3,{margin:5,fontSize:RFValue(12,580)}]}>
                            Address
                        </Text>
                    </View>
                    {/* Component for address fields  */}
                    <View style={style.card}>
                    <View >
                    <TextInput style={style.textInput}
                    placeholder="House No.*"
                    onChangeText={(e)=>this.setState({house_no:e})}
                    keyboardType='numeric'
                    value={this.state.house_no} />
                    <TextInput style={style.textInput}
                    value={this.state.area}
                    onChangeText={(e)=>this.setState({area:e})}
                    placeholder="Address (Building,Area)*" />
                    <TextInput style={style.textInput}
                    placeholder="Pin Code*"
                    keyboardType='numeric'
                    maxLength={6}
                    // onEndEditing={this.fetch_address_by_pincode}
                    onChangeText={(e)=>{this.fetch_address_by_pincode(e)}}
                    value={this.state.pinCode} />
                    <TextInput style={style.textInput}
                    value={this.state.landmark}
                    onChangeText={(e)=>this.setState({landmark:e})}
                    placeholder="Locality/Town*" />
                    <TextInput style={style.textInput}
                    onChangeText={(e)=>this.setState({city:e})}
                    value={this.state.city}
                    placeholder="City/District*" />
                    <TextInput style={style.textInput}
                    value={this.state.state}
                    onChangeText={(e)=>this.setState({state:e})}
                    placeholder="State*" />
                    
                    </View>
                
                </View>
                        {this.state.isLoading ? 
                        <View>
                            <ActivityIndicator size='large' color='#bc3b3b' />
                        </View>:
                        <Pressable onPress={()=>this.add_address()} style={styles.buttonStyles} >
                            <Text style={styles.buttonText}>
                                Add Address
                            </Text>
                            </Pressable>
                        }
                </ScrollView>
                       
            </View>
        )
    }
}

export default NewAddress

const style = StyleSheet.create({
    card:{
        backgroundColor:"white",
        width:win.width,
        padding:10
    },
    textInput:{
        width:"100%",
        height:40,
        borderWidth:1,
        borderColor:"#d3d3d3",
        borderRadius:3,
        marginBottom:15,
        paddingLeft:10
    }
})

