import React, { Component } from 'react';
import { Dimensions, TextInput } from 'react-native';
import {
    View,ImageBackground,
    StyleSheet,Pressable,ScrollView,Linking,
    Image,Text,TouchableOpacity
} from 'react-native';
import {Icon,Header} from "react-native-elements"
import LinearGradient from 'react-native-linear-gradient';

//Global Style Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class Answers extends Component{
    constructor(props){
        super(props);
        this.state={
            likeColor:"black",
            dislikeColor:"black"
        }
    }

    renderLeftComponent(){
        return(
            <View style={{flexDirection:"row",width:win.width/2,}}>
                <Text style={{top:2}} >
                <Icon name="chevron-back-outline" color="#222222" size={22} type="ionicon"
                onPress={()=>this.props.navigation.goBack()}/>
                </Text>
            </View>
        )
    }
    like=()=>{
        
        if(this.state.likeColor="black"){
        this.state.likeColor="green",
        this.setState({likeColor:"green"}),
        this.setState({dislikeColor:"black"})
        }
    }
    dislike=()=>{
        
        if(this.state.dislikeColor="black"){
        this.state.dislikeColor="red",
        this.setState({dislikeColor:"red"}),
        this.setState({likeColor:"black"})
        }
    }
    
    render(){
        let {color}=this.state;
        return(
            <View>
                {/* View for header component */}
                <View>
                    <Header
                    // containerStyle={{height:82}}
                    statusBarProps={{ barStyle: 'light-content' }}
                    leftComponent={this.renderLeftComponent()}
                    ViewComponent={LinearGradient} // Don't forget this!
                    linearGradientProps={{
                    colors: ['white', 'white'],
                    start: { x: 0, y: 0.5 },
                    end: { x: 1, y: 0.5 }
                    }}
                />
                </View>
                
                <View style={style.answerView}>
                <Text style={[styles.p,{color:"black",
                fontFamily: "Raleway-Medium",}]}>
                    {this.props.route.params.item.faq_title}
                </Text>
                    <Text style={[styles.h3,{paddingTop:10,fontSize:13, color:"#7c7d7e"}]}>
                        {this.props.route.params.item.faq_description} 
                    </Text>
                </View>

                <View style={style.questView} >
                <Text style={styles.h4}>Was this Helpful?</Text>
                <View style={{flexDirection:"row"}}>
                    <Text style={{paddingRight:10}}>
                <Icon type="ionicon" onPress={()=>this.like()} color={this.state.likeColor}
                 name="thumbs-up-outline"
/></Text>
                <Icon type="ionicon" name="thumbs-down-outline" onPress={()=>this.dislike()} color={this.state.dislikeColor} /> 
                </View>
            </View>
            <View style={[style.questView,{marginTop:0}]} >
                <Text style={styles.h4}>Still not resolved?</Text>
                <Text style={[styles.h5,{color:"#326bf3"}]} onPress={() => Linking.openURL('mailto:support@kamaljewellers.com') }>
                        Mail Us
                    </Text>
            </View>
            

            </View>

        )
    }
}
export default Answers


const style=StyleSheet.create({
    questView:{padding:10,
        backgroundColor:"#fff",
        marginTop:10,
        margin:10,
        borderRadius:10,
        flexDirection:"row",
        justifyContent:"space-between"
    },
    answerView:{backgroundColor:"white",borderTopWidth:2,borderColor:"#f5f5f5", paddingTop:15,paddingBottom:20,padding:10}
    
})