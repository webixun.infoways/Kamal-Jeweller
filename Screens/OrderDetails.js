import React, {Component, useState} from 'react';
import { View, Text , ScrollView, Image,
    FlatList, Button, StyleSheet,TouchableOpacity, BackHandler, ImageBackground, Pressable, PlatformColor} from 'react-native';
    import StepIndicator from 'react-native-step-indicator';
import {Dimensions} from 'react-native';
import {Icon,Header,Rating, AirbnbRating} from "react-native-elements";
import LinearGradient from 'react-native-linear-gradient';
import { RFValue } from 'react-native-responsive-fontsize';
import Toast from 'react-native-simple-toast';
import moment from 'moment';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const styles=require("../Components/Style")

const win = Dimensions.get('window');

const {width,height} = Dimensions.get("window");

 class OrderDetails extends Component{
     constructor(props){
         super(props);
        //  console.warn(this.props.route.params.details)
         this.state={
            //  details:this.props.route.params.details,
            reviewProduct:"",
            id:"",
            rating:"",
            order_id:this.props.route.params.order_id,
            address:"",
            order_status:"",
            order_amount:"",
            payment_mode:"",
            shipping_charge:"",
            products:[],
            total_amount:"",
            order_history:[],
            market_price:"",
            isLoading:false
                     
        }
     }

     renderLeftComponent(){
        return(
            <View style={{flexDirection:"row",width:win.width/2,}}>
                <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
                </View>
                <Text style={[styles.headerHeadingText,
                    ]}>Order Details</Text>
            </View>
        )
    }

    renderRightComponent(){
        return(
            <View >
                <Icon
                name='cart-outline'
                size={24}
                color='#222222' 
                type="ionicon"
                onPress={()=>this.props.navigation.navigate("MyCart")}
                // style={{alignSelf:"center"}}
                /> 
            </View>
        )
    }

    componentDidMount(){
        this.order_details();
    }

    order_details=()=>{
        this.setState({isLoading:true})
        fetch(global.api+"order_detail", { 
            method: 'POST',
              headers: {    
                  Accept: 'application/json',  
                    'Content-Type': 'application/json',
                    'Authorization':global.token  
                   }, 
                    body: JSON.stringify({   
                       order_id:this.state.order_id
                            })}).then((response) => response.json())
                            .then((json) => {
                                // console.warn(json)
                                this.setState({details:json})
                                this.setState({products:json.data[0].order_item})
                                this.setState({order_history:json.data[0].order_history})
                                console.warn(this.state.order_history)
                                json.data.map(value=>{
                                    this.setState({address:value.shipping_address,
                                        payment_mode:value.payment_mode,order_status:value.order_status,
                                        shipping_charge:value.shipping_charge,discount:value.discount})

                                })
                                this.setState({order_amount:json.total,total_amount:json.with_shipping_charge,market_price:json.mptotal})
                               
                                // if(!json.status)
                                // {
                                //     var msg=json.msg;
                                //     Toast.show(msg);
                                // }
                                // else{
                                //    Toast.show("Product rated successfully!") 
                                // }
                               return json;    
                           }).catch((error) => {  
                                   console.error(error);   
                                }).finally(() => {
                                    this.setState({isLoading:false})
                                });
    }

    // rate product
    rate_product=(id)=>{
        if(this.state.rating == ""){
            Toast.show("Select rating before rating the product!")
        }
        else{
            fetch(global.api+"rate_and_review", { 
                method: 'POST',
                  headers: {    
                      Accept: 'application/json',  
                        'Content-Type': 'application/json',
                        'Authorization':global.token  
                       }, 
                        body: JSON.stringify({   
                           type:"rating",
                           product_id:id,
                           rating:this.state.rating
                                })}).then((response) => response.json())
                                .then((json) => {
                                    // console.warn(json)
                                    if(!json.status)
                                    {
                                        var msg=json.msg;
                                        // Toast.show(msg);
                                    }
                                    else{
                                       Toast.show("Product rated successfully!") 
                                       this.order_details();
                                    }
                                   return json;    
                               }).catch((error) => {  
                                       console.error(error);   
                                    }).finally(() => {
                                       
                                    });   
        }
    }

  

    // flatlist render item 
    productCard=({item})=>(
        <View style={[style.card,{backgroundColor:"#fafafa", borderRadius:10,}]}>
                   
                    <View style={{flexDirection:"row",marginBottom:-10}}>
                               <View style={{width:Dimensions.get('window').width/1.1}}>
                                    <View style={{flexDirection:"row",justifyContent:"space-between"}}>
                                        <View style={{flexDirection:"row"}}>
                                            <Image source={{uri:global.img_url+item.picture[0].src}} style={{width:45,height:50,marginTop:7}} />
                                            <Text style={style.name}>{item.product_name}</Text>
                                        </View>

                                        <View>
                                            <Text style={[styles.h6,{fontSize:RFValue(9,580),fontFamily: "Montserrat-SemiBold",
                                            textDecorationLine:"line-through",textDecorationStyle:"solid",marginTop:10}]}>
                                                ₹ {item.market_price}/-
                                            </Text>
                                            <Text style={[styles.h6,{fontSize:RFValue(10,580),fontFamily: "Montserrat-SemiBold"}]}>
                                                ₹ {item.price}/-
                                            </Text>
                                        </View>
                                      
                                    </View>
                               </View>
                         
                    
                    </View>
                    
                    
                            <View style={{flexDirection:"row",top:-18,left:55}}>
                            {/* <Icon  name="star" size={15} color="grey"  
                            /> */}
                            <Text style={[styles.h5,{marginLeft:3,top:-3, fontSize:RFValue(9,580)}]}>Qty: {item.product_quantity}</Text>
                            {/* <Text style={[styles.h5,{marginLeft:15,top:-2,fontSize:RFValue(9,580)}]}>Size:14</Text> */}
    
                            </View>
                    
                   
               

                
                {/* View for Rate Product */}

                <View>
                    {/* <Text style={{fontFamily: "Raleway-Medium",fontSize:RFValue(11,580),}}>Rate Product </Text> */}
                    
                    {
                        item.rating == null ?
                        <View  style={{flexDirection:"row",marginTop:-5,justifyContent:"space-between",marginLeft:35, padding:5}}>
                            <Text style={{left:10,top:-1}}>
                        
                            <AirbnbRating 
                            showRating={false}
                            readonly={true}
                            defaultRating={0}
                            fractions={1}
                            size={17}
                            // onFinishRating={this.rate_product}
                            onFinishRating={(e)=>this.setState({rating:e})}
                            style={{marginLeft:10}}
                            />
                            </Text>

                            <Pressable style={{marginLeft:25, padding:5,borderRadius:5,backgroundColor:"#bc3b3b",alignItems:"center"}}
                            onPress={()=>this.rate_product(item.id)}
                            >
                                <Text style={{fontFamily: "Raleway-Medium",fontSize:RFValue(10,580),paddingBottom:-10,color:"#fff",alignSelf:"center"}}>Rate Product </Text>
                            </Pressable>

                        </View>

                       
                        :
                        <View>
                        <Text style={{left:10,top:-1}}>
                    
                        <AirbnbRating 
                        showRating={false}
                        readonly={true}
                        defaultRating={item.rating.ratings}
                        fractions={1}
                        size={17}
                        // onFinishRating={this.rate_product}
                        style={{marginLeft:10}}
                        />
                    </Text>
                   
                    </View>
                        
                    }
                </View>
                </View>
    );
  
    render(){
    return(
      <View style={[style.container,{backgroundColor:"#fff"}]}>
                {/* View for header component */}
               <View>
                    <Header
                    // containerStyle={{height:82}}
                    statusBarProps={{ barStyle: 'light-content' }}
                    leftComponent={this.renderLeftComponent()}
                    // rightComponent={this.renderRightComponent()}
                    ViewComponent={LinearGradient} // Don't forget this!
                    linearGradientProps={{
                    colors: ['white', 'white'],
                    start: { x: 0, y: 0.5 },
                    end: { x: 1, y: 0.5 }
                    }}
                />
                </View>
         
               <ScrollView> 
                   {this.state.isLoading ?
                    <View>
                        <Loaders />
                    </View>
                    :
                    <View style={style.card}>
                        <View  style={{flexDirection:"row"}}>
                            <View style={[style.iconView,{marginBottom:Platform.OS == "ios" ? 10 : 0}]}>
                            <Icon name="cube-outline" size={20} type="ionicon" color="#fff" style={{marginTop:-2}} />
                            </View>
                            <View style={{margin:7}}>
                            <Text style={{color:"#222",fontSize:RFValue(10,580),fontFamily: "Raleway-Medium",}}>{this.state.order_status}</Text>
                            <Text style={{fontFamily: "Raleway-Regular",fontSize:RFValue(9,580)}}>Estimated delivery 
                            {/* {moment.utc(this.state.details.data[0].expected_time).local().startOf('seconds').fromNow()} */}
                            </Text>
                            </View>
                            </View>

                            {/* View for step indicator */}

                        <View style={{borderTopWidth:1,borderBottomWidth:1,borderColor:"#d3d3d3",paddingVertical:7,}}>  
                        <Text style={[styles.p,{top:2,marginBottom:10}]}>
                                Order Id: {this.state.order_id} 
                                </Text> 
                        {/* <Steps date={this.state.details.timestamps} /> */}
                        </View>

                        <View>
                        <FlatList
                        navigation={this.props.navigation}
                        showsVerticalScrollIndicator={false}
                        data={this.state.products}
                        renderItem={this.productCard}
                        keyExtractor={item=>item.id}  />
                        </View>

                        {/* View for shipping details */}

                        <View style={{padding:20, backgroundColor:"white",paddingVertical:10}}>
                            <Text style={[styles.h3,{fontSize:13, fontFamily:"Raleway-Medium",color:"#222222",}]}>
                                Shipping Details
                            </Text>
                            <View style={{top:5,borderTopWidth:1,borderBottomWidth:1,borderColor:"#d3d3d3",paddingVertical:10}}>
                            
                            <Text style={[styles.h4,{fontFamily:"Montserrat-Regular",color:"grey",fontSize:RFValue(10,580)}]}>{this.state.address}</Text>
                        </View>
                        </View>
                        
                        {/* View for Price details */}
                        <View style={{padding:20,paddingBottom:10,paddingTop:-10, backgroundColor:"white",}}>
                            <Text style={[styles.h3,{fontSize:13, fontFamily:"Raleway-Medium",color:"#222222",}]}>
                                Price Details
                            </Text>
                            <View style={{top:5,borderTopWidth:1,borderColor:"#d3d3d3",paddingVertical:10}}>
                                <View style={{flexDirection:"row",justifyContent:"space-between"}}>
                            <Text style={[styles.h4]}>Selling Price</Text>
                            <Text style={[styles.h6,{fontSize:RFValue(11,580),fontFamily: "Montserrat-Medium"}]}>₹ {this.state.market_price}</Text>
                            </View>
                            <View style={{flexDirection:"row",top:5, justifyContent:"space-between"}}>
                            <Text style={[styles.h4]}>Discount</Text>
                            <Text style={[styles.h6,{color:"green",fontSize:RFValue(11,580),fontFamily: "Montserrat-Medium"}]}>- ₹ {this.state.discount}</Text>
                            </View>
                            <View style={{flexDirection:"row",top:5, justifyContent:"space-between"}}>
                            <Text style={[styles.h4]}>Shipping Fee</Text>
                            <Text style={[styles.h6,{fontSize:RFValue(11,580),fontFamily: "Montserrat-Medium"}]}>₹ {this.state.shipping_charge}</Text>
                            </View>
                            <View style={{flexDirection:"row",marginTop:10,paddingVertical:10,borderTopWidth:1,borderColor:"#d3d3d3",paddingBottom:20, justifyContent:"space-between"}}>
                            <Text style={[styles.h4,]}>
                                Total Amount
                            </Text>
                            <Text style={[styles.h6,{fontSize:RFValue(11,580),fontFamily: "Montserrat-Medium"}]}>₹ {this.state.total_amount}</Text>
                            </View>
                        </View>
                        </View>

                    <View style={{flexDirection:"row",padding:10,marginTop:-20,marginBottom:30}}>
                        <Icon name="circle" size={10} color="grey" />
                        <Text style={[styles.h3,{fontSize:13, fontFamily:"Raleway-Medium",color:"#222222",top:-4,left:5 }]}>Payment Mode : 
                        {this.state.payment_mode == "cod" ?
                        <Text> Cash on Delivery (COD)</Text>
                        :
                        <Text> {this.state.payment_mode}</Text>}</Text>
                    </View>
                    
                    <View style={{marginBottom:20}}>
                            {/* order status */}
                            <View style={{borderTopWidth:1,borderBottomWidth:1,borderColor:"#d3d3d3",paddingVertical:7}}>
                                <Text style={[styles.p,{top:2,marginBottom:10}]}>Order Status</Text>
                            </View>
                            
                            {
                                this.state.order_history.map((value)=>{
                                    return(
                                        <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
                                            <Icon name="checkmark-done-circle" type="ionicon" color="#bc3b3b" size={30} style={{marginTop:30}}/>
                                            <View style={style.orderStatusCard}>
                                                <Text style={{fontSize:RFValue(11,580),marginLeft:10}}>{value.action}</Text>
                                                <Text style={{fontSize:RFValue(9,580),marginLeft:10}}> {moment(value.created_at).format("DD-MMM-YY hh:mm A")}</Text>
                                            </View>
                                    </View>
                                    )
                                })
                            }
                    </View>

                    </View>
                    }
                
              </ScrollView>
      </View>
    )   
  }
}

export default OrderDetails;

class Loaders extends Component{
    render(){
        return(
            <View>
                <SkeletonPlaceholder>
                    <View>
                        <View style={{flexDirection:"row"}}>
                        <View style={{height:40,width:40,borderRadius:100,marginLeft:10,marginTop:5}}/>
                        <View style={{marginTop:5,marginLeft:10}}>
                            <View style={{height:15,width:80}}/>
                            <View style={{height:15,width:150,marginTop:5}}/>
                        </View>
                        </View>
                        <View style={{height:25,width:150,marginTop:15,marginLeft:10}}/>
                        {/* products */}
                        <View style={{marginTop:15,height:80,width:Dimensions.get('window').width/1.05,alignSelf:"center",borderRadius:3}}/>
                        <View style={{marginTop:15,height:80,width:Dimensions.get('window').width/1.05,alignSelf:"center",borderRadius:3}}/>

                        {/* shipping */}
                        <View>
                            <View style={{height:15,width:120,marginTop:20,marginLeft:25}}/>
                            <View style={{marginTop:15,height:80,width:Dimensions.get('window').width/1.15,alignSelf:"center",borderRadius:3}}/>
                        </View>

                        {/* price details */}
                        <View>
                            <View style={{height:15,width:120,marginTop:20,marginLeft:25}}/>

                            <View style={{flexDirection:"row",justifyContent:"space-between",paddingHorizontal:30}}>
                                <View style={{height:15,width:100,marginTop:15}}/>
                                <View style={{height:15,width:100,marginTop:15}}/>
                            </View>
                            <View style={{flexDirection:"row",justifyContent:"space-between",paddingHorizontal:30}}>
                                <View style={{height:15,width:100,marginTop:15}}/>
                                <View style={{height:15,width:100,marginTop:15}}/>
                            </View>
                            <View style={{flexDirection:"row",justifyContent:"space-between",paddingHorizontal:30}}>
                                <View style={{height:15,width:100,marginTop:15}}/>
                                <View style={{height:15,width:100,marginTop:15}}/>
                            </View>
                            <View style={{flexDirection:"row",justifyContent:"space-between",paddingHorizontal:30}}>
                                <View style={{height:15,width:100,marginTop:15}}/>
                                <View style={{height:15,width:100,marginTop:15}}/>
                            </View>
                        </View>

                        {/* payment */}
                        <View style={{height:15,width:150,marginTop:20,marginLeft:25}}/>

                        {/* order status */}
                        <View style={{height:25,width:150,marginTop:15,marginLeft:10}}/>

                        <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
                            <View style={{height:40,width:40,borderRadius:50,marginTop:20}}/>
                            <View style={{marginTop:15,height:50,width:Dimensions.get('window').width/1.2,alignSelf:"center",borderRadius:25}}/>
                        </View>

                        <View style={{flexDirection:"row",justifyContent:"space-evenly"}}>
                            <View style={{height:40,width:40,borderRadius:50,marginTop:20}}/>
                            <View style={{marginTop:15,height:50,width:Dimensions.get('window').width/1.2,alignSelf:"center",borderRadius:25}}/>
                        </View>

                        <View style={{flexDirection:"row",justifyContent:"space-evenly",marginBottom:10}}>
                            <View style={{height:40,width:40,borderRadius:50,marginTop:20}}/>
                            <View style={{marginTop:15,height:50,width:Dimensions.get('window').width/1.2,alignSelf:"center",borderRadius:25}}/>
                        </View>
                    </View>
                </SkeletonPlaceholder>
            </View>
        )
    }
}


const labels = ["Cart","Delivery Address","Order Summary","Payment Method","Track"];
const customStyles = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize:30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: '#bc3b3b',
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: '#bc3b3b',
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: '#bc3b3b',
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: '#bc3b3b',
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: '#fe7013',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelSize: 13,
  currentStepLabelColor: '#fe7013',
  
}

const data=[
    {
        label:"Ordered and approved",
        status:"Your order has been placed",
        dateTime:"Sat, 3rd Nov 2021"
    },
    {
        label:"Shipped",
        status:"Shipped from Dehradun",
        dateTime:"Sat, 3rd Nov 2021"
    },
    {
        label:"At your nearest hub",
        status:"Reached dehradun",
        dateTime:"Sat, 3rd Nov 2021"
    },
    {
        label:"Out for delivery",
        status:"Will deliver today",
        dateTime:"Sat, 3rd Nov 2021"
    },
    {
        label:"Delivered",
        status:"Your order has been delivered",
        dateTime:"Sat, 5th Nov 2021"
    },
]

class Steps extends Component{
    constructor(props) {
        super(props);
        this.state = {
            currentPosition: 0
        }
    }

    onPageChange(position){
        this.setState({currentPosition: position});
    }
    render(){
        return(
            <View style={style.indicatorContainer}>
             <StepIndicator
         customStyles={customStyles}
         currentPosition={this.state.currentPosition}
         labels={labels}
         direction="vertical"
         renderLabel={({position,stepStatus,label,currentPosition}) => {
             return(
                 <View style={style.lblContainer}>
                     <Text style={style.lbltext}>{data[currentPosition].label}</Text>
                     <Text style={[style.lblstatus,{marginTop:-5}]}>{data[currentPosition].status}</Text>
                     <Text style={[style.lblstatus,{marginBottom:10}]}>
                         {this.props.date}</Text>
                 </View>
             )
         }}
    />
            </View>
        )
    }
}


const style = StyleSheet.create({
  container: { flex:1 },
        header:{
            textAlign:"center",
            fontSize:22,
            fontWeight:"bold",
            marginLeft:90,
            marginTop:13
      
          },
          heading:{
            fontSize:20,
            marginLeft:10,
            color:"black"
        },
        iconView:{
            backgroundColor:"#bc3b3b",
            width:35,
            height:35,
            marginTop:10,
            alignItems:"center",
             padding:8,
              borderRadius:25
        },
        card:{
            // height:230,
            marginTop:5,
            alignSelf:"center",
            elevation:0.6,
            shadowOffset:{  width: 1,  height: 2  },
            shadowColor: Platform.OS == "android" ? 'black' : "#f5f5f5",
            shadowOpacity: 1.0,
            width:"100%",
            backgroundColor:"#fff",
            // borderRadius:10,
            paddingHorizontal:10,
        },
        name: {
            fontSize: RFValue(10, 580),
            color: '#222222',
            fontFamily: 'Montserrat-Medium',
            marginTop: 10,
            margin: 10,
          },
        indicatorContainer:{
            height:250,
            width:100,
            marginTop:-10,
            marginBottom:20
            // backgroundColor:"black"
        },
        lblContainer:{
            marginTop:30,
            paddingLeft:10,
            width:width-150,
            // paddingTop:-10

        },
        lbltext:{
                fontSize:RFValue(10,580),
                top:-5,
                fontFamily: "Raleway-Regular",

        },
        lblstatus:{
            fontSize:RFValue(9,580),
            color:"grey",
            fontFamily: "Raleway-Regular",
        },
        orderStatusCard:{
            padding:10,
            backgroundColor:"#FFE4E1",
            borderRadius:50,
            width:Dimensions.get('window').width/1.2,
            marginTop:15,
        }
       

})