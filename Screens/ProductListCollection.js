import React, {Component} from 'react';
import {
  ImageBackground,
  Text,
  View,
  ScrollView,
  FlatList,
  StyleSheet,
  Image,
  Dimensions,
  ActivityIndicator,
  TouchableOpacity,
  TextInput,
  Pressable,
} from 'react-native';
import {Header, Icon} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import {RFValue} from 'react-native-responsive-fontsize';
import RBSheet from 'react-native-raw-bottom-sheet';
import SwiperFlatList from 'react-native-swiper-flatlist';
import Toast from 'react-native-simple-toast';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
//Global StyleSheet Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class ProductListCollection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoading: false,
      object: {},
      item:"",
      page:1,
      sliders:[]
      //   sort: 'new',
    };
  }

  renderLeftComponent() {
    return (
      <View style={{flexDirection: 'row', width: win.width }}>
        <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
        </View>
        <Text style={[styles.headerHeadingText]}>
          {this.props.route.params.name}
        </Text>
      </View>
    );
  }

  renderRightComponent() {
    return (
      <View
      style={{width:"130%", top:8,flexDirection:"row",justifyContent:"space-between"}}>
        <Icon
          name="heart-outline"
          size={22}
          color="#222222"
          type="ionicon"
          // style={{margin:10}}
          onPress={() => this.props.navigation.navigate('Wishlist')}
          // onPress={()=>this.onClick()}
        />
        <Icon
          name="notifications-outline"
          size={22}
          color="#222222"
          // style={{margin:10}}
          type="ionicon"
          onPress={() => this.props.navigation.navigate('Notifications')}
          // style={{marginTop:5}}
        />
        <Pressable onPress={()=>this.props.navigation.navigate("MyCart")} style={{marginRight:10}}>
              <Icon
                name='cart-outline'
                size={22}
                color='black' 
                type="ionicon"
                
                // style={{marginTop:5}} 
              /> 
              {this.state.item==""?null:
              <View style={{backgroundColor:"#bc3b3b",marginTop:-30, borderRadius:60,left:14}} >
              <Text style={{color:"#fff",left:7}}>{this.state.item}</Text>
              </View>
                }
              </Pressable>
      </View>
    );



    
  }

  componentDidMount() {
    this.get_productList();
    this.my_cart();
    this.fetch_category_sliders();
    this.focusListener = this.props.navigation.addListener('focus', () => {
      this.get_productList();
      this.my_cart();
    });
  
  }

  // cart item count
  my_cart=()=>{
    fetch(global.api + 'my-cart', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    })
      .then(response => response.json())
      .then(json => {
        console.warn(json);
        if (json.status) {
          this.setState({item: json.data.length});
        //   alert(this.state.item)
          
        } else {
          this.setState({item: ''});
        }

        return json;
      })
      .catch(error => console.error(error))
      .finally(() => {
      });
  }

  change_filter = filter => {
    this.setState({data: '', isLoading: true,sort_by:filter});
    this.get_productList(1,filter);
  };

  get_productList = (page_id,sort_by) => {
    //   alert(e)
    this.setState({isLoading:true})
    fetch(global.api + 'get-product-list', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        page:page_id,
        sort: sort_by,
        category_id: this.props.route.params.id,
      }),
    })
      .then(response => response.json())
      .then(json => {
        // console.warn(this.state.sort);
        console.warn(json.data);
        if (json.status) {
          this.setState({data: json.data.data});
          json.data.data.map(value => {
            const object = this.state.object;
            if (value.wishlist == null) {
              object[value.id] = false;
            } else {
              object[value.id] = true;
            }
            this.setState({object});
          });
        } else {
          // Toast.show('No data found');
          this.setState({data:[]})
        }

        return json;
      })
      .catch(error => console.error(error))
      .finally(() => {
        this.setState({isLoading: false});
      });
  };

  load_more = ()=>
  {
      var data_size=this.state.data.length;
      if(data_size>9)
      {
          var page=this.state.page+1;
          this.setState({load_more:true,page:page});
          // this.fetch_data(this.state.select_cat);
          this.fetch_data(page,this.state.sort_by);
      }
  }
  // add to wishlist

  like = id => {
    const object = this.state.object;
    if (object[id] == true) {
      object[id] = false;
      var type = 'no';
    } else {
      object[id] = true;
      var type = 'yes';
    }
    this.setState({object});
    fetch(global.api + 'add-to-wishlist', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        product_id: id,
        type: type,
      }),
    })
      .then(response => response.json())
      .then(json => {
        console.warn(json);
        if (!json.status) {
          var msg = json.msg;
          Toast.show(msg);
        } else {
          Toast.show(json.msg);
        }
      })
      .catch(error => {
        console.error(error);
      })
      .finally(() => {
        this.setState({isloading: false});
      });
  };


  // fetching product details
  fetch_category_sliders=()=>{
      fetch(global.api + 'get-category-banner',
          {
              method: 'POST',
              headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                  'Authorization': global.token
              },
              body: JSON.stringify({
                  category_id: this.props.route.params.id,
              })
          })
          .then((response) => response.json())
          .then((json) => {
          
           if(json.status){
            if(json.data.length > 0){
              this.setState({sliders:json.data})
              console.warn(this.state.sliders.length)
            }
           }
           else{
            // Toast.show(json.message)
           }
          })
          .catch((error) => console.error(error))
          .finally(() => {
          });
}
  
  render() {
    return (
      <View style={[styles.container, {backgroundColor: '#f5f5f5'}]}>
        {/* View for header component */}
        <View>
          <Header
            // containerStyle={{height: 80}}
            statusBarProps={{barStyle: 'light-content'}}
            leftComponent={this.renderLeftComponent()}
            rightComponent={this.renderRightComponent()}
            ViewComponent={LinearGradient} // Don't forget this!
            linearGradientProps={{
              colors: ['white', 'white'],
              start: {x: 0, y: 0.5},
              end: {x: 1, y: 0.5},
            }}
          />
        </View>
       
        <ScrollView showsVerticalScrollIndicator={false}>
          {this.state.sliders.length > 0 ?
           <Carousel data={this.state.sliders}
           isLoading={this.state.isLoading} />
           :
           <></>
          }
         
          <View>
            {/* <CategoryButtons navigation={this.props.navigation}/> */}

            {}
            <Product
              navigation={this.props.navigation}
              data={this.state.data}
              id={this.props.route.params.id}
              isLoading={this.state.isLoading}
              object={this.state.object}
              get_productList={this.get_productList}
              like={this.like}
            />
          </View>
        </ScrollView>

        {/* Bottom buttons */}
        <View>
          <View style={{flexDirection: 'row', marginTop: -20, height: 40}}>
            <Pressable
              style={styles.bottomButton1}
              onPress={() => this.RBSheet.open()}>
              <Text style={styles.bottomButton1Text}>GENDER</Text>
            </Pressable>
            <Pressable
              style={styles.bottomButton2}
              onPress={() => {
                this.RBSheet1.open();
              }}>
              <Icon
                name="swap-vertical-outline"
                type="ionicon"
                color="#222222"
                style={{top: 10}}
                size={18}
              />
              <Text style={styles.bottomButton2Text}>SORT BY</Text>
            </Pressable>
            {/* <Pressable
              style={styles.bottomButton2}
              //  onPress={()=>this.props.navigation.navigate("MyCart")}
            >
              <Icon
                name="funnel-outline"
                type="ionicon"
                color="#222222"
                style={{top: 10}}
                size={18}
              />
              <Text style={styles.bottomButton2Text}>FILTER</Text>
            </Pressable> */}
          </View>
          {/* Bottom Sheet for Gender */}
          <RBSheet
            ref={ref => {
              this.RBSheet = ref;
            }}
            closeOnDragDown={true}
            closeOnPressMask={true}
            height={150}
            customStyles={{
              wrapper: {
                // backgroundColor: "transparent",
                borderWidth: 1,
              },
              draggableIcon: {
                backgroundColor: '#fff',
              },
            }}>
            {/* bottom sheet elements  */}
            <View>
              {/* Bottom sheet View */}

              <View style={{width: '100%'}}>
                <View
                  style={{
                    width: '95%',
                    paddingBottom: -19,
                    borderBottomWidth: 1,
                    borderColor: '#d3d3d3',
                    alignSelf: 'center',
                  }}>
                  <Text
                    style={[
                      styles.bottomButton1Text,
                      {alignSelf: 'flex-start', fontSize: RFValue(13,580), top: -10},
                    ]}>
                    GENDER
                  </Text>
                </View>
                <Pressable
                  onPress={() => {
                    this.change_filter('male'), this.RBSheet.close();
                  }}>
                  <Text style={style.text1}>Men</Text>
                </Pressable>

                <Pressable
                  onPress={() => {
                    this.change_filter('female'), this.RBSheet.close();
                  }}>
                  <Text style={[style.text1]}>Women</Text>
                </Pressable>
              </View>
            </View>
          </RBSheet>

          {/* Bottom Sheet for SORT BY*/}
          <RBSheet
            ref={ref => {
              this.RBSheet1 = ref;
            }}
            closeOnDragDown={true}
            closeOnPressMask={true}
            height={220}
            customStyles={{
              container: {
                // borderTopLeftRadius:20,
                // borderTopRightRadius:20
              },
              wrapper: {
                // backgroundColor: "transparent",
                borderWidth: 1,
              },
              draggableIcon: {
                backgroundColor: '#fff',
              },
            }}>
            {/* bottom sheet elements */}
            <View>
              {/* Bottom sheet View */}

              <View style={{width: '100%'}}>
                <View
                  style={{
                    width: '95%',
                    paddingBottom: -19,
                    borderBottomWidth: 1,
                    borderColor: '#d3d3d3',
                    alignSelf: 'center',
                  }}>
                  <Text
                    style={[
                      styles.bottomButton1Text,
                      {alignSelf: 'flex-start', fontSize: RFValue(13,580), top: -10},
                    ]}>
                    SORT BY
                  </Text>
                </View>
                <View style={{justifyContent: 'space-evenly'}}>
                  <Pressable
                    onPress={() => {
                      this.change_filter('new'), this.RBSheet1.close();
                    }}>
                    <Text style={style.text1}>What's New</Text>
                  </Pressable>
                  <Pressable
                    onPress={() => {
                      this.change_filter('high_to_low'), this.RBSheet1.close();
                    }}>
                    <Text style={style.text1}>Price - High to Low</Text>
                  </Pressable>

                  <Pressable
                    onPress={() => {
                      this.change_filter('discount'), this.RBSheet1.close();
                    }}>
                    <Text style={style.text1}>Discount</Text>
                  </Pressable>
                  <Pressable
                    onPress={() => {
                      this.change_filter('low_to_high'), this.RBSheet1.close();
                    }}>
                    <Text style={style.text1}>Price - Low to High</Text>
                  </Pressable>
                </View>
              </View>
            </View>
          </RBSheet>
        </View>
      </View>
    );
  }
}
var width = Dimensions.get('window').width ;
//Array for main carousel images
const linkArray = [
  {image: require('../image/greenRing.jpg')},
  {image: require('../image/diamondRing.png')},
  {image: require('../image/blueRing.jpg')},
];

//this is for Main Carousel
class Carousel extends Component {
  //for swiper flat list
  renderItem = ({item}) => (
    <View style={style.child}>
      <ImageBackground
      // imageStyle={{height:160,width:width}}
        resizeMode="cover"
        source={{uri:"https://kamaljewellers.in/CDN/"+item.image}}
        style={{
          width,
          height: '100%',
          alignSelf: 'center',
          // because it's parent
          //width:width*0.9
        }}></ImageBackground>
    </View>
  );

  render() {
    return (
      <View>
       {this.props.isLoading ?
       <View>
         <SkeletonPlaceholder>
           <View style={{height:180,width:Dimensions.get('window').width}}/>
         </SkeletonPlaceholder>
       </View>
        :
        <View style={style.carousel}>
        <SwiperFlatList
          style={{flex: 1}}
          autoplay
          autoplayDelay={2}
          autoplayLoop
          index={0}
          showPagination
          renderItem={this.renderItem}
          paginationActiveColor="#bc3b3b"
          paginationDefaultColor='#696969'
          data={this.props.data}
          paginationStyleItem={{
            width: 7,
            height: 7,
            marginLeft: 0,
            marginRight: 5,
          }}></SwiperFlatList>
        </View>
      }
      </View>
    );
  }
}

class CategoryButtons extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View>
        <View style={{flexDirection: 'row', padding: 10}}>
          <TouchableOpacity style={style.button}>
            <Text style={style.buttonText}>For You</Text>
          </TouchableOpacity>
          <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View
              style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
              <TouchableOpacity style={style.catButton}>
                <Text style={style.catButtonText}>Necklace</Text>
              </TouchableOpacity>

              <TouchableOpacity style={style.catButton}>
                <Text style={style.catButtonText}>Ring</Text>
              </TouchableOpacity>

              <TouchableOpacity style={style.catButton}>
                <Text style={style.catButtonText}>Bracelets</Text>
              </TouchableOpacity>

              <TouchableOpacity style={style.catButton}>
                <Text style={style.catButtonText}>Chains</Text>
              </TouchableOpacity>

              <TouchableOpacity style={style.catButton}>
                <Text style={style.catButtonText}>Coins</Text>
              </TouchableOpacity>

              <TouchableOpacity style={style.catButton}>
                <Text style={style.catButtonText}>Mangalsutra</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

class BottomButtons extends Component {
  render() {
    return (
      <View>
        <View style={{flexDirection: 'row', marginTop: -20, height: 40}}>
          <Pressable
            style={styles.bottomButton1}
            onPress={() => this.RBSheet.open()}>
            <Text style={styles.bottomButton1Text}>GENDER</Text>
          </Pressable>
          <Pressable
            style={styles.bottomButton2}
            onPress={() => {
              this.RBSheet1.open();
            }}>
            <Icon
              name="swap-vertical-outline"
              type="ionicon"
              color="#222222"
              style={{top: 10}}
              size={18}
            />
            <Text style={styles.bottomButton2Text}>SORT BY</Text>
          </Pressable>
          <Pressable
            style={styles.bottomButton2}
            //  onPress={()=>this.props.navigation.navigate("MyCart")}
          >
            <Icon
              name="funnel-outline"
              type="ionicon"
              color="#222222"
              style={{top: 10}}
              size={18}
            />
            <Text style={styles.bottomButton2Text}>FILTER</Text>
          </Pressable>
        </View>
        {/* Bottom Sheet for Gender */}
        <RBSheet
          ref={ref => {
            this.RBSheet = ref;
          }}
          closeOnDragDown={true}
          closeOnPressMask={true}
          height={120}
          customStyles={{
            container: {
              // borderTopLeftRadius:20,
              // borderTopRightRadius:20
            },
            wrapper: {
              // backgroundColor: "transparent",
              borderWidth: 1,
            },
            draggableIcon: {
              backgroundColor: '#fff',
            },
          }}>
          {/* bottom sheet elements  */}
          <View>
            {/* Bottom sheet View */}

            <View style={{width: '100%'}}>
              <View
                style={{
                  width: '95%',
                  paddingBottom: -19,
                  borderBottomWidth: 1,
                  borderColor: '#d3d3d3',
                  alignSelf: 'center',
                }}>
                <Text
                  style={[
                    styles.bottomButton1Text,
                    {alignSelf: 'flex-start', fontSize: 12, top: -10},
                  ]}>
                  GENDER
                </Text>
              </View>
              <Pressable
                onPress={() => {
                  this.props.change_filter('male'), this.RBSheet.close();
                }}>
                <Text style={style.text1}>Men</Text>
              </Pressable>

              <Pressable
                onPress={() => {
                  this.props.change_filter('female'), this.RBSheet.close();
                }}>
                <Text style={style.text1}>Women</Text>
              </Pressable>
            </View>
          </View>
        </RBSheet>

        {/* Bottom Sheet for SORT BY*/}
        <RBSheet
          ref={ref => {
            this.RBSheet1 = ref;
          }}
          closeOnDragDown={true}
          closeOnPressMask={true}
          height={200}
          customStyles={{
            container: {
              // borderTopLeftRadius:20,
              // borderTopRightRadius:20
            },
            wrapper: {
              // backgroundColor: "transparent",
              borderWidth: 1,
            },
            draggableIcon: {
              backgroundColor: '#fff',
            },
          }}>
          {/* bottom sheet elements */}
          <View>
            {/* Bottom sheet View */}

            <View style={{width: '100%'}}>
              <View
                style={{
                  width: '95%',
                  paddingBottom: -19,
                  borderBottomWidth: 1,
                  borderColor: '#d3d3d3',
                  alignSelf: 'center',
                }}>
                <Text
                  style={[
                    styles.bottomButton1Text,
                    {alignSelf: 'flex-start', fontSize: 12, top: -10},
                  ]}>
                  SORT BY
                </Text>
              </View>
              <View style={{justifyContent: 'space-evenly'}}>
                <Pressable
                  onPress={() => {
                    this.props.change_filter('new'), this.RBSheet1.close();
                  }}>
                  <Text style={style.text1}>What's New</Text>
                </Pressable>
              
                <Pressable
                  onPress={() => {
                    this.props.change_filter('high_to_low'),
                      this.RBSheet1.close();
                  }}>
                  <Text style={style.text1}>Price - High to Low</Text>
                </Pressable>

                <Pressable
                  onPress={() => {
                    this.props.change_filter('discount'), this.RBSheet1.close();
                  }}>
                  <Text style={style.text1}>Discount</Text>
                </Pressable>
                <Pressable
                  onPress={() => {
                    this.props.change_filter('low_to_high'),
                      this.RBSheet1.close();
                  }}>
                  <Text style={style.text1}>Price - Low to High</Text>
                </Pressable>
              </View>
            </View>
          </View>
        </RBSheet>
      </View>
    );
  }
}
class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      load_more: false,
      object: {},
      page: 0,
      cat_id: this.props.id,
    };
  }

  load_data = () => {
    var data_size = this.props.data.length;
    // alert(this.state.cat_id)
    if (data_size > 9) {
      this.get_productList1();
    }
  };

  get_productList1 = e => {
    //   alert(e)
    this.setState({load_more: true});
    fetch(global.api + 'get-product-list', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        sort: '',
        category_id: this.state.cat_id,
      }),
    })
      .then(response => response.json())
      .then(json => {
        // console.warn(this.state.sort);
        // console.warn(json);
        if (json.data.data.length > 0) {
          if (json.status) {
            var obj = json.data.data;
            var joined = this.props.data.concat(obj);
            this.setState({data: joined});
            json.data.data.map(value => {
              const object = this.state.object;
              if (value.wishlist == null) {
                object[value.id] = false;
              } else {
                object[value.id] = true;
              }
              this.setState({object});
            });
          }
        } else {
          // Toast.show('No data found');
          this.setState({data:[]})
        }

        return json;
      })
      .catch(error => console.error(error))
      .finally(() => {
        this.setState({isLoading: false, load_more: false});
      });
  };

  productCard = ({item}) => (
    <View style={{width: '50%'}}>
      {/* product card */}

      <Pressable
        style={style.card}
        onPress={() =>
          this.props.navigation.navigate('Products', {
            id: item.id,
            object: this.state.object[item.id],
          })
        }>
          
        <Image source={{uri:global.img_url+item.picture[0].src}} style={style.img} />

        {/* view for icon and text */}
        <View
          style={{
            flexDirection: 'row',
            width: '100%',
            backgroundColor: '#fff',
          }}>
          {/* view for text */}
          <View style={{flexDirection: 'column', width: '80%'}}>
            <Text style={style.text} numberOfLines={1}>
              {item.name} 
            </Text>
            {/* <Text
              style={[styles.p, {fontSize: 11, top: -10, left: 5}]}
              numberOfLines={2}>
              {item.discription}
            </Text> */}
           <View style={{flexDirection:"row"}}>
              <Text style={[style.text, {top: -10,fontSize:RFValue(10,580),left: 2,
            textDecorationLine:"line-through"}]}>
                ₹ {item.market_price}
              </Text>
              <Text style={[style.text, {top: -10,fontSize:RFValue(10,580), left: 2}]}>
                ₹ {item.price}
              </Text>
           </View>
          </View>

          {/* icon view */}
          <View style={{width: '20%', marginTop: 5}}>
            <Icon
              name={this.props.object[item.id] ? 'heart' : 'heart-outline'}
              onPress={() => this.props.like(item.id)}
              type="ionicon"
              color={this.props.object[item.id] ? 'red' : '#7c7d7e'}
            />
          </View>
        </View>
      </Pressable>
    </View>
  );

  render() {
    return (
      <View>
        {this.props.isLoading ? (
          <View>
            <Loaders />
          </View>
        ) : (
          <View>
            {this.props.data == '' ?
              <View>
                <Image source={require('../image/no_search.webp')} style={{height:150,width:170,alignSelf:"center",marginTop:50}}/>
                <Text style={{color:"#bc3b3b",fontSize:RFValue(13,580),alignSelf:"center"}}>No match found!</Text>
              </View>
              :
              <View style={{marginBottom: 15}}>
              <FlatList
                columnWrapperStyle={{flex: 1 / 2}}
                numColumns={2}
                navigation={this.props.navigation}
                showsVerticalScrollIndicator={false}
                data={this.props.data}
                renderItem={this.productCard}
                keyExtractor={item => item.id}
                onEndReachedThreshold={0.5}
                onEndReached={() => {
                  this.load_data();
                }}
              />
            </View>
          }
          </View>
        )}
        {this.state.load_more?
           <View>
               <ActivityIndicator size="large" color="#326bf3" />
           </View>
             :
        <View></View>
        }
      </View>
    );
  }
}

export default ProductListCollection;

class Loaders extends Component{
  render(){
    return(
      <View>
        <SkeletonPlaceholder>
          <View style={{flexDirection:"row"}}>
            <View style={{height:250,width:200,marginTop:5}}/>
            <View style={{height:250,width:200,marginTop:5,left:5}}/>
          </View>

          <View style={{flexDirection:"row"}}>
            <View style={{height:250,width:200,marginTop:5}}/>
            <View style={{height:250,width:200,marginTop:5,left:5}}/>
          </View>

          <View style={{flexDirection:"row"}}>
            <View style={{height:250,width:200,marginTop:5}}/>
            <View style={{height:250,width:200,marginTop:5,left:5}}/>
          </View>
        </SkeletonPlaceholder>
      </View>
    )
  }
}

const style = StyleSheet.create({
  child: {
    height: 185,
    width: width * 0.9,
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 25,
    marginLeft: 20,
    // borderRadius:-5,
  },
  carousel: {
    width,
    //   borderRadius: 15,
    height: 180,
    alignItems: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    //   marginTop:5,
  },
  button: {
    backgroundColor: '#BC3B3B',
    padding: 5,
    borderRadius: 25,
    width: 80,
    justifyContent: 'center',
  },
  buttonText: {
    alignSelf: 'center',
    color: '#fff',
    fontFamily: 'Montserrat-Medium',
    fontSize: RFValue(11, 580),
  },
  catButton: {
    // backgroundColor:"#BC3B3B",
    padding: 10,
    marginLeft: 10,
    borderRadius: 25,
    justifyContent: 'center',
    borderColor: '#EBEBEB',
    borderWidth: 1,
    // width:100
  },
  catButtonText: {
    alignSelf: 'center',
    color: '#7D7D7D',
    fontFamily: 'Montserrat-Medium',
    fontSize: RFValue(12, 580),
  },
  img: {
    width: '100%',
    height: 200,
    // borderColor:"#000",
    // borderWidth:0.5
  },
  card: {
    width: Dimensions.get('window').width / 2.02,
    marginTop: 3,
    // margin:50
    //    marginLeft:50
  },
  text: {
    fontFamily: 'Raleway-Medium',
    color: '#222222',
    padding: 5,
    fontSize: RFValue(12, 580),
  },
  text1: {
    fontFamily: 'Raleway-Medium',
    color: '#222222',
    marginLeft: 10,
    marginTop:5,
    padding: 5,
    fontSize: RFValue(12, 580),
  },
});
