import React, { Component } from 'react';
import {
    View, ImageBackground,
    StyleSheet, Pressable,
    Image, TouchableOpacity, Text, SafeAreaView, Dimensions
} from 'react-native';
import Swiper from 'react-native-swiper';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { AuthContext } from '../AuthContextProvider.js';

//Global Style Import
const style = require('../Components/Style.js');

class Skip extends Component {
    static contextType = AuthContext;
    constructor(props) {
        super(props);
        this.state={
            status:false
        }
    }

    slide2 =()=>{
        // this.setState({status : !this.state.status})
        alert('hi')

    }

    render() {
        return (

            <View style={{ flex: 1, backgroundColor: "white" }}>

                {/* Main Slider */}
                <Swiper style={styles.wrapper}
                    showsButtons={false}
                    paginationStyle={
                        { bottom: 70 }
                    }
                //  autoplay={true}
                >
                    {/* Slide 1 */}
                    <View style={styles.slide1}>
                        <ImageBackground
                            imageStyle={{ opacity: 0.3 }} source={require("../image/skip1.jpg")}
                            style={styles.slide1Image}>
                            <Text style={styles.text}>Find Best Jewellery</Text>
                            <Text style={styles.textSmall}>Discover the best jewellery from the only shop</Text>
                            <Text style={styles.textSmall}>with all the latest designs and customizations.</Text>
                        </ImageBackground>
                    </View>

                    {/* Slide 2 */}
                    <View style={styles.slide2} >
                        <ImageBackground
                            imageStyle={{ opacity: 0.3 }} source={require("../image/skip5.jpg")}
                            style={styles.slide1Image}>
                            <Text style={styles.text}>Latest Designs</Text>
                            <Text style={styles.textSmall}>Get latest and trendy designs in all the</Text>
                            <Text style={styles.textSmall}>categories in gold, diamonds and other.</Text>
                        </ImageBackground>
                    </View>
                  

                    {/* Slide 3 */}
                    <View style={styles.slide3}>
                        <ImageBackground
                            imageStyle={{ opacity: 0.3 }} source={require("../image/skip3.jpg")}
                            style={styles.slide1Image}>
                            <Text style={styles.text1}>Best Quality Jewellery</Text>
                            <Text style={styles.textSmall1}>Discover the best jewellery from the only shop</Text>
                            <Text style={styles.textSmall1}>with all the latest designs and customizations.</Text>
                            {/* next button */}
                            <TouchableOpacity style={styles.buttonStyles}
                                onPress={()=>{this.context.logout();}}>
                                <Text style={styles.buttonText}>Next</Text>
                            </TouchableOpacity>
                        </ImageBackground>
                    </View>
                </Swiper>
            </View>
        )
    }
}

export default Skip;


//Internal Stylesheet
const styles = StyleSheet.create({
    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#fff"

    },
    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#fff"
    },
    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#fff"
    },
    slide1Image: {
        height: "100%",
        width: "100%",

    },
    slide2Image: {
        height: "100%",
        width: "100%",
    },
    slide3Image: {
        height: "100%",
        width: "100%",
    },
    text: {
        top: 420,
        color: '#000',
        fontSize: RFValue(25, 580),
        fontFamily: "DancingScript-Bold",
        alignSelf: 'center'
    },
    textSmall: {
        top: 440,
        color: '#4a4b4d',
        fontSize: RFValue(11, 580),
        fontFamily: "Montserrat-Regular",
        alignSelf: 'center',
    },
    text1: {
        top: 420,
        color: '#000',
        fontSize: RFValue(25, 580),
        fontFamily: "DancingScript-Bold",
        alignSelf: 'center',
    },
    textSmall1: {
        top: 440,
        color: '#4a4b4d',
        fontSize: RFValue(11, 580),
        fontFamily: "Montserrat-Regular",
        alignSelf: 'center'
    },
    buttonStyles: {
        backgroundColor: "#BC3B3B",
        width: Dimensions.get("window").width / 1.7,
        alignSelf: "center",
        justifyContent: "center",
        alignItems: "center",
        height: 42,
        borderRadius: 25,
        top: 490,
    },
    buttonText: {
        color: "#fff",
        top:-2,
        fontSize: RFValue(12, 580),
        fontFamily: "Raleway-Medium",
    }
})