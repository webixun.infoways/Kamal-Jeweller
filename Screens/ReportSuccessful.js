import React, { Component } from 'react';
import {
    Text,View,
    StyleSheet,Image
} from 'react-native';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";

//Global Style Import
const styles = require('../Components/Style.js');

class ReportSuccessful extends Component{
    constructor(props){
        super(props);
        this.state={
            show:true
        }
    }

componentDidMount(){
    setTimeout(() => {
        this.props.navigation.navigate("Studio")
      }, 2000)
       
}
    render(){
        return(
            <View style={styles.container}>
                {this.state.show ?
                <View style={{marginTop:300,alignContent:"center"}}>

                    <Image source={require("../image/check.png")} style={style.image}/>
                    
                    <Text style={style.text}>
                        Thanks for letting us know
                    </Text>

                    <Text style={[style.smallText,{marginTop:10}]}>
                        Your feedback is important in helping us keep the 
                    </Text>

                    <Text style={style.smallText}>
                        Kamal Jewellers community safe.
                    </Text>
                </View>
                :
                null
                }
            </View>
        )
    }
}

export default ReportSuccessful;

const style=StyleSheet.create({
    text:{
        fontFamily:"Raleway-SemiBold",
        // fontSize:20,
        fontSize:RFValue(14.5, 580),
        alignSelf:"center",
        marginTop:10
    },
    image:{
        width:70,
        height:70,
        alignSelf:"center"
    },
    smallText:{
        // fontSize:13,
        fontSize:RFValue(9, 580),
        fontFamily:"Roboto-Regular",
        alignSelf:"center"
    }
})