import React, { Component, useState } from 'react';
import {
    View, Text, ScrollView, Image, Pressable,
    TextInput, Button, StyleSheet, TouchableOpacity, BackHandler, ImageBackground, ActivityIndicator
} from 'react-native';
import { Dimensions } from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize';
import { Header, Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import RBSheet from 'react-native-raw-bottom-sheet';
import Toast from 'react-native-simple-toast'


//Global Style Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class EditAddress extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.route.params.id,
            name: "",
            contact: "",
            pinCode: "",
            city: "",
            landmark: "",
            state: "",
            address: "",
            area: "",
            house_no: "",
            isLoading: false


        }
    }

    componentDidMount = () => {
        this.set_address();
    }
    set_address = () => {
        fetch(global.api + "set-address", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': global.token
            },
            body: JSON.stringify({
                address_id: this.state.id,
            })
        }).then((response) => response.json())
            .then((json) => {
                if (json.status) {
                    console.warn(json.data)


                    this.setState({ name: json.data.name })
                    this.setState({ contact: json.data.contact })
                    this.setState({ pinCode: json.data.zip })
                    this.setState({ city: json.data.city })
                    this.setState({ landmark: json.data.landmark })
                    this.setState({ state: json.data.state })
                    this.setState({ address: json.data.address })
                    this.setState({ area: json.data.address })
                    this.setState({ house_no: json.data.house_no })

                }
                return json;
            }).catch((error) => {
                console.error(error);
            }).finally(() => {
                this.RBSheet.close()
            });

    }
    renderLeftComponent() {
        return (
            <View style={{ flexDirection: "row", width: win.width / 2, }}>
                <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
                </View>
                <Text style={[styles.headerHeadingText,
                ]}>Edit Address</Text>
            </View>
        )
    }

    // Set Address function
    add_address = (e) => {
        if (this.state.name == "" || this.state.contact == "" || this.state.zip == "" || this.state.city == "" || this.state.landmark == "" || this.state.state == "" || this.state.area == "" || this.state.house_no == "") {
            Toast.show("All fields are required!")
        }
        else if(this.state.pinCode.length != 6){
            Toast.show("Please enter valid pincode!")
        }
        else {
            this.setState({ isLoading: true })
            fetch(global.api + "check-delivery", {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': global.token
                },
                body: JSON.stringify({
                    pincode: e
                })
            }).then((response) => response.json())
                .then((json) => {
                    console.warn(json)
                    if (!json.status) {
                        Toast.show(json.data)
                        this.setState({isLoading:false})
                    }
                    else {
                        fetch(global.api+"update-address", { 
                            method: 'POST',
                              headers: {    
                                  Accept: 'application/json',  
                                    'Content-Type': 'application/json',
                                    'Authorization':global.token  
                                   }, 
                                    body: JSON.stringify({   
                                        user_id:global.user,
                                       name:this.state.name,
                                       contact:this.state.contact,
                                       zip:this.state.pinCode,
                                       city:this.state.city,
                                       landmark:this.state.landmark,
                                       state:this.state.state,
                                       address:this.state.area,
                                       house_no:this.state.house_no,
                                       address_id:this.state.id,
                                            })}).then((response) => response.json())
                                            .then((json) => {
                                                console.warn(json)
                                                if(!json.status)
                                                {
                                                   
                                                    Toast.show("Something went wrong!");
                                                }
                                                else{
                                                   this.props.navigation.goBack();
                                                   Toast.show(json.success)
                                                }
                                               return json;    
                                           }).catch((error) => {  
                                                   console.error(error);   
                                                }).finally(() => {
                                                   this.setState({isLoading:false})
                                                });

                    }
                    return json;
                }).catch((error) => {
                    console.error(error);
                }).finally(() => {

                });

        }
    }

    fetch_address_by_pincode = (e) => {
        // console.warn(e)
        this.setState({ pinCode: e })
        fetch('https://demo.webixun.com/api-fetch-city.php?pincode=' + e,
            {
                method: 'GET',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': global.token
                },
            })
            .then((response) => response.json())
            .then((json) => {
                console.warn(json)
                this.setState({ city: json.district, state: json.state })
                return json;
            })
            .catch((error) => console.error(error))
            .finally(() => {
                this.setState({ isLoading: false });
            });
    }
    render() {
        return (
            <View style={styles.container}
            >

                {/* View for header component */}
                <View >
                    <Header
                        //    containerStyle={{height:82}}
                        statusBarProps={{ barStyle: 'light-content' }}
                        leftComponent={this.renderLeftComponent()}
                        // rightComponent={this.renderRightComponent()}
                        // data={this.props.route.params}
                        ViewComponent={LinearGradient} // Don't forget this!
                        linearGradientProps={{
                            colors: ['white', 'white'],
                            start: { x: 0, y: 0.5 },
                            end: { x: 1, y: 0.5 }
                        }} />
                </View>
                <ScrollView >
                    <View style={{ backgroundColor: "#f5f5f5", paddingHorizontal: 10 }}>
                        <Text style={[styles.h3, { margin: 5, fontSize: RFValue(12, 580) }]}>
                            Contact Details
                        </Text>
                    </View>

                    {/* Component for contact fieldds */}
                    <View style={style.card}>
                        <View >
                            <TextInput style={style.textInput}
                                placeholder="Name*"
                                onChangeText={(e) => this.setState({ name: e })}
                                value={this.state.name} />
                            <TextInput style={style.textInput}
                                placeholder="Mobile No*"
                                maxLength={10}
                                keyboardType='numeric'
                                onChangeText={(e) => this.setState({ contact: e })}
                                value={this.state.contact} />
                        </View>

                    </View>

                    <View style={{ backgroundColor: "#f5f5f5", paddingHorizontal: 10 }}>
                        <Text style={[styles.h3, { margin: 5, fontSize: RFValue(12, 580) }]}>
                            Address
                        </Text>
                    </View>
                    {/* Component for address fields  */}
                    <View style={style.card}>
                        <View >
                            <TextInput style={style.textInput}
                                placeholder="House No.*"
                                onChangeText={(e) => this.setState({ house_no: e })}
                                keyboardType='numeric'
                                value={this.state.house_no} />
                            <TextInput style={style.textInput}
                                value={this.state.area}
                                onChangeText={(e) => this.setState({ area: e })}
                                placeholder="Address (Building,Area)*" />
                            <TextInput style={style.textInput}
                                placeholder="Pin Code*"
                                keyboardType='numeric'
                                maxLength={6}
                                // onEndEditing={this.fetch_address_by_pincode}
                                onChangeText={(e) => { this.fetch_address_by_pincode(e) }}
                                value={this.state.pinCode} />
                            <TextInput style={style.textInput}
                                value={this.state.landmark}
                                onChangeText={(e) => this.setState({ landmark: e })}
                                placeholder="Locality/Town*" />
                            <TextInput style={style.textInput}
                                onChangeText={(e) => this.setState({ city: e })}
                                value={this.state.city}
                                placeholder="City/District*" />
                            <TextInput style={style.textInput}
                                value={this.state.state}
                                onChangeText={(e) => this.setState({ state: e })}
                                placeholder="State*" />

                        </View>

                    </View>
                    {this.state.isLoading ?
                        <View>
                            <ActivityIndicator size='large' color='#bc3b3b' />
                        </View> :
                        <Pressable onPress={() => this.add_address(this.state.pinCode)} style={styles.buttonStyles} >
                            <Text style={styles.buttonText}>
                                Update Address
                            </Text>
                        </Pressable>
                    }
                </ScrollView>

            </View>
        )
    }
}

export default EditAddress

const style = StyleSheet.create({
    card: {
        backgroundColor: "white",
        width: win.width,
        padding: 10
    },
    textInput: {
        width: "100%",
        height: 40,
        borderWidth: 1,
        borderColor: "#d3d3d3",
        borderRadius: 3,
        marginBottom: 15
    }
})

