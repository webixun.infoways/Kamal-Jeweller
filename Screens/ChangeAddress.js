import React, { Component } from 'react';
import {
    Text,View,
    StyleSheet,Image,Dimensions,
    TouchableOpacity,Pressable,PermissionsAndroid,ActivityIndicator
} from 'react-native';
import {Icon} from "react-native-elements"
import LinearGradient from 'react-native-linear-gradient';
import {Searchbar} from "react-native-paper";
import MapView, {Marker,  } from 'react-native-maps';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import Geolocation from '@react-native-community/geolocation';
navigator.geolocation = require('@react-native-community/geolocation');
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import Geocoder from "react-native-geocoding";
import LocationEnabler from 'react-native-location-enabler';

//Global Style Import
const styles = require('../Components/Style.js');

const latitudeDelta = 0.015;
const longitudeDelta=0.0121;

const win = Dimensions.get('window');

const {
  PRIORITIES: { HIGH_ACCURACY },
  addListener,
  checkSettings,
  requestResolutionSettings
} = LocationEnabler

class AddressChange extends Component{
    constructor(props){
        super(props);
    }

    render(){
        return(
            <View style={style.container}>
                {/* Component call for map */}
                <Map navigation={this.props.navigation}
                state={this.props.state}/>
            </View>
        )
}
}
export default AddressChange;

//this component is for map
class Map extends Component{
  constructor(props)
    {
        super(props);
        this.state={
          // region:{
            latitudeDelta:0.1,
            longitudeDelta:0.05,
            latitude:'',
            longitude:'',
          // },
            modalVisibleSorry:false,
            msg:'',
            timing:'',
            address:'',
            city:"",
            postal_code:"",
            area:"",
            state:"",
            object:{},
            home:'',
            isLoading:false,
        }
        
    }

    locationOn =()=>{
      const listener = addListener(({ locationEnabled }) =>
      console.log(`Location are ${ locationEnabled ? 'enabled' : 'disabled' }`)
    );
  
    // Define configuration
    const config = {
      priority: HIGH_ACCURACY, // default BALANCED_POWER_ACCURACY
      alwaysShow: true, // default false
      needBle: false, // default false
    };
  
    // Check if location is enabled or not
    checkSettings(config);
  
    // If location is disabled, prompt the user to turn on device location
    requestResolutionSettings(config);
  
    // ...
    // Removes this subscription
    listener.remove();
    
    }

  //Current location
  componentDidMount = async () =>{

  try {
    
    this.locationOn();
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      {
        title: "Kamal Jewellers wants to access your location!",
        message:
          "We'll send you alerts about" +
          "the latest deals and offer.",
        buttonNegative: "Cancel",
        buttonPositive: "OK"
      }
    );
    // Fetching current location
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      Geolocation.getCurrentPosition(
          (info) => {
            this.setState({latitude:info.coords.latitude,longitude:info.coords.longitude})
            this.fetch_location(this.state.latitude,this.state.longitude);
          },
          (error) => {
            // See error code charts below.
            console.log(error.code, error.message);
          },
          { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
      );
    }
  } catch (err) {
    console.warn(err);
  }

     
}


    // Marker 
    onRegionChange = region =>{
      this.setState({latitude:region.latitude, longitude:region.longitude});
      this.fetch_location(region.latitude,region.longitude);
    }


    // For location search
    search_location = (data,details) =>
    {

      console.log(details);
      // alert(details.address_components[0].long_name)
      this.setState({address:details.address_components[0].long_name})
      this.setState({postal_code:details.address_components[1].long_name})
       this.setState({city:details.address_components[3].long_name})
       this.setState({area:details.address_components[2].long_name})
       this.setState({state:details.address_components[4].long_name})
       this.setState({
         latitude:details.geometry.location.lat,
        longitude:details.geometry.location.lng,});
        this.setState({latitude:this.state.latitude,longitude:this.state.longitude})
        global.address=this.state.address;
        global.postal_code=this.state.postal_code;
        global.city=this.state.city;
        global.area=this.state.area;
        global.state=this.state.state;

      }


      fetch_location=(lati,longi)=>
      {
        Geocoder.init(global.google_key);
        Geocoder.from(lati, longi).then(json => {

          this.setState({address:json.results[1].address_components[1].long_name})
          this.setState({postal_code:json.results[0].address_components[1].long_name})
           this.setState({city:json.results[0].address_components[3].long_name})
           this.setState({area:json.results[0].address_components[2].long_name})
           this.setState({state:json.results[0].address_components[4].long_name})
     
        var addressComponent = json.results[1].address_components[1].long_name;
 

        console.log(json.results[1].address_components[1]);
     }).catch(error => console.warn(error));
        
        global.address=this.state.address;
        global.postal_code=this.state.postal_code;
        global.city=this.state.city;
        global.area=this.state.area;
        global.state=this.state.state;
  
     }

  render(){
    let {region}=this.state;
    return(
      <View style={{height:"100%"}}>
        {
          (this.state.latitude != '' && this.state.longitude != '')?
          <View style={{height:"70%"}}>
         <MapView
            // provider={PROVIDER_GOOGLE} // remove if not using Google Maps
            style={style.map}
            onRegionChangeComplete={this.onRegionChange}
            region={{latitude:this.state.latitude,longitude:this.state.longitude,latitudeDelta:this.state.latitudeDelta,longitudeDelta:this.state.longitudeDelta}}
          >
            
         
         </MapView>
         
          <View style={{position:"absolute",alignSelf:"center",top:220,}}>
            <Image source={require('../image/placeholder.png')} 
            style={{height:35,width:35,}}/>
          </View>
          </View>
          :
          <View style={{alignSelf:"center",marginTop:270, justifyContent:"center"}}>
            <ActivityIndicator size="large" color="#326bf3"/>
            <Text style={styles.h4}>
            Locating...
              </Text>
             </View>
          }
         
          <View style={{position:"absolute",alignSelf:"center",top:220,}}>
            <Image source={require('../image/placeholder.png')} 
            style={{height:35,width:35,}}/>
          </View>
          
           <Pressable style={style.backIcon} 
            onPress={()=>this.props.navigation.goBack()}  >
            <Icon type="ionicon" name="arrow-back-outline" size={20} />
           </Pressable>

           <GooglePlacesAutocomplete
            placeholder='Search'
            fetchDetails={true}
            returnKeyType={'default'}
            onPress={(data, details = null) => {this.search_location(data,details)
            }}
            query={{
              key: global.google_key,
              language: 'en',
            }}
            styles={{
              container: {
                borderRadius:10,
                position:"absolute",
                top:48,
                left:68,
                width:"80%",
                alignSelf:"center",
                fontFamily: "Montserrat-Bold",
                shadowColor: 'grey',
                elevation: 1,
                color:"#000",
              }
            }}
          />

        <View style={style.bottomContainer}>
          
          <Text style={style.select}>Select your location </Text>
          <View style={{width:win.width, padding:10}}>


        {/* Location address area loading */}
        {
          (this.state.address != '')?  
          <View>
        <View style={{flexDirection:"row"}}>
         <Image source={require("../image/placeholder.png")} 
         style={{height:20,width:20,marginTop:5}}/>
         
         
         {/* change button row view */}
         <View style={{flexDirection:"row", width:"100%"}}>
    
         <Text numberOfLines={1} style={style.text}>{this.state.address}</Text>
         {/* <Pressable style={style.changeButton}>
             <Text style={style.changeText} >CHANGE</Text>
         </Pressable> */}
         </View>
    
         </View>
        <View>
         <Text 
         style={{fontFamily: "Montserrat-SemiBold",fontSize:RFValue(11, 580)}}
         >{this.state.postal_code} {this.state.area},{this.state.city},{this.state.state}</Text>
        </View>
        
        {!this.state.isLoading ? 
        <View>
          <Pressable 
            onPress={()=>this.props.navigation.goBack()}
            style={[styles.buttonStyles,{top:20}]}>
                 <Text style={styles.buttonText}>
                 Confirm Location</Text>
           </Pressable>
        </View> :
          <View style={{height:100}}>
            <ActivityIndicator size="large" color="#326bf3"
            style={{top:35}}/>
          </View>
          }
        </View>
       :
       <View style={{flexDirection:"row"}}>
         {/* <Image source={require("../img/icons/pin1.png")} 
         style={{height:20,width:20,marginTop:5}}/> */}
         
         
         {/* change button row view */}
         <View style={{flexDirection:"row", width:"100%"}}>
    
         <Text numberOfLines={1} style={style.text}>Locating...</Text>
         {/* <Pressable style={style.changeButton}>
             <Text style={style.changeText} >CHANGE</Text>
         </Pressable> */}
         </View>
    
         </View>
        }
       
       </View>
             </View>
          {/* <View 
          style={{borderRadius:10
          ,position:"absolute",top:55,height:44,
          width:"85%",alignSelf:"center",
          fontFamily:"Raleway-Regular"}}>
            
            
          </View> */}
      

          {/* <Searchbar
          style={{borderRadius:10
          ,position:"absolute",top:55,height:44,
          width:"85%",justifyContent:"center",alignSelf:"center",
          fontFamily:"Raleway-Regular"}}
          placeholder="Search here..."
          icon={()=><Icon name="search-outline" type="ionicon"/>}
        /> */}
      </View>
    )
  }
}

// Internal styling 

const style = StyleSheet.create({
    container: {
      flex:1,
      backgroundColor:"transparent",
      height:"100%"
    },
    map: {
      height:"100%",
      // flex:0.7
    },      
      bottomContainer:{
        borderTopLeftRadius:15,
        borderTopRightRadius:15,
        backgroundColor:"white",
        position:"absolute",
        bottom:0,
        // flex:0.3,
        height:"30%"
      },
      text:{
        fontFamily: "Montserrat-SemiBold",
        // fontSize:20,
        fontSize:RFValue(14, 580),
        marginLeft:5,
      },
      backIcon:{
          position:"absolute",
          top:50,
          backgroundColor:"white",
          borderRadius:50,
          left:10, 
          width:40,
          justifyContent:"center",
          height:40,
          padding:5
      },
      select:{
        // fontSize:14,
        fontSize:RFValue(11, 580),
        color:"grey",
        fontFamily: "Montserrat-SemiBold",
      }
   });