import React, { Component } from 'react'
import {View, Image,Text, StyleSheet,Dimensions, ScrollView, TouchableOpacity, TextInput}  from "react-native"
import { Header,Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import RBSheet from 'react-native-raw-bottom-sheet'
import { RFValue } from 'react-native-responsive-fontsize';
import Toast from 'react-native-simple-toast';

//Global Style Import
const style = require('../Components/Style.js');

const win = Dimensions.get('window');

class PaymentDetails extends Component {

    constructor(props){
        super(props);
        this.state={
            firstName:'',
            lastName:'',
            cardNo:'',
            cvv:'',
            expYear:'',
            expMonth:'',
        }
    }

    renderLeftComponent(){
        return(
            <View style={{flexDirection:"row",width:win.width/2,}}>
                <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
                </View>
                <Text style={[style.headerHeadingText,
                    ]}>Payment Details</Text>
            </View>
        )
    }

    cardDetails = () =>{
        let validation=/^[a-zA-Z" "]+$/;
        let NoValidation=/^[0-9]+$/;
        let monthValidation=/^[0-9]+$/;
        let yearValidation=/^[0-9]+$/;
        let noValid=NoValidation.test(this.state.cardNo)
        let monthValid=monthValidation.test(this.state.expMonth)
        let yearValid=yearValidation.test(this.state.expYear)
        let isValid = validation.test(this.state.firstName)
        let isvalid = validation.test(this.state.lastName)
          {
              this.setState({msg:""});
                if(this.state.cardNo == "" || this.state.cvv == "" ||
                this.state.firstName == "" || this.state.lastName == "" ||
                this.state.expYear== "" || this.state.expMonth == ""){
                    Toast.show("All fields are required !");
                }
                else if(this.state.cardNo.length<12  ){
                Toast.show("Enter a valid Card Number !");
                }
                else if(!noValid ){
                    Toast.show("Enter a valid Card Number !");
                }
                else if(!monthValid ){
                    Toast.show("Enter a valid Expiry Month !");
                }
                else if(!yearValid ){
                    Toast.show("Enter a valid Expiry Year !");
                }
                else if(!isValid){
                    Toast.show("Enter a valid First name !");
                }
                else if(!isvalid){
                    Toast.show("Enter a valid Last name !");
                }
                else{
                this.setState({isLoading:true});
  
                  // temporary naviagtion remove at the time of api call
                  this.RBSheet.close();
  
                  // var name=this.state.name;
                  // var mail=this.state.mail;
  
                  // fetch("", { 
                  //      method: 'POST',
                  //        headers: {    
                  //            Accept: 'application/json',  
                  //              'Content-Type': 'application/json'  
                  //             }, 
                  //              body: JSON.stringify({   
                  //                 mail: mail, 
                  //                 name: name,
      
                  //                      })}).then((response) => response.json())
                  //                      .then((json) => {
                  //                          if(!json.status)
                  //                          {
                  //                              var msg=json.msg;
                  //                              Toast.show(msg);
                  //                          }
                  //                          else{
                  //                             this.props.navigation.navigate("Location",{
                  //                                 name:this.state.name,
                  //                                 mail:this.state.mail
                  //                             })
                  //                             global.name=this.state.name;
                  //                             global.mail=this.state.mail;
                  //                          }
                  //                         return json;    
                  //                     }).catch((error) => {  
                  //                             console.error(error);   
                  //                          }).finally(() => {
                  //                             this.setState({isLoading:false})
                  //                          });
              }
          }
      }

    render() {
        return (
            <View style={{flex:1,backgroundColor:"white"}}>
                {/* View for header component */}
                <View>
                    <Header
                    containerStyle={{height:66}}
                    statusBarProps={{ barStyle: 'light-content' }}
                    leftComponent={this.renderLeftComponent()}
                    ViewComponent={LinearGradient} // Don't forget this!
                    linearGradientProps={{
                    colors: ['white', 'white'],
                    start: { x: 0, y: 0.5 },
                    end: { x: 1, y: 0.5 }
                    }}
                    />
                </View>

                <View style={{marginHorizontal:25,marginTop:5,}}>
                    <Text style={styles.methodText}>Customize your payment method</Text>
                </View>

                {/* Card component */}
                <Card />

                <TouchableOpacity onPress={()=>this.RBSheet.open()}>
                <View style={[style.buttonStyles,{flexDirection:"row",top:30}]}>
                    <Icon name="add" size={25} type="ionicon" color="white" />
                    <Text style={style.buttonText}> Add another Card</Text>
                </View>
                </TouchableOpacity>

                {/* Bottom sheet */}
                <RBSheet
                ref={ref => {
                    this.RBSheet = ref;
                }}
                height={480}
                openDuration={250}
                customStyles={{
                    container: {
                        borderTopLeftRadius:20,
                        borderTopRightRadius:20
                    }
                }}
                >
            {/* Bottom sheet component */}
            <View>
            <View style={{ borderBottomWidth:1,marginHorizontal:20,marginTop:20, borderColor:"#d3d3d3"}}>
                    <Text style={[styles.methodText,{fontSize:15,alignSelf:"flex-start"}]} >Add credit/debit card</Text>
                    
                </View>
                <TextInput 
                placeholder="Card Number"
                style={styles.textInput} 
                maxLength={12}
                keyboardType="number-pad"
                value={this.state.cardNo}
                onChangeText={(text) => {this.setState({cardNo:text})}}
                />
            <View style={{flexDirection:"row",justifyContent:"space-between",alignItems:"center",alignContent:"center", width:"75%",alignSelf:"center"}}>
                <Text style={{fontSize:RFValue(12,580),top:4, fontFamily: "Raleway-Medium",}}>Expiry Date</Text>
                <TextInput 
                 placeholder="MM"
                 maxLength={2}
                keyboardType="number-pad"
                value={this.state.expMonth}
                onChangeText={(text) => {this.setState({expMonth:text})}}
                style={[styles.textInput,{width:"25%",paddingLeft:28 }]} />
                
                <TextInput 
                 placeholder="YY"
                 maxLength={2}
                keyboardType="number-pad"
                value={this.state.expYear}
                onChangeText={(text) => {this.setState({expYear:text})}}
                style={[styles.textInput,{width:"25%",paddingLeft:30}]} />
            </View>
                <TextInput 
                placeholder="CVV"
                maxLength={3}
                keyboardType="number-pad"
                secureTextEntry={true}
                style={styles.textInput}
                value={this.state.cvv}
                onChangeText={(text) => {this.setState({cvv:text})}} />
                
                <TextInput 
                placeholder="First Name"
                style={styles.textInput}
                value={this.state.firstName}
                onChangeText={(text) => {this.setState({firstName:text})}} />

                <TextInput 
                placeholder="Last Name"
                style={styles.textInput} 
                value={this.state.lastName}
                onChangeText={(text) => {this.setState({lastName:text})}}/>

                <TouchableOpacity onPress={()=>this.cardDetails()}>
                <View style={[style.buttonStyles,{flexDirection:"row",top:20,bottom:10}]}>
                    <Icon name="add" size={25} type="ionicon" color="white" />
                    <Text style={style.buttonText}> Add Card</Text>
                </View>
                </TouchableOpacity>
                </View>
            
        </RBSheet>
          
            </View>
        )
    }
}

export default PaymentDetails


class Card extends Component {
    render() {
        return (
            <View style={styles.card}>
                    <View style={{marginTop:10, borderBottomWidth:1,flexDirection:"row",justifyContent:"space-between", borderColor:"#d3d3d3"}}>
                    <Text style={[styles.methodText,{fontSize:RFValue(11,580)}]}>Card/Cash on delivery</Text>
                    <Icon name="checkmark" type="ionicon" size={25} color="#bc3b3b" />
                </View>
                <View style={{marginTop:15, borderBottomWidth:1,flexDirection:"row",paddingBottom:15, justifyContent:"space-between", borderColor:"#d3d3d3"}} >
                    <Image source={require("../image/cardpic.jpg")} style={{width:45,height:25,marginTop:0}} />
                    <Text style={{fontFamily: "Roboto-Regular",fontSize:RFValue(12,580)}}>**** **** 2609</Text>
                    <View style={{padding:3,borderWidth:1,borderColor:"#bc3b3b",height:25, paddingBottom:0, alignContent:"center", width:85,borderRadius:20,}}>
                        <Text style={{color:"#bc3b3b", fontFamily: "Raleway-Regular",fontSize:RFValue(9,580),alignSelf:"center"}}>Delete Card</Text>
                    </View>
                </View>
                <Text style={[styles.methodText,{fontSize:RFValue(11,580),marginTop:10}]}>Other methods</Text>
            </View>
        )
    }
}




const styles=StyleSheet.create({
    heading:{
        fontSize:20,
        marginLeft:10,
        color:"black"
    },
    methodText:{
        fontSize:RFValue(12,580),
        fontFamily: "Raleway-Medium",
        marginBottom:7,
    },
    card:{
        // height:150,
        marginTop:5,
        alignSelf:"center",
        elevation:1,
        shadowOffset:{  width: 1,  height: 2  },
        shadowColor: 'black',
        shadowOpacity: 1.0,
        width:"90%",
        backgroundColor:"#fafafa",
        borderRadius:10,
        paddingHorizontal:25,
    },
    textInput:{
        width:"85%",
        backgroundColor:"#f5f5f5",
        alignSelf:"center",
        borderRadius:20,
        marginTop:15,
        paddingLeft:20,
        fontFamily: "Roboto-Regular",
        fontSize:RFValue(11,580)
    }
}
)