import React, { Component } from 'react'
import { View, Text , ScrollView, Image,Modal,Pressable,FlatList,ActivityIndicator,
    TextInput, Button, StyleSheet,TouchableOpacity, BackHandler, ImageBackground} from 'react-native';
import {Dimensions} from 'react-native';

import RBSheet from 'react-native-raw-bottom-sheet';
import { RFValue } from 'react-native-responsive-fontsize'
import {Icon,Header} from "react-native-elements"
import LinearGradient from 'react-native-linear-gradient';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import Post from '../Components/Post';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const styles=require("../Components/Style")

const win = Dimensions.get('window');

const options = {
    title: "Pick an Image",
    storageOptions:{
        skipBackup: true,
        path:'images'
    },
    quality:0.5
}

class Studio extends Component{
    constructor(props){
        super(props);
        this.state={
            data: [],
            object: {},
            save: {},
            like: {},
            follow: {},
            isLoading: true,
            load_more: false,
            id: '',
            isFetching: false,
            isLoadingFeed:true,
            page:1
        }
    }

    componentDidMount() {
        this.fetch_feeds(1);
        this.focusListener = this.props.navigation.addListener('focus', () => {
          // this.fetch_feeds(1);
        });
    }

    fetch_feeds = (page_id) => {
        fetch(global.api + 'fetch-feed?page='+page_id, {
          method: 'GET',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: global.token,
          },
        })
          .then(response => response.json())
          .then(json => {
            console.warn(json)
            if (json.status) {
              this.setState({ data: json.data.data });
              console.warn('hh',this.state.data.length);
              if(json.data?.data?.length >0){
              json.data.data.map((value, key) => {
                const object = this.state.object;
                const save = this.state.save;
                const like = this.state.like;
                // const follow = this.state.like;
                like[value.id] = value.feed_like_count;
                if (value.feed_like == null) {
                  object[value.id] = false;
                } else {
                  object[value.id] = true;
                }
    
                // if (value.user_follow == null) {
                //   follow[value.user_id] = false;
                // } else {
                //   follow[value.user_id] = true;
                // }
    
                if (value.feed_save == null) {
                  save[value.id] = false;
                } else {
                  save[value.id] = true;
                }
    
                this.setState({ object });
                // this.setState({follow});
                this.setState({ save });
                this.setState({ like });
              });
              }
            } else {
              Toast.show('No data found');
            }
            return json;
          })
          .catch(error => console.error(error))
          .finally(() => {
            this.setState({ isLoadingFeed: false, load_more: false, isFetching: false });
          });
    };

    //to refresh feeds on swipe down
    onRefresh() {
        this.setState({ isFetching: true,isLoadingFeed:true,page:1 }, () => {
          this.fetch_feeds(1);
        });
    }

    //to load more feeds
    // load_more = ()=>
    // {
    //     console.warn(this.state.data.length)
    //     var data_size=this.state.data.length;

    //     if(data_size>9)
    //     {
    //       var page=this.state.page+1;
    //       this.setState({page:page,load_more:true,data:[]})
    //         this.fetch_feeds(page);

    //     }
    // }

    //function for share
    myShare = async (title, content, url) => {
        const shareOptions = {
        title: title,
        message: content,
        url: url,
        }
        try {
        const ShareResponse = await Share.open(shareOptions);

        } catch (error) {
        console.log("Error=>", error)
        }
    }

    // User follow
    follow = id => {
        const follow = this.state.follow;
        if (this.state.follow[id] == true) {
        follow[id] = false;
        var type = 'unfollow';
        } else {
        follow[id] = true;
        var type = 'follow';
        }
        this.setState({ follow });

        fetch(global.api + 'follow-user', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: global.token,
        },
        body: JSON.stringify({
            following_id: id,
            type: type,
        }),
        })
        .then(response => response.json())
        .then(json => {
            // console.log(json)

            if (!json.status) {
            Toast.show(json.msg);
            } else {
            Toast.show(json.msg);
            }
        });
    };

    // feed like
    like = id => {
        const object = this.state.object;
        const like = this.state.like;
        if (this.state.object[id] == true) {
        object[id] = false;
        like[id] = like[id] - 1;
        var type = 'no';
        } else {
        object[id] = true;
        like[id] = like[id] + 1;
        var type = 'yes';
        }
        this.setState({ like });
        this.setState({ object });

        fetch(global.api + 'feed-like', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: global.token,
        },
        body: JSON.stringify({
            feed_id: id,
            type: type,
        }),
        })
        .then(response => response.json())
        .then(json => {
            if (!json.status) {
            Toast.show("Something went wrong.");
            } else {
            // Toast.show(json.msg);
            }
        });
    };

    // feed save
    bookmark = id => {
        const save = this.state.save;
        if (this.state.save[id] == true) {
        save[id] = false;
        var type = 'unsave';
        } else {
        save[id] = true;
        var type = 'save';
        }
        this.setState({ save });

        fetch(global.api + 'user-feed-save', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: global.token,
        },
        body: JSON.stringify({
            feed_id: id,
            type: type,
        }),
        })
        .then(response => response.json())
        .then(json => {
            // console.log(json)

            if (!json.status) {
            Toast.show("Something went wrong.");
            }
            else {
            // Toast.show(json.msg);
            }
        });
    };

    // report feed
    report = () => {
        this.RBSheet.close();
        fetch(global.api + 'feed-report-user', {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: global.token,
        },
        body: JSON.stringify({
            feed_id: this.state.id,
            report: 'Report this post',
        }),
        })
        .then(response => response.json())
        .then(json => {
            console.warn(json);
            if (!json.status) {
            Toast.show(json.msg);
            } else {
            Toast.show(json.msg);
            this.fetch_feeds();
            }
        });
    };

      // Bottom sheet
  sheet(id, feed_index) {
    this.setState({ id: id, feed_index: feed_index });
    this.RBSheet.open();
  }

  // navigate to user profile
  userProfile = id => {
    if (id == global.user) {
      this.props.navigation.navigate('Profile');
    } else {
      this.props.navigation.navigate('UserProfile', { id: id });
    }
  };

  productCard = ({ item }) => (

    <View>
        <Post item={item} navigation={this.props.navigation}/>
    </View>
    
  );

    renderLeftComponent(){
        return(
            <View style={{flexDirection:"row",width:win.width/2}}>
                <Text style={styles.headerHeadingText}>Studio</Text>
            </View>
        )
    }

    renderRightComponent(){
        return(
            <View style={{top:2,padding:5}}>
                <Icon
                name='bookmark-outline'
                size={22}
                color='#222222' 
                type="ionicon"
                onPress={()=>this.props.navigation.navigate("SavedPost")}
                // style={{alignSelf:"center"}}
                /> 
            </View>
        )
    }


    render(){
        return(
            <View style={[styles.container,{backgroundColor:"#f5f5f5"}]}>
                {/* View for header component */}
                <View>
                        <Header
                        // containerStyle={{height:82}}
                        statusBarProps={{ barStyle: 'light-content' }}
                        leftComponent={this.renderLeftComponent()}
                        rightComponent={this.renderRightComponent()}
                        ViewComponent={LinearGradient} // Don't forget this!
                        linearGradientProps={{
                        colors: ['white', 'white'],
                        start: { x: 0, y: 0.5 },
                        end: { x: 1, y: 0.5 }
                        
                        }}
                    />
                    </View>
                        <ScrollView showsVerticalScrollIndicator={false}>
                    <View>
                    <View>
                        {this.state.isLoadingFeed ? (
                        <View>
                            <Loaders />
                        </View>
                        ) : (
                        (this.state.data.length>0) ?
                            <FlatList
                            navigation={this.props.navigation}
                            showsVerticalScrollIndicator={false}
                            data={this.state.data}
                            renderItem={this.productCard}
                            keyExtractor={item => item.id}
                            onRefresh={() => this.onRefresh()}
                            refreshing={this.state.isFetching}
                            // onEndReachedThreshold={0.5}
                            // onEndReached={() => {
                            //     this.load_more();
                            // }}
                            />
                            :
                            <View >
                            <Image style={{ width: 350, height: 300, marginTop: 80 }}
                                source={require('../image/noFeed.png')} />
                            <Text style={[styles.h3, { alignSelf: "center", marginTop: 10 }]}>
                                No Feeds Found!
                            </Text>
                            </View>
                        )}
                        {this.state.load_more ? (
                        <View
                            style={{
                            alignItems: 'center',
                            flex: 1,
                            backgroundColor: 'white',
                            flex: 1,
                            paddingTop: 20,
                            }}>
                            <ActivityIndicator animating={true} size="large" color="#bc3b3b" />
                            <Text style={styles.p}>Please wait...</Text>
                        </View>
                        ) : (
                        <View></View>
                        )}
                    </View>
                           
                           
                    </View>
                    </ScrollView>
                        
                    <Add navigation={this.props.navigation} />
            </View>
        )
    }
}

export default Studio


class Add extends Component{
    constructor(props){
        super(props);
        this.state={
            image:""
        }
    }

  //function to launch camera
camera =()=>{
    launchCamera(options, (response)=>{
        
        if(response.didCancel){
            console.warn(response)
            console.warn("User cancelled image picker");
        } else if (response.error){
            console.warn('ImagePicker Error: ', response.error);
        }else{
            // const source = {uri: response.assets.uri};
          let path = response.assets.map((path)=>{
              return (
                //  console.warn(path.uri) 
                  this.setState({image:path.uri}) 
              )
          });
          //  this.setState({image:path.uri})
          this.RBSheet.close(),
          this.props.navigation.navigate("CreatePost",{image:this.state.image})
        }
    })
    }
    
    
    //function to launch gallery
    gallery =()=>{
    ImagePicker.openPicker({
        width:300,
        height:400,
        cropping:true,
    }).then(image=>{
        console.log(image);
        // this.setState({image:"Image Uploaded"})
        this.setState({image:image.path});
        // this.upload_image();   
        
        this.props.navigation.navigate("CreatePost",{image:this.state.image})   
    })
    }
    
    //function to upload image
    upload_image = () =>
    {
    this.RBSheet.close()
    this.setState({image_load:true});
    var photo = {
        uri: this.state.image,
        type: 'image/jpeg',
      };
      
     
      
      fetch("", { 
        method: 'POST',
        // body: form,
          headers: {  
            // 'Content-Type': 'multipart/form-data'  
               }, 
                }).then((response) => response.json())
                        .then((json) => {
                            this.setState({photo:this.state.image});
                            let rr=this.state.data;
                            
                            rr.photo=this.state.image;
    
                            try {
                                AsyncStorage.setItem('@key_data', JSON.stringify(rr));
    
                
                            } catch (e) {
                               Toast.show(e);
                            }
    
    
                            Toast.show("Profile Picture Updated!");
                            return(
                                <View>
                                    {/* <Text>hiiii</Text> */}
                                </View>
                            ),
                            console.warn(json);   
                       }).catch((error) => {  
                               console.error(error);   
                            }).finally(() => {
                                this.setState({image_load:false});
                            });
    }

  render(){
        return(
            <View style={style.add}>
                <Pressable onPress={()=>this.RBSheet.open()} >
                    <Icon name="add-outline" 
                    size={28}
                    color='white' 
                    style={{margin:10}}
                    type="ionicon"  />
                </Pressable>
                
                          {/* Bottom Sheet for Camera */}
                          <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        closeOnDragDown={true}
                        closeOnPressMask={true}
                        height={150}
                        customStyles={{
                            container:{
                                borderTopLeftRadius:20,
                                borderTopRightRadius:20
                            },
                        wrapper: {
                            // backgroundColor: "transparent",
                            borderWidth: 1
                        },
                        draggableIcon: {
                            backgroundColor: "grey"
                        }
                        }}
                        >
                        {/* bottom sheet elements */}
                        <View>
                        
                        {/* Bottom sheet View */}
                            <View style={{width:"100%",padding:20}}>
                            <TouchableOpacity onPress={this.camera}>
                                        <Text style={style.iconPencil}>
                                            <Icon name='camera' type="ionicon" color={'#bc3b3b'} size={25}/>
                                        </Text>
                                        <Text style={[style.Text,{fontFamily:"Raleway-SemiBold"}]}>Take a picture</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity onPress={this.gallery} > 
                                        <Text style={style.iconPencil}>
                                            <Icon name='folder' type="ionicon" color={'#bc3b3b'} size={25}/>
                                        </Text>
                                        <Text style={[style.Text,{fontFamily:"Raleway-SemiBold"}]}>Select from library</Text>
                                        </TouchableOpacity>

                            </View>
                        
                        </View>
                        </RBSheet>

            </View>
        )
    }
}
class Story extends Component{
    state = {
        modalVisible: false
      };
        
      setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
      }

      camera =()=>{

        launchCamera(options, (response)=>{
            
            if(response.didCancel){
                console.warn(response)
                console.warn("User cancelled image picker");
            } else if (response.error){
                console.warn('ImagePicker Error: ', response.error);
            }else{
                // const source = {uri: response.assets.uri};
              let path = response.assets.map((path)=>{
                  return (
                      console.warn(path.uri)   
                  )
              })
              this.setState({image:path.uri}) 
            }
        })
        this.RBSheet.close()
      }
      gallery =()=>{
        ImagePicker.openPicker({
            width:300,
            height:400,
            cropping:true,
        }).then(image=>{
            console.log(image);
            this.setState({image:"Image Uploaded"})
            

        })
        this.RBSheet.close()
    }
    
    render(){
        const { modalVisible } = this.state;
        return(
            <View style={{borderBottomWidth:1,borderColor:"#dedede",paddingVertical:10}}> 
                <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
            <View style={{flexDirection:'row',padding:10,}}>
                {/* My story */}
                <View>
                <TouchableOpacity style={style.story} onPress={()=>this.RBSheet.open()}>
                <Text style={{position:"absolute",backgroundColor:"black",padding:2, borderRadius:10, right:-3,top:42}}>
                <Icon name="add" size={16} color={"white"} />
                </Text>
                    <Image source={require("../image/logo/kj.png")} style={style.img} />
                </TouchableOpacity>
                
                <Text style={[styles.h5,{color:"grey",fontSize:RFValue(10,580),alignSelf:"center",left:5,top:3}]}>Your story</Text>
                </View>
                 {/* Bottom Sheet for Camera */}
                 <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        closeOnDragDown={true}
                        closeOnPressMask={true}
                        height={150}
                        customStyles={{
                            container:{
                                borderTopLeftRadius:20,
                                borderTopRightRadius:20
                            },
                        wrapper: {
                            // backgroundColor: "transparent",
                            borderWidth: 1
                        },
                        draggableIcon: {
                            backgroundColor: "grey"
                        }
                        }}
                        >
                        {/* bottom sheet elements */}
                        <View>
                        
                        {/* Bottom sheet View */}
                            <View style={{width:"100%",padding:20}}>
                            <TouchableOpacity onPress={this.camera}>
                                        <Text style={style.iconPencil}>
                                            <Icon name='camera' type="ionicon" color={'#bc3b3b'} size={25}/>
                                        </Text>
                                        <Text style={style.Text}>Take a picture</Text>
                                        </TouchableOpacity>

                                        <TouchableOpacity onPress={this.gallery} > 
                                        <Text style={style.iconPencil}>
                                            <Icon name='folder' type="ionicon" color={'#bc3b3b'} size={25}/>
                                        </Text>
                                        <Text style={style.Text}>Select from library</Text>
                                        </TouchableOpacity>

                            </View>
                        
                        </View>
                        </RBSheet>
                {/* Others Stories */}
                <View>
                <TouchableOpacity style={[style.story,{padding:0}]} onPress={() => this.setModalVisible(true)}>
                    <Image source={require("../image/greenRing.jpg")} style={style.storyimg} />
                </TouchableOpacity>
                <Text style={[styles.h5,{color:"grey",fontSize:RFValue(10,580),alignSelf:"center",left:5,top:3}]}>Ruby</Text>
                </View>

                

                <View>
                <TouchableOpacity style={[style.story,{padding:0}]} onPress={() => this.setModalVisible(true)}>
                    <Image source={require("../image/bracelets.jpg")} style={style.storyimg} />
                </TouchableOpacity>
                <Text style={[styles.h5,{color:"grey",fontSize:RFValue(10,580),alignSelf:"center",left:5,top:3}]}>Shivi</Text>
                </View>

                <View>
                <TouchableOpacity style={[style.story,{padding:0}]} onPress={() => this.setModalVisible(true)}>
                    <Image source={require("../image/chains.jpg")} style={style.storyimg} />
                </TouchableOpacity>
                <Text style={[styles.h5,{color:"grey",fontSize:RFValue(10,580),alignSelf:"center",left:5,top:3}]}>Manik</Text>
                </View>

                <View>
                <TouchableOpacity style={[style.story,{padding:0}]} onPress={() => this.setModalVisible(true)}>
                    <Image source={require("../image/coin.png")} style={style.storyimg} />
                </TouchableOpacity>
                <Text style={[styles.h5,{color:"grey",fontSize:RFValue(10,580),alignSelf:"center",left:5,top:3}]}>Ansh</Text>
                </View>

                <View>
                <TouchableOpacity style={[style.story,{padding:0}]} onPress={() => this.setModalVisible(true)}>
                    <Image source={require("../image/blueRing.jpg")} style={style.storyimg} />
                </TouchableOpacity>
                <Text style={[styles.h5,{color:"grey",fontSize:RFValue(10,580),alignSelf:"center",left:5,top:3}]}>Maeve</Text>
                </View>

                {/* Modal Component for stories */}
                <Modal
                    visible={modalVisible}        
                    onSwipeComplete={() => setModalVisible(!modalVisible)}
                    swipeDirection="right"
                    >
                        {/* View component for modal */}
                         <View>
                
                <ImageBackground source={require("../image/greenRing.jpg")} style={{width:"100%", height:"100%"}} >
                    
                 {/* close and submit button view */}
                 <View style={{flexDirection:"row",paddingBottom:5, top:10,left:10}}>
                        <TouchableOpacity 
                        onPress={() => this.setModalVisible(!modalVisible)} >
                        <Icon type="ionicon" name="arrow-back-outline" size={25} color="white" />
                        </TouchableOpacity>
                        <View style={{flexDirection:"row"}}>
                            <Image source={require("../image/bracelets.jpg")} style={{width:35, height:35,borderRadius:20}} ></Image>
                            <Text style={[styles.h4,{color:"white",top:5,left:5,fontFamily:"Montserrat-Bold",}]}>Ruby </Text>
                            <Text style={[styles.h4,{color:"white",top:5,left:8,fontFamily:"Montserrat-Bold",}]}>5h ago</Text>
                            </View>
                      </View>
                    </ImageBackground>
                
            </View>
                        </Modal>

            </View>
            </ScrollView>
        </View>
        )
    }
}

class CategoryButtons extends Component{
    constructor(props){
        super(props);
        this.state={
            data:[],
            isLoading:true
        }
    }
    componentDidMount(){
        this.get_category()
    }

    // Fetching Jewellery categories
    get_category=()=>{

        fetch(global.api+'get-jwellery-category', 
        {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization':global.token 
              },
            })
            .then((response) => response.json())
            .then((json) => {
                
                if(json.status){
                    this.setState({data:json.data})
                    console.warn(this.state.data)
                
                }
                else{
                    Toast.show("No data found")
                }             
                return json;
            })
            .catch((error) => console.error(error))
            .finally(() => {
                this.setState({ isLoading: false });
            });
    }
    render(){
        let details=this.state.data.map((item,id)=>{
            return(
                <View style={{flexDirection:'row',justifyContent:"space-evenly"}}>
                        <TouchableOpacity style={style.catButton}>
                            <Text style={style.catButtonText}>{item.name}</Text>
                        </TouchableOpacity>
                        </View>
            )
        })
        return(
            <View style={{borderBottomWidth:1,borderColor:"#dedede",paddingVertical:0}}>
                <View style={{flexDirection:'row',padding:10}}>
                    <TouchableOpacity style={style.button}>
                        <Text style={style.buttonText}>For You</Text>
                    </TouchableOpacity>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>

                        {details}

                    </ScrollView>
                </View>
            </View>
        )
    }
}

class Loaders extends Component{
    render(){
      return(
        <View>
          <SkeletonPlaceholder>
            <View>
              <View style={{flexDirection: 'row', alignItems: 'center',marginLeft:5,marginTop:5}}>
                <View style={{width: 60, height: 60, borderRadius: 50}} />
                <View style={{marginLeft: 10}}>
                  <View style={{width: 150, height: 20, borderRadius: 4}} />
                  <View style={{marginTop: 5, width: 90, height: 15, borderRadius: 4}}/>
                </View>
              </View>
              <View style={{marginTop: 5, marginBottom: 30,marginLeft:10}}>
                <View style={{ width: 370, height: 200, borderRadius: 4}}/>
  
                <View style={{flexDirection:"row",marginTop:5}}>
                    <View style={{height:40,width:40,borderRadius:100}}/>
                    <View style={{height:40,width:40,borderRadius:100,marginLeft:5}}/>
                    <View style={{height:40,width:40,borderRadius:100,marginLeft:5}}/>
                </View>
  
                <View style={{marginTop: 5, width: 350, height: 15, borderRadius: 4}}/>
                <View style={{marginTop:5, width: 250, height: 15, borderRadius: 4}}/>
              </View>
            </View>
  
            <View>
              <View style={{flexDirection: 'row', alignItems: 'center',marginLeft:5}}>
                <View style={{width: 60, height: 60, borderRadius: 50}} />
                <View style={{marginLeft: 10}}>
                  <View style={{width: 150, height: 20, borderRadius: 4}} />
                  <View style={{marginTop: 5, width: 90, height: 15, borderRadius: 4}}/>
                </View>
              </View>
              <View style={{marginTop: 5, marginBottom: 30,marginLeft:10}}>
                <View style={{ width: 370, height: 200, borderRadius: 4}}/>
  
                <View style={{flexDirection:"row",marginTop:5}}>
                    <View style={{height:40,width:40,borderRadius:100}}/>
                    <View style={{height:40,width:40,borderRadius:100,marginLeft:5}}/>
                    <View style={{height:40,width:40,borderRadius:100,marginLeft:5}}/>
                </View>
  
                <View style={{marginTop: 5, width: 350, height: 15, borderRadius: 4}}/>
                <View style={{marginTop:5, width: 250, height: 15, borderRadius: 4}}/>
              </View>
            </View>
          </SkeletonPlaceholder>
        </View>
      )
    }
  }

class StoryModal extends Component{
    state = {
        modalVisible: true
      };
        
      setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
      }
    render(){
        
        const { modalVisible } = this.state;
        return(
            <View>
                
                <ImageBackground source={require("../image/greenRing.jpg")} style={{width:"100%", height:"100%"}} >
                    
                 {/* close and submit button view */}
                 <View style={{flexDirection:"row",paddingBottom:5, top:10,left:10}}>
                        <TouchableOpacity>
                        <Icon type="ionicon" name="arrow-back-outline" 
                        onPress={() => this.setModalVisible(!modalVisible)} size={25} color="white" />
                        </TouchableOpacity>
                        <View style={{flexDirection:"row"}}>
                            <Image source={require("../image/bracelets.jpg")} style={{width:35, height:35,borderRadius:20}} ></Image>
                            <Text style={[styles.h4,{color:"white",top:10,left:5,fontWeight:"bold"}]}>5h ago</Text>
                            </View>
                      </View>
                    </ImageBackground>
                
            </View>
        )
    }
}

const style=StyleSheet.create({
    story:{
        backgroundColor:"#fedda3",
        padding:7,
        borderRadius:30,
        width:60,
        height:60,
        elevation:1,
        justifyContent:"center",
        marginLeft:10

    },
    img:{
        width:"100%",
        height:"100%",
    },
    storyimg:{
        width:"100%",
        height:"100%",
        borderRadius:30

    },
    iconPencil:{
        marginLeft:20,
        fontSize:20,
        marginBottom:10,
        // color:"#bc3b3b"
    },
    Text:{
        position:"absolute",
        fontSize:RFValue(13,580),
        marginLeft:80,
        // fontFamily:"Raleway-Medium",
        fontFamily: "Montserrat-Regular",
    },

    button:{
        backgroundColor:"#BC3B3B",
        padding:4,
        borderRadius:25,
        width:80,
        height:30,
        justifyContent:"center"

    },
    buttonText:{
        alignSelf:"center",
        color:"#fff",
        // fontFamily:"Roboto-Regular",
        fontFamily: "Montserrat-Regular",
        fontSize:RFValue(9,580)
    },
    catButton:{
        // backgroundColor:"#BC3B3B",
        // padding:7,
        height:30,
        marginLeft:10,
        borderRadius:25,
        justifyContent:"center",
        borderColor:"#EBEBEB",
        borderWidth:1,
        width:100
    },
    catButtonText:{
        alignSelf:"center",
        color:"#222222",
        // fontFamily:"Roboto-Regular",
        fontFamily: "Montserrat-Regular",
        fontSize:RFValue(9,580)

    },
    add:{
        
            // borderWidth: 1,
            // borderColor: 'rgba(0,0,0,0.2)',
            alignItems: 'center',
            justifyContent: 'center',
            width: 50,
            position: 'absolute',
            bottom: 15,
            right: 10,
            height: 50,
            backgroundColor: '#bc3b3b',
            borderRadius: 100,
         
    }
})