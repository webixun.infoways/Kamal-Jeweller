import React, { Component } from 'react'
// import { TouchableOpacity } from 'react-native'
import {View, Image,Text, StyleSheet,Dimensions, ScrollView, TouchableOpacity, TextInput}  from "react-native"
import {Icon,Header} from "react-native-elements"
import RBSheet from 'react-native-raw-bottom-sheet'
import { RFValue } from 'react-native-responsive-fontsize'
import StepIndicator from 'react-native-step-indicator';
import LinearGradient from 'react-native-linear-gradient';

const styles=require("../Components/Style")

const win = Dimensions.get('window');

class OrderStatus extends Component {

    constructor(props){
        super(props);
    }

    renderLeftComponent(){
        return(
            <View style={{width:win.width/2,flexDirection:"row"}}>
                <Text style={{alignSelf:"center"}}>
                <Icon
                name='chevron-back-outline'
                size={28}
                color='#7c7d7e' 
                style={{margin:10}}
                type="ionicon"
                onPress={()=>this.props.navigation.goBack()}
                // style={{marginTop:5}} 
              /> 
              </Text>
                <Text style={[styles.headerHeadingText,
                    {alignSelf:"center",}]}>Order Status</Text>
            </View>
        )
    }

    renderRightComponent(){
        return(

            <View style={{width:"25%", top:10,flexDirection:"row",justifyContent:"space-between"}}>
                
               <Icon
                name='cart'
                size={22}
                color='#7c7d7e' 
                style={{margin:10}}
                type="ionicon"
                onPress={()=>this.props.navigation.navigate("MyCart")}
                // style={{marginTop:5}} 
              /> 
              
            </View>
          )
    }

    render() {
        return (
            <View style={{flex:1,}}>
               {/* View for header component */}
               <View>
                    <Header
                    statusBarProps={{ barStyle: 'light-content' }}
                    leftComponent={this.renderLeftComponent()}
                    rightComponent={this.renderRightComponent()}
                    ViewComponent={LinearGradient} // Don't forget this!
                    linearGradientProps={{
                    colors: ['white', 'white'],
                    start: { x: 0, y: 0.5 },
                    end: { x: 1, y: 0.5 }
                    }}
                />
                </View>

                {/* Card Component */}
                <TouchableOpacity onPress={()=>this.props.navigation.navigate("OrderDetails")}>
                <Card />
                </TouchableOpacity>
                <Card />

            </View>
        )
    }
}

export default OrderStatus


 class Card extends Component {
    render() {
        return (
            <View style={style.card}>


                <View style={{flexDirection:"row"}}>
                <View style={style.iconView}>
                <Icon name="cube-outline" size={30} type="ionicon" color="#fff" style={{alignSelf:"center"}} />
                </View>
                <View style={{margin:10}}>
                <Text style={{color:"#000",fontFamily: "Montserrat-SemiBold",fontSize:RFValue(12,580)}}>On the way</Text>
                <Text style={{fontFamily: "Montserrat-SemiBold",fontSize:RFValue(11,580)}}>Estimated delivery,10 June'21</Text>
                </View>
                </View>


                <View style={[style.card,{backgroundColor:"#f5f5f5",height:140, borderRadius:10,}]}>
                    
                    <View style={{flexDirection:"row"}}>
                <Image source={require("../image/greenRing.jpg")} style={{width:55,height:60,borderRadius:10, marginTop:10}} />
                <Text style={style.name}>Emerald Engagement Ring</Text>
                </View>
                <View style={{flexDirection:"row",top:-40,left:65}}>
                <Icon style={styles.p} name="star" size={15} color="grey"  />
                <Text style={[styles.p,{marginLeft:5}]}>4.5</Text>
                <Text style={[styles.p,{marginLeft:15}]}>Size:14</Text>
                <Text style={{position:"absolute",right:50}}>
                <Icon name="chevron-forward-outline" type="ionicon" size={22} color="grey" />
                </Text>
                </View>


                <View style={{top:-35,flexDirection:"row"}}>
                <Text style={{fontSize:35,}}>•</Text>
                <Text style={[styles.p,{color:"grey",top:11,fontSize:RFValue(10,580),left:5}]}>
                    Exchange/Return window will be closed on 10July
                </Text>
                </View>

                <View style={{flexDirection:"row",top:-35}}>
                    <Text style={{fontFamily: "Montserrat-SemiBold",fontSize:RFValue(10,580)}}>Rate Product</Text>
                    <Text style={{left:10,top:2}}>
                    <Icon name="star" size={15} color="grey" />
                    <Icon name="star" size={15} color="grey" />
                    <Icon name="star" size={15} color="grey" />
                    <Icon name="star" size={15} color="grey" />
                    <Icon name="star-outline"  size={15} color="grey" />
                    </Text>
                    <View style={{borderWidth:1,borderColor:"#000",height:30,top:-5, width:100,alignItems:"center",marginLeft:40,  alignContent:"center", borderRadius:5,}}>
                        <Text style={{color:"#000",fontSize:RFValue(11,580),fontFamily: "Montserrat-Regular", alignSelf:"center",top:5}}>Cancel </Text>
                    </View>
                </View>
                </View>
            </View>

        )
    }
}


const style=StyleSheet.create({
    heading:{
        fontSize:20,
        marginLeft:10,
        color:"black"
    },
    iconView:{
        backgroundColor:"#000",
        width:50,
        height:50,
        marginTop:10,
         padding:8,
          borderRadius:25
    },
    card:{
        height:230,
        marginTop:10,
        paddingHorizontal:8,
        alignSelf:"center",
        elevation:2,
        shadowOffset:{  width: 1,  height: 2  },
        shadowColor: 'black',
        shadowOpacity: 1.0,
        width:"100%",
        backgroundColor:"#fff",
        // borderRadius:10,
        paddingHorizontal:25,
    },
    name:{
        fontSize:17,
        marginTop:10,
        margin:10
    },
    buttonstyle: {
        backgroundColor: "#BC3B3B",
        flexDirection:"row",
        width: Dimensions.get("window").width / 1.4,
        alignSelf: "center",
        justifyContent: "center",
        alignItems: "center",
        height: 45,
        borderRadius: 25,
        top: 30,
    },
    buttonText: {
        color: "#fff",
        fontSize: RFValue(12, 580),
    },
    textInput:{
        width:"85%",
        backgroundColor:"#f5f5f5",
        alignSelf:"center",
        borderRadius:20,
        marginTop:15,
        paddingLeft:20,
    }
}
)