import React, {Component} from 'react';
// import { TouchableOpacity } from 'react-native'
import {
  View,
  Image,
  Text,
  StyleSheet,
  Dimensions,
  FlatList,
  ActivityIndicator,
  ScrollView,
  TouchableOpacity,
  TextInput,
  Pressable,
  Platform,
} from 'react-native';
import {Header, Icon} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import RBSheet from 'react-native-raw-bottom-sheet';
import {RFValue} from 'react-native-responsive-fontsize';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

//Global Style Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

class MyOrders extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      isLoading: true,
    };
  }

  renderLeftComponent() {
    return (
      <View style={{flexDirection: 'row', width: win.width / 2}}>
        <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
        </View>
        <Text style={[styles.headerHeadingText]}>My Orders</Text>
      </View>
    );
  }

  renderRightComponent() {
    return (
      <View>
        <Icon
          name="cart-outline"
          size={24}
          color="#222222"
          type="ionicon"
          onPress={() => this.props.navigation.navigate('MyCart')}
          // style={{alignSelf:"center"}}
        />
      </View>
    );
  }

  componentDidMount = () => {
    this.my_orders();
  };

  // fetching my orders
  my_orders = () => {
    fetch(global.api + 'my-order', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
    })
      .then(response => response.json())
      .then(json => {
        if (json.status) {
          this.setState({data: json.data});
          console.warn("data",this.state.data);
          
        }
        return json;
      })
      .catch(error => console.error(error))
      .finally(() => {
        this.setState({isLoading: false});
      });
  };

  // Flatlist render item
  orderCard = ({item}) => (
    <View style={style.card}>
      <View style={{flexDirection: 'row'}}>
        <View style={style.iconView}>
          <Icon
            name="cube-outline"
            size={20}
            type="ionicon"
            color="#fff"
            style={{marginTop: -2}}
          />
        </View>
        <View style={{margin: 7}}>
          <Text
            style={{
              color: '#222',
              fontSize: RFValue(10, 580),
              fontFamily: 'Raleway-Medium',
            }}>
            {item.order_status}
          </Text>
          <Text
            style={{fontFamily: Platform.OS == "android" ? 'Roboto-Regular' : null, fontSize: RFValue(9, 580)}}>
            Estimated delivery {item.timestamps} </Text>
        </View>
      </View>
     <View style={{marginTop:10}}>
     {item.order_item.map(value => {
        return (
          <View
            style={[style.card, {backgroundColor: '#fafafa', borderRadius: 3, marginTop:5}]}>
            <Pressable
              onPress={() =>
                this.props.navigation.navigate('OrderDetails', {order_id:item.id})
              }>
              <View style={{flexDirection: 'row', marginBottom: -15}}>
                {/* {value.picture[0]} */}
                {/* <Text>
                {value.picture[0].src}
                </Text> */}
                <Image
                source={{uri: global.img_url +value.product[0].picture[0].src}}
                  // source={require('../image/greenRing.jpg')}
                  style={{width: 50, height: 50, marginTop: 7}}
                />
                <View>
                  <Text style={style.name}>{value.product[0].name}</Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', top: -10, left: 55}}>
                <Text style={[styles.h4, {marginLeft: 5}]}>Qty: {value.product_quantity}</Text>

                {value.option[0].option[0].option_value == null ? 
                <></>: 
                <Text style={[styles.h4, {marginLeft: 5}]}>Size: {value.option[0].option[0].option_value}</Text>
                }
                
              </View>
            </Pressable>
          </View>
        );
      })}
     </View>

      <View
        style={[
          style.card,
          {
            backgroundColor: '#fafafa',
            borderRadius: 3,
            marginBottom: 15,
            marginTop: 5,
          },
        ]}>
        <View style={{flexDirection: 'row', marginBottom: -7}}>
          <Text style={{fontSize: 35, top: -5}}>•</Text>
          <Text
            style={[
              styles.p,
              {color: '#222222', top: 5, fontSize: RFValue(9, 580), left: 5},
            ]}>
            Exchange/Return window will be closed on 31 Jan
          </Text>
        </View>
      </View>

      {/* <View style={[style.card,{backgroundColor:"#fafafa", borderRadius:3,marginBottom:15}]}>
                    <View style={{flexDirection:"row",marginTop:5,marginBottom:5}}>
                        <Text style={{fontFamily: "Raleway-Medium",fontSize:RFValue(10,580)}}>Rate Product</Text>
                        <Text style={{left:10,top:2}}>
                        <Icon name="star-outline" size={17} color="grey" />
                        <Icon name="star-outline" size={17} color="grey" />
                        <Icon name="star-outline" size={17} color="grey" />
                        <Icon name="star-outline" size={17} color="grey" />
                        <Icon name="star-outline"  size={17} color="grey" />
                        </Text>
                    </View>
                    </View>   */}
    </View>
  );

  render() {
    return (
      <View style={{flex: 1,backgroundColor:"#fff"}}>
        {/* View for header component */}
        <View>
          <Header
            // containerStyle={{height: 82}}
            statusBarProps={{barStyle: 'light-content'}}
            leftComponent={this.renderLeftComponent()}
            // rightComponent={this.renderRightComponent()}
            ViewComponent={LinearGradient} // Don't forget this!
            linearGradientProps={{
              colors: ['white', 'white'],
              start: {x: 0, y: 0.5},
              end: {x: 1, y: 0.5},
            }}
          />
        </View>
        {this.state.isLoading ? (
          <View>
            <Loaders />
          </View>
        ) : (
          <View>
            {this.state.data != '' ? (
              <FlatList
                navigation={this.props.navigation}
                showsVerticalScrollIndicator={false}
                data={this.state.data}
                renderItem={this.orderCard}
                keyExtractor={item => item.id}
              />
            ) : (
              <View style={{alignItems: 'center', marginTop: 100,}}>
                <Image
                  source={require('../image/noorder.png')}
                  style={{width: 400, height: 250}}
                />
                <Text style={([styles.h3], {marginTop: 10})}>
                  No Orders Found
                </Text>
                <TouchableOpacity onPress={() => this.props.navigation.navigate("Collection")}>
            <View style={[styles.buttonStyles,{marginTop:30}]}>
              <Text style={styles.buttonText}>Shop Now</Text>
            </View>
          </TouchableOpacity>
              </View>
            )}
          </View>
        )}
      </View>
    );
  }
}

export default MyOrders;

class Loaders extends Component{
  render(){
    return(
      <View>
        <SkeletonPlaceholder>

        <View>
            <View style={{flexDirection:"row"}}>
              <View style={{height:40,width:40,borderRadius:100,marginLeft:10,marginTop:5}}/>
              <View style={{marginTop:5,marginLeft:10}}>
                <View style={{height:15,width:80}}/>
                <View style={{height:15,width:150,marginTop:5}}/>
              </View>
            </View>

            <View style={{marginTop:15,height:60,width:Dimensions.get('window').width/1.05,alignSelf:"center",borderRadius:3}}/>


            <View style={{marginTop:15,height:35,width:Dimensions.get('window').width/1.05,alignSelf:"center",borderRadius:3}}/>
          </View>

          <View>
            <View style={{flexDirection:"row"}}>
              <View style={{height:40,width:40,borderRadius:100,marginLeft:10,marginTop:5}}/>
              <View style={{marginTop:5,marginLeft:10}}>
                <View style={{height:15,width:80}}/>
                <View style={{height:15,width:150,marginTop:5}}/>
              </View>
            </View>

            <View style={{marginTop:15,height:60,width:Dimensions.get('window').width/1.05,alignSelf:"center",borderRadius:3}}/>

            <View style={{marginTop:15,height:35,width:Dimensions.get('window').width/1.05,alignSelf:"center",borderRadius:3}}/>
          </View>

          <View>
            <View style={{flexDirection:"row"}}>
              <View style={{height:40,width:40,borderRadius:100,marginLeft:10,marginTop:5}}/>
              <View style={{marginTop:5,marginLeft:10}}>
                <View style={{height:15,width:80}}/>
                <View style={{height:15,width:150,marginTop:5}}/>
              </View>
            </View>

            <View style={{marginTop:15,height:60,width:Dimensions.get('window').width/1.05,alignSelf:"center",borderRadius:3}}/>

            <View style={{marginTop:15,height:60,width:Dimensions.get('window').width/1.05,alignSelf:"center",borderRadius:3}}/>

            <View style={{marginTop:15,height:35,width:Dimensions.get('window').width/1.05,alignSelf:"center",borderRadius:3}}/>
          </View>
        </SkeletonPlaceholder>
      </View>
    )
  }
}

class Card extends Component {
  render() {
    return (
      <View>
        <View style={style.card}>
          <View style={{flexDirection: 'row'}}>
            <View style={style.iconView}>
              <Icon
                name="cube-outline"
                size={20}
                type="ionicon"
                color="#fff"
                style={{marginTop: -2}}
              />
            </View>
            <View style={{margin: 7}}>
              <Text
                style={{
                  color: '#222',
                  fontSize: RFValue(10, 580),
                  fontFamily: 'Raleway-Medium',
                }}>
                On the way
              </Text>
              <Text
                style={{
                  fontFamily: 'Raleway-Regular',
                  fontSize: RFValue(9, 580),
                }}>
                Estimated delivery 15Nov'21
              </Text>
            </View>
          </View>

          <View
            style={[style.card, {backgroundColor: '#fafafa', borderRadius: 3}]}>
            <Pressable
              onPress={() => this.props.navigation.navigate('OrderDetails')}>
              <View style={{flexDirection: 'row', marginBottom: -15}}>
                <Image
                  source={require('../image/greenRing.jpg')}
                  style={{width: 45, height: 50, marginTop: 7}}
                />
                <View>
                  <Text style={style.name}>Emerald Engagement Ring</Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', top: -15, left: 55}}>
                <Icon style={styles.p} name="star" size={15} color="grey" />
                <Text style={[styles.p, {marginLeft: 5}]}>4.5</Text>
                <Text style={[styles.p, {marginLeft: 15}]}>Size:14</Text>
                <Text style={{position: 'absolute', right: 50}}>
                  <Icon
                    name="chevron-forward-outline"
                    type="ionicon"
                    size={22}
                    color="#222222"
                  />
                </Text>
              </View>
            </Pressable>
          </View>

          <View
            style={[style.card, {backgroundColor: '#fafafa', borderRadius: 3}]}>
            <View style={{flexDirection: 'row', marginBottom: -7}}>
              <Text style={{fontSize: 35, top: -5}}>•</Text>
              <Text
                style={[
                  styles.p,
                  {
                    color: '#222222',
                    top: 5,
                    fontSize: RFValue(9, 580),
                    left: 5,
                  },
                ]}>
                Exchange/Return window will be closed on 10July
              </Text>
            </View>
          </View>

          <View
            style={[
              style.card,
              {backgroundColor: '#fafafa', borderRadius: 3, marginBottom: 15},
            ]}>
            <View style={{flexDirection: 'row', marginTop: 5, marginBottom: 5}}>
              <Text
                style={{
                  fontFamily: 'Raleway-Medium',
                  fontSize: RFValue(10, 580),
                }}>
                Rate Product
              </Text>
              <Text style={{left: 10, top: 2}}>
                <Icon name="star-outline" size={17} color="grey" />
                <Icon name="star-outline" size={17} color="grey" />
                <Icon name="star-outline" size={17} color="grey" />
                <Icon name="star-outline" size={17} color="grey" />
                <Icon name="star-outline" size={17} color="grey" />
              </Text>
            </View>
          </View>
        </View>

        <View style={style.card}>
          <View style={{flexDirection: 'row'}}>
            <View style={style.iconView}>
              <Icon
                name="cube-outline"
                size={20}
                type="ionicon"
                color="#fff"
                style={{marginTop: -2}}
              />
            </View>
            <View style={{margin: 7}}>
              <Text
                style={{
                  color: '#222',
                  fontSize: RFValue(10, 580),
                  fontFamily: 'Raleway-Medium',
                }}>
                Delivered
              </Text>
              <Text
                style={{
                  fontFamily: 'Raleway-Regular',
                  fontSize: RFValue(9, 580),
                }}>
                On Thu,10 June'21
              </Text>
            </View>
          </View>

          <View
            style={[style.card, {backgroundColor: '#fafafa', borderRadius: 3}]}>
            <Pressable
              onPress={() => this.props.navigation.navigate('OrderDetails')}>
              <View style={{flexDirection: 'row', marginBottom: -15}}>
                <Image
                  source={require('../image/greenRing.jpg')}
                  style={{width: 45, height: 50, marginTop: 7}}
                />
                <View>
                  <Text style={style.name}>Emerald Engagement Ring</Text>
                </View>
              </View>
              <View style={{flexDirection: 'row', top: -15, left: 55}}>
                <Icon style={styles.p} name="star" size={15} color="grey" />
                <Text style={[styles.p, {marginLeft: 5}]}>4.5</Text>
                <Text style={[styles.p, {marginLeft: 15}]}>Size:14</Text>
                {/* <Text style={{position:"absolute",right:50}}>
                    <Icon name="chevron-forward-outline" type="ionicon" size={22} color="#222222" />
                    </Text> */}
              </View>
            </Pressable>
          </View>

          <View
            style={[
              style.card,
              {backgroundColor: '#fafafa', borderRadius: 3, marginTop: 15},
            ]}>
            <View style={{flexDirection: 'row', marginBottom: -7}}>
              <Text style={{fontSize: 35, top: -5}}>•</Text>
              <Text
                style={[
                  styles.p,
                  {
                    color: '#222222',
                    top: 5,
                    fontSize: RFValue(9, 580),
                    left: 5,
                  },
                ]}>
                Exchange/Return window will be closed on 10July
              </Text>
            </View>
          </View>

          <View
            style={[
              style.card,
              {backgroundColor: '#fafafa', borderRadius: 3, marginBottom: 15},
            ]}>
            <View style={{flexDirection: 'row', marginTop: 5, marginBottom: 5}}>
              <Text
                style={{
                  fontFamily: 'Raleway-Medium',
                  fontSize: RFValue(10, 580),
                }}>
                Rate Product
              </Text>
              <Text style={{left: 10, top: 2}}>
                <Icon name="star-outline" size={17} color="grey" />
                <Icon name="star-outline" size={17} color="grey" />
                <Icon name="star-outline" size={17} color="grey" />
                <Icon name="star-outline" size={17} color="grey" />
                <Icon name="star-outline" size={17} color="grey" />
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
  heading: {
    fontSize: 12,
    fontFamily: 'Montserrat-Medium',
    marginLeft: 10,
    color: 'black',
  },
  iconView: {
    backgroundColor: '#bc3b3b',
    width: 35,
    height: 35,
    marginTop: 10,
    alignItems: 'center',
    padding: 8,
    borderRadius: 25,
  },
  card: {
    // height:230,
    // marginTop:5,
    paddingHorizontal: 10,
    alignSelf: 'center',
    elevation: 0.6,
    shadowOffset: {width: 1, height: 2},
    shadowColor: Platform.OS == "android" ? 'black' : "#f5f5f5",
    shadowOpacity: 1.0,
    width: '100%',
    backgroundColor: '#fff',
    marginBottom:10
    // borderRadius:10,
    // paddingHorizontal:25,
  },
  name: {
    fontSize: RFValue(10, 580),
    color: '#222222',
    fontFamily: 'Montserrat-Medium',
    marginTop: 10,
    margin: 10,
  },
  buttonstyle: {
    backgroundColor: '#BC3B3B',
    flexDirection: 'row',
    width: Dimensions.get('window').width / 1.4,
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    height: 45,
    borderRadius: 25,
    top: 30,
  },
  buttonText: {
    color: '#fff',
    fontSize: RFValue(12, 580),
  },
  textInput: {
    width: '85%',
    backgroundColor: '#f5f5f5',
    alignSelf: 'center',
    borderRadius: 20,
    marginTop: 15,
    paddingLeft: 20,
  },
});
