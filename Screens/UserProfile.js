import React, { Component } from 'react';
import {
    Text,View,ScrollView,Modal,FlatList,
    StyleSheet,Image,Dimensions,
    TouchableOpacity,Pressable
} from 'react-native';
import { Header,Icon } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import RBSheet from "react-native-raw-bottom-sheet";
import { RFValue } from 'react-native-responsive-fontsize';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import DropButton from '../Components/DropButton.js';
import Toast from 'react-native-simple-toast'
import {Video} from "react-native-video"

//Global StyleSheet Import
const styles = require('../Components/Style.js');

const win = Dimensions.get('window');

const options = {
    title: "Pick an Image",
    storageOptions:{
        skipBackup: true,
        path:'images'
    }
}

class UserProfile extends Component {

    constructor(props){
        super(props);
        this.state={
            activeIndex:0,
            modalVisible: false,
            data:[],
            image:"",
            id:this.props.route.params.id,
            follow:{}
        }
    }
    componentDidMount()
    {
      this.setState({name:global.name});
     
    }

    setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
      }
    segmentClick =(index)=>{
        this.setState({
            activeIndex:index
        })
    }
    postCard=({item})=>(
        <Pressable style={style.card}
                    onPress={()=>this.props.navigation.navigate('SingleUserpost',{data:this.state.data,name:this.state.name,pic:this.state.image,id:this.state.id})}>
                        
                        {/* <Image source={require('../image/v2.jpg')}
                        style={style.img}/> */}
                        <Image source={{uri:"https://demo.webixun.com/KamalJwellersApi/public/"+item.content_src}}
                        style={style.img}/>
                    </Pressable>
    );

    renderSection = () =>{
    
            if(this.state.activeIndex == 0){
                return(
                    <View>
                   
                    <FlatList
                columnWrapperStyle={{ flex: 1,  }}
                numColumns={3}
                navigation={this.props.navigation}
                showsVerticalScrollIndicator={false}
                data={this.state.data}
                renderItem={this.postCard}
                keyExtractor={item=>item.id} />
    
                    
                </View>
                )
            }
            else{
                return(
                    <View>
                    <View style={{flexDirection:"row",justifyContent:"space-between",width:"100%"}}>
                    {/* product card */}
                    <Pressable style={style.card}
                    onPress={() => this.props.navigation.navigate("Video")}>
                        <Image source={require('../image/v1.jpg')}
                        style={style.img1}/>
                    </Pressable>
    
                    {/* product card 2 */}
                    <Pressable style={style.card}
                    onPress={()=>this.props.navigation.navigate('Videos')}>
                        <Image source={require('../image/v2.jpg')}
                        style={style.img1}/>
                    </Pressable>
                    
                    {/* product card 2 */}
                    <Pressable style={style.card}
                    onPress={()=>this.props.navigation.navigate('Videos')}>
                        <Image source={require('../image/v3.jpg')}
                        style={style.img1}/>
                    </Pressable>

                    <Modal visible={this.state.modalVisible}
                    onRequestClose={()=>this.setModalVisible(false)}
                // presentationStyle={"fullScreen"}                
                //swipeDirection={["up", "down", "left", "right"]}
                // transparent={true}
                >
                <View>
                    <Image source={require('../image/v2.jpg')} 
                    style={{height:200,width:win.width}}
//                     controls={true}
// ref={(ref) => {
// this.player = ref}}
 />
                </View>
            </Modal>
    
                    </View>

              
                </View>
                )
            }
        
    }
    renderLeftComponent(){
        return(
            <View style={{flexDirection:"row",width:win.width/2}}>
                <View style={{padding:5,top:2}} >
                    <Icon name="chevron-back-outline" color="#222222" size={20} type="ionicon"
                        onPress={() => this.props.navigation.goBack()} />
                </View>
                <Text style={styles.headerHeadingText}>Profile</Text>
            </View>
        )
    }

    renderRightComponent(){
        return(
            <View >
                <Icon
                name='bookmark-outline'
                size={24}
                color='#222222' 
                type="ionicon"
                onPress={()=>this.props.navigation.navigate("SavedPost")}
                // style={{alignSelf:"center"}}
                /> 
            </View>
        )
    }

    componentDidMount=()=>{
        this.get_profile();
        this.focusListener=this.props.navigation.addListener('focus', ()=>{
            this.get_profile();  
        
        }
        )
    }

    get_profile=()=>{
        fetch(global.api+'get-single-user-profile', 
        {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization':global.token 
              },
              body: JSON.stringify({   
                user_id:this.state.id
              })
            })
            .then((response) => response.json())
            .then((json) => {
                console.warn(json.data)
                if(json.status){
                    this.setState({name:json.data[0].f_name})
                    this.setState({image:json.data[0].profile_pic}) 
                    json.data.map(value=>{
                        this.setState({data:value.feed,post:value.feed.length,
                            followers:value.follower_count,
                        following:value.following_count})
                        const follow=this.state.follow
                    if(value.follow.length==0){
                        follow[value.id] = false;
                        } else {
                          follow[value.id] = true;
                        }
                        this.setState({follow})
                    })  
                    
                }
                            
                return json;
            })
            .catch((error) => console.error(error))
            .finally(() => {
                this.setState({ isLoading: false });
            });
    }

    // Follow user
    follow = id => {
        const follow = this.state.follow;
        if (this.state.follow[id] == true) {
          follow[id] = false;
          var type = 'unfollow';
        } else {
          follow[id] = true;
          var type = 'follow';
        }
        this.setState({follow});
    
        fetch(global.api + 'follow-user', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: global.token,
          },
          body: JSON.stringify({
            following_id: id,
            type: type,
          }),
        })
          .then(response => response.json())
          .then(json => {
            // console.log(json)
    
            if (!json.status) {
              Toast.show(json.msg);
            } else {
              Toast.show(json.msg)
              this.get_profile()
            }
          });
      };
    render() {
        return (
            <View style={styles.container}>
                 {/* View for header component */}
                 <View >
                        <Header
                        // containerStyle={{height:82}}
                        statusBarProps={{ barStyle: 'light-content' }}
                        leftComponent={this.renderLeftComponent()}
                        ViewComponent={LinearGradient} // Don't forget this!
                        linearGradientProps={{
                        colors: ['white', 'white'],
                        start: { x: 0, y: 0.5 },
                        end: { x: 1, y: 0.5 }
                        
                        }}
                    />
                 </View>

                 <ScrollView>
                    <View>
                        <ProfileDetails navigation={this.props.navigation} 
                        image={this.state.image}
                        get_profile={this.get_profile}
                        post={this.state.post}
                        followers={this.state.followers}
                        following={this.state.following}                        />
                    {/* profile name view */}
                    <View style={{paddingTop:15,flexDirection:"row"}}>
                        
                        <Text style={[styles.h3,{fontSize:20,top:-2,marginLeft:25}]}>
                            {this.state.name} 
                        </Text>
                    </View>

                    <View style={{paddingTop:20,flexDirection:"row",justifyContent:"space-evenly"}}>
                        {this.state.follow[this.state.id]?
                        <TouchableOpacity style={[styles.editProfileButton,]}
                        onPress={()=>this.follow(this.state.id)}>
                            <Text style={[styles.editProfileButtonText]}>
                                Unfollow
                            </Text>
                        </TouchableOpacity>
                        :
                        <TouchableOpacity style={[styles.editProfileButton,{backgroundColor:"#bc3b3b"}]}
                        onPress={()=>this.follow(this.state.id)}>
                            <Text style={[styles.editProfileButtonText,{color:"#fff"}]}>
                                Follow
                            </Text>
                        </TouchableOpacity>
                        
                            }

                        {/* <TouchableOpacity style={style.button}
                        onPress={()=>this.RBSheet.open()}>
                            <Icon name="chevron-down-outline" size={17} type="ionicon"/>
                        </TouchableOpacity> */}
                    </View>
                      

                        <View style={{flexDirection:"row",justifyContent:"space-around",
                    marginTop:15}}>
                            <TouchableOpacity 
                            style={{
                                borderColor:"#f5f5f5",
                                borderWidth:1,
                                width:Dimensions.get('window').width,
                                padding:10,
                                alignContent:"center"
                            }}
                            onPress={()=>this.segmentClick(0)}
                            activeOpacity={this.state.activeIndex == 0}>
                                <Icon name="apps" type="ionicon"
                                color={this.state.activeIndex == 0 ? "#BC3B3B" : '#4a4b4d'}/>
                            </TouchableOpacity>

                            {/* <TouchableOpacity 
                             style={{
                                borderColor:"#f5f5f5",
                                borderWidth:1,
                                width:Dimensions.get('window').width/2,
                                padding:10,
                                alignContent:"center"
                            }}
                            onPress={()=>this.segmentClick(1)}
                            activeOpacity={this.state.activeIndex == 1}>
                                <Icon name="videocam" type="ionicon"
                                color={this.state.activeIndex == 1 ? "#BC3B3B" : '#4a4b4d'}/>
                            </TouchableOpacity> */}
                        </View>
                        
                        {this.renderSection()}
                    </View>
                 </ScrollView>
            </View>
        )
    }
}


export default UserProfile;


class ProfileDetails extends Component{

    render(){
        return(
            <View style={{flexDirection:"row",width:"100%"}}>
                {/* profile image and camera icon view */}
                <View style={{width:"25%",marginTop:0}}>
                    {this.props.image=="" ?
                    <Image source={require('../image/dummyuser.jpg')} 
                        style={style.profileImg}/>
                        :
                        <Image source={{uri:global.image_url+this.props.image}} style={style.profileImg}  />
                    }
                
                </View>
        
                {/* Bottom Sheet for gallery open */}
                <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        closeOnDragDown={true}
                        closeOnPressMask={true}
                        height={130}
                        customStyles={{
                            container: {
                                borderTopRightRadius: 20,
                                borderTopLeftRadius: 20,
                              },
                        draggableIcon: {
                            backgroundColor: ""
                        }
                        }}
                        >
                        {/* bottom sheet elements */}
                        <View>
                            
                            {/* open camera and library */}
                                <View style={{width:"100%",padding:20}}>
                                        <TouchableOpacity onPress={this.camera}>
                                            <Text style={style.icon}>
                                                <Icon name='camera' type="ionicon" color={'#bc3b3b'} size={25}/>
                                            </Text>
                                            <Text style={style.Text}>Take a picture</Text>
                                            </TouchableOpacity>

                                            <TouchableOpacity onPress={this.gallery} > 
                                            <Text style={style.icon}>
                                                <Icon name='folder' type="ionicon" color={'#bc3b3b'} size={25}/>
                                            </Text>
                                            <Text style={style.Text}>Select from library</Text>
                                        </TouchableOpacity>

                                </View>
                            
                        </View>
                    </RBSheet>

                <View style={{width:"75%",flexDirection:"column",paddingTop:20}}>
                    {/* edit profile button */}
                    <View style={{flexDirection:"row",marginLeft:10, justifyContent:"space-evenly",
            marginTop:10}}>
                <View style={{flexDirection:"column",
                //  borderRightWidth:2,borderColor:"#d3d3d3",
                  paddingRight:30}}>
                    <Text style={[styles.h3,{fontFamily: "Montserrat-Medium",fontSize:RFValue(12,580), alignSelf:"center"}]}>
                        {this.props.post}
                    </Text>
                    <Text style={[styles.smallHeading,{fontSize:RFValue(11,580),fontFamily:"Raleway-Medium"}]}>
                        Posts</Text>
                </View>

                <View style={{flexDirection:"column",
                // borderRightWidth:2,borderColor:"#d3d3d3",
                 paddingRight:30}}>
                    <Text style={[styles.h3,{fontFamily: "Montserrat-Medium",fontSize:RFValue(12,580),alignSelf:"center"}]}>
                        {this.props.followers}
                    </Text>
                    <Text style={[styles.smallHeading,{fontSize:RFValue(11,580),fontFamily:"Raleway-Medium"}]}>
                        Followers</Text>
                </View>

                <View style={{flexDirection:"column"}}>
                    <Text style={[styles.h3,{fontFamily: "Montserrat-Medium",fontSize:RFValue(12,580),alignSelf:"center"}]}>
                        {this.props.following}
                    </Text>
                    <Text style={[styles.smallHeading,{fontSize:RFValue(11,580),fontFamily:"Raleway-Medium"}]}>
                        Following</Text>
                </View>
            </View>
                </View>

             
            </View>
        )
    }
}

// Followngs and posts details
// class Details extends Component{
//     render(){
//         return(
           
//         )
//     }
// }

const style=StyleSheet.create({
    profileImg:{
        height:80,
        width:80,
        top:10,
        borderRadius:50,
        alignSelf:"center",
        // left:20
    },
    camIcon:{
        backgroundColor:"#dcdcdc",
        height:28,
        width:28,
        padding:6,
        alignContent:"center",
        borderRadius:30, 
        justifyContent:"center",
        left:30,
        top:-7,
        alignSelf:"center"
    },
    icon:{  
        marginLeft:20,
        // fontSize:20,
        fontSize:RFValue(14.5, 580),
        marginBottom:10
    },
    Text:{
        position:"absolute",
        // fontSize:20,
        fontSize:RFValue(14, 580),
        marginLeft:80,
        // fontFamily:"Raleway-Medium",
        fontFamily: "Raleway-SemiBold",
    },
    img:{
        width:"100%",
        height:130,
        // borderColor:"#000",
        // borderWidth:0.5
    },
    card:{
        width:"33.3%",
        borderWidth:0.2,
        // padding:5
    },
    img1:{
        width:"100%",
        height:200,
        // borderColor:"#000",
        // borderWidth:0.5
    }
})