import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  Pressable,
  Image,
  Text,
  Dimensions,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {Header, Icon, Rating} from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';
import Share from 'react-native-share';
import RBSheet from 'react-native-raw-bottom-sheet';
import Toast from 'react-native-simple-toast';
import RazorpayCheckout from 'react-native-razorpay';

const styles = require('../Components/Style');

class ThankYou extends Component {
  constructor(props) {
    super(props);
    console.warn('cart products are  ', this.props.item);
    this.state = {
      product_id: [],
      product_quantity: '',
      product_price: '',
    };
  }
  // componentDidMount() {
  //   this.props.item.map(value => {
  //     console.warn(value);
  //     this.state.product_id.push(value.product_id);
  //     this.setState({product_price: value.price});
  //     this.setState({product_quantity: value.product_quantity});
  //   });
  // }

  render() {
    return (
      <View style={styles.container}>
        
       
          <View style={{alignItems: 'center',marginTop:70}}>
            <Image
              source={require('../image/ty.png')}
              style={{
                width: 140,
                height: 180,
                marginTop: 10,
                alignSelf: 'center',
              }}
            />
            <Text style={[styles.h3]}>Thank You!</Text>
            <Text style={[styles.h4, {color: '#222222'}]}>for your order</Text>
            <Text
              style={[
                styles.h3,
                {
                  marginHorizontal: 25,
                  fontSize: RFValue(10, 580),
                  textAlign: 'center',
                },
              ]}>
              Your order is now being processed. We will let you know once the
              order is picked from the outlet.Check the status
            </Text>

            {/* <View style={{marginTop: 20}}>
              <Text
                style={{
                  alignSelf: 'center',
                  fontFamily: 'Raleway-Medium',
                  fontSize: RFValue(12, 580),
                }}>
                Rate Us
              </Text>
              <Text style={{top: 5, alignSelf: 'center'}}>
                <Rating
                  type="star"
                  ratingCount={5}
                  imageSize={20}
                  defaultRating={1}
                  startingValue={1}
                  ratingCount={5}
                  //   showRating
                  // showRating fractions="{1}"
                  startingValue="{0}"
                  onFinishRating={this.ratingCompleted}
                />
              </Text>
              <TextInput
                placeholder="Say something..."
                style={style.textInput}
              />
            </View> */}
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('MyOrders')}
              style={[styles.buttonStyles, {top: 40}]}>
              <Text style={[styles.buttonText, {color: '#fff'}]}>
                Track My Order
              </Text>
            </TouchableOpacity>
            <Text
              style={[styles.p, {top: 40, marginBottom: 10}]}
              onPress={() => this.props.navigation.navigate('Home')}>
              Back To Home
            </Text>
          </View>
      </View>
    );
  }
}

export default ThankYou;

const style = StyleSheet.create({
  textInput: {
    backgroundColor: '#F2F2F2',
    width: Dimensions.get('window').width / 1.2,
    borderRadius: 10,
    height: 45,
    alignSelf: 'center',
    top: 25,
    paddingLeft: 20,
    color: '#222222',
    fontSize: RFValue(11, 580),
  },
});
