import React, { Component } from 'react';
import {
    View, ScrollView, Modal,
    StyleSheet, Pressable, TextInput, ImageBackground,
    Image, TouchableOpacity, Text, SafeAreaView, Dimensions, ActivityIndicator, Platform
} from 'react-native';
import Swiper from 'react-native-swiper';
import { RFValue } from "react-native-responsive-fontsize";
import { Icon, Rating } from 'react-native-elements';
import { Picker } from '@react-native-picker/picker';
import ImageViewer from 'react-native-image-zoom-viewer';
import Toast from 'react-native-simple-toast'
import DateTimePicker from "react-native-modal-datetime-picker";
import moment from 'moment';
import SelectDropdown from 'react-native-select-dropdown';
import SwiperFlatList from 'react-native-swiper-flatlist';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import Share from 'react-native-share'

//Global Style Import
const style = require('../Components/Style.js');

// const images = [{
//     url: 'https://i.pinimg.com/originals/c4/94/de/c494de1a3c7ec4decc0ef6d1a313b204.jpg'
// }, {
//     url: 'https://www.gemsociety.org/wp-content/uploads/2019/02/vintage-custom-made.jpg'
// }, {
//     url: 'https://us.123rf.com/450wm/papastudio/papastudio1812/papastudio181200001/120629342-diamond-on-gold-ring-wedding-ring.jpg?ver=6'
// }]

class Product extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: false,
            fav: false,
            modalVisible: false,
            id: this.props.route.params.id,
            object: {},
            like: {},
            cart: null,
            data: [],
            obj: this.props.route.params.object,
            quantity: 1,
            price: "",
            item: "",
            images: [],
            size: "",
            size_id: '',
            s_id: "",
            weight: "",
            weight_id: "",
            option: [],
            product_quantity:'',
            isLoading:false,
            available_Qty:'',
            market_price:"",
            discount:''
        }
    }

    setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
    }

    componentDidMount() {
        this.get_productDetails();
        this.my_cart();
        this.focusListener = this.props.navigation.addListener('focus', () => {
            this.get_productDetails();
            this.my_cart();
        })
    }

    // cart item count
    my_cart = () => {
        fetch(global.api + 'my-cart', {
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                Authorization: global.token,
            },
        })
            .then(response => response.json())
            .then(json => {
                if (json.status) {
                    this.setState({ item: json.data.length });
                } else {
                    this.setState({ item: '' });
                }

                return json;
            })
            .catch(error => console.error(error))
            .finally(() => {
                this.setState({ isLoading: false });
            });
    }

    // fetching product details
    get_productDetails = () => {
        this.setState({isLoading:true})
        fetch(global.api + 'get-product-details',
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': global.token
                },
                body: JSON.stringify({
                    product_id: this.state.id,
                })
            })
            .then((response) => response.json())
            .then((json) => {
                if (json.status) {
                    console.warn('ss',json)
                    json.data.map(value => {
                        this.setState({ price: value.price,market_price:json.data[0].market_price,
                        discount:json.data[0].market_price - value.price })
                        this.setState({ images: value.picture })
                        this.setState({available_Qty:json.data[0].quantity})
                        console.warn("pictures", this.state.available_Qty)
                        this.setState({ cart: value.cart })
                        // console.warn("cart is", this.state.cart)
                        const like = this.state.like
                        if (value.wishlist == null) {
                            like[value.id] = false

                        } else {
                            like[value.id] = true
                        }
                        this.setState({ like })

                    })
                    this.setState({ data: json.data })
                    this.setState({product_quantity:this.state.data[0].quantity})
                    console.warn("data",this.state.data[0].quantity)
                    var ss = [];
                    var si = [];
                    var ww = [];
                    var wi = [];
                    this.state.data.map((details, id) => {
                        this.setState({ option: details.option })
                        {
                            details.option[1].size.map(key => {
                                ss.push(key.option_value)
                                si.push(key.option_id)
                                this.setState({ size: ss })
                                this.setState({ size_id: si })
                            })
                        }
                        {
                            details.option[0].size.map(key => {
                                ww.push(key.option_value)
                                this.setState({ weight: ww })
                                wi.push(key.option_id)
                                this.setState({ weight_id: wi })
                            })
                        }
                    })


                }
                else {
                    Toast.show("None data found")
                }
                return json;
            })
            .catch((error) => console.error(error))
            .finally(() => {
                this.setState({ isLoading: false });
            });
    }

    setSize = (itemValue) => {
        var ss = this.state.size_id[itemValue]
        this.setState({ s_id: ss })
        console.warn(this.state.s_id)
    }

    setWeight = (itemValue) => {
        var ss = this.state.weight_id[itemValue]
        this.setState({ w_id: ss })
        console.warn(this.state.w_id)
    }

    // Increase quantity 
    pluss = () => {
        this.setState({ quantity: this.state.quantity + 1 })
    }

    // Decreasde quantity
    minus = () => {
        if (this.state.quantity > 1) {
            this.setState({ quantity: this.state.quantity - 1 })
        }
        else {
            Toast.show("Number of pieces can not be less than one")
        }
    }

    add_to_cart = (id) => {

        if(this.state.available_Qty >0){
        // const object = this.state.object;
        // if (object[id] == true) {
        //     object[id] = false;
        //     var type = "no"

        // }
        // else {
        //     object[id] = true;
        //     var type = "yes"
        // }
        // this.setState({ object })
        fetch(global.api + "add-to-cart", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': global.token
            },
            body: JSON.stringify({
                type: "yes",
                product_id: this.state.id,
                product_quantity: this.state.quantity,
                size: 5,
                size_option_id: this.state.s_id,
                weight_option_id: this.state.w_id

            })
        }).then((response) => response.json())
            .then((json) => {
                if (!json.status) {

                    Toast.show("Select size or weight of a Product")
                    // console.warn('aaaa',json)
                }
                else {
                    //    this.RBSheet.close();
                    Toast.show(json.msg);
                    this.my_cart();
                   
                }
                return json;
            }).catch((error) => {
                console.error(error);
            }).finally(() => {
                this.setState({ isLoading: false })
                this.get_productDetails();
                this.my_cart();
            });
        }
        else{
            Toast.show("Out of Stock!")
        }
    }

    addCart = (id) => {
       
        if(this.state.available_Qty >0){
            // const object = this.state.object;
            // if (object[id] == true) {
            //     object[id] = false;
            //     var type = "no"

            // }
            // else {
            //     object[id] = true;
            //     var type = "yes"
            // }
            // this.setState({ object })
            fetch(global.api + "add-to-cart", {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': global.token
                },
                body: JSON.stringify({
                    type: "yes",
                    product_id: this.state.id,
                    product_quantity: this.state.quantity,
                    size: 5,
                    size_option_id: 1,
                    weight_option_id: 1

                })
            }).then((response) => response.json())
                .then((json) => {
                    if (!json.status) {

                        console.warn(json)
                    }
                    else {
                        //    this.RBSheet.close();
                        Toast.show(json.msg)
                        this.my_cart();
                        
                      
                    }
                    return json;
                }).catch((error) => {
                    console.error(error);
                }).finally(() => {
                    this.setState({ isLoading: false })
                    this.get_productDetails();
                    this.my_cart();
                });
            }
            else{
                Toast.show("Out of Stock!")
            }
    }

    buy =(id)=>{
        if(this.state.available_Qty >0){
            // const object = this.state.object;
            // if (object[id] == true) {
            //     object[id] = false;
            //     var type = "no"

            // }
            // else {
            //     object[id] = true;
            //     var type = "yes"
            // }
            // this.setState({ object })
            fetch(global.api + "add-to-cart", {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': global.token
                },
                body: JSON.stringify({
                    type: "yes",
                    product_id: this.state.id,
                    product_quantity: this.state.quantity,
                    size: 5,
                    size_option_id: 1,
                    weight_option_id: 1

                })
            }).then((response) => response.json())
                .then((json) => {
                    if (!json.status) {

                        console.warn(json)
                    }
                    else {
                        //    this.RBSheet.close();
                        Toast.show(json.msg)
                        this.my_cart();
                        this.props.navigation.navigate("MyCart")
                        
                      
                    }
                    return json;
                }).catch((error) => {
                    console.error(error);
                }).finally(() => {
                    this.setState({ isLoading: false })
                    this.get_productDetails();
                    this.my_cart();
                });
            }
            else{
                Toast.show("Out of Stock!")
            }
    }

    buy_now = (id) => {             
        if(this.state.available_Qty >0){
            // const object = this.state.object;
            // if (object[id] == true) {
            //     object[id] = false;
            //     var type = "no"
    
            // }
            // else {
            //     object[id] = true;
            //     var type = "yes"
            // }
            // this.setState({ object })
            fetch(global.api + "add-to-cart", {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': global.token
                },
                body: JSON.stringify({
                    type: "yes",
                    product_id: this.state.id,
                    product_quantity: this.state.quantity,
                    size: 5,
                    size_option_id: this.state.s_id,
                    weight_option_id: this.state.w_id
    
                })
            }).then((response) => response.json())
                .then((json) => {
                    if (!json.status) {
    
                        Toast.show("Select size or weight of a Product")
                        // console.warn('aaaa',json)
                    }
                    else {
                        //    this.RBSheet.close();
                        Toast.show(json.msg);
                        this.my_cart();
                        this.props.navigation.navigate("MyCart")
                       
                    }
                    return json;
                }).catch((error) => {
                    console.error(error);
                }).finally(() => {
                    this.setState({ isLoading: false })
                    this.my_cart();
                });
            }
            else{
                Toast.show("Out of Stock!")
            }
    }

    buySelect = (id) => {
        if(this.state.available_Qty >0){
            // const object = this.state.object;
            // if (object[id] == true) {
            //     object[id] = false;
            //     var type = "no"
    
            // }
            // else {
            //     object[id] = true;
            //     var type = "yes"
            // }
            // this.setState({ object })
            fetch(global.api + "add-to-cart", {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': global.token
                },
                body: JSON.stringify({
                    type: "yes",
                    product_id: this.state.id,
                    product_quantity: this.state.quantity,
                    size: 5,
                    size_option_id: this.state.s_id,
                    weight_option_id: this.state.w_id
    
                })
            }).then((response) => response.json())
                .then((json) => {
                    if (!json.status) {
    
                        Toast.show("Select size or weight of a Product")
                        // console.warn('aaaa',json)
                    }
                    else {
                        //    this.RBSheet.close();
                        Toast.show(json.msg);
                        this.my_cart();
                        this.props.navigation.navigate("MyCart")
                       
                    }
                    return json;
                }).catch((error) => {
                    console.error(error);
                }).finally(() => {
                    this.setState({ isLoading: false })
                    this.get_productDetails();
                    this.my_cart();
                });
            }
            else{
                Toast.show("Out of Stock!")
            }
        }

    //share funtion
    myShare = async (title,content,url)=>{
        const shareOptions={
            title:title,
            message:title+content,
            url:url,
        }
        try{
            const ShareResponse = await Share.open(shareOptions);

        }catch(error){
            console.log("Error=>",error)
        }
    }

    renderItem = ({ item }) => (
        <TouchableOpacity style={[style.child,]}>
            <ImageBackground
                resizeMode="cover"
                source={{ uri: "https://kamaljewellers.in/CDN/" + item.src }}
                style={{
                    width: Dimensions.get('window').width,
                    height: '100%',
                    alignSelf: 'center',
                    // because it's parent
                    //width:width*0.9
                }}></ImageBackground>

        </TouchableOpacity>
    );

    render() {
        return (
            <View style={[style.container, { backgroundColor: "#fff",flex:1,height:Dimensions.get("screen").height }]}>
                <ScrollView showsVerticalScrollIndicator={false}>
                <View style={{ height: 400 ,}}>

                    {/* Main Slider */}
                    {/* <Swiper style={styles.wrapper}
                        showsButtons={false}
                        paginationStyle={{
                            bottom: 50,
                        }}
                        dotColor="#DDDDDD"
                        activeDotColor="#BC3B3B"
                    //  autoplay={true}
                    >
                        {this.state.images.map(value=>{
                            <View style={styles.slide1}>
                            <TouchableOpacity
                                onPress={() => this.setModalVisible(true)} >
                                <Image source={{ uri:"https://kamaljewellers.in/CDN/"+value.src }}
                                    style={styles.slide1Image} />
                            </TouchableOpacity>
                        </View>
                        })}                       
                    </Swiper> */}
                  {this.state.isLoading ?
                  <View>
                      <SkeletonPlaceholder>
                          <View>
                              <View style={{height:400,width:Dimensions.get('window').width}}/>
                          </View>

                          
                      </SkeletonPlaceholder>
                  </View>
                    :
                    <SwiperFlatList
                    autoplay
                    autoplayDelay={2}
                    autoplayLoop
                    index={0}
                    showPagination
                    paginationActiveColor='#bc3b3b'
                    renderItem={this.renderItem}
                    data={this.state.images}
                    paginationStyleItem={{
                        width: 7,
                        height: 7,
                        marginTop: -10,
                        marginLeft: 0,
                        marginRight: 5,
                    }}></SwiperFlatList>
                }


                    {/* View for header */}
                    <View style={{
                        flexDirection: 'row', position: "absolute",
                        justifyContent: "space-between", width: "100%",
                    }}>
                        <TouchableOpacity style={{ margin: 40, marginLeft: 15, top: 5, alignItems: "center", padding: 5, borderRadius: 60, opacity: 0.5, backgroundColor: "black", height: 40, width: 40 }}
                            onPress={() => this.props.navigation.goBack()}>
                            <Icon name="chevron-back-outline" size={27}
                                type="ionicon" color="#fff" />
                        </TouchableOpacity>
                       <View style={{flexDirection:"row",justifyContent:"flex-end",width:"50%"}}>
                       <TouchableOpacity style={{ margin: 40,top: 5, paddingLeft: 7,paddingTop:7, alignItems: "center",marginRight:-50,
                        padding: 5, borderRadius: 60, opacity: 0.5, backgroundColor: "black", height: 40, width: 40 }}
                                onPress={() => this.myShare("Checkout this product on Kamal Jewellers || \n",this.state.data[0].name +" \n"+global.shareLink+'/productView/'+this.state.data[0].id)} >
                                <Icon
                                    name='share-social-outline'
                                    size={23}
                                    color='#fff'
                                    type="ionicon"

                                // style={{marginTop:5}} 
                                />

                            </TouchableOpacity>
                        <View>
                            <TouchableOpacity style={{ margin: 40, right: -20, top: 5, paddingLeft: 5, alignSelf: "flex-end", alignItems: "center", padding: 5, borderRadius: 60, opacity: 0.5, backgroundColor: "black", height: 40, width: 40 }}
                                onPress={() => this.props.navigation.navigate("MyCart")}>
                                <Icon
                                    name='cart-outline'
                                    size={25}
                                    color='#fff'
                                    type="ionicon"

                                // style={{marginTop:5}} 
                                />

                            </TouchableOpacity>
                            {this.state.item == "" ? null :
                                <View style={{ backgroundColor: "#bc3b3b", width: 20, borderRadius: 50, bottom: 80, left: 90 }} >
                                    <Text style={{ color: "#fff", left: 7 }}>{this.state.item}</Text>
                                </View>
                            }
                        </View>
                       </View>
                    </View>


                </View>

                {/* view for main component call */}
                <View style={styles.mainContainer}>
                    {
                        this.state.isLoading ?
                        <View>
                            <SkeletonPlaceholder>
                                <View>
                                    <View style={{margin:10,flexDirection:"row",justifyContent:"space-between"}}>
                                       <View>
                                            <View style={{height:25,width:200}}/>
                                            <View style={{height:20,width:150,marginTop:5}}/>
                                            <View style={{height:20,width:150,marginTop:5}}/>
                                       </View>
                                       <View style={{height:50,width:50,borderRadius:50}}/>

                                    </View>

                                    <View style={{margin:10,flexDirection:"row",justifyContent:"space-between",width:"100%"}}>
                                        <View style={{width:"45%"}}>
                                            <View style={{height:20,width:150,marginTop:5}}/>
                                            <View style={{height:20,width:150,marginTop:5}}/>
                                        </View>

                                        <View style={{flexDirection:"row",justifyContent:"space-evenly",width:"55%",marginTop:10}}>
                                            <View style={{height:25,borderRadius:25,width:40}}/>
                                            <View style={{height:25,borderRadius:25,width:40}}/>
                                            <View style={{height:25,borderRadius:25,width:40}}/>
                                        </View>
                                    </View>

                                    <View style={{height:60,width:Dimensions.get('window').width/1.05,
                                    borderRadius:5,alignSelf:"center",marginTop:10}}/>
                                    
                                    <View style={{margin:10}}>
                                        <View style={{height:20,width:150,marginTop:5}}/>
                                        <View style={{marginTop:10,justifyContent:"space-between",flexDirection:"row"}}>
                                            <View style={{height:40,width:150,borderRadius:10}}/>
                                            <View style={{height:25,width:50}}/>
                                        </View>

                                    </View>

                                    <View style={{height:40,width:300,borderRadius:25,marginTop:10,alignSelf:"center"}}/>
                                </View>
                            </SkeletonPlaceholder>
                        </View>
                        :

                        <MainContainer navigation={this.props.navigation}
                            id={this.state.id}
                            like={this.state.like}
                            data={this.state.data}
                            cart={this.state.cart}
                            obj={this.state.obj}
                            quantity={this.state.quantity}
                            pluss={this.pluss}
                            minus={this.minus}
                            size={this.state.size}
                            weight={this.state.weight}
                            setSize={this.setSize}
                            setWeight={this.setWeight}
                        />
                    }


                    

                </View>
                </ScrollView>

               
                    <View>
                        {/* view for sticky bottom buttons */}
                        <View style={{ flexDirection: "row", marginTop: -20, height: 50 }}>
                        
                        
                            {this.state.cart != null ?
                                <Pressable style={style.stickyButton2}
                                    onPress={() => this.props.navigation.navigate("MyCart")}>
                                    <Text style={style.stickyButton2Text}>
                                        Go to cart
                                    </Text>
                                </Pressable>
                                :
                                <View style={style.stickyButton2}>
                                    {this.state.option.length == 0?
                                    <Pressable style={style.stickyButton2}
                                    onPress={() => this.addCart(this.state.id)}>
                                    <Text style={style.stickyButton2Text}>
                                        Add to cart
                                    </Text>
                                </Pressable>:
                                <Pressable style={style.stickyButton2}
                                    onPress={() => this.add_to_cart(this.state.id)}>
                                    <Text style={style.stickyButton2Text}>
                                        Add to carts
                                    </Text>
                                </Pressable>
                }
                                </View>
                            }


<View>
                            {this.state.cart == null ?
                            <>
                                {this.state.option.length == 0?
                            <Pressable style={[style.stickyButton1,{height:50}]}
                            onPress={() => { 
                                {this.buy(this.state.id)

                            }}}>
                            <Text style={style.stickyButton1Text}>
                                Buy now
                            </Text>
                        </Pressable>
                            :
                            <Pressable style={[style.stickyButton1,{height:50}]}
                                    
                            onPress={() => {this.buySelect()}}>
                                    <Text style={style.stickyButton1Text}>
                                        Buy 
                                    </Text>
                                </Pressable>
                        }
                           </>
                            :   
                            <View>
                                <Pressable style={[style.stickyButton1,{height:50}]}
                                onPress={()=>this.props.navigation.navigate("MyCart")}
                                >
                            <Text style={style.stickyButton1Text}>
                                Buy now1
                            </Text>
                        </Pressable>
                            </View>
                        }
                        </View>

                        </View>
                    </View>
                
                

            </View>
        )
    }
}

export default Product;

class MainContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            selectedValue: "",
            selectedValue2: "",
            pinCode: "",
            status: false,
            modalVisible: false,
            choosenDate: "DD/MM/YYYY",
            isVisible: false,
            time: "",
            pincode: "",
            note: "",
            // size:""
        }
    }

    //modal For appointment
    setModalVisible = (visible) => {
        this.setState({ modalVisible: visible });
    }

    showPicker = () => {
        // alert("hi");
        this.setState({
            isVisible: true
        })

    }

    handlePicker = (date) => {
        if (this.state.choosenDate == "DD/MM/YYYY") {
            this.setState({ choosenDate: this.state.choosenDate })
        }
        this.setState({
            isVisible: false,
            choosenDate: moment(date).format('DD/MM/YYYY')
        })
    }

    hidePicker = () => {
        this.setState({
            isVisible: false
        })
    }

    // remove from wishlist
    remove = (id) => {
        const like = this.props.like;
        if (like[id] == true) {
            like[id] = false;
            var type = "no"

        }
        else {
            like[id] = true;
            var type = "yes"
        }
        this.setState({ like })
        fetch(global.api + "add-to-wishlist", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': global.token
            },
            body: JSON.stringify({
                type: type,
                product_id: id,
            })
        }).then((response) => response.json())
            .then((json) => {
                if (!json.status) {
                    var msg = json.msg;
                    Toast.show(msg);
                }
                else {
                    Toast.show(json.msg)

                }
                return json;
            }).catch((error) => {
                console.error(error);
            }).finally(() => {
                this.setState({ isLoading: false })
            });

    }

    // Appointment book
    book = () => {
        if (this.state.time == "" || this.state.choosenDate == "DD/MM/YYYY") {
            Toast.show("All fields are required!")
        }
        else {
            this.setState({ isLoading: true })
            fetch(global.api + "book-an-appointment", {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': global.token
                },
                body: JSON.stringify({
                    appoint_time: this.state.time,
                    appoint_date: this.state.choosenDate,
                    status: "active"
                })
            }).then((response) => response.json())
                .then((json) => {
                    if (!json.status) {
                        var msg = json.msg;
                        Toast.show(msg);
                    }
                    else {
                        Toast.show("Appointment Booked Successfully!")

                    }
                    return json;
                }).catch((error) => {
                    console.error(error);
                }).finally(() => {
                    this.setState({ isLoading: false, modalVisible: false })
                });
        }

    }

    // Check delivery at pincode
    check_delivery = () => {
        if(this.state.pincode == ""){
            Toast.show("Enter a pincode");
        }
        else{
            this.setState({ status: true })
        fetch(global.api + "check-delivery", {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Authorization': global.token
            },
            body: JSON.stringify({
                pincode: this.state.pincode
            })
        }).then((response) => response.json())
            .then((json) => {
                console.warn(json)
                if (!json.status) {
                    this.setState({ note: json.data })
                    Toast.show(this.state.note);
                }
                else {
                    this.setState({ note: json.data })
                    Toast.show(this.state.note);

                }
                return json;
            }).catch((error) => {
                console.error(error);
            }).finally(() => {

            });
        }
    }

    render() {
        let details = this.props.data.map((details, id) => {
            return (
                <View>

                    <View style={{ flexDirection: "row", justifyContent: "space-between", marginHorizontal: 10, }}>
                        {/* View for name and ratings */}
                        <View style={{ marginHorizontal: 10, }} >
                            <Text style={[style.h3, { paddingTop: 20, fontSize: 17 }]}>
                                {details.name}</Text>

                            <Text style={{ top: 5 }}>
                                {/* {details.rating.ratings} */}
                                {details.rating == null ?
                                    <Rating
                                        showRating={false}
                                        readonly={false}
                                        fractions={1}
                                        startingValue={0}
                                        imageSize={15}
                                        style={{ marginLeft: 10 }}
                                    /> :
                                    <Rating
                                        showRating={false}
                                        readonly={false}
                                        fractions={1}
                                        startingValue={details.rating.ratings}
                                        imageSize={15}
                                        style={{ marginLeft: 10 }}
                                    />
                                }
                            </Text>


                            <View style={{ flexDirection:"row"}}>
                                <Text style={[style.h3, {
                                    fontFamily: "Montserrat-SemiBold",
                                    fontSize: 14, marginTop: 10,textDecorationLine:"line-through"
                                }]}>
                                    ₹ {details.market_price}
                                </Text>

                                <Text style={[style.h3, {
                                    fontFamily: "Montserrat-SemiBold",
                                    fontSize: 14, marginTop: 10,paddingLeft:20
                                }]}>
                                    ₹ {details.price}
                                </Text>
                                {/* <Text style={[style.small, , {
                                    fontFamily: "Montserrate-SemiBold", fontSize: 12, alignSelf: "flex-end",
                                    right: 20
                                }]}>
                                    per Piece
                                </Text> */}
                            </View>
                            
                        </View>
                        {/* view for heart */}
                        <View>
                            <View style={styles.heartView}>
                                <Icon name={this.props.like[details.id] ? "heart" : "heart-outline"}
                                    onPress={() => this.remove(details.id)}
                                    type="ionicon" color={this.props.like[details.id] ? "red" : "#7c7d7e"} />
                            </View>
                            {/* View for price */}
                                     

                        </View>
                    </View>

                    {/* View for line */}
                    <View style={style.verticalLine}></View>

                    {/* View for dropdowns */}
                    {details.option.length == 0
                        ? null :
                        <View>
                            <Text style={[styles.heading, { paddingLeft: 20, top: 10, fontSize: 14, paddingBottom: 10 }]}>
                                Customize your order
                            </Text>

                            {/* picker for size */}
                            <View style={{ marginLeft: 30, marginTop: 10, marginRight: 30, }}>
                                <SelectDropdown
                                    buttonStyle={{ width: "100%" }}
                                    dropdownStyle={{ height: 200 }}
                                    data={this.props.size}
                                    onSelect={(selectedValue, index) => {
                                        this.props.setSize(index);
                                    }}
                                    buttonTextAfterSelection={(selectedValue, index) => {
                                        return selectedValue
                                    }}
                                    rowTextForSelection={(item, index) => {
                                        return item
                                    }}
                                    defaultButtonText='Select Size'
                                />
                            </View>

                            {/* picker for gross weight */}
                            <View style={{ marginLeft: 30, marginTop: 10, marginRight: 30, }}>
                                <SelectDropdown
                                    buttonStyle={{ width: "100%" }}
                                    dropdownStyle={{ height: 200 }}
                                    style={{ height: 150 }}
                                    data={this.props.weight}
                                    onSelect={(selectedValue2, index) => {
                                        this.props.setWeight(index);
                                    }}
                                    buttonTextAfterSelection={(selectedValue2, index) => {
                                        return selectedValue2
                                    }}
                                    rowTextForSelection={(item, index) => {
                                        return item
                                    }}
                                    defaultButtonText='Select Gross weight'
                                />
                            </View>
                            {/* <Text style={[style.headingSmall,
                        { alignSelf: 'center', top: 5, bottom: 5 }]}>
                            Diamond weight 0.148 carat
                        </Text> */}
                        </View>
                    }

                    {/* View for line */}
                    {details.option.length == 0
                        ? null :
                        <View style={[style.verticalLine,
                        {
                            marginTop: 20, width: Dimensions.get('window').width / 1.1,
                            alignSelf: "center"
                        }]}>
                        </View>
                    }

                    {/* View for number of pieces */}
                    <View style={{
                        flexDirection: 'row', width: "100%", marginTop: 20,
                        marginnBottom: 20, justifyContent: "space-between"
                    }}>
                        <View style={{ width: "50%" }}>
                            <Text style={[styles.heading, { paddingLeft: 20, fontSize: 14, marginTop: 5 }]}>
                                Number of pieces
                            </Text>
                            {
                                details.quantity == 0 ?
                                <Text style={[style.headingSmall,
                                    { top: 5, paddingLeft: 5, fontSize: RFValue(10, 580) }]}>Out of Stock</Text>
                                :
                                <Text style={[style.headingSmall,
                                    { top: 5, paddingLeft: 5, fontSize: RFValue(10, 580) }]}>
                                        Only <Text style={{ fontFamily: "Montserrat-Medium", paddingLeft: 5 }}>
                                            {details.quantity}  </Text>
                                        left in stock.
                                    </Text>
                            }
                        </View>

                        {
                            details.quantity <= 0 ?
                            <View style={{ flexDirection: 'row', marginTop: 10, width: "50%", justifyContent: "space-evenly", right: 10 }}>
                            <Pressable style={styles.button}
                                onPress={() => {
                                    Toast.show("Out of stock!")
                                }}>
                                <Icon name="remove" type="ionicon" size={20} color="#BC3B3B"
                                    style={{ top: -2 }} />
                            </Pressable>

                            <View style={[styles.button]}>
                                <Text style={[style.h4, { top: 0, alignSelf: "center", color: "#BC3B3B" }]}>
                                    0
                                </Text>
                            </View>

                            <Pressable style={styles.button} onPress={() => {
                               Toast.show("Out of stock!")
                            }}>
                                <Icon name="add" type="ionicon" size={20} color="#BC3B3B"
                                    style={{ top: -2 }} />
                            </Pressable>
                            </View>
                            :
                            <View style={{ flexDirection: 'row', marginTop: 10, width: "50%", justifyContent: "space-evenly", right: 10 }}>
                            <Pressable style={styles.button}
                                onPress={() => {
                                    // this.props.minus()
                                    Toast.show("The product quantity should be 1")
                                }}
                                >
                                <Icon name="remove" type="ionicon" size={20} color="#BC3B3B"
                                    style={{ top: -2 }} />
                            </Pressable>

                            <View style={[styles.button]}>
                                <Text style={[style.h4, { top: 0, alignSelf: "center", color: "#BC3B3B" }]}>
                                    {this.props.quantity}
                                </Text>
                            </View>

                            <Pressable style={styles.button} 
                            onPress={() => {
                                // this.props.pluss()
                                Toast.show("The product quantity should be 1")
                            }}
                            >
                                <Icon name="add" type="ionicon" size={20} color="#BC3B3B"
                                    style={{ top: -2 }} />
                            </Pressable>
                            </View>
                        }
                        

                    </View>

                    {/* View for terms and conditions */}
                    <View style={styles.view}>
                        <Text style={[style.h6, { padding: 5, lineHeight: 25, color: "#8C8D8E", alignSelf: "center" }]}>
                            *Weight and price of the jewellery item may vary subject
                            to the stock available.
                        </Text>
                    </View>

                    {/* View for line */}
                    <View style={[style.verticalLine,
                    {
                        marginTop: 15, width: Dimensions.get('window').width / 1.1,
                        alignSelf: "center"
                    }]}>
                    </View>

                    {/* view for description */}
                    {details.discription == '' ?
                        <></>
                    :   
                    <View style={{marginTop:10,}}>
                        <Text style={[styles.heading, { paddingLeft: 20, fontSize: 14, marginTop: 5 }]}>
                            Description</Text>
                        <Text style={[styles.text, { top: 10,color: "#8C8D8E", fontSize: 12, bottom: 5, paddingLeft: 20, lineHeight: 18, textAlign:"justify", marginRight:20 }]}>
                                {details.discription}
                            </Text>
                    </View>
                    }

                    {/* view for pincode options */}
                    <View>
                        <View style={{ flexDirection: 'row', justifyContent: "space-between", top: 20 }}>
                            <View>
                                <Text style={[styles.text, { marginLeft: 20 }]}>
                                    Delivery options for {" "}
                                    {this.state.pincode}
                                </Text>
                                {/* {this.state.status ?
                                    <View>
                                        {this.state.note == "Delivery available at this Pincode!" ?
                                        <Text style={[style.headingSmall,
                                            { alignSelf: 'center', top: 10,fontWeight:"500",color:"green", marginBottom: 10, paddingLeft: 5, fontSize: RFValue(11, 580) }]}>
                                                {this.state.note}
    
                                            </Text>
                                        :
                                        <Text style={[style.headingSmall,
                                            { alignSelf: 'center', top: 10, marginBottom: 10, paddingLeft: 5, fontSize: RFValue(11, 580) }]}>
                                                {this.state.note}
    
                                            </Text>
                                            }
                                    </View>
                                    : */}
                                    <View style={{ marginLeft: 20 }}>
                                        <TextInput placeholder="Enter Pincode"
                                            maxLength={6}
                                            keyboardType='numeric'
                                            value={this.state.pincode}
                                            onChangeText={(text) => this.setState({ pincode: text })}
                                            style={{ fontSize: RFValue(11, 580),width:200,height:40,
                                            fontFamily: "Montserrat-Medium", borderWidth: 1, borderColor: "#7c7d7e", marginBottom: 10,
                                            paddingLeft:10,marginTop:10,borderRadius:5 , paddingTop:Platform.OS == "ios" ? 0 : 10}} />
                                    </View>
                                {/* } */}
                            </View>
                            {/* {this.state.status ?
                                <TouchableOpacity
                                    onPress={() => this.setState({ status: false })}
                                    style={[style.changeButton, { right: 55, top: 28 }]}>
                                    <Text style={[styles.text, { color: "#262626", top: 3 }]}>
                                        Change
                                    </Text>
                                </TouchableOpacity>
                                : */}
                                <TouchableOpacity
                                    onPress={() => this.check_delivery()}
                                    style={[style.changeButton, { right: 55, top: 28 }]}>
                                    <Text style={[styles.text, { color: "#bc3b3b", fontSize: 15, top: 3 }]}>
                                        Check
                                    </Text>
                                </TouchableOpacity>
                            {/* } */}
                        </View>
                    </View>

                    {/* View for buttons */}
                    <View style={{ marginBottom: 10, marginTop: 20 }}>
                        <Pressable onPress={() => this.setState({ modalVisible: true })} style={[style.buttonStyles, {
                            marginTop: 30, bottom: 15,
                            width: Dimensions.get("window").width / 1.2,
                        }]}>
                            <Text style={style.buttonText}>
                                Book an appointment
                            </Text>
                        </Pressable>
                        <Modal
                            //  animationType="slide"
                            transparent={true}
                            visible={this.state.modalVisible}
                        >
                            <View style={styles.centeredView}>
                                <View style={styles.modalView}>
                                    {/* Modal close button */}
                                    <View style={{ flexDirection: "row", alignSelf: "flex-end", paddingBottom: 5 }}>
                                        <TouchableOpacity
                                            style={styles.buttonClose}
                                            onPress={() => this.setModalVisible(!this.state.modalVisible)}>
                                            <Icon type="ionicon" name="close-circle-outline" size={25} />
                                        </TouchableOpacity>
                                    </View>

                                    <View style={{ padding: 20 }}>

                                        <Text
                                            onPress={() => this.showPicker()}
                                            style={[styles.textinput, { padding: 10 }]}
                                        >
                                            {this.state.choosenDate}
                                        </Text>

                                        {/* date picker */}
                                        <DateTimePicker
                                            isVisible={this.state.isVisible}
                                            mode={"date"}
                                            is24Hour={false}
                                            minimumDate={new Date()}
                                            onConfirm={this.handlePicker}
                                            onCancel={this.hidePicker}
                                        />

                                        {/* picker for gross weight */}
                                        <Picker
                                            selectedValue={this.state.time}
                                            onValueChange={(e) => this.setState({ time: e })}
                                            style={{
                                                backgroundColor: "#fff",
                                                marginTop: Platform.OS == "android" ? 15 : 0,
                                                width: Dimensions.get('window').width / 1.5,
                                                alignSelf: "center",
                                                height: 25,
                                                marginBottom: 5
                                            }}
                                            itemStyle={{
                                                fontFamily: "Tangerine-Bold",
                                            }}
                                        >
                                            <Picker.Item label="Select time" />
                                            <Picker.Item label="11:00AM" value="11:00AM" />
                                            <Picker.Item label="12:00PM" value="12:00PM" />
                                            <Picker.Item label="01:00PM" value="01:00PM" />
                                            <Picker.Item label="02:00PM" value="02:00PM" />
                                            <Picker.Item label="03:00PM" value="03:00PM" />
                                            <Picker.Item label="04:00PM" value="04:00PM" />
                                            <Picker.Item label="05:00PM" value="05:00PM" />
                                            <Picker.Item label="06:00PM" value="06:00PM" />
                                        </Picker>

                                        {this.state.isLoading ?
                                            <ActivityIndicator size="large" color="#bc3b3b" />
                                            :
                                            <>
                                            {
                                                (Platform.OS == "android") ?
                                                <Pressable onPress={() => this.book()} style={[style.buttonStyles, {
                                                    marginTop: 40, bottom: 5,
                                                    width: Dimensions.get("window").width / 2,
                                                }]}>
                                                    <Text style={style.buttonText}>
                                                        Submit
                                                    </Text>
                                                </Pressable>
                                                :
                                                <Pressable onPress={() => this.book()} style={[style.buttonStyles, {
                                                    marginTop: 190, bottom: 5,
                                                    width: Dimensions.get("window").width / 2,
                                                }]}>
                                                    <Text style={style.buttonText}>
                                                        Submit
                                                    </Text>
                                                </Pressable>
                                            }
                                            </>
                                        }

                                    </View>
                                </View>
                            </View>
                        </Modal>
                    </View>




                </View>
            )
        })

        return (
            <View>
                {this.state.isLoading ?
                <View>
                    <SkeletonPlaceholder>
                        <View>
                            <View style={{height:25,width:100}}/>
                        </View>
                    </SkeletonPlaceholder>
                </View>
                :
                <View>
                    {this.state.data == "" ?
                    <View style={{ marginTop: 100 }}>
                        <ActivityIndicator size="large" color="#bc3b3b" />
                    </View>
                    :
                    <View>
                        {details}
                    </View>
                }
                </View>
                }
            </View>
        )
    }
}


//Internal Stylesheet
const styles = StyleSheet.create({
    slide1: {
        height: "100%",
        width: "100%",

    },
    slide2: {
        height: "100%",
        width: "100%",
        backgroundColor: "#fff"
    },
    slide3: {
        height: "100%",
        width: "100%",
        backgroundColor: "#fff"
    },
    slide1Image: {
        height: "100%",
        width: "100%",

    },
    slide2Image: {
        height: "100%",
        width: "100%",
    },
    slide3Image: {
        height: "100%",
        width: "100%",
    },

    mainContainer: {
        height: "200%",
        elevation: 7,
        shadowRadius: 10,
        shadowColor: '#171717',
        shadowOffset: {width: -2, height: 4},
        shadowOpacity: 0.2,
        shadowRadius: 3,
        // position:"absolute",
        // // borderWidth:1,
        // borderTopLeftRadius: 40,
        // borderTopRightRadius: 40,
        // width: "100%",
        // top: -18,
        // backgroundColor:"red",
        // bottom: -10,
        backgroundColor: "#fff",
        zIndex: 1,
        // flex: 1,
    },
    heartView: {
        shadowRadius: 50,
        shadowOffset: { width: 1, height: 1 },
        elevation: 2,
        height: 50,
        width: 50,
        backgroundColor: "#fff",
        justifyContent: "center",
        borderRadius: 50,
        marginTop: 10
    },
    heading: {
        fontSize: RFValue(13, 580),
        // fontFamily:"Raleway-Bold",
        fontFamily: "Montserrat-Medium",
        color: '#4a4b4d',
    },
    button: {
        borderColor: "#bc3b3b",
        borderWidth: 1,
        borderRadius: 10,
        width: 35,
        height: 22,

    },
    view: {
        borderColor: "#EBEBEB",
        borderWidth: 1,
        borderRadius: 5,
        width: Dimensions.get('window').width / 1.1,
        marginTop: 25,
        alignSelf: "center",
        paddingBottom: 5,

    },
    text: {
        // fontFamily:"Raleway-Regular",
        fontFamily: "Montserrat-Medium",
        color: "#222222",
        fontSize: RFValue(10, 580)
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 50,
    },
    modalView: {
        margin: 20,
        width: "80%",
        height: "50%",
        backgroundColor: "#fff",
        borderRadius: 20,
        padding: 20,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    textinput: {
        backgroundColor: "white",
        color: "black",
        fontSize: RFValue(12, 580),
        paddingLeft: 15,
        marginTop: 15,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: "#d3d3d3"
    }

})
