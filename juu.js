import React, {Component} from 'react';
import {View} from 'react-native';
import {Icon} from 'react-native-elements';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {RFValue} from 'react-native-responsive-fontsize';
import AsyncStorage from '@react-native-async-storage/async-storage';

//File Imports
import Splash from './Screens/Splash.js';
import Skip from './Screens/Skip.js';
import MobileLogin from './Screens/MobileLogin.js';
import OtpVerify from './Screens/OtpVerify.js';
import CompleteProfile from './Screens/CompleteProfile.js';
import Collections from './Screens/Collections';
import Studio from './Screens/Studio.js';
import Profile from './Screens/Profile.js';
import More from './Screens/More.js';
import Home from './Screens/Home.js';
import ProductListCollection from './Screens/ProductListCollection.js';
import Products from './Screens/Products.js';
import CreatePost from './Screens/CreatePost.js';
import AboutUs from './Screens/AboutUs.js';
import Answers from './Screens/Answers.js';
import Checkout from './Screens/Checkout.js';
import MyCart from './Screens/MyCart.js';
import MyOrders from './Screens/MyOrders.js';
import MyProfile from './Screens/MyProfile.js';
import Notifications from './Screens/Notifications.js';
import OrderDetails from './Screens/OrderDetails.js';
import OrderStatus from './Screens/OrderStatus.js';
import PaymentDetails from './Screens/PaymentDetails.js';
import Saved from './Screens/Saved.js';
import Support from './Screens/Support.js';
import Wishlist from './Screens/Wishlist.js';
import ChangeAddress from './Screens/ChangeAddress.js';
import Report from './Screens/Report.js';
import ReportSuccessful from './Screens/ReportSuccessful.js';
import Comments from './Screens/Comments.js';
import ProductOffer from './Screens/ProductOffer.js';
import SavedPost from './Screens/SavedPost.js';
import Collection from './Screens/Collection.js';
import Search from './Screens/Search.js';
import NewAddress from './Screens/NewAddress.js';
import ProfilePost from './Screens/ProfilePost.js';
import Video from './Screens/Video.js';
import Videos from './Screens/Video.js';
import OneSignal from 'react-native-onesignal';
import EditComment from './Screens/EditComment.js';
import {AuthContext} from './AuthContextProvider';
import UserProfile from './Screens/UserProfile.js';
import PaymentOptions from './Screens/PaymentOptions.js';
import ThankYou from './Screens/ThankYou.js';
import EditAddress from './Screens/EditAddress.js';
import SingleUserpost from './Screens/SingleUserpost.js';
import Followers from './Screens/Followers.js';
import Following from './Screens/Following.js';
import PaymentFailed from './Screens/PaymentFailed.js';
import VendorRatings from './Screens/VendorRatings.js';
import SingleFeed from './Screens/SingleFeed.js';
import linking from './src/linking.js';

//OneSignal Init Code
OneSignal.setLogLevel(6, 0);
OneSignal.setAppId('e29cac5f-5d6a-4e3b-a7f7-6cf11934545c');
//END OneSignal Init Code

//Prompt for push on iOS
OneSignal.promptForPushNotificationsWithUserResponse(response => {
  console.log('Prompt response:', response);
});

//Method for handling notifications received while app in foreground
OneSignal.setNotificationWillShowInForegroundHandler(
  notificationReceivedEvent => {
    console.log(
      'OneSignal: notification will show in foreground:',
      notificationReceivedEvent,
    );
    let notification = notificationReceivedEvent.getNotification();
    console.log('notification: ', notification);
    const data = notification.additionalData;
    console.warn('additionalData: ', data);
    // Complete with null means don't show a notification.
    notificationReceivedEvent.complete(notification);
  },
);

//Method for handling notifications opened
OneSignal.setNotificationOpenedHandler(notification => {
  console.warn('OneSignal: notification opened:', notification.additionalData);
});

//Navigators Import
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

global.google_key = 'AIzaSyDG9RC60WCZTRhE2Du-BhOzrEgsCwckN7M';
global.api = 'https://demo.webixun.com/KamalJwellersApi/public/api/';
global.login_data = true;
global.image_url =
  'https://demo.webixun.com/KamalJwellersApi/public/profile_pic/';
global.img_url = 'https://kamaljewellers.in/CDN/';
global.shareLink = 'https://demo.webixun.com/KamalJwellersApi/public/';

//Tab Screens Navigator
class TabNav extends Component {
  render() {
    return (
      <Tab.Navigator
        initialRouteName="Home"
        screenOptions={({route}) => ({
          headerShown: false,
          tabBarIcon: ({focused, color, tintColor}) => {
            let iconName;
            if (route.name == 'Collection') {
              iconName = focused ? 'grid' : 'grid-outline';
            } else if (route.name == 'Studio') {
              iconName = focused ? 'tv' : 'tv-outline';
            } else if (route.name == 'Home') {
              // return (
              //   <View style={{padding:10,elevation:1,
              //   justifyContent:"center",width:60,height:60,borderRadius:50,
              //   backgroundColor:focused ? "#bc3b3b": color}}>

              //     <MaterialCommunityIcons name="home" color="#fff" size={35}
              //     type="ionicon" style={{alignSelf:"center"}}/>
              //   </View>
              // )
              iconName = focused ? 'home' : 'home-outline';
            } else if (route.name == 'Profile') {
              iconName = focused ? 'person' : 'person-outline';
            } else if (route.name == 'More') {
              iconName = focused ? 'list' : 'list-outline';
            }
            return (
              <Icon name={iconName} color={color} type="ionicon" size={20} />
            );
          },
        })}
        tabBarOptions={{
          labelPosition: 'below-icon',
          activeTintColor: '#BC3B3B',
          inactiveTintColor: '#7c7d7e',
          style: {
            backgroundColor: 'white',
            height: 90,
          },

          labelStyle: {
            fontSize: RFValue(9, 580),
            paddingBottom: 5,
          },
        }}>
        <Tab.Screen name="Collection" component={Collection} />

        <Tab.Screen name="Studio" component={Studio} />

        <Tab.Screen name="Home" component={Home} />

        <Tab.Screen name="Profile" component={Profile} />

        <Tab.Screen name="More" component={More} />
      </Tab.Navigator>
    );
  }
}

//Main App Component
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isloading: true,
      islogin: false,
      step: 'done',
    };
  }

  componentDidMount() {
    AsyncStorage.getItem('@auth_login', (err, result) => {
      // console.warn(result)
      if (JSON.parse(result) != null) {
        this.setState({islogin: true, step: JSON.parse(result).use_type});
        global.token = JSON.parse(result).token;
        global.user = JSON.parse(result).user_id;
        global.step = this.state.step;
      }
    });
    setTimeout(() => {
      this.setState({isloading: false});
    }, 1000);
  }

  login = step => {
    this.setState({islogin: true, step: step});
  };

  logout = () => {
    this.setState({islogin: false});
  };

  render() {
    if (this.state.isloading) {
      return <Splash />;
    } else {
      return (
        <AuthContext.Provider value={{login: this.login, logout: this.logout}}>
          <NavigationContainer linking={linking}>
            <Stack.Navigator>
              {!this.state.islogin ? (
                <>
                  <Stack.Screen
                    name="MobileLogin"
                    component={MobileLogin}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="OtpVerify"
                    component={OtpVerify}
                    options={{headerShown: false}}
                  />
                </>
              ) : this.state.islogin && this.state.step == 'steps' ? (
                <>
                  <Stack.Screen
                    name="CompleteProfile"
                    component={CompleteProfile}
                    options={{headerShown: false}}
                  />
                </>
              ) : (
                // User is signed in
                <>
                  {/* <Stack.Screen name="VendorRatings" component={VendorRatings} options={{ headerShown: false }} /> */}
                  <Stack.Screen
                    name="Home"
                    component={TabNav}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="ProductList"
                    component={ProductListCollection}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="Products"
                    component={Products}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="CreatePost"
                    component={CreatePost}
                    options={{headerShown: false}}
                  />
                  <Stack.Screen
                    name="EditComment"
                    component={EditComment}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="AboutUs"
                    component={AboutUs}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="Answers"
                    component={Answers}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="Checkout"
                    component={Checkout}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="PaymentOptions"
                    component={PaymentOptions}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="MyCart"
                    component={MyCart}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="MyOrders"
                    component={MyOrders}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="MyProfile"
                    component={MyProfile}
                    options={{headerShown: false}}
                  />
                  <Stack.Screen
                    name="UserProfile"
                    component={UserProfile}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="Notifications"
                    component={Notifications}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="OrderDetails"
                    component={OrderDetails}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="OrderStatus"
                    component={OrderStatus}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="PaymentDetails"
                    component={PaymentDetails}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="Saved"
                    component={Saved}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="Support"
                    component={Support}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="Wishlist"
                    component={Wishlist}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="ChangeAddress"
                    component={ChangeAddress}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="Video"
                    component={Videos}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="Report"
                    component={Report}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="ReportSuccessful"
                    component={ReportSuccessful}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="Comments"
                    component={Comments}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="ProductOffer"
                    component={ProductOffer}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="SavedPost"
                    component={SavedPost}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="Search"
                    component={Search}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="NewAddress"
                    component={NewAddress}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="ProfilePost"
                    component={ProfilePost}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="Videos"
                    component={Videos}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="ThankYou"
                    component={ThankYou}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="EditAddress"
                    component={EditAddress}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="SingleUserpost"
                    component={SingleUserpost}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="PaymentFailed"
                    component={PaymentFailed}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="Followers"
                    component={Followers}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="Following"
                    component={Following}
                    options={{headerShown: false}}
                  />

                  <Stack.Screen
                    name="SingleFeed"
                    component={SingleFeed}
                    options={{headerShown: false}}
                  />
                </>
              )}
            </Stack.Navigator>
          </NavigationContainer>
        </AuthContext.Provider>
      );
    }
  }
}

export default App;
