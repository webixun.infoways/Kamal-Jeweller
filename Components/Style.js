import {StyleSheet,Dimensions } from 'react-native';
import { RFValue } from "react-native-responsive-fontsize";

module.exports =  StyleSheet.create({
    container: {
      flex:1,
      backgroundColor:'#ffffff'
    },

    h1: {
        // fontSize:32,
        fontSize:RFValue(25, 580),
        // fontFamily:"Playfair-Regular",
        fontFamily: "Tangerine-Bold",
      },

      h2: {
        // fontSize:24,
        fontSize:RFValue(18, 580),
        // fontFamily:"Raleway-SemiBold",
        fontFamily: "Montserrat-Regular",
        color:'#4a4b4d'
      },

      h3: {
        // fontSize:18.72,
        fontSize:RFValue(14, 580),
        // fontFamily:"Raleway-SemiBold",
        fontFamily: "Raleway-Medium",
        color:'#222222'
      },

      h4: {
        // fontSize:16,
        fontSize:RFValue(10.5, 580),
        // fontFamily:"Raleway-SemiBold",
        fontFamily: "Raleway-Regular",
        color:'#222222'
      },

      h5: {
        // fontSize:13.28,
        fontSize:RFValue(10, 580),
        // fontFamily:"Raleway-SemiBold",
        fontFamily: "Raleway-Regular",
        color:'#222222'
      },

      h6: {
        // fontSize:12,
        fontSize:RFValue(9, 580),
        // fontFamily:"Raleway-SemiBold",
        fontFamily: "Montserrat-MediumItalic",
        color:'#222222'
      },
      smallHeading:
      {
        // fontSize:15,
        fontSize:RFValue(11, 580),
        // fontFamily:"Raleway-SemiBold",
        fontFamily: "Montserrat-Regular",
      },
      small:
      {
        // fontSize:14,
        fontSize:RFValue(13, 580),
        // fontFamily:"Raleway-Regular",
        fontFamily: "Montserrat-Bold",
        color:'#5d5d5d',
        marginLeft:15
      },
      p:
      {
        // fontSize:15,
        fontSize:RFValue(11, 580),
        fontFamily: "Montserrat-Regular",
        marginTop:5,
        color:'#222222',
      },
      signIn: {
        width: '100%',
        height: 50,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10,
        flexDirection:"row",
        justifyContent:"space-evenly"
    },
    textSignIn:{
      fontFamily: "Tangerine-Bold",
      // fontSize:18,
      fontSize:RFValue(14, 580),
    },
    heading:{
        // color:"#1F449B",
       color:"#222222",
        // fontSize:24,
        fontSize:RFValue(13, 580),
        // fontFamily:"Raleway-SemiBold",
        fontFamily: "Raleway-Medium",
        marginTop:10,
        marginLeft:15
    },
    buttonStyles:{
      backgroundColor: "#BC3B3B",
      width: Dimensions.get("window").width / 1.7,
      alignSelf: "center",
      justifyContent: "center",
      alignItems: "center",
      height: 45,
      marginBottom:10,
      borderRadius: 25,
    },
    buttonText: {
        color: "#fff",
        top:-2,
        fontSize: RFValue(12, 580),
        fontFamily: "Raleway-Medium",
    },
    textInput: {
      marginTop: 20,
      // borderWidth: 0.2,
      backgroundColor: '#fff',
      borderRadius:5,
      width: "80%",
      height: 50,
      alignContent: 'center',
      alignSelf: 'center',
      shadowColor: 'grey',
      shadowOpacity: 1.5,
      // elevation: 2,
      // shadowRadius: 10,
      // shadowOffset: { width:1, height: 1 },
      // fontSize:20,
      fontSize:RFValue(14.5, 580),
      fontFamily: "Tangerine-Bold",
    },
    card:{
      backgroundColor:"#fff",
      paddingHorizontal:10,
      borderRadius:50
    },
    headingSmall:{
      // fontSize:12,
      fontSize:RFValue(10, 580),
      // fontFamily:"Raleway-Medium",
      fontFamily: "Raleway-Regular",
      color:"#D19292",
      marginLeft:15
    },
    image:{
      height:60,
      width:60,
    },
    catHeading:{
      fontSize:RFValue(9, 580),
      fontFamily:"Raleway-Regular",
      // fontFamily: "Montserrat-Regular",
      color:"#222222",
      alignSelf:'center'
    },
    sliderImage:{
      height:180,
      width:200,
      borderRadius:5,
      marginLeft:10,
    },
    card1:{
      backgroundColor:"#fff",
      marginBottom:10,
      elevation:1,
      borderColor:"#fafafa",
      borderWidth:0.5,
      paddingBottom:10,
      marginLeft:5,
      marginRight:5      
    },
    recommendedImage:{
      height:130,
      width:160,
      borderRadius:5,
      alignSelf:"center",
      // top:5
    },
    search:{
        marginTop:15,
        marginBottom:5,
        elevation:0,
        // borderWidth:1,
        height:45,
        fontSize:RFValue(10,580),
        borderColor:"#d3d3d3",
        borderRadius:25,
        alignSelf:"center",
        backgroundColor:"#f2f2f2",
        width:Dimensions.get('window').width/1.1
    },
    headerHeadingText:{
        fontFamily: "Montserrat-Medium",
        color:"#4A4B4D",
        alignSelf:"center",
        fontSize:RFValue(14.5, 580),
        margin:6,
    },
    sideBorder:{
      height:900,
      backgroundColor:"#BC3B3B",
      flex:1,
      width:"20%",
      marginTop:10,
      borderTopRightRadius:50,
      borderBottomRightRadius:50,
      padding:10,
      position:"absolute",
    },
    categoryComponent:{
      backgroundColor:"#fff",
      shadowRadius: 50,
      shadowOffset: { width: 50, height: 50 },
      elevation:3,
      alignSelf:"center",
      // position:"absolute",
      left:0,
      width:Dimensions.get('window').width/1.3,
      height:80,
      top:10,
      paddingHorizontal:20,
      flexDirection:"row",
      // borderColor:"#bc3b3b",
      borderBottomLeftRadius:25,
      borderTopLeftRadius:25,
      borderTopRightRadius:10,
      borderBottomRightRadius:10,
      marginBottom:10
    },
    verticalLine:{
        backgroundColor:"#EBEBEB",
        width:"85%",
        marginHorizontal:20,
        height:1,
        marginTop:10
    },
    changeButton:{
      flexDirection:"row"
    },
    stickyButton1:{
      backgroundColor:"#EBEBEB",
      width:Dimensions.get('window').width/2,
      justifyContent:"center",
      // borderTopLeftRadius:10,
      // borderTopRightRadius:10
    },
    stickyButton2:{
      backgroundColor:"#BC3B3B",
      width:Dimensions.get('window').width/2,
      justifyContent:"center",
      // borderTopLeftRadius:10,
      // borderTopRightRadius:10
    },
    stickyButton1Text:{
      color:"#bc3b3b",
      alignSelf:"center",
      fontSize:RFValue(13,580),
      fontFamily: "Montserrat-Medium",
    },
    stickyButton2Text:{
      color:"#fff",
      alignSelf:"center",
      fontSize:RFValue(13,580),
      fontFamily: "Montserrat-Medium",
    },
    bottomButton1:{
      backgroundColor:"#fff",
      width:Dimensions.get('window').width/2,
      justifyContent:"center",
    },
    bottomButton2:{
      backgroundColor:"#fff",
      width:Dimensions.get('window').width/2,
      justifyContent:"center",
      borderLeftWidth:1,
      flexDirection:"row",
      borderColor:"#d3d3d3"
    },
    bottomButton1Text:{
      color:"#222222",
      alignSelf:"center",
      fontSize:RFValue(11,580),
      fontFamily: "Raleway-Regular",
    },
    bottomButton2Text:{
      color:"#222222",
      left:5,
      alignSelf:"center",
      fontSize:RFValue(11,580),
      fontFamily: "Raleway-Medium",
    },
    locationButton:{
      flexDirection:"row",
      marginTop:15,
        marginBottom:5,
        elevation:0,
        borderWidth:2,
        height:45,
        fontSize:RFValue(10,580),
        borderColor:"#C2C3C3",
        borderRadius:5,
        // alignSelf:"center",
        width:Dimensions.get('window').width/1.4,
        marginLeft:20,
    },
    editProfileButton:{
        borderWidth:1,
        padding:3,
        height:32,
        // left:-5,
        marginTop:-12,
        justifyContent:"center",
        borderColor:"#DADBDB",
        borderRadius:5,
        width:Dimensions.get('window').width-50,
        justifyContent:"center"
    },
    editProfileButtonText:{
      alignSelf:"center",
      fontFamily: "Raleway-Medium",
      fontSize:RFValue(11,580)
    },
    cardImg:{
      borderRadius:100,
      borderWidth:2,
      borderColor:"#bc3b3b",
      padding:10,
      marginLeft:5,
      marginRight:5
    }
   
  });