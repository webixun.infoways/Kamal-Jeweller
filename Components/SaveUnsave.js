import React, { Component } from 'react';
import {
  View,Animated,StyleSheet,Text
} from 'react-native';
import {Header,Icon} from "react-native-elements";
import Toast from "react-native-simple-toast";
class SaveUnsave extends React.Component {
constructor(props)
{
    super(props);
    this.state={
        status:false
    }
}

componentDidMount()
{
    if(this.props.issave != null)
    {
        this.setState({status:true});
    }
}


 //function to save a post
 save = (id) =>
 {
   if(this.state.status == true )
   {
    this.setState({status:false });
     var type="unsave"
   }
   else
     {
        this.setState({status:true });
       var type="save"
     }
   fetch(global.api + 'user-feed-save',{
     method:"POST",
     headers: {
       Accept: 'application/json',
       'Content-Type': 'application/json',
       'Authorization':global.token 
     },
     body:JSON.stringify({
       feed_id:id,
       type:type
     })
   })
   .then((response)=>response.json())
   .then((json)=>{
     // console.log(json)

     if(!json.status){
       Toast.show(json.msg)
     }
     else
     {
       if(json.msg == "Saved"){
         Toast.show("Feed saved!")
       }
       else{
         Toast.show("Feed unsaved!")
       }

       }

   }).catch((error) => console.error(error));
 }


  render() {

    return (
      <View style={{marginTop:5}}>
       <Icon type="ionicon" name={this.state.status ? "bookmark" : "bookmark-outline"}
                  color={this.state.status ? "#bc3b3b" : "black"}
                  onPress={() => this.save(this.props.feed_id)} size={22}/> 


      </View>
    );
  }
}


export default SaveUnsave;