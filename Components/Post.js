import React, { Component } from 'react';
import {
  View,
  Pressable,
  StyleSheet,
  ActivityIndicator,
  Image,
  Text,
  Dimensions,
  ScrollView,
  TouchableOpacity,
  ImageBackground,
  FlatList,
  Platform,
} from 'react-native';
import { Header, Icon } from 'react-native-elements';
import moment from 'moment';
import Share from 'react-native-share';
import RBSheet from 'react-native-raw-bottom-sheet';
import Toast from 'react-native-simple-toast';
import { RFValue } from 'react-native-responsive-fontsize';
import LikeDislike from './LikeDislike.js';
import SaveUnsave from './SaveUnsave.js'

//Global Style Import
const styles = require('../Components/Style.js');

//this is the component for Post
class Post extends Component {
  constructor(props) {
    console.warn("props",props)
    super(props);
    this.state = {
      status:false,
    }
    };
  
  //function for share
  myShare = async (title, content, url) => {
    const shareOptions = {
      title: title,
      message: content,
      url: url,
    }
    try {
      const ShareResponse = await Share.open(shareOptions);

    } catch (error) {
      console.log("Error=>", error)
    }
  }
            
  // Bottom sheet
  sheet(id, feed_index) {
    this.setState({ id: id, feed_index: feed_index });
    this.RBSheet.open();
  }

  // report feed
  report = () => {
    this.RBSheet.close();
    fetch(global.api + 'feed-report-user', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: global.token,
      },
      body: JSON.stringify({
        feed_id: this.state.id,
        report: 'Report this post',
      }),
    })
      .then(response => response.json())
      .then(json => {
        console.warn(json);
        if (!json.status) {
          Toast.show(json.msg);
        } else {
          Toast.show(json.msg);
          this.fetch_feeds();
        }
      });
  };

  likeDislike= (id) =>
{
    if(this.state.status)
    {
        this.setState({status:false,like_count:this.state.like_count-1})
        var type="no"
    }
    else
    {
        this.setState({status:true,like_count:this.state.like_count+1})
        var type="yes"
    }

    fetch(global.api + 'feed-like',{
        method:"POST",
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization':global.token 
        },
        body:JSON.stringify({
          feed_id:id,
          type:type
        })
      })
      .then((response)=>response.json())
      .then((json)=>{
        console.warn(json)

        if(!json.status){

            if(this.state.status)
    {
        this.setState({status:false,like_count:this.state.like_count-1})

    }
    else
    {
        this.setState({status:true,like_count:this.state.like_count+1})

    }

          Toast.show(json.msg)
          // console.warn(json)
        }
        else
        {


          }

      }).catch((error) => console.error(error));

  }

  // navigate to user profile
  userProfile = id => {
    if (id == global.user) {
      this.props.navigation.navigate('Profile');
    } else {
      this.props.navigation.navigate('UserProfile', { id: id });
    }
  };


  render() {
    const {item} = this.props;
    return (
      <View>
        {this.state.isLoading ? (
          <View style={{ marginTop: 200 }}>
            <ActivityIndicator size="large" color="#bc3b3b" />
          </View>
        ) : (
          (this.state.data != "") ?
          <View style={style.card}>
          {/* Card Header */}
          <View style={style.cardHeader}>
            <TouchableOpacity onPress={() => this.userProfile(item.user_id)}>
              <View style={{ flexDirection: 'row' }}>
                {/* logo */}
                {item.user_profile_pic ? (
                  <Image
                    source={{ uri: global.image_url + item.user_profile_pic }}
                    style={style.profileImage}
                  />
                ) : (
                  <Image
                    source={require('../image/dummyuser.jpg')}
                    style={style.profileImage}
                  />
                )}
                {/* name and time */}
                <View style={{ flexDirection: 'column', paddingLeft: 10}}>
                  <Text style={style.name}>{item.user_name}</Text>
                  <Text style={style.postTime}>
                    {moment
                      .utc(item.created_at)
                      .local()
                      .startOf('seconds')
                      .fromNow()}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            {/* follow button */}
            {/* <View>
              {item.user_id != global.user ? (
                <TouchableOpacity
                  style={[style.followButton]}
                  onPress={() => this.follow(item.user_id)}>
                  <Text style={style.followButtonText}>
                    {this.state.follow[item.user_id] ? 'Unfollow' : 'Follow'}
                  </Text>
                </TouchableOpacity>
              ) : null}
            </View> */}
            <View style={{ flexDirection: 'row' }}>
              <Follow navigation={this.props.navigation} />
    
              <Text
                style={{ alignContent: 'flex-end', top: 5, left: 5 }}
                onPress={() => this.sheet(item.id)}>
                <Icon
                  name="ellipsis-vertical"
                  type="ionicon"
                  color="#7c7d7e"
                  size={23}
                />
              </Text>
            </View>
          </View>
    
          {/* Bottom Sheet for Post options */}
    
          <RBSheet
            ref={ref => {
              this.RBSheet = ref;
            }}
            // animationType="fade"
            closeOnDragDown={true}
            closeOnPressMask={true}
            height={160}
            customStyles={{
              container: {
                borderTopRightRadius: 20,
                borderTopLeftRadius: 20,
              },
              draggableIcon: {
                backgroundColor: '',
              },
            }}>
            {/* bottom sheet elements */}
            <View>
              {/* new container search view */}
              <View>
                {/* to share */}
                <View style={{ flexDirection: 'row', padding: 10 }}>
                  <TouchableOpacity
                    style={{ flexDirection: 'row' }}
                    onPress={() => this.myShare(item.feed_description, 'Checkout this latest feed', global.shareLink + '/feedView/' + item.id)}
                  >
                    <View
                      style={{
                        backgroundColor: '#f5f5f5',
                        height: 40,
                        width: 40,
                        justifyContent: 'center',
                        borderRadius: 50,
                      }}>
                      <Icon type="ionicon" name="share-social" />
                    </View>
                    <Text
                      style={[styles.h4, { alignSelf: 'center', marginLeft: 20 }]}>
                      Share
                    </Text>
                  </TouchableOpacity>
                </View>
    
                {item.user_id == global.user ? (
                  <View style={{ flexDirection: 'row', padding: 10 }}>
                    <TouchableOpacity
                      style={{ flexDirection: 'row' }}
                    // onPress={()=>this.myShare(news.heading,"https://greenrabbit.in/hnn/public/news-content/"+news.id)}
                    >
                      <View
                        style={{
                          backgroundColor: '#f5f5f5',
                          height: 40,
                          width: 40,
                          justifyContent: 'center',
                          borderRadius: 50,
                        }}>
                        <Icon type="ionicon" name="trash-outline" />
                      </View>
                      <Text
                        style={[styles.h4, { alignSelf: 'center', marginLeft: 20 }]}>
                        Delete
                      </Text>
                    </TouchableOpacity>
                  </View>
                ) : (
                  //  {/* to report */}
                  <View style={{ flexDirection: 'row', padding: 10 }}>
                    <TouchableOpacity
                      style={{ flexDirection: 'row' }}
                      onPress={() => this.report()}>
                      <View
                        style={{
                          backgroundColor: '#f5f5f5',
                          height: 40,
                          width: 40,
                          justifyContent: 'center',
                          borderRadius: 50,
                        }}>
                        <Icon type="ionicon" name="trash-bin" />
                      </View>
                      <Text
                        style={[styles.h4, { alignSelf: 'center', marginLeft: 20 }]}>
                        Report
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}
              </View>
            </View>
          </RBSheet>
    
          {/* Image */}
          <View>
            {item.feed_content.map(value => {
              return (
                <ImageBackground
                  source={{
                    uri:
                      'https://demo.webixun.com/KamalJwellersApi/public/' +
                      value.content_src,
                  }}
                  style={style.postImage}></ImageBackground>
              );
            })}
    
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={{ flexDirection: 'row' }}>
                {/* <Text style={{ margin: 3, marginLeft: 10 }}>
                  <Icon
                    name={this.state.object[item.id] ? 'thumb-up' : 'thumb-up'}
                    color={this.state.object[item.id] ? '#BC3B3B' : '#7c7d7e'}
                    onPress={() => this.like(item.id)}
                    size={25}
                  />
                </Text> */}
                <Text style={{ margin: 3, marginLeft: 10,marginRight:item.feed_like_count == 0 ? 5 : 5 }}>
                  <LikeDislike feed_id={item.id} 
                  like_count={item.feed_like_count} islike={item.feed_like}/> 
                </Text>
               
    
                <Text style={{ margin: 5}}>
                  <Icon
                    type="ionicon"
                    onPress={() =>
                      this.props.navigation.navigate('Comments', {
                        description: item.feed_description,
                        name: item.user_name,
                        time: item.created_at,
                        id: item.id,
                        pic: item.user_profile_pic,
                      })
                    }
                    name="chatbubble-outline"
                    color="#222222"
                    size={22}
                  />
                </Text>
    
                <Text style={{ margin: 5, }}>
                  <Icon
                    type="ionicon"
                    name="share-social-outline"
                    color="#222222"
                    size={22}
                    onPress={() => this.myShare(item.feed_description, 'Checkout Our Latest Update', global.shareLink + '/feedView/' + item.id)}
                  />
                </Text>
              </View>
    
              <View>
                <Text style={{ margin: 5, justifyContent: 'flex-end' }}>
                  <SaveUnsave feed_id={item.id}  issave={item.feed_save}/>
                </Text>
                {/* <Text style={{ margin: 5, justifyContent: 'flex-end' }}>
                  <Icon
                    type="ionicon"
                    name={
                      this.state.save[item.id] ? 'bookmark' : 'bookmark-outline'
                    }
                    color={this.state.save[item.id] ? '#BC3B3B' : '#222222'}
                    onPress={() => this.bookmark(item.id)}
                    size={22}
                  />
                </Text> */}
              </View>
            </View>
    
            {/* Like counts */}
            {/* <View>
              {item.like_count > 0 ? (
                <>
                  <Text style={{ margin: 1, marginLeft: 13 }}>
                    {item.like_count} likes
                  </Text>
                </>
              ) : (
                <></>
              )}
            </View> */}
            <Text
              style={{
                color: '#222222',
                padding: 10,
                marginTop: -8,
                fontFamily: 'Raleway-Regular',
              }}>
              {item.feed_description}
            </Text>
          </View>
    
          {/* <View>
                    <Icon type="ionicon" name="heart" />
                  </View> */}
        </View>
            :
            <View >
              <Image style={{ width: 350, height: 300, marginTop: 80 }}
                source={require('../image/noFeed.png')} />
              <Text style={[styles.h3, { alignSelf: "center", marginTop: 10 }]}>
                No Feeds Found!
              </Text>
            </View>
        )}
        {this.state.load_more ? (
          <View
            style={{
              alignItems: 'center',
              flex: 1,
              backgroundColor: 'white',
              flex: 1,
              paddingTop: 20,
            }}>
            <ActivityIndicator animating={true} size="large" color="#bc3b3b" />
            <Text style={styles.p}>Please wait...</Text>
          </View>
        ) : (
          <View></View>
        )}
      </View>
    );
  }
}


export default Post;

class Follow extends Component {
  constructor(props) {
    super(props);

    this.state = { follow: true };
  }
  follow = () => {
    if (this.state.follow == true) {
      this.setState({ follow: false });
    } else {
      this.setState({ follow: true });
    }
  };
  render() {
    return <View></View>;
  }
}

//internal stylesheet
const style = StyleSheet.create({
  card: {
    backgroundColor: '#fff',
    marginBottom: 3,
    borderRadius: 20,
  },
  cardHeader: {
    flexDirection: 'row',
    padding: 5,
    justifyContent: 'space-between',
    // backgroundColor:"red"
  },
  profileImage: {
    height: 45,
    width: 45,
    borderRadius: 30,
  },
  name: {
    // fontFamily:"Raleway-SemiBold",
    fontFamily: 'Raleway-Medium',
    fontSize: RFValue(12, 580),
    color: '#bc3b3b',
  },
  postTime: {
    // top:-2,
    // fontFamily: "DancingScript-Bold",
    color: 'grey',
    fontSize: RFValue(10, 580),
  },
  postImage: {
    width: Dimensions.get('window').width,
    height:400,
    // height:"100%"
  },
  followButton: {
    // borderRadius:25,
    // borderColor:"#000",
    // borderWidth:1,
    alignItems: 'center',
    alignContent: 'center',
    justifyContent: 'center',
    // padding:5,
    height: 28,
    width: 90,
    top: 7,
    left: 75,
  },
  followButtonText: {
    // fontFamily:"Roboto-SemiBold",
    fontFamily: 'Raleway-Regular',
    fontSize: RFValue(11, 580),
    // alignSelf:"center",
    textAlign: 'center',
    top: -3,
    left: 3,
    color: '#bc3b3b',
  },

  unFollowButtonText: {
    color: '#222222',
    // fontFamily:"Roboto-SemiBold",
    fontFamily: 'Raleway-Regular',
    fontSize: 11,
    // alignSelf:"center",
    textAlign: 'center',
    top: 0,
    // left:3
  },
  shop: {
    backgroundColor: '#bc3b3b',
    padding: 9,
    paddingTop: 6,
    paddingVertical: 5,
    position: 'absolute',
    bottom: 15,
    left: 25,
    borderRadius: 25,
    // borderWidth:0.5,
    // borderColor:"#d3d3d3",
    shadowColor: '#fff',
    shadowOffset: { width: 50, height: 50 },
    shadowOpacity: 2,
    shadowRadius: 2,
    elevation: 10,
  },
  add: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 50,
    position: 'absolute',
    bottom: 10,
    right: 10,
    height: 50,
    backgroundColor: '#bc3b3b',
    borderRadius: 100,
  },
});
