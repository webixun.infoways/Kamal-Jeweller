import React, { Component } from 'react';
import {
    Text,View,ScrollView,
    StyleSheet,Image,Dimensions,
    TouchableOpacity,Pressable
} from 'react-native';
// import Video from 'react-native-video';

export default class Videos extends Component{
    constructor(props){
        super(props);
        this.state={
            repeat:false,
            rate:1,
            volume:1,
            muted:false,
            resizeMode:'contain',
            duration:0.0,
            currentTime:0.0,
            paused:true,
            rateText:'1.0',
            pausedText:'Play',
            hideControls:false
        }
    }

    onLoad =(data) =>{
        this.setState({duration:data.duration
        });
    }

    onPre =(data) =>{
        this.setState({currentTime:data.currentTime
        });
    }
    
    onEnd=()=>{
        this.setState({
            pausedText: 'Play', paused:true
        });
        this.player.seek(0);
    }
    render(){
    return(
    <View style={{alignItems: 'center',flex:1,justifyContent: 'center'}}>
    <Text>
        My video project!
    </Text>
    {/* <Video
    source={require('../image/jewelleryVideo.mp4')}
    style={{ width: 300, height: 300 }}
    controls={true}
    ref={(ref) => {
    this.player = ref
    }}
    repeat={this.state.repeat}
    rate={this.state.rate}
    volume={this.state.volume}
    muted={this.state.muted}
    resizeMode="contain"
    paused={this.state.paused}
    onLoad={this.onLoad}
    onProgress={this.onProgress}
    /> */}
    </View>
    )
    }
    }