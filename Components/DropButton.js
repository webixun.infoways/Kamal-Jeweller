import React, { Component } from 'react';
import {
    View,
    StyleSheet,TextInput,Pressable,
    Image,Text,Dimensions, ScrollView, TouchableOpacity
} from 'react-native';
import { RFValue } from 'react-native-responsive-fontsize'
import {Header,Icon, Rating} from "react-native-elements";
import LinearGradient from 'react-native-linear-gradient';
import RBSheet from "react-native-raw-bottom-sheet";
import Toast from "react-native-simple-toast";

const styles=require("../Components/Style")

class DropButton extends Component {

    constructor(props) {
        super(props);
        // Don't call this.setState() here!
        this.state = {
          data:[],
          object:{},
          save:{},  
          isLoading: true,
          follow:true,
          unfollow:true
        };
      }
    
    unfollow=()=>{
        if(this.state.unfollow==false){
            this.setState({unfollow:true})
        }
        else{
            this.setState({unfollow:false})
        }
      }

      
    myShare = async (title,content,url)=>{
        const shareOptions={
            title:title,
            message:content,
            url:url,
        }
        try{
            const ShareResponse = await Share.open(shareOptions);
    
        }catch(error){
            console.log("Error=>",error)
        }
    }

    render(){
        return(
            <View>
                 <View style={{flex:1,backgroundColor:"white"}}>
                    {/* edit profile button */}
                    <View style={{paddingTop:20,flexDirection:"row",justifyContent:"space-evenly"}}>
                        <TouchableOpacity style={styles.editProfileButton}
                        onPress={()=>this.props.navigation.navigate('MyProfile')}>
                            <Text style={styles.editProfileButtonText}>
                                Edit Profile
                            </Text>
                        </TouchableOpacity>

                        {/* <TouchableOpacity style={style.button}
                        onPress={()=>this.RBSheet.open()}>
                            <Icon name="chevron-down-outline" size={17} type="ionicon"/>
                        </TouchableOpacity> */}
                    </View>

                    </View>
                    
               {/* Bottom Sheet for Post options */}

               <RBSheet
                        ref={ref => {
                            this.RBSheet = ref;
                        }}
                        closeOnDragDown={true}
                        closeOnPressMask={true}
                        height={220}
                        customStyles={{
                            container: {
                                borderTopRightRadius: 20,
                                borderTopLeftRadius: 20,
                              },
                        draggableIcon: {
                            backgroundColor: ""
                        }
                        }}
                        >
                            {/* bottom sheet elements */}
                        <View >
                            {/* new container search view */}
                                <View>
                                    {/* to share */}
                                    <View style={{flexDirection:"row",padding:10}}>
                                    <TouchableOpacity style={{flexDirection:"row"}} 
                                   >
                                        <View style={{backgroundColor:"#f5f5f5",
                                        height:40,width:40,justifyContent:"center",borderRadius:50}}>
                                        <Icon type="ionicon" name="share-social"/>
                                        </View>
                                        <Text style={[styles.h4,{alignSelf:"center",marginLeft:20}]}>
                                        Share</Text>
                                        </TouchableOpacity>
                                    </View>

                                    {/* to unfollow */}
                                    <View style={{flexDirection:"row",padding:10}} >
                                      {this.state.unfollow ?
                                      <TouchableOpacity style={{flexDirection:"row"}} onPress={()=>this.unfollow()}>
                                      <View style={{backgroundColor:"#f5f5f5",
                                      height:40,width:40,justifyContent:"center",borderRadius:50}}>
                                        <Icon type="ionicon" name="person-remove"/>
                                      </View>
                                      <Text style={[styles.h4,{alignSelf:"center",marginLeft:20}]}>
                                        Unfollow</Text>
                                      </TouchableOpacity>
                                      :
                                      <TouchableOpacity style={{flexDirection:"row"}} onPress={()=>this.unfollow()}>
                                        <View style={{backgroundColor:"#f5f5f5",
                                        height:40,width:40,justifyContent:"center",borderRadius:50}}>
                                          <Icon type="ionicon" name="person-add"/>
                                        </View>
                                        <Text style={[styles.h4,{alignSelf:"center",marginLeft:20}]}>
                                          Follow</Text>
                                        </TouchableOpacity>}
                                    </View>

                                    {/* to report */}
                                    <View style={{flexDirection:"row",padding:10}}>
                                     

                                        <TouchableOpacity style={{flexDirection:"row"}} onPress={()=>this.props.navigation.navigate("Report")}>
                                        <View style={{backgroundColor:"#f5f5f5",
                                        height:40,width:40,justifyContent:"center",borderRadius:50}} >
                                        <Icon type="ionicon" name="trash-bin"/>
                                        </View>
                                        <Text style={[styles.h4,{alignSelf:"center",marginLeft:20}]}
                                        >Report</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
        
       
           
                        </View>
                        </RBSheet>
            </View>
        )
    }
}

export default DropButton;


const style=StyleSheet.create({
    buttonStyles: {
        backgroundColor: "#BC3B3B",
        flexDirection:"row",
        width: Dimensions.get("window").width / 1.4,
        alignSelf: "center",
        justifyContent: "center",
        alignItems: "center",
        height: 45,
        borderRadius: 25,
        top: 40,
        marginBottom:55
    },
    buttonText: {
        color: "#fff",
        fontSize: RFValue(11, 580),
    },
    button:{
        borderWidth:1,
        padding:3,
        height:32,
        marginTop:-12,
        justifyContent:"center",
        borderColor:"#DADBDB",
        borderRadius:5,
        width:Dimensions.get('window').width/9,
        justifyContent:"center"
    }
}
)