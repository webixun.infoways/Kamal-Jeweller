import React, { Component } from 'react';
import {
  View,Animated,StyleSheet,Text
} from 'react-native';
import {Header,Icon} from "react-native-elements";
import { RFValue } from 'react-native-responsive-fontsize';
import Toast from "react-native-simple-toast";
class LikeDislike extends React.Component {
constructor(props)
{
    super(props);
    this.state={
        status:false,
        like_count:this.props.like_count
    }
}

componentDidMount()
{
    if(this.props.islike != null)
    {
        this.setState({status:true});
    }
}

likeDislike= (id) =>
{
    if(this.state.status)
    {
        this.setState({status:false,like_count:this.state.like_count-1})
        var type="no"
    }
    else
    {
        this.setState({status:true,like_count:this.state.like_count+1})
        var type="yes"
    }

    fetch(global.api + 'feed-like',{
        method:"POST",
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          'Authorization':global.token 
        },
        body:JSON.stringify({
          feed_id:id,
          type:type
        })
      })
      .then((response)=>response.json())
      .then((json)=>{
        console.log(json)

        if(!json.status){

            if(this.state.status)
    {
        this.setState({status:false,like_count:this.state.like_count-1})

    }
    else
    {
        this.setState({status:true,like_count:this.state.like_count+1})

    }

          Toast.show(json.msg)
          // console.warn(json)
        }
        else
        {


          }

      }).catch((error) => console.error(error));

}
  render() {

    return (
      <View >
       <Text>
       <Icon name={this.state.status ? 'thumb-up' : 'thumb-up'}
                  color={this.state.status ? "#bc3b3b" : "#7c7d7e"}
                  onPress={() => this.likeDislike(this.props.feed_id)} size={25}
                  /> 
       </Text>

      <View style={{marginTop:5,left:-3}}>
          {(this.state.like_count>0)?
            <Text style={{ margin: 1,fontSize:RFValue(8,580)}}>

                {this.state.like_count} Likes

                </Text>
                :
                <></>

        }
      </View>

      </View>
    );
  }
}


export default LikeDislike; 