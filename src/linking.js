const config={
   screens:{
       SingleFeed:'feedView/:feed_id',
       Comments:'feedComment/:id',
       OrderDetails:'ViewOrder/:id',
       MyCart:'cartView/:user_id',
       Products:'productView/:id'
   },
};

const linking={
   prefixes: ['kamaljewellers://', 'http://kamaljewellers.com', 'https://kamaljewellers.com'],
   config,
}

export default linking;